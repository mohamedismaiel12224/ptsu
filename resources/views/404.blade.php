@extends('layouts.layout')
@section('title', 'Error PTSU')
@section('about')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.error_page')}}</h3>
			</div>
		</div>
	</section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->


<!-- =========================== Error page content ==================== -->

	<div class="container error_page">
		<h1>404</h1>
		<h2>Looks like somthing went wrong</h2>
		<p>The page you are looking for was moved, removed, renamed or might never existed.</p>
		<div style="margin-bottom: 135px;">
			<a href="{{url('/')}}" class="transition3s">{{trans('lang.go_home')}}</a>
		</div>
	</div>
@endsection