@extends('layouts.layout')
@section('title', 'About PTSU')
@section('about')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.contact_us')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{trans('lang.contact_us')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->


<!-- ======================== Contact Us Map ===========================- -->

	<div class="information_area container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single_info">
				<div class="icon"><span class="transition3s ficon flaticon-map"></span></div> <!-- /icon -->
				<h6>{{trans('lang.address')}}</h6>
				<p>{{$setting->address}}</p>
			</div> <!-- /single_info -->

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single_info">
				<div class="icon"><span class="transition3s ficon flaticon-message"></span></div> <!-- /icon -->
				<h6>{{trans('lang.email_address')}}</h6>
				<p>{{$setting->email}}</p>
			</div> <!-- /single_info -->

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single_info">
				<div class="icon"><span class="transition3s ficon flaticon-phone"></span></div> <!-- /icon -->
				<h6>{{trans('lang.phone')}}</h6>
				<p>{{$setting->phone}}</p>
			</div> <!-- /single_info -->

		</div>
	</div> <!-- /information_area -->

	<div class="contact_us_map">
		<!-- Change the data-map-lat and data-map-lng value to show your custom place -->
		<div class="google-map" id="map3" data-map-lat="40.712784" data-map-lng="-74.005941" data-icon-path="images/map/map-icon.png" data-map-zoom="12" style="height:100%;"></div>
	</div>
@endsection