@extends('layouts.layout')
@section('title', 'PTSU')
@section('about')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{$page->name}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{$page->name}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->
<!-- ========================== /Innaer Banner ========================= -->
<!-- =================== We are Right Firm Style two ================= -->
	<section class="container">
		<div class="title_holder_center title_holder">
			<h3><span class="firm">{{$page->name}}</span></h3>
		</div> <!-- /title_holder_center -->
		<div class="row" style="display:block">
			{!! $page->detail!!}
		</div>
	</section>
@endsection