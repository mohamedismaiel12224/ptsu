@extends('layouts.layout')
@section('title', 'Certification PTSU')
@section('certificate')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.verify_the_certificate')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{trans('lang.certifications')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->
<!-- ======================== Blog Page content ======================= -->

	<section class="blog_with_sidebar blog_grid container">

		<div class="row fixed_height cert-fixed_height">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 side_bar_style_two">
				<table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>رمز البرنامج</th>
                  <th>اسم المتدرب</th>
                  <th>الأيميل</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cert as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><a href="#" target="_blank"> {{$value->name}}</a></td>
                    <td><a href="#" target="_blank"> {{$value->username}}</a></td>
                    <td><a href="#" target="_blank"> {{$value->email}}</a></td>
                    <td>
                    	<!-- <a href="{{url('cert/'.$value->id)}}" class="smooth_scroll" style="margin-right: 10px;">عرض <i class="fa fa-pencil-square-o"></i></a> -->
                    	<form action="{{url('cert')}}" method="post">
	                        <input type="hidden" name="cert_id" value="{{$value->id}}">
	                        {{csrf_field()}}
	                        <button type="submit" class="smooth_scroll" >عرض</button>
	                    </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
			</div>
		</div> <!-- /row -->
	</section> <!-- /blog_with_sidebar -->
<!-- ======================== /Blog Page content ======================= -->
@endsection