@extends('layouts.layout')
@section('title', 'About PTSU')
@section('about')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.about_us')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{trans('lang.about_us')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->
<!-- ========================== /Innaer Banner ========================= -->
<!-- =================== We are Right Firm Style two ================= -->
	<section class="container">
		<div class="title_holder_center title_holder">
			<p class="upper_title">{{trans('lang.about_us')}}</p>
			<h3>We are <span class="firm">PTSU</span></h3>
			<p>{{trans('lang.professional_training_systems_union')}}</p>
		</div> <!-- /title_holder_center -->
		<div class="our_goal row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div>
					<h4 class="mTop">{{trans('lang.who_are')}}</h4>
					<p>@if(check_language()){!!$setting->we_are_ar!!}@else{!!$setting->we_are!!}@endif</p>
				</div>
				<div class="invo-mfix">
					<h4 class="mTop">{{trans('lang.our_mission')}}</h4>
					<p>@if(check_language()){!!$setting->our_mission_ar!!}@else{!!$setting->our_mission!!}@endif</p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				
				<div>
					<h4 class="mTop">{{trans('lang.our_vision')}}</h4>
					<p>@if(check_language()){!!$setting->our_vision_ar!!}@else{!!$setting->our_vision!!}@endif</p>
				</div>
				<div class="invo-mfix">
					<h4 class="mTop">{{trans('lang.our_service')}}</h4>
				<p>@if(check_language()){!!$setting->our_rate_ar!!}@else{!!$setting->our_rate!!}@endif</p>
				</div>
			</div>
		</div>
	</section>
@endsection