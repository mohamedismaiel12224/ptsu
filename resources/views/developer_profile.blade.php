@extends('layouts.layout')
@section('title', 'PTSU | Developers')
@section('developers')
<!-- ========================== Innaer Banner ========================= -->


	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.system_developers')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">Home</a></li>
					<li>/</li>
					<li>{{trans('lang.developers')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->



<!-- ==========================TEam Details Page =================== -->

	<div class="attorney_details container">
		<div class="row">
			<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 pull-right attorney_history">
				<div class="name">
					<h4>{{$developer->name}}</h4>
				</div> <!-- /name -->

				<div class="bio">
					<h6>{{trans('lang.cv')}}</h6>
					<p>{{$developer->cv}}</p>
				</div> <!-- /bio -->
			</div> <!-- /attorney_history -->

			<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 attorney_sidebar pull-left">
				<div>
					<img src="{{asset('assets/images/team/'.$developer->profilepic)}}" alt="iamges" class="img-responsive">
				</div> <!-- /img_holder -->
				<!-- contact details -->
				<div class="contact_details">
					<div class="single_contact_info">
						<div class="icon"><span class="transition3s ficon flaticon-share"></span></div> <!-- /icon -->
						<div class="contact_info">
							<ul>
								@if($developer->facebook != '#')
								<li class="round_border transition3s"><a href="{{$developer->facebook}}" class="transition3s"><i class="fa fa-facebook"></i></a></li>
								@endif
								@if($developer->twitter != '#')
								<li class="round_border transition3s"><a href="{{$developer->twitter}}" class="transition3s"><i class="fa fa-twitter"></i></a></li>
								@endif
								@if($developer->linkedin != '#')
								<li class="round_border transition3s"><a href="{{$developer->linkedin}}" class="transition3s"><i class="fa fa-linkedin"></i></a></li>
								@endif
								@if($developer->google != '#')
								<li class="round_border transition3s"><a href="{{$developer->google}}" class="transition3s"><i class="fa fa-google-plus"></i></a></li>
								@endif
							</ul>
						</div> <!-- /contact_info -->
					</div>
				</div>
			</div>
		</div> <!-- /row -->
	</div> <!-- /attorney_details -->


<!-- ========================== /TEam Details Page =================== -->

@endsection