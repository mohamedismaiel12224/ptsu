@extends('layouts.layout')
@section('title', 'PTSU | Login')
@section('login')
		<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.register')}}</h3>
			</div>
		</div>
	</section> <!-- /inner_banner -->

<!-- ============================ Contact Form =========================== -->


	<div class="contact_us_form">
		<div class="container">
			@if (count($errors) > 0)
		        <div class="alert alert-danger">
		        	<ul>
		            @foreach ( $errors->all() as $error )
		                <li>{{ $error }}</li>
		            @endforeach
		            </ul>
		        </div>
		    @endif
		    @if(\session('suc'))
		    <div class="alert alert-success">
		        {{\session('suc')}}
		    </div>
		    @endif
			<form action="{{url('register2')}}" class="form-validation" method="post">
				<h4>{{trans('lang.we_welcome_you_in_ptsu')}}</h4>
				<div class="input_wrapper row">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
						<input name="name" type="text" placeholder="{{trans('lang.name')}}" value="{{old('name')}}">
					</div>
					<div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
						<input name="email" type="email" placeholder="{{trans('lang.email')}}" value="{{old('email')}}">
					</div>
					<div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
						<input name="phone" type="text" placeholder="{{trans('lang.phone')}}" value="{{old('phone')}}">
					</div>
					<div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
						<input name="password" type="password" placeholder="{{trans('lang.password')}}">
					</div>
					<div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
						<input name="confirm_password" type="password" placeholder="{{trans('lang.confirm_password')}}">
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<select style="width: 100%;height: 40px" name="system">
							<option selected="selected" disabled="" value="0">{{trans('lang.system_name')}}</option>
							@foreach($system as $val)
							<option value="{{$val->id}}">{{$val->name}}</option>
							@endforeach
		                </select>
					</div>
					<div class="col-lg-7 col-md-4 col-sm-6 col-xs-12" style="display: flex;flex-direction: row-reverse;margin-top: 15px">
						<input type="radio" style="height: 20px;width: 70px" name="gender" value="1">{{trans('lang.male')}}
						<input type="radio" style="height: 20px;width: 70px" name="gender" value="0">{{trans('lang.female')}}
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button class="transition3s" title="Send">{{trans('lang.register')}}</button>
					</div>
				</div> <!-- /input_wrapper -->
			</form>
		</div>
	</div> <!-- /contact_form -->

<!-- ============================ /Contact Form =========================== -->
@endsection