@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($library))
				تعديل للمكتبة الألكترونية
				@else
				اضافة مكتبة الألكترونية
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('section_lib')}}">المكتبة الألكترونية</a></li>
				@if(isset($library))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($library)) action="{{url('section_lib/'.$library->id)}}" @else   action="{{url('section_lib')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الفئة</label>

									<div class="col-sm-10">
										<input type="text" name="section" class="form-control" placeholder="" value="@if(isset($library)){{$library->section}}@else{{old('section')}}@endif">
									</div>
								</div>
							</div>
							@if(isset($library))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection