@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				اضافة مرفق
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('elibrary')}}">المكتبة الألكترونية</a></li>
				<li class="active">تعديل</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" action="{{url('elibrary/'.$section_id)}}" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">اسم المرفق</label>

									<div class="col-sm-10">
										<input type="text" name="material_name" class="form-control" placeholder="" value="@if(isset($library)){{$library->material_name}}@else{{old('material_name')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label center">ارفاق الملف</label>

									<div class="col-sm-10">
                                    	<input class="form-control" type="file" name="file">
									</div>
								</div>
							</div>
							<input type="hidden" name="_method" value="patch"> 
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection