@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($course))
				تعديل برنامج تدريبى
				@else
				اضافة برنامج تدريبى
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('courses')}}">البرامج التدريبية</a></li>
				@if(isset($course))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($course)) action="{{url('courses/'.$course->id)}}" @else   action="{{url('courses')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رمز البرنامج</label>
									<div class="col-sm-10">
										<input type="text" name="name" class="form-control" placeholder="" value="@if(isset($course)){{$course->name}}@else{{old('name')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رقم البرنامج</label>

									<div class="col-sm-10">
										<input type="text" name="co_no" class="form-control" placeholder="" value="@if(isset($course)){{$course->co_no}}@else{{old('co_no')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">عنوان البرنامج عريى</label>

									<div class="col-sm-10">
										<input type="text" name="address" class="form-control" placeholder="" value="@if(isset($course)){{$course->address}}@else{{old('address')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">عنوان البرنامج انجليزى</label>

									<div class="col-sm-10">
										<input type="text" name="address_en" class="form-control" placeholder="" value="@if(isset($course)){{$course->address_en}}@else{{old('address_en')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">عدد الساعات</label>

									<div class="col-sm-10">
										<input type="text" name="hours" class="form-control" placeholder="" value="@if(isset($course)){{$course->hours}}@else{{old('hours')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">دولة التنفيذ</label>

									<div class="col-sm-10">
										<input type="text" name="country" class="form-control" placeholder="" value="@if(isset($course)){{$course->country}}@else{{old('country')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">التاريخ</label>

									<div class="col-sm-10">
										<input type="text" class="form-control pull-right" id="datepicker" name="created_at"  value="@if(isset($course)){{date('m/d/Y', strtotime($course->created_at))}}@else{{old('created_at')}}@endif">
									</div>
								</div>
							</div>
							@if(isset($course))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection