@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($single))
        تعديل
        @else
        اضافة
        @endif
        @if($status==5) وصف نتائج نظام ETPM  @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($single))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
              @if(\session('success'))
              <div class="alert alert-success">
                  {{\session('success')}}
              </div>
              @endif
              @if(\session('error'))
              <div class="alert alert-danger">
                  {{\session('error')}}
              </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($single)) action="{{url('description_evaluate/'.$single->id)}}" @else   action="{{url('description_evaluate')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label center">الوصف Description</label>

                  <div class="col-sm-10">
                    <input type="text" required="" name="description" class="form-control" placeholder="" value="@if(isset($single)){{$single->description}}@else{{old('description')}}@endif">
                  </div>
                </div>
              </div>
              @if(isset($single))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="status" value="{{$status}}"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">تعديل</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection