@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				تغيير كلمة المرور
			</h1>
			<ol class="breadcrumb">
				<li class="active">تغيير كلمة المرور</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
						@if(\session('success'))
					    <div class="alert alert-success">
					        {{\session('success')}}
					    </div>
					    @endif
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post"  action="{{url('change_update/')}}" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الرقم السري</label>
									<div class="col-sm-10">
										<input class="form-control" type="password" name="password" >
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">تأكيد الرقم السري</label>
									<div class="col-sm-10">
										<input class="form-control" type="password" name="confirm_password">
									</div>
								</div>
							</div>
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">تعديل</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection