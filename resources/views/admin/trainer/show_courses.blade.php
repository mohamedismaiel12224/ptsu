@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        البرامج التدريبية ل {{$trainer->name}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">المتدربين</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>اسم البرنامج</th>
                  <th>رقم البرنامج</th>
                  <th>عدد الساعات</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($courses as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->co_no}}</td>
                    <td>{{$value->hours}}</td>
                    <td >
                      @if($value->status==0)
                      <a href="{{url('give_cert/'.$trainer->id.'/1/'.$value->course_id)}}" class="btn btn-success" style="margin-right: 10px;">منح شهادة <i class="fa fa-pencil-square-o"></i></a>
                      @else
                      <a href="{{url('trainer/'.$value->id)}}" class="btn btn-primary" style="margin-right: 10px;">عرض الشهادة <i class="fa  fa-television"></i></a>
                      <a href="{{url('give_cert/'.$trainer->id.'/0/'.$value->course_id)}}" class="btn btn-danger" style="margin-right: 10px;">منع الشهادة <i class="fa fa-pencil-square-o"></i></a>
                      @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection