@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                <li><a href="{{url('trainer')}}">المتدربين</a></li>
                <li class="active">شهادة</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content" >
            <div class="invoice">
                <img src="{{asset('assets/images/cert.jpg')}}">
                <div class="ab5ab5">
                    <div style="text-align: center;">
                        <h1 >Certificate</h1>
                        <h4 >HEREBY CERTIFICATE THAT</h4>
                        <h3 >{{$user->name}}</h3>
                    </div>
                    <div class="row" style="text-align: center;">
                        <h4>Has completed a course training in The Profassional Training Systems Union</h4>
                        <h4>which was conducted</h4>
                        <h4>according to the standard and guidelines established by PTSU</h4>
                        <h4>The holder of this certificate has been trained by a registered PTSU</h4>
                        <h4>.and has achieved the standards required to merit this certificate</h4>
                        <h3>The Profassional Training Systems Union</h3>
                        <h4>Has therefore conferred upon the holder this course in</h4>
                        <h3>{{$course->address_en}}</h3>
                        <h5>With <b>{{$course->hours}}</b> training hours, and accordingly the certificate on {{date('d/m/Y',strtotime($course->created_at))}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8 invoice-block no-print">
                        <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                            <i class="fa fa-print"></i>
                        </a>
                    </div>

                    <h6> {{$user->cert_no}} </h6>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection