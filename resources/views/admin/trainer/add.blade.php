@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($trainer))
				تعديل متدرب
				@else
				اضافة متدرب
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('trainer')}}">المتدربين</a></li>
				@if(isset($trainer))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($trainer)) action="{{url('trainer/'.$trainer->id)}}" @else   action="{{url('trainer')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الأسم</label>
									<div class="col-sm-10">
										<input type="text" name="name" class="form-control" placeholder="" value="@if(isset($trainer)){{$trainer->name}}@else{{old('name')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الأيميل</label>
									<div class="col-sm-10">
										<input type="email" name="email" class="form-control" placeholder="" value="@if(isset($trainer)){{$trainer->email}}@else{{old('email')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رقم التواصل</label>
									<div class="col-sm-10">
										<input type="text" name="phone" class="form-control" placeholder="" value="@if(isset($trainer)){{$trainer->phone}}@else{{old('phone')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">النوع</label>
									<div class="col-sm-10">
										<div class="radio">
											<label style="padding-left: 50px;">
												<input type="radio" name="gender" id="optionsRadios1" value="1" @if(isset($trainer) && ($trainer->gender==1))checked @endif>ذكر
											</label>    
											<label style="padding-left: 50px;">
												<input type="radio" name="gender" id="optionsRadios1" value="0" @if(isset($trainer) && ($trainer->gender==0))checked @endif>أنثى
											</label>
										</div>
									</div>
				                </div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رقم الهوية</label>
									<div class="col-sm-10">
										<input type="text" name="nno" class="form-control" placeholder="" value="@if(isset($trainer)){{$trainer->nno}}@else{{old('nno')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center"> البرنامج التدريبى</label>
									<div class="col-sm-10">
										<select class="form-control select2" multiple="multiple" data-placeholder="اختر البرامج التدريبية" style="width: 100%;" name="course[]">
											@foreach($courses as $val)
											<option value="{{$val->id}}"
												@if (isset($courses_selected))
													@foreach($courses_selected as $v)  
														@if($v->course_id==$val->id)selected @endif
													@endforeach 
												@endif >{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الدولة</label>
									<div class="col-sm-10">
										<input type="text" name="country" class="form-control" placeholder="" value="@if(isset($trainer)){{$trainer->country}}@else{{old('country')}}@endif">
									</div>
								</div>
							</div>
							@if(isset($trainer))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection