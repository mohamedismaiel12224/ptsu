@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($procedure))
        تعديل
        @else
        اضافة
        @endif
       @if($type==1) اجراءات مؤشر @elseif($type==2) محكات مؤشر @endif {{$checkup->name}}
       <script >
         var type = {{$type}};

       </script>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($procedure))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
              @if(\session('success'))
              <div class="alert alert-success">
                  {{\session('success')}}
              </div>
              @endif
              @if(\session('error'))
              <div class="alert alert-danger">
                  {{\session('error')}}
              </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($procedure)) action="{{url('procedures/'.$procedure->id)}}" @else   action="{{url('procedures')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
              
                @if(isset($procedure))
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">@if($type==1) اجراءات @elseif($type==2) محكات @endif</label>

                  <div class="col-sm-10">
                    <input type="text" required="" name="procedure" class="form-control" placeholder="" value="@if(isset($procedure)){{$procedure->procedure}}@else{{old('procedure')}}@endif">
                  </div>
                </div>
                @else
                <div class="add_procedure_here">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-1 control-label center">@if($type==1) اجراءات @elseif($type==2) محكات @endif</label>
                    <div class="col-sm-10">
                      <input type="text" name="procedure[]" class="form-control" >
                    </div>
                    <label for="name" class="col-sm-1">
                      <a href="#" id="add_procedure_field">
                        <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                      </a>
                    </label>
                  </div>
                </div>
                @endif
              </div>
              @if(isset($procedure))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="type" value="{{$type}}"> 
                <input type="hidden" name="checkup_id" value="{{$checkup_id}}"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection