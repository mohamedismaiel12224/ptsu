@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="text-center">
        @if($domain->status==6) مؤشرات المعيار  @else  فحوصات المجال   @endif {{$domain->name}}
      </h1>
      <a href="{{url('checkup/create/'.$domain->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة @if($domain->status==6) مؤشر  @else  فحص   @endif  <i class="fa fa-plus"></i></a>

      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">@if($domain->status==6) مؤشرات المعيار  @else  فحوصات المجال   @endif {{$domain->name}}</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>الأسم</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($checkups as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td style="display: flex">
                    <a href="{{url('checkup/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                    <form action="{{url('checkup/'.$value->id)}}" method="post"  style="margin-right: 10px;">
                        <input type="hidden" name="_method" value="delete">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')">حذف <i class='fa fa-trash-o'></i></button>
                    </form>
                    @if($domain->status==6) 
                    <a href="{{url('procedures/'.$value->id.'/1')}}" class="btn btn-primary" style="margin-right: 10px;">اجراءات </a>
                    <a href="{{url('procedures/'.$value->id.'/2')}}" class="btn bg-navy" style="margin-right: 10px;">محكات </a>

                    @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection