@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($domain))
        تعديل
        @else
        اضافة
        @endif
        @if($type_id==1) مجال لنواتج التدريب @elseif($type_id==2) مجال للمحتوى التدريبي @elseif($type_id==3)  مجال للأنشطة التدريبية  @elseif($type_id==4)  التقييم العام   @elseif($type_id==5) تقييم مهارات العرض والالقاء @elseif($type_id==6) معايير @elseif($type_id==7) المتدربين والمادة التدريبية @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($domain))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
              @if(\session('success'))
              <div class="alert alert-success">
                  {{\session('success')}}
              </div>
              @endif
              @if(\session('error'))
              <div class="alert alert-danger">
                  {{\session('error')}}
              </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($domain)) action="{{url('domain/'.$domain->id)}}" @else   action="{{url('domain')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label center">الأسم</label>

                  <div class="col-sm-10">
                    <input type="text" required="" name="name" class="form-control" placeholder="" value="@if(isset($domain)){{$domain->name}}@else{{old('name')}}@endif">
                  </div>
                </div>
                @if(!isset($domain))
                @if($type_id==6)
                <div class="add_indictor_here">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-1 control-label center">المؤشرات</label>
                    <div class="col-sm-10">
                      <input type="text" name="checkup[]" class="form-control" >
                    </div>
                    <label for="name" class="col-sm-1">
                      <a href="#" id="add_indictor_field">
                        <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                      </a>
                    </label>
                  </div>
                </div>
                @else
                <div class="add_checkup_here">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-1 control-label center">الفحوصات</label>
                    <div class="col-sm-10">
                      <input type="text" name="checkup[]" class="form-control" >
                    </div>
                    <label for="name" class="col-sm-1">
                      <a href="#" id="add_checkup_field">
                        <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                      </a>
                    </label>
                  </div>
                </div>
                @endif
                @endif
              </div>
              @if(isset($domain))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="type_id" value="{{$type_id}}"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection