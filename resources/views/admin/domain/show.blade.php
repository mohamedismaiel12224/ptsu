@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if($type_id==1) مجالات نواتج التدريب @elseif($type_id==2) مجالات المحتوى التدريبي @elseif($type_id==3)  مجالات الأنشطة التدريبية  @elseif($type_id==4)  التقييم العام @elseif($type_id==5) تقييم مهارات العرض والالقاء @elseif($type_id==6) المعايير @elseif($type_id==7) المتدربين والمادة التدريبية @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">@if($type_id==1) مجالات نواتج التدريب @elseif($type_id==2) مجالات المحتوى التدريبي @elseif($type_id==3)  مجالات الأنشطة التدريبية  @elseif($type_id==4)  التقييم العام @elseif($type_id==5) تقييم مهارات العرض والالقاء @elseif($type_id==6) المعايير @elseif($type_id==7) المتدربين والمادة التدريبية @endif</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>الأسم</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($domains as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td style="display: flex">
                    <a href="{{url('domain/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                    <form action="{{url('domain/'.$value->id)}}" method="post"  style="margin-right: 10px;">
                        <input type="hidden" name="_method" value="delete">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')">حذف <i class='fa fa-trash-o'></i></button>
                    </form>
                    <a href="{{url('domain/'.$value->id)}}" class="btn btn-primary" style="margin-right: 10px;">@if($type_id==6) المؤشرات @else الفحوصات @endif <i class="fa fa-desktop"></i></a>

                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection