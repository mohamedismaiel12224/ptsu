@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        صلاحية ملفات زمالة الأتحاد
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">صلاحية ملفات زمالة الأتحاد</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>اسم المتدرب</th>
                  <th>االملف</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($fellow as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td><a href="{{url('/assets/image/library/'.$value->url)}}" download>تحميل</a></td>
                    <td style="display: flex">
                    <a href="{{url('fellowships/'.$value->id.'/1')}}" class="btn btn-primary checkFellow" style="margin-right: 10px;">عرض فقط <i class="fa  fa-television"></i></a>
                    <a href="{{url('fellowships/'.$value->id.'/2')}}" class="btn btn-info checkFellow" style="margin-right: 10px;">تحميل <i class="fa fa-download"></i></a>
                    <a href="{{url('fellowships/'.$value->id.'/3')}}" class="btn btn-success checkFellow" style="margin-right: 10px;">عرض وتعديل <i class="fa fa-pencil-square-o"></i></a>
                    <form action="{{url('fellowships/'.$value->id)}}" method="post">
                        <input type="hidden" name="_method" value="delete">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger" onclick="bootbox.confirm("هل انت متأكد؟", function(result){});">حذف <i class='fa fa-trash-o'></i></button>
                    </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection