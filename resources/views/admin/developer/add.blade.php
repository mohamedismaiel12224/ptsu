@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($developer))
				تعديل مطور
				@else
				اضافة مطور
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('developers')}}">المطورين</a></li>
				@if(isset($developer))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($developer)) action="{{url('developers/'.$developer->id)}}" @else   action="{{url('developers')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الأسم</label>

									<div class="col-sm-10">
										<input type="text" name="name" class="form-control" placeholder="" value="@if(isset($developer)){{$developer->name}}@else{{old('name')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label center">صورة المطور</label>

									<div class="col-sm-10">
                                    	<input class="form-control" type="file" name="profilepic">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">السيرة الذاتية</label>

									<div class="col-sm-10">
										<textarea  class="form-control" name="cv" rows="10" cols="80">@if(isset($developer)){{$developer->cv}}@else{{old('cv')}}@endif</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">فيس بوك</label>

									<div class="col-sm-10">
										<input type="text" name="facebook" class="form-control" placeholder="" value="@if(isset($developer)){{$developer->facebook}}@else{{old('facebook')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">تويتر</label>

									<div class="col-sm-10">
										<input type="text" name="twitter" class="form-control" placeholder="" value="@if(isset($developer)){{$developer->twitter}}@else{{old('twitter')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">لينكد ان</label>

									<div class="col-sm-10">
										<input type="text" name="linkedin" class="form-control" placeholder="" value="@if(isset($developer)){{$developer->linkedin}}@else{{old('linkedin')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">جوجل بلس</label>

									<div class="col-sm-10">
										<input type="text" name="google" class="form-control" placeholder="" value="@if(isset($developer)){{$developer->google}}@else{{old('google')}}@endif">
									</div>
								</div>
							</div>
							@if(isset($developer))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection