@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if($type_id==1) مستوى التقويم @elseif($type_id==2) طرق التدريب @elseif($type_id==3) وقت التقويم @elseif($type_id==4) الأدوات @elseif($type_id==5) المسئولية @elseif($type_id==6) الفئات @elseif($type_id==7) معينات الوصول @elseif($type_id==8) آلية التنفيذ @elseif($type_id==9) مصادر المحتوى @elseif($type_id==10) المؤثرات @elseif($type_id==11) الحلول @elseif($type_id==12) تقييم المتدربين  @elseif($type_id==13) تقييم المادة التدريبية  @elseif($type_id==14) مستوى التقويم @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">@if($type_id==1) مستوى التقويم @elseif($type_id==2) طرق التدريب @elseif($type_id==3) وقت التقويم @elseif($type_id==4) الأدوات @elseif($type_id==5) المسئولية @elseif($type_id==6) الفئات @elseif($type_id==7) معينات الوصول @elseif($type_id==8) آلية التنفيذ @elseif($type_id==9) مصادر المحتوى @elseif($type_id==10) المؤثرات @elseif($type_id==11) الحلول @elseif($type_id==12) تقييم المتدربين @elseif($type_id==13) تقييم المادة التدريبية @elseif($type_id==14) مستوى التقويم @endif</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>الأسم</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($attr as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td style="display: flex">
                    <a href="{{url('attribute/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                    @if($value->id>4)
                    <form action="{{url('attribute/'.$value->id)}}" method="post">
                        <input type="hidden" name="_method" value="delete">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')">حذف <i class='fa fa-trash-o'></i></button>
                    </form>
                    @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection