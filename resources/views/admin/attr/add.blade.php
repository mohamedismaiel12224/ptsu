@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($attribute))
				تعديل
				@else
				اضافة
				@endif
				@if($type_id==1) مستوى التقويم @elseif($type_id==2) طرق التدريب @elseif($type_id==3) وقت التقويم @elseif($type_id==4) الأدوات @elseif($type_id==5) المسئولية @elseif($type_id==6) الفئات @elseif($type_id==7) معينات الوصول @elseif($type_id==8) آلية التنفيذ @elseif($type_id==9) مصادر المحتوى @elseif($type_id==10) المؤثرات @elseif($type_id==11) الحلول @elseif($type_id==12) تقييم المتدربين  @elseif($type_id==13) تقييم المادة التدريبية @elseif($type_id==14) مستوى التقويم @endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				@if(isset($attribute))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
					    @if(\session('success'))
					    <div class="alert alert-success">
					        {{\session('success')}}
					    </div>
					    @endif
					    @if(\session('error'))
					    <div class="alert alert-danger">
					        {{\session('error')}}
					    </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($attribute)) action="{{url('attribute/'.$attribute->id)}}" @else   action="{{url('attribute')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الأسم</label>

									<div class="col-sm-10">
										<input type="text" required="" name="name" class="form-control" placeholder="" value="@if(isset($attribute)){{$attribute->name}}@else{{old('name')}}@endif">
									</div>
								</div>
							
							</div>
							@if(isset($attribute))
								<input type="hidden" name="_method" value="patch"> 
							@else
								<input type="hidden" name="type_id" value="{{$type_id}}"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection