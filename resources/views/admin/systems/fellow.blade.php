@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        الأنظمة المهنية
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">الأنظمة المهنية</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>اسم النظام</th>
                  <th>اللوجو</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sys as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td><img src="{{asset('assets/images/home/'.$value->logo)}}" width="200px"></td>
                    <td style="display: flex">
                      @if($value->id == 1)
                        @if($value->status==1)
                        <a href="{{url('mit')}}" class="btn btn-primary" style="margin-right: 10px;">تصميم الحقائب التدريبية <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                        @if($value->status_arbitration==1)
                        <a href="{{url('mit_arbitration')}}" class="btn bg-navy" style="margin-right: 10px;">تحكيم الحقائب التدريبية <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                      @elseif($value->id==2)
                        @if($value->status==1)
                        <a href="{{url('need')}}" class="btn btn-primary" style="margin-right: 10px;">مشاريعي <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                      @elseif($value->id==3)
                        @if($value->status==1 || $value->status_arbitration==1)
                        <a href="{{url('performance_evaluate')}}" class="btn btn-primary" style="margin-right: 10px;">مشاريعي <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                      @elseif($value->id==4)
                        @if($value->status==1)
                        <a href="{{url('etpm')}}" class="btn btn-primary" style="margin-right: 10px;">مشاريعي <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                      @elseif($value->id==6)
                        @if($value->status==1)
                        <a href="{{url('efm/efm')}}" class="btn btn-primary" style="margin-right: 10px;">مشاريعي <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                      @endif
                      <a href="{{url('sys_fellow/'.$value->id)}}" class="btn btn-success" style="margin-right: 10px;">مرفقات النظام <i class="fa fa-television"></i></a>

                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection