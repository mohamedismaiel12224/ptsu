@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 class="center">
				مستوى التعلم  Learning level
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">مستوى التعلم</li>
			</ol>
		</section>
		@if(\session('success'))
		<div class="alert alert-success">
				{{\session('success')}}
		</div>
		@endif
		@if(\session('error'))
		<div class="alert alert-danger">
				{{\session('error')}}
		</div>
@endif
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body table-responsive">
							<table id="example1" class="table table-bordered table-striped" data-page-length='100'>
								<thead>
								<tr>
									<th> # </th>
									<th style="width:30%">نواتج التدريب و الأرتباطات Training Outputs & Collerations</th>
									<th  style="width:20%">مستوى التعلم  Learning level</th>
									<th  style="width:20%">التقدير Degree</th>
									<th  style="width:20%">العمليات المتاحة Available Operatins</th>
								</tr>
								</thead>
								<tbody>
								@foreach($all as $key=>$value)
								<tr>
										<td>
											{{$key+1}}
										</td>
										<td>{{$value->name}}</td>
										<td>@if(is_numeric($value->learning_level)){{round($value->learning_level,2)}} @else {{degree($value->learning_level)}}@endif</td>
										<td>
											{{degree($value->learning_level)}}
										</td>
										<td>
                  							<a href="{{url('learning_level_edit/'.$value->series_no.'/'.$mit->id)}}" class="btn btn-primary" style="margin-right: 10px;">تدوين <i class="fa fa-pencil"></i></a>
										</td>
								</tr>
								@endforeach
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection