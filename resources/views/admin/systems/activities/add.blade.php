@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($activities))
				تعديل نشاط تدريبي
				@else
				إضافة نشاط تدريبي
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				@if(isset($activities))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
					    @if(\session('error'))
						    <div class="alert alert-danger">
						        {{\session('error')}}
						    </div>
						@endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($activities)) action="{{url('training_activities/'.$activities->id)}}" @else   action="{{url('training_activities')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center"> نواتج التدريب</label>
									<div class="col-sm-10">
										<select class="form-control select2"  data-placeholder="اختر ناتج التدريب" style="width: 100%;" name="series_no">
											<option value="0" disabled="" selected="">اختر ناتج التدريب</option>
											@foreach($training as $val)
											<option value="{{$val->series_no}}"  
												@if(isset($activities) && $activities->series_no==$val->series_no)selected @endif>{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">العنوان</label>
									<div class="col-sm-10">
										<input type="text" name="address" class="form-control" placeholder="" value="@if(isset($activities)){{$activities->address}}@else{{old('address')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الفئة</label>
									<div class="col-sm-10">
										@if(isset($activities))
											@php($arr = explode(',',$activities->category_id))
										@endif
										<select class="form-control select2" multiple="multiple" data-placeholder="اختر الفئة" style="width: 100%;" name="category_id[]">
											@foreach($category as $val)
											<option value="{{$val->id}}"  
												@if(isset($arr))
													@foreach($arr as $v)
														@if($v==$val->id)selected @endif 
													@endforeach 
												@endif>{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
				                </div>
				                <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">معينات الوصول</label>
									<div class="col-sm-10">
										@if(isset($activities))
											@php($arr = explode(',',$activities->aids_id))
										@endif
										<select class="form-control select2" multiple="multiple" data-placeholder="اختر معينات الوصول" style="width: 100%;" name="aids_id[]">
											@foreach($aids as $val)
											<option value="{{$val->id}}"  
												@if(isset($arr))
													@foreach($arr as $v)
														@if($v==$val->id)selected @endif 
													@endforeach 
												@endif>{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
				                </div>
				                <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">ألية التنفيذ</label>
									<div class="col-sm-10">
										@if(isset($activities))
											@php($arr = explode(',',$activities->imp_id))
										@endif
										<select class="form-control select2" multiple="multiple" data-placeholder="اختر ألية التنفيذ" style="width: 100%;" name="imp_id[]">
											@foreach($imp as $val)
											<option value="{{$val->id}}"  
												@if(isset($arr))
													@foreach($arr as $v)
														@if($v==$val->id)selected @endif 
													@endforeach 
												@endif>{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
				                </div>
				                <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">فكرة النشاط</label>
									<div class="col-sm-10">
										<input type="text" name="idea" class="form-control" placeholder="" value="@if(isset($activities)){{$activities->idea}}@else{{old('idea')}}@endif">
									</div>
								</div>
				                <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الزمن المستغرق</label>
									<div class="col-sm-10">
										<div class="radio">
											<label style="padding-left: 150px;font-size: 25px">
												<input type="radio" name="status" id="value_method" value="0" checked="">قيمة
											</label>    
											<label style="padding-left: 150px;font-size: 25px">
												<input type="radio" name="status" id="math_method" value="2">حساب
											</label>
										</div>
									</div>
				                </div>
				                <hr>
				                <div id="value_div">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">قيمة الزمن المستغرق</label>

										<div class="col-sm-10">
											<input type="text" name="value" class="form-control">
										</div>
									</div>
								</div>
								<div id="math_div" class="none">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">(O) الوقت المتفائل</label>

										<div class="col-sm-10">
											<input type="text" name="o" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">(M) الوقت التكراري</label>

										<div class="col-sm-10">
											<input type="text" name="m" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">(P) الوقت المتشائم</label>

										<div class="col-sm-10">
											<input type="text" name="p" class="form-control">
										</div>
									</div>
								</div>
							</div>
							@if(isset($activities))
								<input type="hidden" name="_method" value="patch"> 
							@else
								<input type="hidden" name="pro_id" value="{{$mit->id}}">
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection