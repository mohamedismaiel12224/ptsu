@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 class="center">
				الفواصل وفترة الأنجاز   Linking Activities & period of achievement
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">الفواصل وفترة الأنجاز</li>
			</ol>
		</section>
		@if(\session('success'))
		<div class="alert alert-success">
				{{\session('success')}}
		</div>
		@endif
		@if(\session('error'))
		<div class="alert alert-danger">
				{{\session('error')}}
		</div>
@endif
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body table-responsive">
							<form class="form-horizontal" method="post"  action="{{url('achievement_update/'.$mit->id)}}" enctype="multipart/form-data">
							{{csrf_field()}}

								<table id="example1" class="table table-bordered table-striped"  data-page-length='100'>
									<thead>
									<tr>
										<th> # </th>
										<th>نواتج التدريب Training Outputs</th>
										<th style="width: 30%">الفواصل Linking Activities</th>
										<th>زمن انشطة هذا الناتج Time</th>
										<th style="width: 30%">فترة الأنجاز period of achievement</th>
									</tr>
									</thead>
									<tbody>
									@foreach($training as $key=>$value)
									<tr>
										<td>
											{{$key+1}}
										</td>
										<td>{{$value->name}}</td>
										<td>
											<select class="form-control select2" multiple="" data-placeholder="اختر الفواصل" style="width: 100%;" name="separate{{$value->series_no}}[]">
											<option value="1" @if($value->separate==1)selected="" @endif>حركى</option>
											<option value="2" @if($value->separate==2)selected="" @endif>فكري</option>
											<option value="3" @if($value->separate==3)selected="" @endif>سمعي وبصري</option>
							                </select>
										</td>
										<td>{{$value->sum}}</td>
										<td>
											<input type="text" name="achievement{{$value->series_no}}" value="{{$value->achievement}}">
										</td>
									</tr>
									@endforeach
									</tbody>
								</table>
								<div class="box-footer">
									<button type="submit" class="btn btn-info ">حفظ</button>
								</div>
								<!-- /.box-footer -->
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection