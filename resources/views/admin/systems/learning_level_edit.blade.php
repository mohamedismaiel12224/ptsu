
@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				تدوين مستوى تعلم
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">تدوين مستوى تعلم</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
					    @if(\session('error'))
					    <div class="alert alert-danger">
					        {{\session('error')}}
					    </div>
						@endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post"  action="{{url('learning_level_update/'.$series_no.'/'.$mit->id)}}" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الطرق</label>
									<div class="col-sm-10">
										<div class="radio">
											<label style="padding-left: 150px;font-size: 25px">
												<input type="radio" name="status" id="value_method" value="0" checked="">قيمة
											</label>    
											<label style="padding-left: 150px;font-size: 25px">
												<input type="radio" name="status" id="select_method" value="1">تحديد
											</label>
											<label style="padding-left: 150px;font-size: 25px">
												<input type="radio" name="status" id="math_method" value="2">حساب
											</label>
										</div>
									</div>
				                </div>
				                <hr>
				                <div id="value_div">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">قيمة مستوى التعلم</label>

										<div class="col-sm-10">
											<input type="text" name="value" class="form-control">
										</div>
									</div>
								</div>
								<div id="select_div"  class="none">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">اختيار مستوى التعلم</label>

										<div class="col-sm-10">
											<select name="select" class="form-control">
												<option value="0" selected="" disabled="">اختر مستوى التعلم</option>
												<option value="s">متفوق</option>
												<option value="m">متوسط</option>
												<option value="d">صعوبات</option>
											</select>
										</div>
									</div>
								</div>
								<div id="math_div" class="none">
									@if($type==2||$type==5)
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">∑M =مجموع درجة المستهدفين في أداء المهارة .</label>

										<div class="col-sm-10">
											<input type="text" name="M" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">N =اجمالي عدد المستهدفين</label>

										<div class="col-sm-10">
											<input type="text" name="n" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">m =الدرجة الكلية للمهارة</label>

										<div class="col-sm-10">
											<input type="text" name="m" class="form-control">
										</div>
									</div>
									@else
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">A =عدد المستهدفين الذين حققوا الهدف .</label>

										<div class="col-sm-10">
											<input type="text" name="a" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label center">N =اجمالي عدد المستهدفين</label>

										<div class="col-sm-10">
											<input type="text" name="n" class="form-control">
										</div>
									</div>
									@endif
								</div>
							</div>
							
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection