@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($colleration))
				تعديل ارتباط
				@else
				إضافة ارتباط
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				@if(isset($colleration))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($colleration)) action="{{url('colleration/'.$colleration->id)}}" @else   action="{{url('colleration')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center"> نواتج التدريب  Training Outputs</label>
									<div class="col-sm-10">
										@if(isset($colleration))
											@php($arr = explode(',',$colleration->training_outputs_id))
										@endif
										<select class="form-control select2" data-placeholder="اختر نواتج التدريب" style="width: 100%;" name="training_outputs_id[]">
											@foreach($training as $val)
											<option value="{{$val->id}}"  
												@if(isset($arr))
													@foreach($arr as $v)
														@if($v==$val->id)selected @endif 
													@endforeach 
												@endif>{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">وصف الأرتباط  Colleration Detail</label>

									<div class="col-sm-10">
										<textarea  class="form-control" name="detail" rows="5" cols="80">@if(isset($colleration)){{$colleration->detail}}@else{{old('detail')}}@endif</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center"> مستوى التقويم  Evaluation Level</label>
									<div class="col-sm-10">
										<select class="form-control select2"  data-placeholder="اختر مستوى التقويم" style="width: 100%;" name="evaluate">
											@foreach($evaluate as $val)
											<option value="{{$val->id}}"  @if(isset($colleration)&&$colleration->evaluate_id==$val->id)selected @endif >{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">النوع  Type</label>
									<div class="col-sm-10">
										<div class="radio">
											<label style="padding-left: 50px;">
												<input type="radio" name="status" id="optionsRadios1" value="0" @if(isset($colleration) && ($colleration->status==0))checked @endif>يسبق  After
											</label>    
											<label style="padding-left: 50px;">
												<input type="radio" name="status" id="optionsRadios1" value="1" @if(isset($colleration) && ($colleration->status==1))checked @endif>يتبع  Before 
											</label>
										</div>
									</div>
				                </div>
							</div>
							@if(isset($colleration))
								<input type="hidden" name="_method" value="patch"> 
							@else
								<input type="hidden" name="pro_id" value="{{$mit->id}}">
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection