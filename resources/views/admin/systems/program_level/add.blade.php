@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        @if(isset($program))
        تعديل مستوى برنامج
        @else
        إضافة مستوى برنامج
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">@if(isset($program)) تعديل مستوى برنامج @else إضافة مستوى برنامج @endif</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <form class="form-horizontal" method="post" @if(isset($program)) action="{{url('program_level/'.$program->id)}}" @else action="{{url('program_level/')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}

                <table id="example1" class="table table-bordered table-striped"  data-page-length='100'>
                  <thead>
                  <tr>
                    <th> التسلسل </th>
                    <th>نواتج التدريب و الأرتباطات</th>
                    <th>اختيار</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($all as $key=>$value)
                  <tr>
                      <td>
                        {{$value->series_no}}
                      </td>
                      <td>{{$value->name}}</td>
                      <td>
                        @if(isset($program))
                        @php($arr = explode(',',$program->series_no_arr))
                        @endif
                        <input type="checkbox" name="series[{{$value->series_no}}]" value="{{$value->series_no}}" @if(isset($program)) @foreach($arr as $val) @if($value->series_no == $val) checked="" @endif @endforeach @endif>
                      </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <div class="box-footer">
                @if(isset($program))
                <input type="hidden" name="_method" value="patch"> 
                @else
                <input type="hidden" name="pro_id" value="{{$mit->id}}">
                @endif
                  <button type="submit" class="btn btn-info ">حفظ</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection