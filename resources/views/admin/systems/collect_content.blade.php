@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 class="center">
				جمع المحتوى Collect Content
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">جمع المحتوى</li>
			</ol>
		</section>
		@if(\session('success'))
		<div class="alert alert-success">
				{{\session('success')}}
		</div>
		@endif
		@if(\session('error'))
		<div class="alert alert-danger">
			{{\session('error')}}
		</div>
		@endif
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
						<div class="box-body table-responsive">
							<form class="form-horizontal" method="post"  action="{{url('collect_content_update/'.$mit->id)}}" enctype="multipart/form-data">
							{{csrf_field()}}
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-1 control-label ">العمق الفكري Intellectual Depth</label>
									<div class="col-sm-11">
										<textarea  name="public_depth" rows="4" class="form-control">{{$mit->public_depth}}</textarea>
									</div>
								</div>
								<hr>
								<table id="example1" class="table table-bordered table-striped" data-page-length='100'>
									<thead>
									<tr>
										<th> # </th>
										<th>نواتج التدريب Training Outputs</th>
										<th style="width: 28%">التأصيل  Indigenization</th>
										<th style="width: 28%">الخبرة الإلزامية   Expertise Mandatory - ماينبغي  </th>
										<th style="width: 28%">الخبرة الداعمة  Expertise Supporting - مايحسن  </th>
									</tr>
									</thead>
									<tbody>
									@foreach($all as $key=>$value)
									<tr>
										<td>
											{{$key+1}}
										</td>
										<td>{{$value->name}}</td>
										<td>
											<input type="text" style="width: 100%" name="mental_depth{{$value->series_no}}" value="{{$value->mental_depth}}">
										</td>
										<td  @if(volume($value->learning_level,$value->type,1)) style="background-color: #5aa3e391;" @endif>
											<input type="text" style="width: 100%" name="compulsive{{$value->series_no}}" value="{{$value->compulsive}}">
										</td>
										<td  @if(volume($value->learning_level,$value->type,2)) style="background-color: #5aa3e391;" @endif>
											<input type="text" style="width: 100%" name="buttress{{$value->series_no}}" value="{{$value->buttress}}">
										</td>
									</tr>
									@endforeach
									</tbody>
								</table>
								<div class="box-footer">
									<button type="submit" class="btn btn-info ">حفظ</button>
								</div>
								<!-- /.box-footer -->
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection