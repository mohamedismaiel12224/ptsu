@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 class="center">
				النـــــوع  Type
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">النوع</li>
			</ol>
		</section>
		@if(\session('success'))
		<div class="alert alert-success">
				{{\session('success')}}
		</div>
		@endif
		@if(\session('error'))
		<div class="alert alert-danger">
				{{\session('error')}}
		</div>
@endif
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body table-responsive">
							<form class="form-horizontal" method="post"  action="{{url('mit_type_update/'.$mit->id)}}" enctype="multipart/form-data">
							{{csrf_field()}}

								<table id="example1" class="table table-bordered table-striped"  data-page-length='100'>
									<thead>
									<tr>
										<th> # </th>
										<th style="width: 50%">نواتج التدريب و الإرتباطات  Training Outputs & Collerations</th>
										<th  style="width: 40%">النـــــوع  Type</th>
									</tr>
									</thead>
									<tbody>
									@foreach($all as $key=>$value)
									<tr>
											<td>
												{{$key+1}}
											</td>
											<td>{{$value->name}}</td>
											<td>
												<select class="form-control select2"  data-placeholder="اختر النوع" style="width: 100%;" name="type{{$value->series_no}}">
												<option value="0" disabled="" selected="">اختر النوع</option>
												<option value="1" @if($value->type == 1) selected="" @endif>رئيس معرفي</option>
												<option value="2" @if($value->type == 2) selected="" @endif>رئيس مهاري</option>
												<option value="3" @if($value->type == 3) selected="" @endif>رئيس اتجاهي</option>
												<option value="4" @if($value->type == 4) selected="" @endif>مساعد معرفي</option>
												<option value="5" @if($value->type == 5) selected="" @endif>مساعد مهاري</option>
												<option value="6" @if($value->type == 6) selected="" @endif>مساعد اتجاهي</option>
													
								                </select>
											</td>
									</tr>
									@endforeach
									</tbody>
								</table>
								<div class="box-footer">
									<button type="submit" class="btn btn-info ">حفظ</button>
								</div>
								<!-- /.box-footer -->
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection