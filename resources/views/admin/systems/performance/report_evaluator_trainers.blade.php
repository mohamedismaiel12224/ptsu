@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        تقييم مهارات العرض والإلقاء
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تقييم مهارات العرض والإلقاء</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Performance Evaluate System</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>نموذج تقييم الأداء</h3>
           <h3>عنوان البرنامج التدريبي {{$ev->name}}</h3>
          </div>
        </div>
        <hr>
        @foreach($evaluator as $key_ev=> $evaluator)
        <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة للمقيم</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">اسم المقيم : </th>
                  <td>{{$evaluator->name}}</td>
                </tr>
                <tr>
                  <th>الايميل :</th>
                  <td>{{$evaluator->email}}</td>
                </tr>
                <tr>
                  <th>رقم الجوال :</th>
                  <td>{{$evaluator->phone}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">المجالات Fields</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
                <div class="bs-example">
                    <div id="accordion{{$key_ev}}-1" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key_ev}}-1" href="#collapse{{$key_ev}}-1"> - المدرب </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key_ev}}-1" class="panel-collapse collapse ">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                    <table  class="table table-bordered table-striped" data-page-length='100'>
                                      <thead>
                                      <tr>
                                        <th> # </th>
                                        <th>العبارة Word</th>
                                        <th>التقييم Evaluate</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($evaluator->trainers as $key=>$value)
                                      <tr>
                                        <td>
                                          {{$key+1}}
                                        </td>
                                        <td>{{$value->name}}</td>
                                       <td class="text-center">
                                        <input type="text" class="form-control text-center" disabled="" value="@if(isset($value->evaluate) && ($value->evaluate==5))ممتاز Excellent @elseif(isset($value->evaluate) && ($value->evaluate==4))جيد جدا Very Good @elseif(isset($value->evaluate) && ($value->evaluate==3)) جديد Good @elseif(isset($value->evaluate) && ($value->evaluate==2)) مقبول Acceptable @elseif(isset($value->evaluate) && ($value->evaluate==1)) ضعيف Week @endif">
                                       </td>
                                      </tr>
                                      @endforeach
                                      </tbody>
                                    </table> 
                                    </ul>
                                </div>
                                <div class="box-body">
                                  <div class="form-group">

                                    <div class="col-sm-10">
                                      <input type="text" disabled="" class="form-control" placeholder="" value="@if(isset($evaluator->trainers[0])){{$evaluator->trainers[0]->notes}}@endif">
                                    </div>
                                    <label for="inputEmail3" class="col-sm-2 control-label center">ملاحظات Notes</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bs-example">
                    <div id="accordion{{$key_ev}}-2" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key_ev}}-2" href="#collapse{{$key_ev}}-2"> - المادة التدريبية</a>
                                </h4>
                            </div>
                            <div id="collapse{{$key_ev}}-2" class="panel-collapse collapse ">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                    <table  class="table table-bordered table-striped" data-page-length='100'>
                                      <thead>
                                      <tr>
                                        <th> # </th>
                                        <th>العبارة Word</th>
                                        <th>التقييم Evaluate</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($evaluator->ev_training_material as $key=>$value)
                                      <tr>
                                        <td>
                                          {{$key+1}}
                                        </td>
                                        <td>{{$value->name}}</td>
                                       <td class="text-center">
                                        <input type="text" class="form-control text-center" disabled="" value="@if(isset($value->evaluate) && ($value->evaluate==5))ممتاز Excellent @elseif(isset($value->evaluate) && ($value->evaluate==4))جيد جدا Very Good @elseif(isset($value->evaluate) && ($value->evaluate==3)) جديد Good @elseif(isset($value->evaluate) && ($value->evaluate==2)) مقبول Acceptable @elseif(isset($value->evaluate) && ($value->evaluate==1)) ضعيف Week @endif">
                                       </td>
                                      </tr>
                                      @endforeach
                                      </tbody>
                                    </table> 
                                    </ul>
                                </div>
                                <div class="box-body">
                                  <div class="form-group">

                                    <div class="col-sm-10">
                                      <input type="text" disabled="" class="form-control" placeholder="" value="@if(isset($evaluator->ev_training_material[0]))xx{{$evaluator->ev_training_material[0]->notes}}@endif">
                                    </div>
                                    <label for="inputEmail3" class="col-sm-2 control-label center">ملاحظات Notes</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          </ul>
        </div>
        <br>
        <br>
        @endforeach
        <div class="box-footer no-print">
            <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                <i class="fa fa-print"></i>
            </a>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection