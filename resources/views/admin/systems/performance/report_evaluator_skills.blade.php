@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        تقييم مهارات العرض والإلقاء
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تقييم مهارات العرض والإلقاء</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
   table { page-break-inside:auto }
      tr    { page-break-inside:avoid; page-break-after:auto }
      thead { display:table-header-group }
      tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Performance Evaluate System</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>نموذج تقييم الأداء</h3>
           <h3>عنوان البرنامج التدريبي {{$ev->name}}</h3>
          </div>
        </div>
        <hr>
        @foreach($evaluator as $key_ev=> $evaluator)
        @php($sum_ev=0)
        @php($count_ev=0)
        <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة للمقيم</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">اسم المقيم : </th>
                  <td>{{$evaluator->name}}</td>
                </tr>
                <tr>
                  <th>الايميل :</th>
                  <td>{{$evaluator->email}}</td>
                </tr>
                <tr>
                  <th>رقم الجوال :</th>
                  <td>{{$evaluator->phone}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">المجالات Fields</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
                @foreach($evaluator->domain as $key=>$domain)
                @php($sum=0)
                @php($count=0)
                <div class="bs-example">
                    <div id="accordion{{$key+1}}" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key_ev+1}}-{{$key+1}}" href="#collapse{{$key_ev+1}}-{{$key+1}}">{{$key+1}}- {{$domain->name}} </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key_ev+1}}-{{$key+1}}" class="panel-collapse collapse ">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid; overflow-x: scroll;">
                                      <table  class="table table-bordered table-striped " data-page-length='100'>
                                        <thead>
                                        <tr>
                                          <th> # </th>
                                          <th>العبارة Word</th>
                                          @foreach($domain->group as $key_group=>$group)
                                          <th>التقييم {{$key_group+1}} Evaluate </th>
                                          @endforeach
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($domain->checkup as $key_check=>$value)
                                        <tr>
                                          <td>
                                            {{$key_check+1}}
                                          </td>
                                          <td>{{$value->name}}</td>
                                          @foreach($value->group as $group)
                                         <td class="text-center">
                                          @php($sum+=$group->evaluate)
                                          @php($count++)
                                          @php($sum_ev+=$group->evaluate)
                                          @php($count_ev++)
                                          @if($status==5)
                                          <input type="text" class="form-control text-center" disabled="" value="@if(isset($group->evaluate) && ($group->evaluate==5))مميز Distinctive @elseif(isset($group->evaluate) && ($group->evaluate==4)) ممتاز Excellent @elseif(isset($group->evaluate) && ($group->evaluate==3)) متوسط Medium @elseif(isset($group->evaluate) && ($group->evaluate==2)) ضعيف Week @elseif(isset($group->evaluate) && ($group->evaluate==1)) مرفوض Refused @endif">
                                          @elseif($status==7)
                                          <input type="text" class="form-control text-center" disabled="" value="@if(isset($group->evaluate) && ($group->evaluate==5))ممتاز Excellent @elseif(isset($group->evaluate) && ($group->evaluate==4))جيد جدا Very Good @elseif(isset($group->evaluate) && ($group->evaluate==3)) جديد Good @elseif(isset($group->evaluate) && ($group->evaluate==2)) مقبول Acceptable @elseif(isset($group->evaluate) && ($group->evaluate==1)) ضعيف Week @endif">
                                          @endif
                                         </td>
                                         @endforeach
                                        </tr>
                                        @endforeach
                                        <tr>
                                          <td>#</td>
                                          <td>ملاحظات Notes</td>
                                          @foreach($domain->checkup[0]->group as $group)
                                          <td> 
                                            <textarea class="form-control" disabled="">@if(isset($group)){{$group->notes}}@endif</textarea> 
                                          </td>
                                          @endforeach
                                        </tr>
                                        </tbody>
                                      </table> 
                                    </ul>
                                </div>
                                <div class="box-body chart-responsive">
                                    <div class="chart"  id="line-chart-{{$key_ev+1}}-{{$key+1}}" style="width:100%;height: 400px;"></div>
                                </div>
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                      <table  class="table table-bordered table-striped" data-page-length='100'>
                                        <thead>
                                        <tr>
                                          <th>رقم التقييم Evaluate No</th>
                                          <th> المتوسط الحسابي Mean </th>
                                          <th>التقدير Evaluate</th>
                                          <th> الوصف  Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($domain->group as $key_group=>$g)
                                        @php($sum_group=0)
                                        @php($count_group=0)
                                          @foreach($g->checkup as $ch)
                                            @php($sum_group+=$ch->evaluate)
                                          @php($count_group++)
                                          @endforeach
                                        <tr>
                                          <td>
                                            {{$key_group+1}}
                                          </td>
                                          <td>{{round($sum_group/$count_group,2)}}</td>
                                          @if(($sum_group/$count_group)>=4.2)
                                          <td>مميز Distinctive</td>
                                          <td> أداء تدريبي مميز وعالي جدا</td>

                                          @elseif((($sum_group/$count_group)< 4.2)&& ($sum_group/$count_group)>=3.4)
                                          <td>ممتاز Excellent</td>
                                          <td> أداء تدريبي ممتاز</td>
                                          @elseif((($sum_group/$count_group)< 3.4)&& ($sum_group/$count_group)>=2.6)
                                          <td>متوسط Medium</td>
                                          <td> أداء تدريبي متوسط بحاجة إلى التطوير والتحسين </td>

                                          @elseif((($sum_group/$count_group)< 2.6)&& ($sum_group/$count_group)>=1.8)
                                          <td>ضعيف Week</td>
                                          <td> أداء تدريبي ضعيف بحاجة الى التحسين </td>
                                          @elseif(($sum_group/$count_group)< 1.8)
                                          <td>مرفوض Refused</td>
                                          <td> أداء تدريبي مرفوض</td>

                                          @endif
                                          
                                        </tr>
                                        @endforeach
                                        </tbody>
                                      </table> 
                                    </ul>
                                </div>
                                @if($count!=0)
                                <div class="box-body">
                                  <div style="margin: 20px;">
                                      <h3 class="text-center">المتوسط الحسابي لمجال {{$domain->name}} = {{round($sum/$count,2)}}</h3>
                                      @if(($sum/$count)>=4.2)
                                      <h3 class="text-center">التقدير العام = مميز Distinctive</h3>
                                      <h3 class="text-center">الوصف = أداء تدريبي مميز وعالي جدا</h3>

                                      @elseif((($sum/$count)< 4.2)&& ($sum/$count)>=3.4)
                                      <h3 class="text-center">التقدير العام = ممتاز Excellent</h3>
                                      <h3 class="text-center">الوصف = أداء تدريبي ممتاز</h3>
                                      @elseif((($sum/$count)< 3.4)&& ($sum/$count)>=2.6)
                                      <h3 class="text-center">التقدير العام = متوسط Medium</h3>
                                      <h3 class="text-center">الوصف = أداء تدريبي متوسط بحاجة إلى التطوير والتحسين </h3>

                                      @elseif((($sum/$count)< 2.6)&& ($sum/$count)>=1.8)
                                      <h3 class="text-center">التقدير العام = ضعيف Week</h3>
                                      <h3 class="text-center">الوصف = أداء تدريبي ضعيف بحاجة الى التحسين </h3>
                                      @elseif(($sum/$count)< 1.8)
                                      <h3 class="text-center">التقدير العام = مرفوض Refused</h3>
                                      <h3 class="text-center">الوصف = أداء تدريبي مرفوض</h3>

                                      @endif
                                  </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
          </ul>
        </div>
        @if($count_ev!=0)
          <div class="box-body">
            <div style="margin: 20px;border: #000 3px solid;">
                <h3 class="text-center">المتوسط الحسابي لجميع المجالات  للمقيم  <b style="color: #5252ff">{{$evaluator->name}}</b> = {{round($sum_ev/$count_ev,2)}}</h3>
                @if(($sum_ev/$count_ev)>=4.2)
                <h3 class="text-center">التقدير العام = مميز Distinctive</h3>
                <h3 class="text-center">الوصف = أداء تدريبي مميز وعالي جدا</h3>

                @elseif((($sum_ev/$count_ev)< 4.2)&& ($sum_ev/$count_ev)>=3.4)
                <h3 class="text-center">التقدير العام = ممتاز Excellent</h3>
                <h3 class="text-center">الوصف = أداء تدريبي ممتاز</h3>
                @elseif((($sum_ev/$count_ev)< 3.4)&& ($sum_ev/$count_ev)>=2.6)
                <h3 class="text-center">التقدير العام = متوسط Medium</h3>
                <h3 class="text-center">الوصف = أداء تدريبي متوسط بحاجة إلى التطوير والتحسين </h3>

                @elseif((($sum_ev/$count_ev)< 2.6)&& ($sum_ev/$count_ev)>=1.8)
                <h3 class="text-center">التقدير العام = ضعيف Week</h3>
                <h3 class="text-center">الوصف = أداء تدريبي ضعيف بحاجة الى التحسين </h3>
                @elseif(($sum_ev/$count_ev)< 1.8)
                <h3 class="text-center">التقدير العام = مرفوض Refused</h3>
                <h3 class="text-center">الوصف = أداء تدريبي مرفوض</h3>

                @endif
            </div>
          </div>
          @endif
        <br>
        <br>
        @endforeach
        <div class="box-footer no-print">
            <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                <i class="fa fa-print"></i>
            </a>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection