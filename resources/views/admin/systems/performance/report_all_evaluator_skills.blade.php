@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        تقييم مهارات العرض والإلقاء
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تقييم مهارات العرض والإلقاء</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Performance Evaluate System</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>نموذج تقييم الأداء</h3>
           <h3>عنوان البرنامج التدريبي {{$ev->name}}</h3>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">المجالات Fields</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
                @php($sum=0)
                @php($count=0)
                @foreach($domains as $key=>$domain)
                @php($sum_check=0)
                @php($count_check=0)
                <div class="bs-example">
                    <div id="accordion{{$key+1}}" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">{{$key+1}}- {{$domain->name}} </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                    <table  class="table table-bordered table-striped" data-page-length='100'>
                                      <thead>
                                      <tr>
                                        <th> # </th>
                                        <th>العبارة Word</th>
                                        <th>@if($status==5) مميز Distinctive @else ممتاز Excellent @endif</th>
                                        <th>@if($status==5) ممتاز Excellent @else جيد جدا Very Good @endif</th>
                                        <th>@if($status==5) متوسط Medium @else جديد Good @endif</th>
                                        <th>@if($status==5) ضعيف Week @else مقبول Acceptable @endif</th>
                                        <th>@if($status==5) مرفوض Refused @else ضعيف Week @endif</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($domain->checkup as $ke=>$value)
                                      <tr>
                                        <td>
                                          {{$ke+1}}
                                        </td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->evaluate_5}}</td>
                                        <td>{{$value->evaluate_4}}</td>
                                        <td>{{$value->evaluate_3}}</td>
                                        <td>{{$value->evaluate_2}}</td>
                                        <td>{{$value->evaluate_1}}</td>
                                      </tr>
                                      @php($x = $value->evaluate_5*5+$value->evaluate_4*4+$value->evaluate_3*3+$value->evaluate_2*2+$value->evaluate_1)
                                      @php($y = $value->evaluate_5+$value->evaluate_4+$value->evaluate_3+$value->evaluate_2+$value->evaluate_1)
                                      @php($sum_check+=$x)
                                      @php($count_check+=$y)
                                      @php($sum+=$x)
                                      @php($count+=$y)
                                      @endforeach
                                      </tbody>
                                    </table> 
                                    </ul>
                                </div>
                            </div>

                            <div class="box-body chart-responsive">
                                <div id="bar-chart-{{$key+1}}" style="height: 300px;width: 100%"></div>
                            </div>
                            <div class="box-body">
                              <div style="margin: 20px;">
                                  @if($count_check != 0)
                                  <h3 class="text-center">المتوسط الحسابي لمجال "{{$domain->name}}" = {{round($sum_check/$count_check,2)}}</h3>
                                  @if(($sum_check/$count_check)>=4.1)
                                  <h3 class="text-center">تقدير مجال {{$domain->name}} = مميز Distinctive</h3>
                                  <h3 class="text-center">الوصف = أداء تدريبي مميز وعالي جدا </h3>

                                  @elseif((($sum_check/$count_check)<4.1)&& ($sum_check/$count_check)>=3.41)
                                  <h3 class="text-center">تقدير مجال {{$domain->name}} = ممتاز Excellent</h3>
                                  <h3 class="text-center">الوصف = أداء تدريبي ممتاز</h3>
                                  @elseif((($sum_check/$count_check)<3.41)&& ($sum_check/$count_check)>=2.61)
                                  <h3 class="text-center">تقدير مجال {{$domain->name}} = متوسط Medium</h3>
                                  <h3 class="text-center">الوصف = أداء تدريبي متوسط بحاجة إلى التطوير والتحسين </h3>

                                  @elseif((($sum_check/$count_check)<2.61)&& ($sum_check/$count_check)>=1.81)
                                  <h3 class="text-center">تقدير مجال {{$domain->name}} = ضعيف Week</h3>
                                  <h3 class="text-center">الوصف = أداء تدريبي ضعيف بحاجة الى التحسين  </h3>
                                  @elseif(($sum_check/$count_check)<1.81)
                                  <h3 class="text-center">تقدير مجال {{$domain->name}} = مرفوض Refused</h3>
                                  <h3 class="text-center">الوصف = أداء تدريبي مرفوض</h3>

                                  @endif
                                  @endif
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
          </ul>
        </div>
        <div class="box-body">
          <div style="margin: 20px;">
              @if($count != 0)
              <h3 class="text-center">المتوسط الحسابي لجميع المجالات = {{round($sum/$count,2)}}</h3>
              @if(($sum/$count)>=4.2)
              <h3 class="text-center">التقدير العام = مميز Distinctive</h3>
              <h3 class="text-center">الوصف = أداء تدريبي مميز وعالي جدا</h3>

              @elseif((($sum/$count)<4.2)&& ($sum/$count)>=3.4)
              <h3 class="text-center">التقدير العام = ممتاز Excellent</h3>
              <h3 class="text-center">الوصف = أداء تدريبي ممتاز</h3>
              @elseif((($sum/$count)<3.4)&& ($sum/$count)>=2.6)
              <h3 class="text-center">التقدير العام = متوسط Medium</h3>
              <h3 class="text-center">الوصف = أداء تدريبي متوسط بحاجة إلى التطوير والتحسين </h3>

              @elseif((($sum/$count)<2.6)&& ($sum/$count)>=1.8)
              <h3 class="text-center">التقدير العام = ضعيف Week</h3>
              <h3 class="text-center">الوصف = أداء تدريبي ضعيف بحاجة الى التحسين </h3>
              @elseif(($sum/$count)<1.8)
              <h3 class="text-center">التقدير العام = مرفوض Refused</h3>
              <h3 class="text-center">الوصف = أداء تدريبي مرفوض</h3>

              @endif
              @endif
          </div>
        </div>
        <br>
        <br>
        <div class="box-footer no-print">
            <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                <i class="fa fa-print"></i>
            </a>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection