@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        المتدربين
      </h1>
     
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">المتدربين</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>الاسم Name</th>
                  <th>الايميل Email</th>
                  <th>رقم التواصل Phone</th>
                  <th>المجموعة Group</th>
                  <th>العمليات المتاحة Operations</th>
                </tr>
                </thead>
                <tbody>
                @foreach($trainers as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->phone}}</td>
                    <td>{{$value->group}}</td>
                    <td style="display: flex">
                      @if(isset($all_trainers))
                      <a href="{{url('report_evaluator/'.$value->id.'/'.$ev->id)}}" class="btn btn-primary" style="margin-right: 10px;">نتائج المقيمين <i class="fa fa-file"></i></a>
                      <a href="{{url('report_all_evaluator/'.$value->id.'/'.$ev->id)}}" class="btn bg-navy" style="margin-right: 10px;">النتيجة العامة <i class="fa fa-file"></i></a>

                      @elseif(isset($evaluator))
                      <a href="{{url('new_trainer_evaluate/'.$value->id.'/'.$ev->id)}}" class="btn btn-primary" style="margin-right: 10px;">تقييم جديد <i class="fa fa-television"></i></a>
                      <a href="{{url('trainer_evaluate_group/'.$value->id.'/'.$ev->id)}}" class="btn btn-info" style="margin-right: 10px;">التقييمات السابقة <i class="fa fa-television"></i></a>
                      @else
                      <a href="{{url('performance_evaluate/trainers/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('performance_evaluate/trainers/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>
                      @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection