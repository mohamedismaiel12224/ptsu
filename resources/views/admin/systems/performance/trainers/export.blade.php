@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        إضافة متدربين
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($trainer))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{url('performance_evaluate/trainers/export2/'.$ev->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المشروع Project Name</label>

                  <div class="col-sm-10">
                    <select class="form-control select2 select_project"  data-placeholder="التقييم" style="width: 100%;" name="project">
                      <option value="0" disabled="" selected="">المشاريع</option>
                      @foreach($projects as $pro)
                      <option value="{{$pro->id}}" >{{$pro->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المتدربين</label>

                  <div class="col-sm-10">
                    <select class="form-control select2 select_trainers" multiple=""  data-placeholder="المتدربين" style="width: 100%;" name="trainers[]">
                      
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المجموعة</label>

                  <div class="col-sm-10">
                    <select class="form-control select2 " data-placeholder="المجموعة" style="width: 100%;" name="group_id">
                        @foreach($groups as $group)
                        <option value="{{$group->id}}" @if(isset($trainer) && $trainer->group_id == $group->id)selected @endif>{{$group->group}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                
              </div>
              @if(isset($trainer))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="pro_id" value="{{$ev->id}}">
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection