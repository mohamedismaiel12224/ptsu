@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        تقييم مهارات العرض والإلقاء
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تقييم مهارات العرض والإلقاء</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Evaluate Performance System</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>نموذج تقييم الأداء</h3>
           <h3>اسم الشركة {{$ev->name}}</h3>
          </div>
        </div>
        <hr>
        <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة للمتدرب</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">اسم المتدرب : </th>
                  <td>{{$trainer->name}}</td>
                </tr>
                <tr>
                  <th>الايميل :</th>
                  <td>{{$trainer->email}}</td>
                </tr>
                <tr>
                  <th>رقم الجوال :</th>
                  <td>{{$trainer->phone}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">تقييم</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
            <form class="form-horizontal" method="post"   action="{{url('trainer_evaluate/'.$trainer->id.'/'.$ev->id)}}"  enctype="multipart/form-data">
              {{csrf_field()}}
                <div class="bs-example">
                    <div id="accordion1" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion1" href="#collapse1">- المدرب </a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                    <table  class="table table-bordered table-striped" data-page-length='100'>
                                      <thead>
                                      <tr>
                                        <th> # </th>
                                        <th>العبارة Word</th>
                                        <th>التقييم Evaluate</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($ev_trainers as $key=>$value)
                                      <tr>
                                        <td>
                                          {{$key+1}}
                                        </td>
                                        <td>{{$value->name}}</td>
                                       <td class="text-center">
                                        <select class="form-control select2"  data-placeholder="التقييم" style="width: 100%;" name="evaluate{{$value->id}}">
                                                <option value="0" disabled="" selected="">التقييم</option>
                                                <option value="5" @if(isset($value->evaluate) && ($value->evaluate==5)) selected @endif >ممتاز Excellent</option>
                                                <option value="4" @if(isset($value->evaluate) && ($value->evaluate==4)) selected @endif>جيد جدا Very Good</option>
                                                <option value="3" @if(isset($value->evaluate) && ($value->evaluate==3)) selected @endif>جديد Good</option>
                                                <option value="2" @if(isset($value->evaluate) && ($value->evaluate==2)) selected @endif>مقبول Acceptable</option>
                                                <option value="1" @if(isset($value->evaluate) && ($value->evaluate==1)) selected @endif>ضعيف Week</option>
                                              
                                              </select>
                                       </td>
                                      </tr>
                                      @endforeach
                                      </tbody>
                                    </table> 
                                    </ul>
                                </div>
                                <div class="box-body">
                                  <div class="form-group">

                                    <div class="col-sm-10">
                                      <input type="text" name="notes1" class="form-control" placeholder="" value="@if(isset($ev_trainers[0])){{$ev_trainers[0]->notes}}@endif">
                                    </div>
                                    <label for="inputEmail3" class="col-sm-2 control-label center">ملاحظات Notes</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bs-example">
                    <div id="accordion2" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2">- المادة التدريبية </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                    <table  class="table table-bordered table-striped" data-page-length='100'>
                                      <thead>
                                      <tr>
                                        <th> # </th>
                                        <th>العبارة Word</th>
                                        <th>التقييم Evaluate</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($ev_training_material as $key=>$value)
                                      <tr>
                                        <td>
                                          {{$key+1}}
                                        </td>
                                        <td>{{$value->name}}</td>
                                       <td class="text-center">
                                        <select class="form-control select2"  data-placeholder="التقييم" style="width: 100%;" name="evaluate{{$value->id}}">
                                                <option value="0" disabled="" selected="">التقييم</option>
                                                <option value="5" @if(isset($value->evaluate) && ($value->evaluate==5)) selected @endif >ممتاز Excellent</option>
                                                <option value="4" @if(isset($value->evaluate) && ($value->evaluate==4)) selected @endif>جيد جدا Very Good</option>
                                                <option value="3" @if(isset($value->evaluate) && ($value->evaluate==3)) selected @endif>جديد Good</option>
                                                <option value="2" @if(isset($value->evaluate) && ($value->evaluate==2)) selected @endif>مقبول Acceptable</option>
                                                <option value="1" @if(isset($value->evaluate) && ($value->evaluate==1)) selected @endif>ضعيف Week</option>
                                              
                                              </select>
                                       </td>
                                      </tr>
                                      @endforeach
                                      </tbody>
                                    </table> 
                                    </ul>
                                </div>
                                <div class="box-body">
                                  <div class="form-group">
                                    <div class="col-sm-10">
                                      <input type="text" name="notes2" class="form-control" placeholder="" value="@if(isset($ev_training_material[0])){{$ev_training_material[0]->notes}}@endif">
                                    </div>
                                    <label for="inputEmail3" class="col-sm-2 control-label center">ملاحظات Notes</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info ">حفظ</button>
              </div>
            </form>
          </ul>
        </div>
        <br>
        <br>
        <br>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection