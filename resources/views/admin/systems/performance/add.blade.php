@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        البيانات العامة
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('performance_evaluate')}}">مشاريعى</a></li>
        @if(isset($ev))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($ev)) action="{{url('performance_evaluate/'.$ev->id)}}" @else   action="{{url('performance_evaluate')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">اسم المشروع Project Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" placeholder="" value="@if(isset($ev)){{$ev->name}}@else{{old('name')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">نظام التقييم Evaluate System</label>

                  <div class="col-sm-10">
                    <select name="type" class="form-control">
                        <option value="0" selected="" disabled="">اختر نظام التقييم</option>
                        @if($sys->status==1)
                        <option value="1" @if(isset($ev)&& $ev->type == 1) selected="" @endif>تقييم مهارات العرض والتقديم</option>
                        @endif
                        @if($sys->status_arbitration==1)
                        <option value="2" @if(isset($ev)&& $ev->type == 2) selected="" @endif>التقييم الشامل للمدربين</option>
                        @endif
                      </select>
                  </div>
                </div>
              </div>
              @if(isset($ev))
                <input type="hidden" name="_method" value="patch"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection