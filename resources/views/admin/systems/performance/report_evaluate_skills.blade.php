@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        تقييم مهارات العرض والإلقاء
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تقييم مهارات العرض والإلقاء</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Performance Evaluate System</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>نموذج تقييم الأداء</h3>
           <h3>عنوان البرنامج التدريبي {{$ev->name}}</h3>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">المجالات Fields</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
                @foreach($domains as $key=>$domain)
                <div class="bs-example">
                    <div id="accordion{{$key+1}}" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">{{$key+1}}- {{$domain->name}} </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                    <table  class="table table-bordered table-striped" data-page-length='100'>
                                      <thead>
                                      <tr>
                                        <th> # </th>
                                        <th>العبارة Word</th>
                                        <th>مميز Distinctive</th>
                                        <th>ممتاز Excellent</th>
                                        <th>متوسط Medium</th>
                                        <th>ضعيف Week</th>
                                        <th>مرفوض Refused</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($domain->checkup as $ke=>$value)
                                      <tr>
                                        <td>
                                          {{$ke+1}}
                                        </td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->evaluate_5}}</td>
                                        <td>{{$value->evaluate_4}}</td>
                                        <td>{{$value->evaluate_3}}</td>
                                        <td>{{$value->evaluate_2}}</td>
                                        <td>{{$value->evaluate_1}}</td>
                                      </tr>
                                      @endforeach
                                      </tbody>
                                    </table> 
                                    </ul>
                                </div>
                            </div>
                            <div class="box-body">
                                <div style="margin: 20px;">
                                    <ol>
                                        @foreach($chart_skill[$key] as $val)
                                        <li><h4>({{$val['count']}}) {{$val['name']}}</h4></li>
                                        @endforeach
                                    </ol>
                                </div>
                                <div id="donut-chart-{{$key+1}}" style="height: 300px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
          </ul>
        </div>
        <br>
        <br>
        <div class="box-footer no-print">
            <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                <i class="fa fa-print"></i>
            </a>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection