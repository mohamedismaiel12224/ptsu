@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 class="center">
				الممارسة  Practice
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">الممارسة</li>
			</ol>
		</section>
		@if(\session('success'))
		<div class="alert alert-success">
				{{\session('success')}}
		</div>
		@endif
		@if(\session('error'))
		<div class="alert alert-danger">
				{{\session('error')}}
		</div>
@endif
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body table-responsive">
							<form class="form-horizontal" method="post"  action="{{url('investigation/'.$mit->id)}}" enctype="multipart/form-data">
							{{csrf_field()}}

								<table id="example1" class="table table-bordered table-striped" data-page-length='100'>
									<thead>
									<tr>
										<th> # </th>
										<th>نواتج التدريب و الأرتباطات Training Outputs & Collerations</th>
										<th>عمق وشمول  Depth and Inclusiveness</th>
										<th>التحقيق Investigation</th>
									</tr>
									</thead>
									<tbody>
									@foreach($all as $key=>$value)
									<tr>
											<td>
												{{$key+1}}
											</td>
											<td>{{$value->name}}</td>
											<td>{{depth($value->learning_level,$value->type)}}</td>
											<td>
												<select class="form-control select2"  data-placeholder="اختر النوع" style="width: 100%;" name="investigation{{$value->series_no}}">
												<option value="0" disabled="" selected="">اختر التحقيق</option>
												<option value="1" @if($value->investigation == 1) selected="" @endif>داخل</option>
												<option value="2" @if($value->investigation == 2) selected="" @endif>خارج</option>
												<option value="3" @if($value->investigation == 3) selected="" @endif>داخل و خارج</option>
												
								                </select>
											</td>
									</tr>
									@endforeach
									</tbody>
								</table>
								<div class="box-footer">
									<button type="submit" class="btn btn-info ">حفظ</button>
								</div>
								<!-- /.box-footer -->
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection