@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($training))
				تعديل ناتج تدريب
				@else
				اضافة ناتج تدريب
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				@if(isset($training))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($training)) action="{{url('training_outputs/'.$training->id)}}" @else   action="{{url('training_outputs')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">ناتج التدريب  Training Output</label>
									<div class="col-sm-10">
										<input type="text" name="name" class="form-control" placeholder="" value="@if(isset($training)){{$training->name}}@else{{old('name')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center"> مستوى التقويم  Evaluation Level</label>
									<div class="col-sm-10">
										<select class="form-control select2"  data-placeholder="اختر مستوى التقويم" style="width: 100%;" name="evaluate">
											@foreach($evaluate as $val)
											<option value="{{$val->id}}"  @if(isset($training)&&$training->evaluate_id==$val->id)selected @endif >{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
								</div>
							</div>
							@if(isset($training))
								<input type="hidden" name="_method" value="patch"> 
							@else
								<input type="hidden" name="pro_id" value="{{$mit->id}}">
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection