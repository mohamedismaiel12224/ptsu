@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        نموذج تحكيم
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">نموذج تحكيم</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Training needs analysis tool</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$need->logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>اداة تحليل الاحتياجات التدريبية</h3>
           <h3>اسم الشركة {{$need->name}}</h3>
          </div>
        </div>
        <hr>
        <!-- <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">اسم المنظمة : </th>
                  <td>{{$need->name}}</td>
                </tr>
                <tr>
                  <th>الموقع :</th>
                  <td>{{$need->location}}</td>
                </tr>
                <tr>
                  <th>المسؤول المباشر :</th>
                  <td>{{$need->responsible}}</td>
                </tr>
                <tr>
                  <th>الايميل :</th>
                  <td>{{$need->email}}</td>
                </tr>
                <tr>
                  <th>رقم التواصل :</th>
                  <td><span class="badge bg-light-blue">{{$need->phone}}</span></td>
                </tr>
                <tr>
                  <th>الفاكس :</th>
                  <td><span class="badge bg-light-blue">{{$need->fax}}</span></td>
                </tr>
                <tr>
                  <th>الاهداف العامة للمنظمة :</th>
                  <td>{{$need->goals}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr> -->
        <div class="rtl">
          <h2 class="text-center">المسؤوليات والمهام  Responsibilities And Tasks</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
            <form class="form-horizontal" method="post"  action="{{url('update_need_arbitration/'.$need->id)}}"  enctype="multipart/form-data">
              {{csrf_field()}}
                @foreach($responsibility as $key=>$resp)
                <div class="bs-example">
                    <div id="accordion{{$key+1}}" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">{{$key+1}}- {{$resp->name}} </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                    @foreach($resp->tasks as $ke=>$task)
                                    <div class="bs-example">
                                    <div id="accordion{{$key+1}}-{{$ke+1}}" class="panel-group">
                                      <div class="panel panel-default">
                                          <div class="panel-heading">
                                              <h4 class="panel-title">
                                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}" href="#collapse{{$key+1}}-{{$ke+1}}">{{$ke+1}}- {{$resp->name}} / {{$task->task}}</a>
                                              </h4>
                                          </div>
                                          <div id="collapse{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse in">
                                            <div class="panel-body table-responsive">
                                              <ul style="border: #00c0ef 2px solid;">
                                                <table  class="table table-bordered table-striped" data-page-length='100'>
                                                  <thead>
                                                  <tr>
                                                    <th> # </th>
                                                    <th>السؤال Question</th>
                                                    <th>نوع السؤال Type Question</th>
                                                    <th>وضوحه Clearance</th>
                                                    <th>مناسبته Suitable</th>
                                                    <th>ملحوظات Notes</th>
                                                  </tr>
                                                  </thead>
                                                  <tbody>
                                                  @foreach($task->questions as $key=>$value)
                                                  <tr>
                                                    <td>
                                                      {{$key+1}}
                                                    </td>
                                                    <td>{{$value->question}}</td>
                                                    <td>@if($value->type==1) معرفي @elseif($value->type==2) مهاري @else اتجاهي @endif</td>

                                                     <td class="text-center">
                                                      <select class="form-control select2"  data-placeholder="الوضوح" style="width: 100%;" name="clear{{$value->id}}">
                                                          <option value="0" disabled="" selected="">اختر وضوحه</option>
                                                          <option value="1" @if(isset($value->answer) && ($value->answer->clear == 1)) selected @endif >واضح Clear</option>
                                                          <option value="2" @if(isset($value->answer) && ($value->answer->clear == 2)) selected @endif >غير واضح Not Clear</option>
                                                        </select>
                                                     </td>
                                                     <td class="text-center">
                                                      <select class="form-control select2"  data-placeholder="التناسب" style="width: 100%;" name="suitable{{$value->id}}">
                                                          <option value="0" disabled="" selected="">اختر التناسب</option>
                                                          <option value="1" @if(isset($value->answer) && ($value->answer->suitable == 1)) selected @endif >مناسب Suitable</option>
                                                          <option value="2" @if(isset($value->answer) && ($value->answer->suitable == 2)) selected @endif >غير مناسب Not Suitable</option>
                                                        </select>
                                                     </td>
                                                     <td>
                                                       <input type="text" name="suggest{{$value->id}}" class="form-control" placeholder="" value="@if(isset($value->answer)&&$value->answer){{$value->answer->suggest}}@endif">
                                                     </td>
                                                  </tr>
                                                  @endforeach
                                                  </tbody>
                                                </table> 
                                              </ul>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                    </div>
                                    <hr>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
              <div class="box-footer">
                <button type="submit" class="btn btn-info ">حفظ</button>
              </div>
            </form>
          </ul>
        </div>
        <br>
        <br>
        <br>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection