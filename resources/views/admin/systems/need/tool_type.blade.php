@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">اختيار نوع الأداة Select Performance Type
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">اختيار نوع الأداة</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{url('update_tool_type/'.$need->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center"> نوع الأداة Performance Type</label>
                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر  نوع الأداة" style="width: 100%;" name="tool_type">
                      <option value="0" disabled="" selected="">اختر نوع الأداة</option>
                      <option value="1"  @if(isset($need)&&$need->tool_type==1)selected @endif >اختبار Test</option>
                      <option value="2"  @if(isset($need)&&$need->tool_type==2)selected @endif >مقابلة Interview</option>
                      <option value="3"  @if(isset($need)&&$need->tool_type==3)selected @endif >استبانة Detect</option>
                      <option value="4"  @if(isset($need)&&$need->tool_type==4)selected @endif >ملاحظة Note</option>
                      
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection