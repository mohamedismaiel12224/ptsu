@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
         أسئلة/عبارات الأداة المختارة  Questions And Words
      </h1>
      <a href="{{url($url_create.'/create/'.$need->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">أسئلة/عبارات الأداة المختارة</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>السؤال Question</th>
                  <th>المهمة Task</th>
                  <th>النوع Type</th>
                  @if($need->tool_type==1||$need->tool_type==2||$need_pro==2)
                  <th>نوع الاجابات Question Type</th>
                  <th>الاجابات Answers</th>
                  @endif
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($questions as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->question}}</td>
                    <td>{{$value->task}}</td>
                    <td>@if($value->type==1) معرفي Knowledge @elseif($value->type==2) مهاري Skill @else اتجاهي Attitude @endif</td>
                  
                    @if($need->tool_type==1||$need->tool_type==2||$need_pro==2)
                    <td>@if($need_pro==2 && $value->type==2) تدريج Scaling @else @if($value->type_answer==1) نصي Text @elseif($value->type_answer==2) اختيار من متعدد Select From multiple @else مربع اختيار Square Choice @endif  @endif</td>
                    <td>@if($value->type_answer==2||$value->type_answer==3){{get_answers($value->id)}}@else - @endif</td>
                    @endif
                    <td style="display: flex">
                      <a href="{{url($url_create.'/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('questions_need/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>

                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection