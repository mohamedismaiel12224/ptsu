@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($que))
        تعديل سؤال / عبارة
        @else
        إضافة سؤال / عبارة
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($que))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($que)) action="{{url('questions_need/'.$que->id)}}" @else   action="{{url('questions_need')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label center">السؤال Question</label>
                  <div class="col-sm-11">
                    <input type="text" name="question" class="form-control" placeholder="" value="@if(isset($que)){{$que->question}}@else{{old('question')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label center">المهمة Task</label>
                  <div class="col-sm-11">
                    <select class="form-control select2"  data-placeholder="اختر المهمة" style="width: 100%;" name="task_id">
                      <option value="0" disabled="" selected="">المهمة</option>
                      @foreach($tasks as $value)
                      <option value="{{$value->id}}" @if(isset($que)&&$que->task_id==$value->id) selected @endif>{{$value->name}} / {{$value->task}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label center">النوع Type</label>
                  <div class="col-sm-11">
                    <select class="form-control select2"  data-placeholder="اختر النوع" style="width: 100%;" name="type">
                      <option value="0" disabled="" selected="">النوع</option>
                      <option value="1" @if(isset($que)&&$que->type==1) selected @endif>معرفي Knowledge</option>
                      <option value="2" @if(isset($que)&&$que->type==2) selected @endif>مهاري Skill</option>
                      <option value="3" @if(isset($que)&&$que->type==3) selected @endif>اتجاهي Attitude</option>
                    
                    </select>
                  </div>
                </div>
                <input type="hidden" name="url" value="{{$url}}">
                @if($need->tool_type==1||$need->tool_type==2||$need_pro==2)
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label center">نوع الاجابة Question Type</label>
                  <div class="col-sm-11">
                    <div class="radio">
                      <label style="padding-left: 150px;font-size: 22px">
                        <input type="radio" name="type_answer" id="value_method" value="1" checked="">نصي Text
                      </label>    
                      <label style="padding-left: 150px;font-size: 22px">
                        <input type="radio" name="type_answer" id="select_method" value="2" @if(isset($que)&&$que->type_answer==2) checked @endif>اختيار من متعدد  Select From multiple
                      </label>
                      <label style="padding-left: 150px;font-size: 22px">
                        <input type="radio" name="type_answer" id="math_method" value="3" @if(isset($que)&&$que->type_answer==3) checked @endif>مربع اختيار Square Choice
                      </label>
                    </div>
                  </div>
                </div>
                <hr>
                <div id="math_div" @if(isset($que)&&$que->type_answer==3) @else class="none" @endif>
                  
                  @if(isset($que)&& isset($answers) && count($answers)>0)
                    @foreach($answers as $key=>$value)
                     <div class="form-group">
                      <label for="inputEmail3" class="col-sm-1 control-label center">الاختيار Select</label>
                      <div class="col-sm-11">
                        <input type="text" name="answers[]" class="form-control" placeholder="" value="{{$value->answer}}">
                      </div>
                    </div>
                    @endforeach
                    @else
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-1 control-label center">الاختيار Select</label>
                      <div class="col-sm-11">
                        <input type="text" name="answers[]" class="form-control" placeholder="" value="نعم">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-1 control-label center">الاختيار Select</label>
                      <div class="col-sm-11">
                        <input type="text" name="answers[]" class="form-control" placeholder="" value="لا">
                      </div>
                    </div>
                    @endif
                </div>
                <div id="select_div"   @if(isset($que)&&$que->type_answer==2) @else class="none" @endif>
                  <div class="add_answer_here">
                    @if(isset($que)&& isset($answers) && count($answers)>0)
                    @foreach($answers as $key=>$value)
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-1 control-label center">الاجابات</label>
                      <div class="col-sm-10">
                        <input type="text" name="answer[]" class="form-control" value="{{$value->answer}}" >
                      </div>
                      <label for="name" class="col-sm-1">
                        @if($key==0)
                        <a href="#" id="add_answer_field">
                          <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                        </a>
                        @else
                        <a href="#" id="remove_field">
                          <i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i>
                        </a>
                        @endif
                      </label>
                    </div>
                    @endforeach
                    @else
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-1 control-label center">الاجابات</label>
                      <div class="col-sm-10">
                        <input type="text" name="answer[]" class="form-control" >
                      </div>
                      <label for="name" class="col-sm-1">
                        <a href="#" id="add_answer_field">
                          <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                        </a>
                      </label>
                    </div>
                    @endif
                  </div>
                </div>
                @endif
              @if(isset($que))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="pro_id" value="{{$need->id}}"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection