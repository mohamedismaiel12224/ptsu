@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        بيانات الدراسة
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('need')}}">مشاريعى</a></li>
        @if(isset($need))
        <li class="active">تعديل بيانات الدراسة</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post"  action="{{url('update_studying_need_data/'.$need->id)}}"  enctype="multipart/form-data">
              {{csrf_field()}}
                <input type="hidden" name="_method" value="patch"> 

              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">عدد الموظفين</label>

                  <div class="col-sm-10">
                    <input type="text" name="employee_no" class="form-control" placeholder="" value="@if(isset($need)){{$need->employee_no}}@else{{old('employee_no')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المؤهلات العليا</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر المؤهلات العليا" style="width: 100%;" name="qualification">
                        <option disabled="" selected="" value="0" >اختر المؤهلات العليا</option>
                        <option @if($need->qualification==1) selected="" @endif value="1" >إبتدائي</option>
                        <option @if($need->qualification==2) selected="" @endif value="2" >متوسط</option>
                        <option @if($need->qualification==3) selected="" @endif value="3" >ثانوي</option>
                        <option @if($need->qualification==4) selected="" @endif value="4" >بكالوريوس</option>
                        <option @if($need->qualification==5) selected="" @endif value="5" >ماجستير</option>
                        <option @if($need->qualification==6) selected="" @endif value="6" >دكتوراه</option>
                    
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المستوى الوظيفي </label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر المستوى الوظيفي" style="width: 100%;" name="job_level">
                        <option disabled="" selected="" value="0" >اختر المستوى الوظيفي</option>
                        <option @if($need->qualification==1) selected="" @endif value="1" >قادة</option>
                        <option @if($need->qualification==2) selected="" @endif value="2" >مدراء تنفيذيون</option>
                        <option @if($need->qualification==3) selected="" @endif value="3" >رؤساء أقسام</option>
                        <option @if($need->qualification==4) selected="" @endif value="4" >موظفون عاديون</option>
                    
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">طبيعة المشكلة</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر طبيعة المشكلة" style="width: 100%;" name="problem_nature">
                        <option disabled="" selected="" value="0" >اختر طبيعة المشكلة</option>
                        <option @if($need->qualification==1) selected="" @endif value="1" >علاقات</option>
                        <option @if($need->qualification==2) selected="" @endif value="2" >انتاج</option>
                        <option @if($need->qualification==3) selected="" @endif value="3" >انضباط</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">وصف المشكلة</label>

                  <div class="col-sm-10">
                    <input type="text" name="description" class="form-control" placeholder="" value="@if(isset($need)){{$need->description}}@else{{old('description')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">عدد تكرارها الاسبوعي</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر عدد تكرارها الاسبوعي" style="width: 100%;" name="week_repeat">
                        <option disabled="" selected="" value="0" >اختر عدد تكرارها الاسبوعي</option>
                        @for($i=1;$i<=20;$i++)
                        <option @if($need->week_repeat==$i) selected="" @endif value="{{$i}}" >{{$i}}</option>
                        @endfor
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">معالجات سابقة</label>

                  <div class="col-sm-10">
                    <input type="text" name="previouse_treatment" class="form-control" placeholder="" value="@if(isset($need)){{$need->previouse_treatment}}@else{{old('previouse_treatment')}}@endif">
                  </div>
                </div>
                <h4 >تبريرات المسؤولين</h4>
                <div class="add_justification_here">
                  @if(count($justification)>0)
                  @foreach($justification as $key=>$value)
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label center">الصفة</label>
                    <div class="col-sm-4">
                      <select class="form-control select2" data-placeholder="اختر الصفة" style="width: 100%;" name="adjective[]">
                          <option disabled="" selected="" value="0" >اختر الصفة</option>
                          <option @if($value->adjective==1)selected @endif value="1" >مالك</option>
                          <option @if($value->adjective==2)selected @endif value="2" >مدير مباشر </option>
                          <option @if($value->adjective==3)selected @endif value="3" >مشرف</option>
                          <option @if($value->adjective==4)selected @endif value="4" >موظف</option>
                          <option @if($value->adjective==5)selected @endif value="5" >خبير خارجي</option>
                      </select>
                    </div>
                    <label for="inputEmail3" class="col-sm-1 control-label center">التبرير</label>

                    <div class="col-sm-4">
                      <input type="text" name="justification[]" class="form-control" value="{{$value->justification}}" placeholder="" >
                    </div>
                    @if($key==0)
                    <a href="#" id="add_justification_field">
                      <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                    </a>
                    @else
                    <label for="name" class="col-sm-1">
                      <a href="#" id="remove_field"><i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i></a>
                    </label>
                    @endif
                  </div>
                  @endforeach
                  @else
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label center">الصفة</label>

                    <div class="col-sm-4">
                      <select class="form-control select2" data-placeholder="اختر الصفة" style="width: 100%;" name="adjective[]">
                          <option disabled="" selected="" value="0" >اختر الصفة</option>
                          <option  value="1" >مالك</option>
                          <option  value="2" >مدير مباشر </option>
                          <option  value="3" >مشرف</option>
                          <option  value="4" >موظف</option>
                          <option  value="5" >خبير خارجي</option>
                      </select>
                    </div>
                    <label for="inputEmail3" class="col-sm-1 control-label center">التبرير</label>

                    <div class="col-sm-4">
                      <input type="text" name="justification[]" class="form-control" placeholder="" >
                    </div>
                    <a href="#" id="add_justification_field">
                        <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                      </a>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">دواعي التحليل</label>

                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر ادواعي التحليل" style="width: 100%;" name="need_analysis">
                        <option disabled="" selected="" value="0" >اختر دواعي التحليل</option>
                        <option @if($need->need_analysis==1) selected="" @endif value="1" >اشكاليات</option>
                        <option @if($need->need_analysis==2) selected="" @endif value="2" > تطوير ونمو </option>
                        <option @if($need->need_analysis==3) selected="" @endif value="3" >تغيير مستقبلي</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">مواضع الاستدلال</label>

                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر مواضع الاستدلال" style="width: 100%;" name="heuristic_sites">
                        <option disabled="" selected="" value="0" >اختر مواضع الاستدلال</option>
                        <option @if($need->heuristic_sites==1) selected="" @endif value="1" >بطائق الوصف الوظيفي</option>
                        <option @if($need->heuristic_sites==2) selected="" @endif value="2" > الأنشطة </option>
                        <option @if($need->heuristic_sites==3) selected="" @endif value="3" >الأولويات</option>
                        <option @if($need->heuristic_sites==4) selected="" @endif value="4" >النتائج</option>
                    </select>
                  </div>
                </div>
                <h4 >مناخ المنظمة</h4>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">علاقة الرئيس بالمرؤسين</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر علاقة الرئيس بالمرؤسين" style="width: 100%;" name="rel_pres_emp">
                        <option disabled="" selected="" value="0" >اختر علاقة الرئيس بالمرؤسين</option>
                        <option @if($need->rel_pres_emp==1) selected="" @endif value="1" >ممتازة</option>
                        <option @if($need->rel_pres_emp==2) selected="" @endif value="2" > جيدة </option>
                        <option @if($need->rel_pres_emp==3) selected="" @endif value="3" >ضعيفة</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">علاقة المشرفين بالمرؤسين</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر علاقة المشرفين بالمرؤسين" style="width: 100%;" name="rel_sup_emp">
                        <option disabled="" selected="" value="0" >اختر علاقة المشرفين بالمرؤسين</option>
                        <option @if($need->rel_sup_emp==1) selected="" @endif value="1" >ممتازة</option>
                        <option @if($need->rel_sup_emp==2) selected="" @endif value="2" > جيدة </option>
                        <option @if($need->rel_sup_emp==3) selected="" @endif value="3" >ضعيفة</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">عالقة المرؤسين مع بعضهم البعض</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر عالقة المرؤسين مع بعضهم البعض" style="width: 100%;" name="rel_emp_emp">
                        <option disabled="" selected="" value="0" >اختر عالقة المرؤسين مع بعضهم البعض</option>
                        <option @if($need->rel_emp_emp==1) selected="" @endif value="1" >ممتازة</option>
                        <option @if($need->rel_emp_emp==2) selected="" @endif value="2" > جيدة </option>
                        <option @if($need->rel_emp_emp==3) selected="" @endif value="3" >ضعيفة</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center"> أنظمة المكافئات والترقيات</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر أنظمة المكافئات والترقيات" style="width: 100%;" name="bonus">
                        <option disabled="" selected="" value="0" >اختر  أنظمة المكافئات والترقيات</option>
                        <option @if($need->bonus==1) selected="" @endif value="1" >ممتازة</option>
                        <option @if($need->bonus==2) selected="" @endif value="2" > جيدة </option>
                        <option @if($need->bonus==3) selected="" @endif value="3" >ضعيفة</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">التحفيز والتشجيع على الإبداع</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر التحفيز والتشجيع على الإبداع" style="width: 100%;" name="motivation">
                        <option disabled="" selected="" value="0" >اختر التحفيز والتشجيع على الإبداع</option>
                        <option @if($need->motivation==1) selected="" @endif value="1" >ممتازة</option>
                        <option @if($need->motivation==2) selected="" @endif value="2" > جيدة </option>
                        <option @if($need->motivation==3) selected="" @endif value="3" >ضعيفة</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">فرص النمو والتطور</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اختر فرص النمو والتطور" style="width: 100%;" name="develop">
                        <option disabled="" selected="" value="0" >اختر فرص النمو والتطور</option>
                        <option @if($need->develop==1) selected="" @endif value="1" >ممتازة</option>
                        <option @if($need->develop==2) selected="" @endif value="2" > جيدة </option>
                        <option @if($need->develop==3) selected="" @endif value="3" >ضعيفة</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center"> الأمان الوظيفي</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" data-placeholder="اخت الأمان الوظيفي" style="width: 100%;" name="safe">
                        <option disabled="" selected="" value="0" >اختر الأمان الوظيفي</option>
                        <option @if($need->safe==1) selected="" @endif value="1" >ممتازة</option>
                        <option @if($need->safe==2) selected="" @endif value="2" > جيدة </option>
                        <option @if($need->safe==3) selected="" @endif value="3" >ضعيفة</option>
                    </select>
                  </div>
                </div>
                <h4 >المؤشرات</h4>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الأداء الوظيفي</label>

                  <div class="col-sm-10">
                    <input type="text" name="performance" class="form-control" placeholder="" value="@if(isset($need)){{$need->performance}}@else{{old('performance')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center"> ترتيب المنظمة</label>

                  <div class="col-sm-10">
                    <input type="text" name="arrange" class="form-control" placeholder="" value="@if(isset($need)){{$need->arrange}}@else{{old('arrange')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">عدد الشكاوى</label>

                  <div class="col-sm-10">
                    <input type="text" name="beefs_no" class="form-control" placeholder="" value="@if(isset($need)){{$need->beefs_no}}@else{{old('beefs_no')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center"> معدل الانتاج</label>

                  <div class="col-sm-10">
                    <input type="text" name="productivity" class="form-control" placeholder="" value="@if(isset($need)){{$need->productivity}}@else{{old('productivity')}}@endif">
                  </div>
                </div>
              @if(isset($need))
                <input type="hidden" name="_method" value="patch"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection