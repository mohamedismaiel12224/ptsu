@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
<div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        تقرير المحكمين
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تقرير المحكمين</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif
    <style type="text/css">
        .bs-example{
          margin: 20px;
        }
        .panel-title .glyphicon{
            font-size: 14px;
        }
    </style>
    <style type="text/css">
        @media print{

        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        }
    </style>
    <section class="content">
        <div class=" box box-info">
            <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
                <div class="col-md-4 text-center">
                    <h3>International System for Standards Training</h3>
                    <h3>Training needs analysis tool</h3>
                </div>
                <div class="col-md-4  text-center">
                    <img src="{{asset('assets/images/home/'.$need->logo)}}" width="200px">
                </div>
                <div class="col-md-4 text-center">
                   <h3>الأنظمة المهنية للتدريب</h3>
                   <h3>اداة تحليل الاحتياجات التدريبية</h3>
                   <h3>اسم الشركة {{$need->name}}</h3>
                  </div>
            </div>
            <hr>
            <div class="text-center" style="border: #000 3px solid;">
              <h2>البيانات العامة</h2>
              <div class="table-responsive">
                <table class="table mytable table-striped">
                  <tbody>
                    <tr>
                      <th style="width:50%">اسم المنظمة : </th>
                      <td>{{$need->name}}</td>
                    </tr>
                    <tr>
                      <th>الموقع :</th>
                      <td>{{$need->location}}</td>
                    </tr>
                    <tr>
                      <th>المسؤول المباشر :</th>
                      <td>{{$need->responsible}}</td>
                    </tr>
                    <tr>
                      <th>الايميل :</th>
                      <td>{{$need->email}}</td>
                    </tr>
                    <tr>
                      <th>رقم التواصل :</th>
                      <td><span class="badge bg-light-blue">{{$need->phone}}</span></td>
                    </tr>
                    <tr>
                      <th>الفاكس :</th>
                      <td><span class="badge bg-light-blue">{{$need->fax}}</span></td>
                    </tr>
                    <tr>
                      <th>الاهداف العامة للمنظمة :</th>
                      <td>{{$need->goals}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <hr>
            <div class="rtl">
                <h2 class="text-center">المسؤوليات والمهام Responsibilities And Tasks</h2>
                <ul style="border: #00c0ef 3px solid;margin: 10px">
                    @foreach($responsibility as $key=>$resp)
                    <div class="bs-example">
                        <div id="accordion{{$key+1}}" class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">{{$key+1}}- {{$resp->name}} </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                    <div class="panel-body table-responsive">
                                        <ul style="border: #00c0ef 2px solid;">
                                            @php($avg_resp=0)
                                            @foreach($resp->tasks as $ke=>$task)
                                            <div class="bs-example">
                                                <div id="accordion{{$key+1}}-{{$ke+1}}" class="panel-group">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}" href="#collapse{{$key+1}}-{{$ke+1}}">{{$ke+1}}- {{$resp->name}} / {{$task->task}}</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse in">
                                                            <div class="panel-body table-responsive">
                                                                <ul style="border: #00c0ef 2px solid;">
                                                                    <table  class="table table-bordered table-striped" data-page-length='100'>
                                                                    <thead>
                                                                        <tr>
                                                                            <th> # </th>
                                                                            <th>السؤال</th>
                                                                            @if(isset($is_user))
                                                                            <th>وضوحه</th>
                                                                            <th>مناسبته</th>
                                                                            <th>ملحوظات</th>
                                                                            @else
                                                                            <th>واضح Clear</th>
                                                                            <th>غير واضح Not Clear</th>
                                                                            <th>مناسب Suitable</th>
                                                                            <th>غير مناسب Not Suitable</th>
                                                                            @endif
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @php($avg_task=0)
                                                                        @foreach($task->questions as $k=>$value)
                                                                        <tr>
                                                                            <td>
                                                                                {{$k+1}}
                                                                            </td>
                                                                            <td>{{$value->question}}</td>
                                                                            @if(isset($is_user))
                                                                            <td>
                                                                                @if($value->count_clear_1==1)
                                                                                واضح Clear
                                                                                @elseif($value->count_clear_2==1)
                                                                                غير واضح Not Clear
                                                                                @endif
                                                                            </td>
                                                                            <td>
                                                                                @if($value->count_suitable_1==1)
                                                                                مناسب Suitable
                                                                                @elseif($value->count_suitable_2==1)
                                                                                غير مناسب Not Suitable
                                                                                @endif
                                                                            </td>
                                                                            <td>
                                                                                {{$value->suggest}}
                                                                            </td>
                                                                            @else
                                                                            <td>
                                                                                {{$value->count_clear_1}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_clear_2}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_suitable_1}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_suitable_2}}
                                                                            </td>
                                                                            @endif
                                                                            <!-- <td>
                                                                            @if($value->count_user !=0)
                                                                            @php($avg_task+= ($value->count_1+$value->count_2*2+$value->count_3*3+$value->count_4*4+$value->count_5*5) / $value->count_user ) 
                                                                            @endif
                                                                            @if($value->count_user!=0)
                                                                            {{($value->count_1+$value->count_2*2+$value->count_3*3+$value->count_4*4+$value->count_5*5)/$value->count_user}}
                                                                            @else 0 @endif
                                                                           </td> -->
                                                                      </tr>
                                                                      @endforeach
                                                                      </tbody>
                                                                    </table> 
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <div style="margin: 20px;">
                                                                <!-- <h3 class="text-center">المتوسط الحسابي للمهمة {{$resp->name}} / {{$task->task}} = {{round($avg_task/count($task->questions),2)}}</h3>
                                                              
                                                                @php($avg_resp+=($avg_task/count($task->questions))) -->
                                                                <ol>
                                                                    @foreach($chart_task[$key]['tasks'][$ke]['task'] as $val)
                                                                    <li><h4>({{$val['count']}}) {{$val['name']}}</h4></li>
                                                                    @endforeach
                                                                </ol>
                                                            </div>
                                                            <div id="donut-chart-{{$key+1}}-{{$ke+1}}" style="height: 300px;"></div>
                                                            <ol>
                                                                @foreach($chart_task2[$key]['tasks'][$ke]['task'] as $val)
                                                                <li><h4>({{$val['count']}}) {{$val['name']}}</h4></li>
                                                                @endforeach
                                                            </ol>
                                                            <div id="donut-chart2-{{$key+1}}-{{$ke+1}}" style="height: 300px;"></div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <!-- <div class="box-body">
                                    <div style="margin: 20px;">
                                        <h3 class="text-center">المتوسط الحسابي لكل مسئولية {{$resp->name}}  = {{round($avg_resp/count($resp->tasks),2)}}</h3>
                                        <h3 class="text-center">النسبة المئوية لكل مسئولية = % {{round(20*$avg_resp/count($resp->tasks),2)}}</h3>
                                        <h3 class="text-center">الاولية لكل مسئولية = {{round((($avg_resp/count($resp->tasks))*$resp->public_avg)/5,2)}}</h3>
                                      @if(round((($avg_resp/count($resp->tasks))*$resp->public_avg)/5,2)>=0.6 && round((($avg_resp/count($resp->tasks))*$resp->public_avg)/5,2) <= 3.73)
                                      <h3 class="text-center">عــــالية</h3>
                                      @elseif(round((($avg_resp/count($resp->tasks))*$resp->public_avg)/5,2)>=3.74 && round((($avg_resp/count($resp->tasks))*$resp->public_avg)/5,2) <= 6.87)
                                      <h3 class="text-center">متوسطة</h3>
                                      @elseif(round((($avg_resp/count($resp->tasks))*$resp->public_avg)/5,2)>=6.88 && round((($avg_resp/count($resp->tasks))*$resp->public_avg)/5,2) <= 10)
                                      <h3 class="text-center">متدنية</h3>
                                      @endif
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </ul>
            </div>
            <br>
            <br>
            <br>
            <!-- /.row -->
            <div class="box-footer no-print">
                <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                    <i class="fa fa-print"></i>
                </a>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection