@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        تحديد المؤثرات وفرص التحسين
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تحديد المؤثرات وفرص التحسين</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <form class="form-horizontal" method="post"  action="{{url('need/update_affects_improve/'.$need->id)}}"  enctype="multipart/form-data">
          {{csrf_field()}}
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>اسم المسؤولية</th>
                  <th>المتوسط العام</th>
                  <th>المتوسط الحسابي</th>
                  <th>النسبة المئوية</th>
                  <th>الاولية</th>
                  <th>الاولية</th>
                  <th>المؤثر</th>
                  <th>الحل</th>
                </tr>
                <tr>
                  <th> # </th>
                  <th>Responsibility Name</th>
                  <th>Public Mean</th>
                  <th>Mean</th>
                  <th>Percentage</th>
                  <th>Initial</th>
                  <th>Initial</th>
                  <th>Affect</th>
                  <th>Solution</th>
                </tr>
                </thead>
                <tbody>
                @foreach($responsibility as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->public_avg}}</td>
                    <td>{{$value->avg_resp }}</td>
                    <td>% {{20*$value->avg_resp }}</td>
                    <td>{{$value->first }}</td>
                    <td>{{$value->first_text }}</td>
                    <td>
                      <select class="form-control select2"  data-placeholder="المؤثرات" style="width: 100%;" name="affect{{$value->id}}">
                        <option value="0" disabled="" selected="">المؤثرات</option>
                        @foreach($affect as $val)
                        <option value="{{$val->id}}" @if($value->affect == $val->id) selected @endif >{{$val->name}}</option>
                        @endforeach
                      </select>
                    </td>
                    <td>
                      <select class="form-control select2"  data-placeholder="الحلول" style="width: 100%;" name="solution{{$value->id}}">
                        <option value="0" disabled="" selected="">الحلول</option>
                        @foreach($solution as $val)
                        <option value="{{$val->id}}" @if($value->solution == $val->id) selected @endif >{{$val->name}}</option>
                        @endforeach
                      </select>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
                  <div class="box-footer">
                  <button type="submit" class="btn btn-info ">حفظ</button>
                </div>
              </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection