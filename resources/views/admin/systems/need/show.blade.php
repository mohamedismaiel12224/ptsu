@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        مشاريعى
      </h1>
      <a href="{{url('need/create')}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">مشاريعى</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>اسم المنظمة</th>
                  <th>الموقع</th>
                  <th>المسؤول المباشر</th>
                  <th>الايميل</th>
                  <th>رقم التليفون</th>
                  <th>الفاكس</th>
                  <th>الأهداف العامة للمنظمة</th>
                  <th>العمليات المتاحة</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->location}}</td>
                    <td>{{$value->responsible}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->phone}}</td>
                    <td>{{$value->fax}}</td>
                    <td>{{$value->goals}}</td>
                    <td style="display: flex">
                      <a href="{{url('need/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('need/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>
                    </td>
                    <td>
                      <a href="{{url('studying_need_data/'.$value->id)}}" class="btn bg-navy" >بيانات الدراسة <i class="fa fa-television"></i></a>
                      @if($value->step_no==4)
                      <a href="{{url('responsiblity_need/'.$value->id)}}" class="btn btn-primary" style="margin-right: 10px;">فتح المشروع <i class="fa fa-television"></i></a>
                      <a href="{{url('need/report/'.$value->id)}}" class="btn bg-navy" style="margin-right: 10px;">التقرير <i class="fa fa-file"></i></a>
                      @else
                      <a href="{{url('need/'.$value->id)}}" class="btn btn-primary" style="margin-right: 10px;">فتح المشروع <i class="fa fa-television"></i></a>
                      @endif
                      

                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection