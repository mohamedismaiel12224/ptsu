@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
<div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        نقرير اداة المجال المهاري
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">نقرير اداة المجال المهاري</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif
    <style type="text/css">
        .bs-example{
          margin: 20px;
        }
        .panel-title .glyphicon{
            font-size: 14px;
        }
    </style>
    <style type="text/css">
        @media print{

        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        }
    </style>
    <section class="content">
        <div class=" box box-info">
            <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
                <div class="col-md-4 text-center">
                    <h3>International System for Standards Training</h3>
                    <h3>Training needs analysis tool</h3>
                </div>
                <div class="col-md-4  text-center">
                    <img src="{{asset('assets/images/home/'.$need->logo)}}" width="200px">
                </div>
                <div class="col-md-4 text-center">
                   <h3>الأنظمة المهنية للتدريب</h3>
                   <h3>اداة تحليل الاحتياجات التدريبية</h3>
                   <h3>اسم الشركة {{$need->name}}</h3>
                  </div>
            </div>
            <hr>
            <div class="text-center" style="border: #000 3px solid;">
              <h2>البيانات العامة</h2>
              <div class="table-responsive">
                <table class="table mytable table-striped">
                  <tbody>
                    <tr>
                      <th style="width:50%">اسم المنظمة : </th>
                      <td>{{$need->name}}</td>
                    </tr>
                    <tr>
                      <th>الموقع :</th>
                      <td>{{$need->location}}</td>
                    </tr>
                    <tr>
                      <th>المسؤول المباشر :</th>
                      <td>{{$need->responsible}}</td>
                    </tr>
                    <tr>
                      <th>الايميل :</th>
                      <td>{{$need->email}}</td>
                    </tr>
                    <tr>
                      <th>رقم التواصل :</th>
                      <td><span class="badge bg-light-blue">{{$need->phone}}</span></td>
                    </tr>
                    <tr>
                      <th>الفاكس :</th>
                      <td><span class="badge bg-light-blue">{{$need->fax}}</span></td>
                    </tr>
                    <tr>
                      <th>الاهداف العامة للمنظمة :</th>
                      <td>{{$need->goals}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <hr>
            <div class="rtl">
                <h2 class="text-center">المسؤوليات والمهام Responsibilities And Tasks</h2>
                <ul style="border: #00c0ef 3px solid;margin: 10px">
                    @foreach($responsibility as $key=>$resp)
                    <div class="bs-example">
                        <div id="accordion{{$key+1}}" class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">{{$key+1}}- {{$resp->name}} </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                    <div class="panel-body table-responsive">
                                        <ul style="border: #00c0ef 2px solid;">
                                            @php($avg_resp=0)
                                            @foreach($resp->tasks as $ke=>$task)
                                            <div class="bs-example">
                                                <div id="accordion{{$key+1}}-{{$ke+1}}" class="panel-group">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}" href="#collapse{{$key+1}}-{{$ke+1}}">{{$ke+1}}- {{$resp->name}} / {{$task->task}}</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse in">
                                                            <div class="panel-body table-responsive">
                                                                <ul style="border: #00c0ef 2px solid;">
                                                                    <table  class="table table-bordered table-striped" data-page-length='100'>
                                                                    <thead>
                                                                        <tr>
                                                                            <th> # </th>
                                                                            <th>السؤال</th>
                                                                            @if(isset($is_user))
                                                                            <th>درجة المقييم</th>
                                                                            @else
                                                                            <th>10</th>
                                                                            <th>9</th>
                                                                            <th>8</th>
                                                                            <th>7</th>
                                                                            <th>6</th>
                                                                            <th>5</th>
                                                                            <th>4</th>
                                                                            <th>3</th>
                                                                            <th>2</th>
                                                                            <th>1</th>
                                                                            @endif
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @php($avg_task=0)
                                                                        @foreach($task->questions as $k=>$value)
                                                                        <tr>
                                                                            <td>
                                                                                {{$k+1}}
                                                                            </td>
                                                                            <td>{{$value->question}}</td>
                                                                            @if(isset($is_user))
                                                                            <td>
                                                                                @if($value->count_10==1)
                                                                                10
                                                                                @elseif($value->count_9==1)
                                                                                9
                                                                                @elseif($value->count_8==1)
                                                                                8
                                                                                @elseif($value->count_7==1)
                                                                                7
                                                                                @elseif($value->count_6==1)
                                                                                6
                                                                                @elseif($value->count_5==1)
                                                                                5
                                                                                @elseif($value->count_4==1)
                                                                                4
                                                                                @elseif($value->count_3==1)
                                                                                3
                                                                                @elseif($value->count_2==1)
                                                                                2
                                                                                @elseif($value->count_1==1)
                                                                                1
                                                                                @endif
                                                                            </td>
                                                                            @else
                                                                            <td>
                                                                                {{$value->count_10}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_9}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_8}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_7}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_6}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_5}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_4}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_3}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_2}}
                                                                            </td>
                                                                            <td>
                                                                                {{$value->count_1}}
                                                                            </td>
                                                                            @endif
                                                                        </tr>
                                                                        @if($value->count_user!=0)
                                                                        @php($avg_task+=($value->count_1+$value->count_2*2+$value->count_3*3+$value->count_4*4+$value->count_5*5+$value->count_6*6+$value->count_7*7+$value->count_8*8+$value->count_9*9+$value->count_10*10)/$value->count_user)
                                                                        @endif
                                                                        @endforeach
                                                                      </tbody>
                                                                    </table> 
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <div style="margin: 20px;">
                                                                @if(count($task->questions)!=0)
                                                                <h3 class="text-center">المتوسط الحسابي للمهمة {{$resp->name}} / {{$task->task}} = {{round($avg_task/count($task->questions),2)}}</h3>
                                                                @php($avg_resp+=($avg_task/count($task->questions)))
                                                                @endif
                                                                <ol>
                                                                    @foreach($chart_task[$key]['tasks'][$ke]['task'] as $val)
                                                                    <li><h4>({{$val['count']}}) {{$val['name']}}</h4></li>
                                                                    @endforeach
                                                                </ol>
                                                            </div>
                                                            <div id="donut-chart-{{$key+1}}-{{$ke+1}}" style="height: 300px;"></div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div style="margin: 20px;">
                                        <h3 class="text-center">مستوى التعلم  = {{100-round(100*$avg_resp/(count($resp->tasks)*10),2)}}</h3>
                                        <h3 class="text-center">التقدير = {{degree(100-(100*$avg_resp/(count($resp->tasks)*10)))}}</h3>
                                        <h3 class="text-center">معامل الصعوبة  = {{round(100*$avg_resp/(count($resp->tasks)*10),2)}}</h3>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </ul>
            </div>
            <br>
            <br>
            <br>
            <!-- /.row -->
            <div class="box-footer no-print">
                <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                    <i class="fa fa-print"></i>
                </a>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection