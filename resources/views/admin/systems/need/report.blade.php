@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Training needs analysis tool</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$need->logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>اداة تحليل الاحتياجات التدريبية</h3>
           <h3>اسم الشركة {{$need->name}}</h3>
          </div>
        </div>
        <hr>
        <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">اسم المنظمة : </th>
                  <td>{{$need->name}}</td>
                </tr>
                <tr>
                  <th>الموقع :</th>
                  <td>{{$need->location}}</td>
                </tr>
                <tr>
                  <th>المسؤول المباشر :</th>
                  <td>{{$need->responsible}}</td>
                </tr>
                <tr>
                  <th>الايميل :</th>
                  <td>{{$need->email}}</td>
                </tr>
                <tr>
                  <th>رقم التواصل :</th>
                  <td><span class="badge bg-light-blue">{{$need->phone}}</span></td>
                </tr>
                <tr>
                  <th>الفاكس :</th>
                  <td><span class="badge bg-light-blue">{{$need->fax}}</span></td>
                </tr>
                <tr>
                  <th>الاهداف العامة للمنظمة :</th>
                  <td>{{$need->goals}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">المسؤوليات والمهام  Responsibilities And Tasks</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
            @foreach($responsibility as $key=>$resp)
            <div class="bs-example">
              <div id="accordion{{$key+1}}" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">المسؤولية {{$key+1}}</a>
                        </h4>
                    </div>
                    <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="table-responsive" style="border: #f50 3px solid;">
                              <table class="table mytable table-striped">
                                <tbody>
                                  <tr>
                                    <th >اسم المسؤولية  Responsibility Name </th>
                                    <td>{{$resp->name}}</td>
                                  </tr>
                                  <tr>
                                    <th >المتوسط العام Mean</th>
                                    <td>{{$resp->avg}}</td>
                                  </tr>
                                  <tr>
                                    <th>المهمات Tasks</th>
                                    
                                    <td style="width: 90%;">
                                      @foreach($resp->tasks as $ke=>$task)
                                          <div class="bs-example">
                                            <div id="accordion{{$key+1}}-{{$ke+1}}" class="panel-group">
                                              <div class="panel panel-default">
                                                  <div class="panel-heading">
                                                      <h4 class="panel-title">
                                                          <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}" href="#collapse{{$key+1}}-{{$ke+1}}">المسؤولية {{$key+1}} / المهمة {{$ke+1}}</a>
                                                      </h4>
                                                  </div>
                                                  <div id="collapse{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse">
                                                    <div class="panel-body table-responsive">
                                                      <ul style="border: #00c0ef 2px solid;">
                                                        <table class="table mytable  table-striped" >
                                                          <tbody>
                                                            <tr>
                                                              <th > # </th>
                                                              <td>{{$ke+1}}</td>
                                                            </tr>
                                                            <tr>
                                                              <th >اسم المهمة </th>
                                                              <td>{{$task->task}}</td>
                                                            </tr>
                                                            <tr>
                                                              <th>معدل الأداء الأسبوعي Weekly Performance Mean</th>
                                                              <td>@if($task->performance==4) دائم ومنتظم @elseif($task->performance==3) متوسط @elseif($task->performance==2) احيانا @elseif($task->performance==1) نادرا @endif</td>
                                                            </tr>
                                                            <tr>
                                                              <th>الأهمية Importance</th>
                                                               <td>@if($task->importance==3) عالي @elseif($task->importance==2) متوسط @elseif($task->importance==1) متدني @endif</td>
                                                            </tr>
                                                            <tr>
                                                              <th>صعوبة الأداء Performance Difficult</th>
                                                              <td>@if($task->difficult==3) عالي @elseif($task->difficult==2) متوسط @elseif($task->difficult==1) متدني @endif</td>
                                                            </tr>
                                                            <tr>
                                                              <th>مجموع النقاط Sum</th>
                                                              <td>{{$task->performance+$task->importance+$task->difficult}}</td>
                                                            </tr>
                                                          </tbody>
                                                        </table>                                                        
                                                      </ul>
                                                    </div>
                                                    
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        @endforeach  
                                    </td>
                                    
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <hr>
            @endforeach
          </ul>
        </div>
        <div class="box-footer no-print">
          <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
              <i class="fa fa-print"></i>
          </a>
        </div>
        <br>
        <br>
        <br>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection