@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
<div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        تقرير اداة المجال المعرفي والاتجاهي
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تقرير اداة المجال المعرفي والاتجاهي</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif
    <style type="text/css">
        .bs-example{
          margin: 20px;
        }
        .panel-title .glyphicon{
            font-size: 14px;
        }
    </style>
    <style type="text/css">
        @media print{

        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        }
    </style>
    <section class="content">
        <div class=" box box-info">
            <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
                <div class="col-md-4 text-center">
                    <h3>International System for Standards Training</h3>
                    <h3>Training needs analysis tool</h3>
                </div>
                <div class="col-md-4  text-center">
                    <img src="{{asset('assets/images/home/'.$need->logo)}}" width="200px">
                </div>
                <div class="col-md-4 text-center">
                   <h3>الأنظمة الدولية لمعايير التدريب</h3>
                   <h3>اداة تحليل الاحتياجات التدريبية</h3>
                   <h3>اسم الشركة {{$need->name}}</h3>
                </div>
            </div>
            <hr>
            <div class="text-center" style="border: #000 3px solid;">
              <h2>البيانات العامة</h2>
              <div class="table-responsive">
                <table class="table mytable table-striped">
                  <tbody>
                    <tr>
                      <th style="width:50%">اسم المنظمة : </th>
                      <td>{{$need->name}}</td>
                    </tr>
                    <tr>
                      <th>الموقع :</th>
                      <td>{{$need->location}}</td>
                    </tr>
                    <tr>
                      <th>المسؤول المباشر :</th>
                      <td>{{$need->responsible}}</td>
                    </tr>
                    <tr>
                      <th>الايميل :</th>
                      <td>{{$need->email}}</td>
                    </tr>
                    <tr>
                      <th>رقم التواصل :</th>
                      <td><span class="badge bg-light-blue">{{$need->phone}}</span></td>
                    </tr>
                    <tr>
                      <th>الفاكس :</th>
                      <td><span class="badge bg-light-blue">{{$need->fax}}</span></td>
                    </tr>
                    <tr>
                      <th>الاهداف العامة للمنظمة :</th>
                      <td>{{$need->goals}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <hr>
            <div class="rtl">
                <h2 class="text-center">المسؤوليات والمهام Responsibilities And Tasks</h2>
                <ul style="border: #00c0ef 3px solid;margin: 10px">
                    @foreach($responsibility as $key=>$resp)
                    <div class="bs-example">
                        <div id="accordion{{$key+1}}" class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">{{$key+1}}- {{$resp->name}} </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                    <div class="panel-body table-responsive">
                                        <ul style="border: #00c0ef 2px solid;">
                                            @php($avg_resp=0)
                                            @foreach($resp->tasks as $ke=>$task)
                                            <div class="bs-example">
                                                <div id="accordion{{$key+1}}-{{$ke+1}}" class="panel-group">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}" href="#collapse{{$key+1}}-{{$ke+1}}">{{$ke+1}}- {{$resp->name}} / {{$task->task}}</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse in">
                                                            <div class="panel-body table-responsive">
                                                                <ul style="border: #00c0ef 2px solid;">
                                                                    @php($avg_task=0)
                                                                    @foreach($task->questions as $k=>$value)
                                                                    <div class="bs-example">
                                                                        <div id="accordion{{$key+1}}-{{$ke+1}}-{{$k+1}}" class="panel-group">
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading">
                                                                                    <h4 class="panel-title">
                                                                                        <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}-{{$k+1}}" href="#collapse{{$key+1}}-{{$ke+1}}-{{$k+1}}"> السؤال / العبارة - {{$k+1}}</a>
                                                                                    </h4>
                                                                                </div>
                                                                                <div id="collapse{{$key+1}}-{{$ke+1}}-{{$k+1}}" class="panel-collapse collapse ">
                                                                                    <div class="panel-body table-responsive">
                                                                                        <ul style="border: #00c0ef 2px solid;">
                                                                                            <li><h3>السؤال = {{$value->question}}</h3></li>
                                                                                            <li><h3>اجتاز = {{$value->count_11}}</h3></li>
                                                                                            <li><h3>لم يجتاز = {{$value->count_10}}</h3></li>
                                                                                            @if($value->count_user!=0)
                                                                                            <li><h3>مستوى التعلم  =  {{100-round(100*$value->count_11/$value->count_user,2)}}</h3></li>
                                                                                            <li><h3>التقدير = {{degree(100-(100*$value->count_11/$value->count_user))}}</h3></li>
                                                                                            <li><h3>معامل الصعوبة  = {{round(100*$value->count_11/$value->count_user,2)}}</h3></li>
                                                                                            @php($avg_task+=$value->count_11/$value->count_user)
                                                                                            @endif
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <div style="margin: 20px;">    
                                                                @if(count($task->questions)!=0)
                                                                @php($avg_resp+=($avg_task/count($task->questions)))
                                                                @endif                                                
                                                                <ol>
                                                                    @foreach($chart_task[$key]['tasks'][$ke]['task'] as $val)
                                                                    <li><h4>({{$val['count']}}) {{$val['name']}}</h4></li>
                                                                    @endforeach
                                                                </ol>
                                                            </div>
                                                            <div id="donut-chart-{{$key+1}}-{{$ke+1}}" style="height: 300px;"></div>
                                                            <div style="margin: 20px;">    
                                                               
                                                                <h3 class="text-center">مستوى التعلم للمهمة = {{100-round(100*$avg_task/count($task->questions),2)}}</h3>
                                                                <h3 class="text-center">التقدير للمهمة = {{degree(100-(100*$avg_task/count($task->questions)))}}</h3>
                                                                <h3 class="text-center">معامل الصعوبة للمهمة = {{round(100*$avg_task/count($task->questions),2)}}</h3>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div style="margin: 20px;">
                                        <h3 class="text-center">مستوى التعلم للكل  = {{100-round(100*$avg_resp/count($resp->tasks),2)}}</h3>
                                        <h3 class="text-center">التقدير للكل = {{degree(100-(100*$avg_resp/count($resp->tasks)))}}</h3>
                                        <h3 class="text-center">معامل الصعوبة للكل = {{round(100*$avg_resp/count($resp->tasks),2)}}</h3>
                                        
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </ul>
            </div>
            <br>
            <br>
            <br>
            <!-- /.row -->
            <div class="box-footer no-print">
                <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                    <i class="fa fa-print"></i>
                </a>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection