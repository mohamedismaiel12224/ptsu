@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        البيانات العامة
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('need')}}">مشاريعى</a></li>
        @if(isset($need))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($need)) action="{{url('need/'.$need->id)}}" @else   action="{{url('need')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">اسم المنظمة</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" placeholder="" value="@if(isset($need)){{$need->name}}@else{{old('name')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الموقع</label>

                  <div class="col-sm-10">
                    <input type="text" name="location" class="form-control" placeholder="" value="@if(isset($need)){{$need->location}}@else{{old('location')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المسؤول المباشر</label>

                  <div class="col-sm-10">
                    <input type="text" name="responsible" class="form-control" placeholder="" value="@if(isset($need)){{$need->responsible}}@else{{old('responsible')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الايميل</label>

                  <div class="col-sm-10">
                    <input type="text" name="email" class="form-control" placeholder="" value="@if(isset($need)){{$need->email}}@else{{old('email')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">رقم التواصل</label>

                  <div class="col-sm-10">
                    <input type="text" name="phone" class="form-control" placeholder="" value="@if(isset($need)){{$need->phone}}@else{{old('phone')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الفاكس</label>

                  <div class="col-sm-10">
                    <input type="text" name="fax" class="form-control" placeholder="" value="@if(isset($need)){{$need->fax}}@else{{old('fax')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الاهداف العامة للمنظمة</label>

                  <div class="col-sm-10">
                    <input type="text" name="goals" class="form-control" placeholder="" value="@if(isset($need)){{$need->goals}}@else{{old('goals')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">شعار المنظمة</label>

                  <div class="col-sm-10">
                    <input type="file" name="logo" class="form-control" >
                  </div>
                </div>
              </div>
              @if(isset($need))
                <input type="hidden" name="_method" value="patch"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection