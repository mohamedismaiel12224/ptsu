@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        نموذج حل الاختبار / المقارنة
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">نموذج حل الاختبار / المقارنة</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Training needs analysis tool</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$need->logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة الدولية لمعايير التدريب</h3>
           <h3>اداة تحليل الاحتياجات التدريبية</h3>
           <h3>الجهة المستفيدة {{$need->responsible}}</h3>
          </div>
        </div>
        <hr>
        <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">اسم المنظمة : </th>
                  <td>{{$need->name}}</td>
                </tr>
                <tr>
                  <th>الموقع :</th>
                  <td>{{$need->location}}</td>
                </tr>
                <tr>
                  <th>المسؤول المباشر :</th>
                  <td>{{$need->responsible}}</td>
                </tr>
                <tr>
                  <th>الايميل :</th>
                  <td>{{$need->email}}</td>
                </tr>
                <tr>
                  <th>رقم التواصل :</th>
                  <td><span class="badge bg-light-blue">{{$need->phone}}</span></td>
                </tr>
                <tr>
                  <th>الفاكس :</th>
                  <td><span class="badge bg-light-blue">{{$need->fax}}</span></td>
                </tr>
                <tr>
                  <th>الاهداف العامة للمنظمة :</th>
                  <td>{{$need->goals}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">المسؤوليات والمهام  Responsibilities And Tasks</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
            <form class="form-horizontal" method="post"  action="{{url('need/'.$url.'/'.$need->id.'/'.$user_id)}}"  enctype="multipart/form-data">
              {{csrf_field()}}
                @foreach($responsibility as $key=>$resp)
                <div class="bs-example">
                    <div id="accordion{{$key+1}}" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">{{$key+1}}- {{$resp->name}} </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                    @foreach($resp->tasks as $ke=>$task)
                                    <div class="bs-example">
                                    <div id="accordion{{$key+1}}-{{$ke+1}}" class="panel-group">
                                      <div class="panel panel-default">
                                          <div class="panel-heading">
                                              <h4 class="panel-title">
                                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}" href="#collapse{{$key+1}}-{{$ke+1}}">{{$ke+1}}- {{$resp->name}} / {{$task->task}}</a>
                                              </h4>
                                          </div>
                                          <div id="collapse{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse in">
                                            <div class="panel-body table-responsive">
                                              <ul style="border: #00c0ef 2px solid;">
                                                <table  class="table table-bordered table-striped" data-page-length='100'>
                                                  <thead>
                                                  <tr>
                                                    <th> # </th>
                                                    <th>السؤال</th>
                                                    @if(!isset($need_skill))
                                                    <th>الاجابة</th>
                                                    @endif
                                                    <th>الدرجة</th>
                                                  </tr>
                                                  </thead>
                                                  <tbody>
                                                  @foreach($task->questions as $key=>$value)
                                                  <tr>
                                                    <td>
                                                      {{$key+1}}
                                                    </td>
                                                    <td>{{$value->question}}</td>
                                                  @if(!isset($need_skill))
                                                   <td class="text-center">
                                                      <input type="text"  class="form-control text-center" disabled="" 
                                                      value="@if(isset($value->answer))@if($value->type_answer==1){{$value->answer->answer}}@elseif($value->type_answer==2||$value->type_answer==3)@foreach($value->answers as $val)@if($value->answer->answer == $val->id){{$val->answer}} @endif @endforeach @else @if($value->answer->answer==1) منعدمة @elseif($value->answer->answer==2) ضعيفة @elseif($value->answer->answer==3) متوسطة @elseif($value->answer->answer==4) كبيرة @elseif($value->answer->answer==5) كبيرة جدا @else {{$value->answer->answer}}@endif @endif @endif">
                                                   
                                                   </td>
                                                   @endif
                                                   <td>
                                                    @if(isset($need_knowledge))
                                                    <select class="form-control select2"  data-placeholder="الدرجة" style="width: 100%;" name="degree{{$value->id}}">
                                                        <option value="0" disabled="" selected="">الدرجة</option>
                                                        <option value="11" @if(isset($value->answer) && ($value->answer->degree==11)) selected @endif >اجتاز Passed</option>
                                                        <option value="10" @if(isset($value->answer) && ($value->answer->degree==10)) selected @endif>لم يجتاز Failed</option>
                                                      </select>
                                                    @elseif(isset($need_skill))
                                                    <select class="form-control select2"  data-placeholder="الدرجة" style="width: 100%;" name="degree{{$value->id}}">
                                                        <option value="0" disabled="" selected="">الدرجة</option>
                                                        @for($i=1;$i<=10;$i++)
                                                        <option value="{{$i}}" @if(isset($value->answer) && ($value->answer->degree==$i)) selected @endif >{{$i}}</option>
                                                        @endfor
                                                      </select>
                                                    @else
                                                     <select class="form-control select2"  data-placeholder="الدرجة" style="width: 100%;" name="degree{{$value->id}}">
                                                        <option value="0" disabled="" selected="">الدرجة</option>
                                                        <option value="5" @if(isset($value->answer) && ($value->answer->degree==5)) selected @endif >5</option>
                                                        <option value="4" @if(isset($value->answer) && ($value->answer->degree==4)) selected @endif>4</option>
                                                        <option value="3" @if(isset($value->answer) && ($value->answer->degree==3)) selected @endif>3</option>
                                                        <option value="2" @if(isset($value->answer) && ($value->answer->degree==2)) selected @endif>2</option>
                                                        <option value="1" @if(isset($value->answer) && ($value->answer->degree==1)) selected @endif>1</option>
                                                      </select>
                                                      @endif
                                                   </td>
                                                  </tr>
                                                  @endforeach
                                                  </tbody>
                                                </table> 
                                              </ul>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                    </div>
                                    <hr>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
              <div class="box-footer">
                <button type="submit" class="btn btn-info ">حفظ</button>
              </div>
            </form>
          </ul>
        </div>
        <br>
        <br>
        <br>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection