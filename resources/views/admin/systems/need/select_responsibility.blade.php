@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">اختيار مسؤولية او اكثر  Select One or more Responsibility
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">اختيار مسؤولية </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{url('need/selected_responsibility/'.$need->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center"> المسؤولية Responsibility</label>
                  <div class="col-sm-10">
                    @php($arr=explode(',',$need->resp_selected))
                    <select class="form-control select2" multiple=""  data-placeholder="اختر  المسؤولية" style="width: 100%;" name="resp[]">
                      @foreach($responsibility as $resp)
                      <option value="{{$resp->id}}" @foreach($arr as $val) @if(isset($need)&&$val==$resp->id)selected @endif @endforeach >{{$resp->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection