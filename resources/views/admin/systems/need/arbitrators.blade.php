@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($word)){{$word}}@else المحكمين @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">@if(isset($word)){{$word}}@else المحكمين @endif</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>الاسم Name</th>
                  <th>الأيميل Email</th>
                  <th>رقم الهاتف Phone</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($arbitrators as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->phone}}</td>
                    <td >
                      
                      @if(isset($url))
                      <a href="{{url('need/'.$url.'/'.$need->id.'/'.$value->id)}}" class="btn btn-primary">@if(isset($need_skill)) تقييم الاداء المهاري @else تصحيح الاجابة @endif<i class="fa  fa-check"></i></a>
                      @endif
                      @if(isset($url_report) )
                      <a href="{{url('need/'.$url_report.'/'.$need->id.'/'.$value->id)}}" class="btn bg-navy">التقرير <i class="fa  fa-file"></i></a>
                      @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection