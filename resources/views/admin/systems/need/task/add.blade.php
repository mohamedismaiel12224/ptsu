@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($task))
        تعديل مهمة ل {{$resp->name}}
        @else
        إضافة مهمة ل {{$resp->name}}
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($task))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($task)) action="{{url('task_need/'.$task->id)}}" @else   action="{{url('task_need')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">اسم المهمة Task Name</label>
                  <div class="col-sm-10">
                    <input type="text" name="task" class="form-control" placeholder="" value="@if(isset($task)){{$task->task}}@else{{old('task')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">معدل الأداء الأسبوعي Weekly Performance Mean</label>
                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر معدل الأداء الأسبوعي" style="width: 100%;" name="performance">
                      <option value="0" disabled="" selected="">معدل الأداء الأسبوعي Weekly Performance Mean</option>
                      <option value="4" @if(isset($task)&&$task->performance==4) selected @endif>دائم ومنتظم Regular</option>
                      <option value="3" @if(isset($task)&&$task->performance==3) selected @endif>متوسط Medium</option>
                      <option value="2" @if(isset($task)&&$task->performance==2) selected @endif>احيانا Sometimes</option>
                      <option value="1" @if(isset($task)&&$task->performance==1) selected @endif>نادرا Rarely</option>
                    
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الأهمية Importance</label>
                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر الأهمية" style="width: 100%;" name="importance">
                      <option value="0" disabled="" selected="">الأهمية</option>
                      <option value="3" @if(isset($task)&&$task->importance==3) selected @endif>عالي High</option>
                      <option value="2" @if(isset($task)&&$task->importance==2) selected @endif>متوسط Medium</option>
                      <option value="1" @if(isset($task)&&$task->importance==1) selected @endif>متدني Low</option>
                    
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">صعوبة الأداء Performance Difficult</label>
                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر صعوبة الأداء" style="width: 100%;" name="difficult">
                      <option value="0" disabled="" selected="">صعوبة الأداء</option>
                      <option value="3" @if(isset($task)&&$task->difficult==3) selected @endif>عالي High</option>
                      <option value="2" @if(isset($task)&&$task->difficult==2) selected @endif>متوسط Medium</option>
                      <option value="1" @if(isset($task)&&$task->difficult==1) selected @endif>متدني Low</option>
                    
                    </select>
                  </div>
                </div>
              </div>
              @if(isset($task))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="pro_id" value="{{$need->id}}"> 
                <input type="hidden" name="resp_id" value="{{$resp->id}}"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection