@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        مهمات المسؤولية {{$resp->name}}
      </h1>
      <a href="{{url('task_need/create/'.$resp->id.'/'.$need->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">مهمات المسؤولية {{$resp->name}}</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>اسم المهمة Task Name</th>
                  <th>معدل الأداء الأسبوعي  Weekly Performance Mean</th>
                  <th>الأهمية Importance</th>
                  <th>صعوبة الأداء Performance Difficult</th>
                  <th>مجموع النقاط Sum</th>
                  <th>العمليات المتاحة Operations</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->task}}</td>
                    <td>@if($value->performance==4) دائم ومنتظم Regular @elseif($value->performance==3) متوسط Medium @elseif($value->performance==2) احيانا Sometimes @elseif($value->performance==1) نادرا Rarely @endif</td>
                    <td>@if($value->importance==3) عالي High @elseif($value->importance==2) متوسط Medium @elseif($value->importance==1) متدني Low @endif</td>
                    <td>@if($value->difficult==3) عالي High  @elseif($value->difficult==2) متوسط Medium @elseif($value->difficult==1) متدني Low @endif</td>
                    <td>{{$value->performance+$value->importance+$value->difficult}}</td>

                   <td style="display: flex">
                      <a href="{{url('task_need/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('task_need/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection