@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <h1>
        الأنظمة المهنية
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">الأنظمة المهنية</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif -->
    <!-- Main content -->
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>

    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
           <img src="{{asset('assets/images/home/arbitration_logo.jpg')}}" width="200px">
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h2>نظام مايت للحقائب التدريبية</h2>
           <h2>نموذج جمع بيانات أولية</h2>
          </div>
        </div>
        <hr>
        <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">العنوان : </th>
                  <td>{{$mit->address}}</td>
                </tr>
                <tr>
                  <th>الفئة المستهدفة :</th>
                  <td>{{$mit->benefit_establishment}}</td>
                </tr>
                <tr>
                  <th>عدد الساعات :</th>
                  <td><span class="badge bg-light-blue">{{$mit->hours}}</span></td>
                </tr>
                <tr>
                  <th>تاريخ الأعداد :</th>
                  <td><span class="badge bg-light-blue">{{date('Y-m-d',strtotime($mit->date))}}</span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="rtl" style="border: #000 3px solid;">
          <h2 class="text-center">دواعي الاحتياج :</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th>المجال</th>
                  <th>الاحتياج</th>
                </tr>
                @foreach($categories as $category)
                <tr>
                  <td>{{$category->category}}</td>
                  <td>
                    @foreach($category->needs as $need)
                    <ol>{{$need->need}}</ol>
                    @endforeach
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div> 
        <div class="rtl" style="border: #000 3px solid;">
          <h2 class="text-center">أهداف الحقيبة التدريبية ونواتج التدريب  :</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th>#</th>
                  <th>ناتج التدريب</th>
                  <th>مستوى التعلم</th>
                  <th>التقدير</th>
                  <th>ملاحظات</th>
                </tr>
                @foreach($goals as $key=>$value)
                <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$value->training_output}}</td>
                  <td>@if(is_numeric($value->learning_level)){{round($value->learning_level,2)}} @else {{degree($value->learning_level)}}@endif</td>
                  <td>
                    {{degree($value->learning_level)}}
                  </td>
                  <td>{{$value->details}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        @if(Auth::user()->role==1||Auth::user()->role==3)
        <div class="box-footer no-print">
          <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
              <i class="fa fa-print"></i>
          </a>
        </div>
        @endif
        <br>
        <br>
        <br>
      </div>
    </section>
  </div>
    @endsection