@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($goal))
            تعديل ناتج تدريب
        @else
            إضافة ناتج تدريب
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($goal))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
              @if(\session('error'))
              <div class="alert alert-danger">
                  {{\session('error')}}
              </div>
            @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post"   @if(isset($goal)) action="{{url('arb_goal/'.$goal->id)}}" @else   action="{{url('arb_goal')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">نواتج التدريب Training Outcome</label>

                  <div class="col-sm-10">
                    <input type="text" name="training_output" class="form-control" placeholder="" value="@if(isset($goal)){{$goal->training_output}}@else{{old('training_output')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">مستوى التعلم Learning level</label>
                  <div class="col-sm-10">
                    <div class="radio">
                      <label style="padding-left: 150px;font-size: 25px">
                        <input type="radio" name="status" id="value_method" value="0" checked="">قيمة Value
                      </label>    
                      <label style="padding-left: 150px;font-size: 25px">
                        <input type="radio" name="status" id="select_method" value="1">تحديد Select
                      </label>
                    </div>
                  </div>
                        </div>
                        <hr>
                        <div id="value_div">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label center">مستوى التعلم Learning Level</label>

                    <div class="col-sm-10">
                      <input type="text" name="value" class="form-control">
                    </div>
                  </div>
                </div>
                <div id="select_div"  class="none">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label center">مستوى التعلم Learning Level</label>

                    <div class="col-sm-10">
                      <select name="select" class="form-control">
                        <option value="0" selected="" disabled="">اختر مستوى التعلم</option>
                        <option value="s">متفوق</option>
                        <option value="m">متوسط</option>
                        <option value="d">صعوبات</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الملحوظات Notes</label>

                  <div class="col-sm-10">
                    <input type="text" name="details" class="form-control" placeholder="" value="@if(isset($goal)){{$goal->details}}@else{{old('details')}}@endif">
                  </div>
                </div>
              </div>
              @if(isset($goal))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="pro_id" value="{{$mit->id}}">
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection