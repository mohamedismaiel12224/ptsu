@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        مشاريعى
      </h1>
      <a href="{{url('mit_arbitration/create')}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">مشاريعى</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>العنوان</th>
                  <th>عدد الساعات</th>
                  <th>الفئة المستهدفة</th>
                  <th>تاريخ الأعداد</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->address}}</td>
                    <td>{{$value->hours}}</td>
                    <td>{{$value->benefit_establishment}}</td>
                    <td>{{date('Y-m-d',strtotime($value->date))}}</td>
                    <td style="display: flex">
                      <a href="{{url('mit_arbitration/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('mit_arbitration/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>
                      @if($value->step_no==3)
                      <a href="{{url('arb_category/'.$value->id)}}" class="btn btn-primary" style="margin-right: 10px;">فتح المشروع <i class="fa fa-television"></i></a>
                      <a href="{{url('arbitrators/mit/'.$value->id)}}" class="btn bg-navy" style="margin-right: 10px;">التقرير <i class="fa fa-file"></i></a>

                      @else
                      <a href="{{url('mit_arbitration/'.$value->id)}}" class="btn btn-primary" style="margin-right: 10px;">فتح المشروع <i class="fa fa-television"></i></a>
                      @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection