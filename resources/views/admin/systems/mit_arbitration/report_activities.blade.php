@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        تحكيم الأنشطة التدريبية Training Activities Checking Model
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تحكيم الأنشطة التدريبية </li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif

<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
             <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
                <div class="col-md-4 text-center">
                 <img src="{{asset('assets/images/home/arbitration_logo.jpg')}}" width="200px">
                <h4>MIT System</h4>
                </div>
                <div class="col-md-4  text-center">
                 <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
                </div>
                <div class="col-md-4 text-center">
                 <h2>نظام مايت للحقائب التدريبية</h2>
                 <h2>{{$mit->address}}</h2>
                </div>
              </div>
              <hr>
            @if(isset($activities))
            <div class="box-body table-responsive">
              <table  class="table table-bordered ">
                <thead>
                <tr>
                  <th> # </th>
                  <th>ترميز النشاط Activity Code</th>
                  <th>نواتج التدريب Training Outcome</th>
                  @foreach($domains as $domain)
                  <th>{{$domain->name}}</th>
                  @endforeach
                  <th>الزمن الأيجابي Positive</th>
                  <th>الزمن المتكرر Frequent</th>
                  <th>الزمن السلبي Negative</th>
                  <th>الزمن المثالي Perfect</th>
                  <th>خ المعياري Standard E</th>
                  <th>زمن المصمم Designer Time</th>
                  <th>التقدير Estimation</th>
                  <th>المطابقة Matching</th>
                  <th>الملحوظات Notes</th>
                </tr>
                </thead>
                <tbody>
                @foreach($activities as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->mark}}</td>
                    <td>{{print_goal($value->training_id)}}</td>
                    @foreach($domains as $domain)
                    <td>{{get_check($domain->id,$value->domain_check_arr)}}</td>
                    @endforeach
                    <td>{{$value->positive_time}}</td>
                    <td>{{$value->repeat_time}}</td>
                    <td>{{$value->negative_time}}</td>
                    <td>{{round(($value->positive_time+$value->negative_time+4*($value->repeat_time))/6,2)}}</td>
                    <td>{{round(($value->negative_time-$value->positive_time)/6,2)}}</td>
                    <td>{{$value->designer_time}}</td>
                    <td>{{evaluate_time($value->positive_time,$value->negative_time,$value->repeat_time,$value->designer_time)}}</td>
                    <td>@if($value->superposing==1)نعم@elseif($value->superposing==2)لا@endif</td>
                    <td>{{$value->details}}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            @endif
            <hr>
          @if(isset($chart_training))
          <h2 class="text-center">مجالات الأنشطة التدريبية</h2>
            @foreach($chart_training as $key=> $training)
            <div class="box-body">
              <div style="margin: 20px;">
                <h3>{{$training['domain']}} - {{$key+1}}</h3>
                <ol>
                  @foreach($training['checkup'] as $val)
                  <li><h4>({{$val['count']}}) {{$val['name']}}</h4></li>
                  @endforeach
                </ol>
              </div>
              <div id="donut-chart-{{$key+1}}" style="height: 300px;"></div>
            </div>
            @endforeach
            @endif
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="box-footer no-print">
          <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
              <i class="fa fa-print"></i>
          </a>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection