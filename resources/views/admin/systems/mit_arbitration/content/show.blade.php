@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        تحكيم المحتوى التدريبي
      </h1>
      <a href="{{url('arbitrator/mit/content/create/'.$mit->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تحكيم المحتوى التدريبي</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>العنوان Address</th>
                  <th>نواتج التدريب Training Outcome</th>
                  <th>الجلسة Session</th>
                  <th>وقت العرض ST</th>
                  @foreach($domains as $key=>$domain)
                  @if($key==1)
                  <th>الخبرة Experience</th>
                  @endif
                  <th>{{$domain->name}}</th>
                  @endforeach
                  <th>الملحوظات Notes</th>
                  <th>العمليات المتاحة Available Operations</th>
                </tr>
                </thead>
                <tbody>
                @foreach($content as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->address}}</td>
                    <td>{{print_goal($value->training_id)}}</td>
                    <td>{{$value->session}}</td>
                    <td>{{$value->time}}</td>
                    @foreach($domains as $key=>$domain)
                    @if($key==1)
                    <td>{{experience($value->training_id,Auth::id())}}</td>
                    @endif
                    <td>{{get_check($domain->id,$value->domain_check_arr)}}</td>
                    @endforeach
                    <td>{{$value->details}}</td>
                   <td style="display: flex">
                      <a href="{{url('arbitrator/mit/content/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('arbitrator/mit/content/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection