@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($content))
            تعديل تحكيم لمحتوى تدريبي
        @else
            إضافة تحكيم لمحتوى تدريبي
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($content))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
              @if(\session('error'))
              <div class="alert alert-danger">
                  {{\session('error')}}
              </div>
            @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post"   @if(isset($content)) action="{{url('arbitrator/mit/content/'.$content->id)}}" @else   action="{{url('arbitrator/mit/content')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">العنوان Address</label>
                  <div class="col-sm-10">
                    <input type="text" name="address" class="form-control" placeholder="" value="@if(isset($content)){{$content->address}}@else{{old('address')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">ناتج التدريب Training Outcome</label>

                  <div class="col-sm-10">
                    <select class="form-control select2 training_output_content"  data-placeholder="اختر ناتج التدريب" style="width: 100%;" name="training_id">
                        <option value="-1" disabled="" selected="">اختر ناتج التدريب</option>
                        <option value="0" @if(isset($content)&&$content->training_id == 0) selected="" @endif >0</option>
                        @foreach($goals as $value)
                        <option value="{{$value->id}}" @if(isset($content)&&$content->training_id == $value->id) selected="" @endif >{{$value->training_output}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الجلسة Session</label>

                  <div class="col-sm-10">
                    <input type="text" name="session" class="form-control" placeholder="" value="@if(isset($content)){{$content->session}}@else{{old('session')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">وقت العرض ST</label>

                  <div class="col-sm-10">
                    <input type="text" name="time" class="form-control" placeholder="" value="@if(isset($content)){{$content->time}}@else{{old('time')}}@endif">
                  </div>
                </div>
              @foreach($domains as $key=> $domain)
                @if($key==1)
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الخبرة Experience</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control experience_input" disabled="" placeholder="" value="@if(isset($content)){{experience($content->training_id,Auth::id())}}@endif">
                  </div>
                </div>
                @endif
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">{{$domain->name}}</label>

                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر {{$domain->name}}" style="width: 100%;" name="domain{{$domain->id}}">
                        <option value="-1" disabled="" selected="">اختر {{$domain->name}}</option>
                        @foreach($domain->checkup as $checkup)
                        <option value="{{$checkup->id}}" @if(isset($content)&&$domain->selected == $checkup->id) selected="" @endif >{{$checkup->name}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
              @endforeach
              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الملحوظات Notes</label>

                  <div class="col-sm-10">
                    <input type="text" name="details" class="form-control" placeholder="" value="@if(isset($content)){{$content->details}}@else{{old('details')}}@endif">
                  </div>
                </div>
              </div>
              @if(isset($content))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="pro_id" value="{{$mit->id}}">
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection