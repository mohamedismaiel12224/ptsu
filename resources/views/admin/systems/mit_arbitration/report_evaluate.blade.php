@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        تحكيم التقييم العام
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تحكيم التقييم العام</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
   bs-example{
      margin: 20px;
    }
   panel-titleglyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <!-- Main content -->
    @php($all_avg=0)
    @php($all_count=0)
    

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
             <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
                <div class="col-md-4 text-center">
                 <img src="{{asset('assets/images/home/arbitration_logo.jpg')}}" width="200px">
                <h4>MIT System</h4>
                </div>
                <div class="col-md-4  text-center">
                 <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
                </div>
                <div class="col-md-4 text-center">
                 <h2>نظام مايت للحقائب التدريبية</h2>
                 <h2>{{$mit->address}}</h2>
                </div>
              </div>
            <hr>
            @if(isset($domains))
            @foreach($domains as $key_domain=>$domain)
            <div class="bs-example">
              <div id="accordion{{$key_domain+1}}" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion{{$key_domain+1}}" href="#collapse{{$key_domain+1}}">{{$key_domain+1}} - {{$domain->name}}</a>
                        </h4>
                    </div>
                    <div id="collapse{{$key_domain+1}}" class="panel-collapse collapse in">
                        <div class="panel-body table-responsive">
                          <ul style="border: #00c0ef 2px solid;">
                            <table  class="table table-bordered table-striped" data-page-length='100'>
                              <thead>
                              <tr>
                                <th> # </th>
                                <th>المعيار Standard</th>
                                @if(isset($is_user))
                                <th>التحكيم Arbitration</th>
                                @else
                                <th>ممتاز Excellent</th>
                                <th>جيد جدا Very Good</th>
                                <th>حيد Good</th>
                                <th>ضعيف Week</th>
                                <th>متروك Careless</th>
                                @endif
                              </tr>
                              </thead>
                              <tbody>
                              @foreach($domain->checkup as $key=>$value)
                              <tr>
                                <td>
                                  {{$key+1}}
                                </td>
                                <td>{{$value->name}}</td>
                                @if(isset($is_user))
                                <td>
                                  @if($value->count_5==1)
                                  ممتاز Excellent
                                  @elseif($value->count_4==1)
                                  جيد جدا Very Good
                                  @elseif($value->count_3==1)
                                  جيد Good
                                  @elseif($value->count_2==1)
                                  ضعيف Week
                                  @elseif($value->count_1==1)
                                  متروك Careless
                                  @endif
                                </td>
                                @else
                               <td>
                                  {{$value->count_5}}
                               </td>
                               <td>
                                  {{$value->count_4}}
                               </td>
                               <td>
                                  {{$value->count_3}}
                               </td>
                               <td>
                                  {{$value->count_2}}
                               </td>
                               <td>
                                  {{$value->count_1}}
                               </td>
                               @endif
                              </tr>
                              @endforeach
                              </tbody>
                            </table> 
                          </ul>
                        </div>
                    </div>
                    @if(isset($chart_training)&& $domain->count!=0)
                      <div class="box-body">
                        <div style="margin: 20px;">
                          @php($all_avg+=$domain->avg)
                          @php($all_count+=$domain->count)
                          <h1 class="text-center">المتوسط الحسابي = {{round($domain->avg/$domain->count,2)}}</h1>
                          <h3 class="text-center">النسبة المئوية للمجال = % {{round(20*$domain->avg/$domain->count,2)}}</h3>
                          @if(($domain->avg/$domain->count)>=4.2)
                          <h1 class="text-center">التقدير : ممتاز</h1>
                          <h4 class="text-center">الوصف : مواد تدريبية مثالية تسهم في تحقيق أهداف المستويات الثمانية</h4>
                          @elseif(($domain->avg/$domain->count)>=3.4 && ($domain->avg/$domain->count)<4.2)
                          <h1 class="text-center">التقدير : جيد جدا</h1>
                          <h4 class="text-center">الوصف : مواد تدريبية ممتازة يمكن تطبيقها دون مراجعة أخرى</h4>
                          @elseif(($domain->avg/$domain->count)>=2.6 && ($domain->avg/$domain->count)<3.4)
                          <h1 class="text-center">التقدير : جيد</h1>
                          <h4 class="text-center">الوصف :  مواد تدريبية صالحة لتحقيق أهداف المستوى الأول والثاني فقط وبحاجة لمراجعة بعض المعايير</h4>
                          @elseif(($domain->avg/$domain->count)>=1.8 && ($domain->avg/$domain->count)<3.4)
                          <h1 class="text-center">التقدير : ضعيف</h1>
                          <h4 class="text-center">الوصف : مواد تدريبية بحاجة لمراجعة المعايير لا تتطابق مع معايير التحكيم</h4>
                          @elseif(($domain->avg/$domain->count)<1.8)
                          <h1 class="text-center">التقدير : متروك</h1>
                          <h4 class="text-center">الوصف :  مواد تدريبية غير صالحة للتدريب</h4>
                          @endif

                          <ol>
                            @foreach($chart_training[$key_domain]['checkup'] as $val)
                            <li><h4>({{$val['count']}}) {{$val['name']}}</h4></li>
                            @endforeach
                          </ol>
                        </div>
                        <div id="donut-chart-{{$key_domain+1}}" style="height: 300px;"></div>
                      </div>
                      @endif
                </div>
              </div>
            </div>
            <hr>
            @endforeach

            @endif
            @if($all_count!=0)
            <div class="box-body">
              <div style="margin: 20px;border: #00c0ef 3px solid; margin: 0">
                @if(isset($note))
                <h3 class="text-center">ملاحظات المحكم = {{$note->notes}}</h3>

                @endif
                <h2 class="text-center">التقدير العام</h2>
                <hr>
                <h1 class="text-center">المتوسط الحسابي = {{round($all_avg/$all_count,2)}}</h1>
                <h3 class="text-center">النسبة المئوية لكل المجالات = % {{round(20*$all_avg/$all_count,2)}}</h3>
                @if($all_avg/$all_count>=4.2)
                <h1 class="text-center">التقدير : ممتاز</h1>
                <h4 class="text-center">الوصف : مواد تدريبية مثالية تسهم في تحقيق أهداف المستويات الثمانية</h4>
                @elseif($all_avg/$all_count>=3.4 && $all_avg/$all_count<4.2)
                <h1 class="text-center">التقدير : جيد جدا</h1>
                <h4 class="text-center">الوصف : مواد تدريبية ممتازة يمكن تطبيقها دون مراجعة أخرى</h4>
                @elseif($all_avg/$all_count>=2.6 && $all_avg/$all_count <3.4)
                <h1 class="text-center">التقدير : جيد</h1>
                <h4 class="text-center">الوصف :  مواد تدريبية صالحة لتحقيق أهداف المستوى الأول والثاني فقط وبحاجة لمراجعة بعض المعايير</h4>
                @elseif($all_avg/$all_count>=1.8 && $all_avg/$all_count<3.4)
                <h1 class="text-center">التقدير : ضعيف</h1>
                <h4 class="text-center">الوصف : مواد تدريبية بحاجة لمراجعة المعايير لا تتطابق مع معايير التحكيم</h4>
                @elseif($all_avg/$all_count<1.8)
                <h1 class="text-center">التقدير : متروك</h1>
                <h4 class="text-center">الوصف :  مواد تدريبية غير صالحة للتدريب</h4>
                @endif

              </div>
            </div>
            @endif
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="box-footer no-print">
          <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
              <i class="fa fa-print"></i>
          </a>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection