@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        التقييم العام
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">التقييم العام</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <style type="text/css">
        .bs-example{
          margin: 20px;
        }
        .panel-title .glyphicon{
            font-size: 14px;
        }
    </style>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <form class="form-horizontal" method="post"  action="{{url('evaluate_arbitration/'.$mit->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}

                @foreach($domains as $key=>$domain)
                  <div class="bs-example">
                    <div id="accordion{{$key+1}}" class="panel-group">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">{{$key+1}} - {{$domain->name}}</a>
                              </h4>
                          </div>
                          <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                              <div class="panel-body table-responsive">
                                <ul style="border: #00c0ef 2px solid;">
                                  <table  class="table table-bordered table-striped" data-page-length='100'>
                                    <thead>
                                    <tr>
                                      <th> # </th>
                                      <th>المعيار</th>
                                      <th>التحكيم</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($domain->checkup as $key=>$value)
                                    <tr>
                                      <td>
                                        {{$key+1}}
                                      </td>
                                      <td>{{$value->name}}</td>
                                     <td>
                                        <select class="form-control select2"  data-placeholder="اختر اداة" style="width: 100%;" name="check{{$domain->id}}-{{$value->id}}">
                                          <option value="0" disabled="" selected="">التحكيم</option>
                                          <option value="5" @if($value->selected==5) selected @endif >ممتاز</option>
                                          <option value="4" @if($value->selected==4) selected @endif>جيد جدا</option>
                                          <option value="3" @if($value->selected==3) selected @endif>جيد</option>
                                          <option value="2" @if($value->selected==2) selected @endif>ضعيف</option>
                                          <option value="1" @if($value->selected==1) selected @endif>متروك</option>
                                        
                                        </select>
                                     </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                  </table> 
                                </ul>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                  @endforeach
                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label center">الملحوظات Notes</label>

                  <div class="col-sm-10">
                    <input type="text" name="details" class="form-control" placeholder="" value="@if(isset($note)){{$note->notes}}@else{{old('details')}}@endif">
                  </div>
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-info ">حفظ</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection