@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        تحكيم نواتج التدريب
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">نموذج TEM</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <form class="form-horizontal" method="post"  action="{{url('training_arbitration/'.$mit->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}

                <table id="example1" class="table table-bordered table-striped" data-page-length='100'>
                  <thead>
                  <tr>
                    <th> # </th>
                    <th>نواتج التدريب</th>
                    @foreach($domains as $domain)
                    <th>{{$domain->name}}</th>
                    @endforeach
                    <th>الممارسة</th>

                  </tr>
                  </thead>
                  <tbody>
                  @foreach($goals as $key=>$value)
                  <tr>
                    <td>
                      {{$key+1}}
                    </td>
                    <td>{{$value->training_output}}</td>
                    @foreach($value->domains as $domain)
                    <td>
                      <select class="form-control select2"  data-placeholder="اختر {{$domain->name}}" style="width: 100%;" name="check{{$domain->id}}-{{$value->id}}">
                        <option value="0" disabled="" selected="">اختر {{$domain->name}}</option>
                        @foreach($domain->checkup as $checkup)
                        <option value="{{$checkup->id}}" @if($domain->selected == $checkup->id) selected="" @endif >{{$checkup->name}}</option>
                        @endforeach
                      </select>
                    </td>
                    @endforeach
                    <td>@if(isset($value->domains[0])){{depth($value->learning_level ,  $value->domains[0]->selected )}} @else {{depth($value->learning_level , 0 )}} @endif</td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <div class="box-footer">
                  <button type="submit" class="btn btn-info ">حفظ</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection