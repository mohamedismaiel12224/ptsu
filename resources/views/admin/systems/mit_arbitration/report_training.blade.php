@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        تحكيم نواتج التدريب Training Outcome Checking Model
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تحكيم نواتج التدريب Training Outcome Checking Model</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
                <div class="col-md-4 text-center">
                 <img src="{{asset('assets/images/home/arbitration_logo.jpg')}}" width="200px">
                <h4>MIT System</h4>
                </div>
                <div class="col-md-4  text-center">
                 <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
                </div>
                <div class="col-md-4 text-center">
                 <h2>نظام مايت للحقائب التدريبية</h2>
                 <h2>{{$mit->address}}</h2>
                </div>
              </div>
            <hr>
            @if(isset($goals))
            @foreach($goals as $key=>$value)
            @php($no_activity = 0)
            <div class="bs-example">
              <div id="accordion{{$key+1}}" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title text-center">
                            <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">ناتج التدريب {{$value->training_output}}</a>
                        </h4>
                    </div>
                    <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="table-responsive" style="border: #f50 3px solid;">
                              <table class="table mytable table-striped">
                                <tbody>
                                  @foreach($value->domains as $domain)
                                  <tr>
                                    <th >{{$domain->name}} </th>
                                    <td>@foreach($domain->checkup as $checkup)
                                       @if($domain->selected == $checkup->id) 
                                          {{$checkup->name}}
                                       @endif 
                                      @endforeach</td>

                                  </tr>
                                  @endforeach
                                  <tr>
                                    <th>الممارسة Practice</th>
                                    <td>@if(isset($value->domains[0])){{depth($value->learning_level ,  $value->domains[0]->selected )}} @else {{depth($value->learning_level , 0 )}} @endif</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <hr>
            @endforeach

            @endif
            <hr>
            @if(isset($chart_training))
          <h2 class="text-center">مجالات نواتج التدريب Training Outcome Fields</h2>

            @foreach($chart_training as $key=> $training)
            <div class="box-body">
              <div style="margin: 20px;">
                <h3>{{$training['domain']}} - {{$key+1}}</h3>
                <ol>
                  @foreach($training['checkup'] as $val)
                  <li><h4>({{$val['count']}}) {{$val['name']}}</h4></li>
                  @endforeach
                </ol>
              </div>
              <div id="donut-chart-{{$key+1}}" style="height: 300px;"></div>
            </div>
            @endforeach
            @endif
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="box-footer no-print">
          <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
              <i class="fa fa-print"></i>
          </a>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection