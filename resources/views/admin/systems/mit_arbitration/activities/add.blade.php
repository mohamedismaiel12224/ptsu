@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- activities Wrapper. Contains page activities -->
  <div class="content-wrapper">
  <!-- activities Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($activities))
            تعديل تحكيم لنشاط تدريبي
        @else
            إضافة تحكيم لنشاط تدريبي
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        @if(isset($activities))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main activities -->
    <section class="activities">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
              @if(\session('error'))
              <div class="alert alert-danger">
                  {{\session('error')}}
              </div>
            @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post"   @if(isset($activities)) action="{{url('arbitrator/mit/activities/'.$activities->id)}}" @else   action="{{url('arbitrator/mit/activities')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">ترميز النشاط Activity Code</label>
                  <div class="col-sm-10">
                    <input type="text" name="mark" class="form-control" placeholder="" value="@if(isset($activities)){{$activities->mark}}@else{{old('mark')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">ناتج التدريب  Training Outcome</label>

                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر ناتج التدريب" style="width: 100%;" name="training_id">
                        <option value="-1" disabled="" selected="">اختر ناتج التدريب</option>
                        <option value="0" @if(isset($activities)&&$activities->training_id == 0) selected="" @endif >0</option>
                        @foreach($goals as $value)
                        <option value="{{$value->id}}" @if(isset($activities)&&$activities->training_id == $value->id) selected="" @endif >{{$value->training_output}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
                
              @foreach($domains as $domain)
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">{{$domain->name}}</label>

                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر {{$domain->name}}" style="width: 100%;" name="domain{{$domain->id}}">
                        <option value="-1" disabled="" selected="">اختر {{$domain->name}}</option>
                        @foreach($domain->checkup as $checkup)
                        <option value="{{$checkup->id}}" @if(isset($activities)&&$domain->selected == $checkup->id) selected="" @endif >{{$checkup->name}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
              @endforeach
              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الزمن الأيجابي Positive</label>

                  <div class="col-sm-10">
                    <input type="text" name="positive_time" class="form-control positive_time" placeholder="" value="@if(isset($activities)){{$activities->positive_time}}@else{{old('positive_time')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الزمن السلبي Negative</label>

                  <div class="col-sm-10">
                    <input type="text" name="negative_time" class="form-control negative_time" placeholder="" value="@if(isset($activities)){{$activities->negative_time}}@else{{old('negative_time')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الزمن المتكرر Frequent</label>

                  <div class="col-sm-10">
                    <input type="text" name="repeat_time" class="form-control repeat_time" placeholder="" value="@if(isset($activities)){{$activities->repeat_time}}@else{{old('repeat_time')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الزمن المثالي Perfect</label>

                  <div class="col-sm-10">
                    <input type="text" disabled class="form-control perfect_time" placeholder="" value="@if(isset($activities)){{round(($activities->positive_time+$activities->negative_time+4*($activities->repeat_time))/6,2)}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">خ المعياري Standard E</label>

                  <div class="col-sm-10">
                    <input type="text" disabled class="form-control standard" placeholder="" value="@if(isset($activities)){{round(($activities->negative_time-$activities->positive_time)/6,2)}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">زمن المصمم Designer Time</label>

                  <div class="col-sm-10">
                    <input type="text" name="designer_time" class="form-control designer_time" placeholder="" value="@if(isset($activities)){{$activities->designer_time}}@else{{old('designer_time')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">التقدير Estimation</label>

                  <div class="col-sm-10">
                    <input type="text" disabled class="form-control evaluate_time" placeholder="" value="@if(isset($activities)){{evaluate_time($activities->positive_time,$activities->negative_time,$activities->repeat_time,$activities->designer_time)}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المطابقة Matching</label>

                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر المطابقة" style="width: 100%;" name="superposing">
                        <option value="-1" disabled="" selected="">اختر المطابقة</option>
                        <option value="1" @if(isset($activities)&&$activities->superposing == 1) selected="" @endif >نعم</option>
                        <option value="2" @if(isset($activities)&&$activities->superposing == 2) selected="" @endif >لا</option>
                        
                      </select>
                  </div>
                </div>
              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الملحوظات Notes</label>

                  <div class="col-sm-10">
                    <input type="text" name="details" class="form-control" placeholder="" value="@if(isset($activities)){{$activities->details}}@else{{old('details')}}@endif">
                  </div>
                </div>
              </div>
              @if(isset($activities))
                <input type="hidden" name="_method" value="patch"> 
              @else
                <input type="hidden" name="pro_id" value="{{$mit->id}}">
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.activities -->
  </div>
@endsection