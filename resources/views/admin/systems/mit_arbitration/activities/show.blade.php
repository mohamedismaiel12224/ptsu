@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        تحكيم الأنشطة التدريبية
      </h1>
      <a href="{{url('arbitrator/mit/activities/create/'.$mit->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تحكيم الأنشطة التدريبية</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th> # </th>
                    <th>ترميز النشاط Activity Code</th>
                    <th>نواتج التدريب Training Outcome</th>
                    @foreach($domains as $domain)
                    <th>{{$domain->name}}</th>
                    @endforeach
                    <th>الزمن الأيجابي Positive</th>
                    <th>الزمن المتكرر Frequent</th>
                    <th>الزمن السلبي Negative</th>
                    <th>الزمن المثالي Perfect</th>
                    <th>خ المعياري Standard E</th>
                    <th>زمن المصمم Designer Time</th>
                    <th>التقدير Estimation</th>
                    <th>المطابقة Matching</th>
                    <th>الملحوظات Notes</th>
                    <th>#</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($activities as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->mark}}</td>
                    <td>{{print_goal($value->training_id)}}</td>
                    @foreach($domains as $domain)
                    <td>{{get_check($domain->id,$value->domain_check_arr)}}</td>
                    @endforeach
                    <td>{{$value->positive_time}}</td>
                    <td>{{$value->repeat_time}}</td>
                    <td>{{$value->negative_time}}</td>
                    <td>{{round(($value->positive_time+$value->negative_time+4*($value->repeat_time))/6,2)}}</td>
                    <td>{{round(($value->negative_time-$value->positive_time)/6,2)}}</td>
                    <td>{{$value->designer_time}}</td>
                    <td>{{evaluate_time($value->positive_time,$value->negative_time,$value->repeat_time,$value->designer_time)}}</td>
                    <td>@if($value->superposing==1)نعم@elseif($value->superposing==2)لا@endif</td>
                    <td>{{$value->details}}</td>
                   <td style="display: flex">
                      <a href="{{url('arbitrator/mit/activities/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('arbitrator/mit/activities/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection