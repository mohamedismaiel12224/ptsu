@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        المحكمين
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">المحكمين</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>الاسم</th>
                  <th>الأيميل</th>
                  <th>رقم الهاتف</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($arbitrators as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->phone}}</td>
                    <td >
                      <a href="{{url('mit_arbitration/report_training/'.$mit->id.'/'.$value->id)}}" class="btn btn-primary">تقرير نواتج التدريب <i class="fa  fa-file"></i></a>
                      <a href="{{url('mit_arbitration/report_content/'.$mit->id.'/'.$value->id)}}" class="btn btn-info">تقرير المحتوى التدريبي <i class="fa  fa-file"></i></a>
                      <a href="{{url('mit_arbitration/report_activities/'.$mit->id.'/'.$value->id)}}" class="btn bg-navy">تقرير الأنشطة التدريبية <i class="fa  fa-file"></i></a>
                      <a href="{{url('mit_arbitration/report_evaluate/'.$mit->id.'/'.$value->id)}}" class="btn btn-primary">تقرير التقييم العام <i class="fa  fa-file"></i></a>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection