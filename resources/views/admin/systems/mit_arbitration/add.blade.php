@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        البيانات العامة
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('mit_arbitration')}}">مشاريعى</a></li>
        @if(isset($mit_arbitration))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($mit_arbitration)) action="{{url('mit_arbitration/'.$mit_arbitration->id)}}" @else   action="{{url('mit_arbitration')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">العنوان</label>

                  <div class="col-sm-10">
                    <input type="text" name="address" class="form-control" placeholder="" value="@if(isset($mit_arbitration)){{$mit_arbitration->address}}@else{{old('address')}}@endif">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الفئة المستهدفة</label>

                  <div class="col-sm-10">
                    <input type="text" name="benefit_establishment" class="form-control" placeholder="" value="@if(isset($mit_arbitration)){{$mit_arbitration->benefit_establishment}}@else{{old('benefit_establishment')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">عدد الساعات</label>

                  <div class="col-sm-10">
                    <input type="text" name="hours" class="form-control" placeholder="" value="@if(isset($mit_arbitration)){{$mit_arbitration->hours}}@else{{old('hours')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">تاريخ الإعداد</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control pull-right" id="datepicker" name="date"  value="@if(isset($mit_arbitration)){{date('m/d/Y', strtotime($mit_arbitration->date))}}@else{{old('date')}}@endif">
                  </div>
                </div>
              </div>
              @if(isset($mit_arbitration))
                <input type="hidden" name="_method" value="patch"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection