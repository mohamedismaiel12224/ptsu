@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <h1>
        hhhhhالأنظمة المهنية
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">الأنظمة المهنية</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif -->
    <!-- Main content -->
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
           <h4>professional Training Sestems Union</h4>
           <h4>MIT System</h4>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h4>إتحاد الأنظمة المهنية للتدريب</h4>
           <h4>نظام MIT للحقائب التدريبية</h4>
           <h4>{{$mit->name}}</h4>
          </div>
        </div>
        <hr>
        <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">اسم المشروع : </th>
                  <td>{{$mit->name}}</td>
                </tr>
                <tr>
                  <th>رقم المشروع :</th>
                  <td>{{$mit->nno}}</td>
                </tr>
                <tr>
                  <th>الجهة المستفيدة :</th>
                  <td>{{$mit->benefit_establishment}}</td>
                </tr>
                <tr>
                  <th>عدد الساعات :</th>
                  <td><span class="badge bg-light-blue">{{$mit->hours}}</span></td>
                </tr>
                <tr>
                  <th>عدد الأيام :</th>
                  <td><span class="badge bg-light-blue">{{$mit->days}}</span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">الجلسات التدريبية  Training sessions</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
            @foreach($sessions as $key=>$session)
            @php($no_activity = 0)
            <div class="bs-example">
              <div id="accordion{{$key+1}}" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}">جلسة  session {{$key+1}}</a>
                        </h4>
                    </div>
                    <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="table-responsive" style="border: #f50 3px solid;">
                              <table class="table mytable table-striped">
                                <tbody>
                                  <tr>
                                    <th >العنوان  Title </th>
                                    <td>{{$session->address}}</td>
                                  </tr>
                                  <tr>
                                    <th>مصارد المحتوى  Content Sources  </th>
                                    <td>{{print_attributes($session->source_id)}}</td>
                                  </tr>
                                  <tr>
                                    <th>أدوات التقويم  Evaluation Tools</th>
                                    <td>{{print_tools($session->id)}}</td>
                                  </tr>
                                  <tr>
                                    <th>الزمن المستغرق  Time</th>
                                    <td>{{$session->sum}}</td>
                                  </tr>
                                  <tr>
                                    <th>التغطية  Coverage</th>
                                    
                                    <td style="width: 90%;">
                                        @foreach($session->branches as $ke=>$branch)
                                          <div class="bs-example">
                                            <div id="accordion{{$key+1}}-{{$ke+1}}" class="panel-group">
                                              <div class="panel panel-default">
                                                  <div class="panel-heading">
                                                      <h4 class="panel-title">
                                                          <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}" href="#collapse{{$key+1}}-{{$ke+1}}">الجلسة  session {{$key+1}} / الفرع  subtitle {{$ke+1}}</a>
                                                      </h4>
                                                  </div>
                                                  <div id="collapse{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse">
                                                    <h3 style="text-align: right;margin-right: 50px;">الفرع  subtitle {{$branch->address}}</h3>

                                                    @foreach($branch->training_output as $k=>$training)
                                                    <div class="bs-example">
                                                      <div id="accordion{{$key+1}}-{{$ke+1}}-{{$k+1}}" class="panel-group">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}-{{$k+1}}" href="#collapse{{$key+1}}-{{$ke+1}}-{{$k+1}}">الجلسة  session {{$key+1}} / الفرع  subtitle {{$ke+1}} / الناتج Output {{$k+1}}</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse{{$key+1}}-{{$ke+1}}-{{$k+1}}" class="panel-collapse collapse">
                                                                <div class="panel-body table-responsive">
                                                                  <ul style="border: #00c0ef 2px solid;">
                                                                    <table class="table mytable  table-striped" >
                                                                      <tbody>
                                                                        <tr>
                                                                          <th > # </th>
                                                                          <td>{{$k+1}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >الاسم Name </th>
                                                                          <td>{{$training->name}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >التقويم  Evaluation </th>
                                                                          <td>{{print_attributes($training->evaluate_id)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >الإرتباط  Correlation</th>
                                                                          <td>@if($training->status=='t') -- @elseif($training->status==0) يسبق  After @elseif($training->status==1) يتبع  Before @else  @endif</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >النوع  Type </th>
                                                                          <td>@if($training->type==1) رئيس معرفي @elseif($training->type==2) رئيس مهاري  @elseif($training->type==3) رئيس اتجاهي  @elseif($training->type==4) مساعد معرفي  @elseif($training->type==5) مساعد مهاري  @elseif($training->type==6) مساعد اتجاهي  @endif</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >مستوى التعلم  Learning level @if(is_numeric($training->learning_level))<span class="badge bg-light-blue">{{round($training->learning_level,2)}}%</span> @else <span class="badge bg-light-blue">{{degree($training->learning_level)}}%</span> @endif</th>
                                                                          <td>
                                                                            <div class="progress progress-xs progress-striped active">
                                                                              <div class="progress-bar progress-bar-primary" @if(is_numeric($training->learning_level)) style="width: {{round($training->learning_level,2)}}%" @else style="width: {{degree($training->learning_level)}}%" @endif ></div>
                                                                            </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >التقدير : </th>
                                                                          <td>{{degree($training->learning_level)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >عمق وشمول  Depth and Inclusiveness </th>
                                                                          <td>{{depth($training->learning_level,$training->type)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >التحقيق  Investigation </th>
                                                                          <td>@if($training->investigation==1) داخل @elseif($training->investigation==2) خارج  @elseif($training->investigation==3) داخل وخارج  @endif</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >طرق التدريب  Training Methods </th>
                                                                          <td>{{print_attributes($training->training_method_id)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th > حجم المحتوى Content size </th>
                                                                          <td>@if(volume($training->learning_level,$training->type,1)) إلزامية @endif @if(volume($training->learning_level,$training->type,2)) داعمة @endif</td>
                                                                        </tr>
                                                                         <tr>
                                                                          <th>العمق الفكري Intellectual Depth</th>
                                                                          <td>{{$mit->public_depth}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >التأصيل  Indigenization</th>
                                                                          <td>{{$training->mental_depth}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >الخبرة الإلزامية   Expertise Mandatory</th>
                                                                          <td @if(volume($training->learning_level,$training->type,1)) style="background-color: #5aa3e391;" @endif>{{$training->compulsive}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >الخبرة الداعمة  Expertise Supporting</th>
                                                                          <td  @if(volume($training->learning_level,$training->type,2)) style="background-color: #5aa3e391;" @endif>{{$training->buttress}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >الفواصل  Linking Activities</th>
                                                                          <td>@if($training->separate==1) حركي  Kinesthetic @elseif($training->separate==2) فكري  Intellectual  @elseif($training->separate==3) سمع بصري  Auditory-Visual  @endif</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <th >فترة الإنجاز  period of achievement </th>
                                                                          <td>{{$training->achievement}}</td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                    <h3 > الأنشطة التدريبية  Training Activities </h3>
                                                                    <div>
                                                                      @if(count($training->activities)>0)
                                                                        <div style="border: #fe4 2px solid;overflow: auto" class="table-responsive">
                                                                          <table class="table mytable table-striped">
                                                                            <tbody>
                                                                              <tr>
                                                                                <th>#</th>
                                                                                <th>العنوان  Title</th>
                                                                                <th>الفئة  Category</th>
                                                                                <th>معينات الوصول  Learning Aids</th>
                                                                                <th>آلية التنفيذ  Implementation Mechanism</th>
                                                                                <th>فكرة النشاط The idea of ​​activity</th>
                                                                                <th>الزمن المستغرق  Time</th>
                                                                              </tr>
                                                                              @foreach($training->activities as $key_active=> $active)
                                                                              <tr>
                                                                                  <td>{{++$no_activity}}</td>
                                                                                  <td>{{$active->address}}</td>
                                                                                  <td>{{print_attributes($active->category_id)}}</td>
                                                                                  <td>{{print_attributes($active->aids_id)}}</td>
                                                                                  <td>{{print_attributes($active->imp_id)}}</td>
                                                                                  <td>{{$active->idea}}</td>
                                                                                  <td>@if(is_numeric($active->time)){{round($active->time,2)}} @else {{$active->time}}@endif</td>
                                                                              </tr>
                                                                              @endforeach
                                                                            </tbody>
                                                                          </table>
                                                                      </div>
                                                                      @else
                                                                      --
                                                                      @endif
                                                                    </div>
                                                                  </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <hr>
                                                    @endforeach
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        @endforeach
                                    </td>
                                    
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <hr>
            @endforeach
          </ul>
        </div>
        <h1 class="center">
          مخطط التعلم  Learning planner
        </h1>
        <div class="box-body chart-responsive" style=" border: #00c0ef 3px solid;margin: 10px">
          <div id="bar-chart" style="height: 500px;"></div>
        </div>
        <hr>
        <br>
        <div class="rtl table-responsive">
          <h1 class="center">
            نموذج TEM
          </h1>
          <table  class="table mytable table-striped" style=" border: #00c0ef 3px solid">
                <thead>
                <tr>
                  <th>نواتج التدريب Training Outputs</th>
                  <th >أسلوب التقويم  Evaluation Method</th>
                  <th >وقت التقويم    Evaluation Time</th>
                  <th >الأدوات Tools</th>
                  <th >المسئولية  Responsibility</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tem as $value)
                <tr>
                 
                  <td>{{$value->name}}</td>
                  <td>
                    {{$value->style}}
                  </td>
                  <td>{{print_attributes($value->time)}}</td>
                  <td>{{print_attributes($value->tool)}}</td>
                  <td>{{print_attributes($value->responsibilty)}}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
        </div>
        <div class="box-footer no-print">
          <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
              <i class="fa fa-print"></i>
          </a>
        </div>
        <br>
        <br>
        <br>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection