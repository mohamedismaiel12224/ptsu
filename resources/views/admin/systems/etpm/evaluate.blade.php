@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
       ETPM نموذج التميز في الأداء التدريبي
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">ETPM نموذج التميز في الأداء التدريبي</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Excellence Training Performance Model</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>ETPM نموذج التميز في الأداء التدريبي</h3>
           <h3>{{$ev->name}}</h3>
          </div>
        </div>
        <hr>
        <div class="text-center" style="border: #000 3px solid;">
          <h2>البيانات العامة للمتدرب</h2>
          <div class="table-responsive">
            <table class="table mytable table-striped">
              <tbody>
                <tr>
                  <th style="width:50%">اسم المتدرب : </th>
                  <td>{{$trainer->name}}</td>
                </tr>
                <tr>
                  <th>الايميل :</th>
                  <td>{{$trainer->email}}</td>
                </tr>
                <tr>
                  <th>رقم الجوال :</th>
                  <td>{{$trainer->phone}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">المعايير Standards</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
            <form class="form-horizontal" method="post"   action="{{url('etpm/trainer_evaluate/'.$group->id)}}"  enctype="multipart/form-data">
              {{csrf_field()}}
                @foreach($domain as $key=>$domain)
                <div class="bs-example">
                    <div id="accordion{{$key+1}}" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}"><b style="font-size: 30px">{{$key+1}}- مؤشرات معيار  {{$domain->name}} </b> </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">

                                    @foreach($domain->checkup as $ke=>$checkup)
                                      <div class="bs-example">
                                          <div id="accordion{{$key+1}}-{{$ke+1}}" class="panel-group">
                                              <div class="panel panel-default">
                                                  <div class="panel-heading">
                                                      <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}" href="#collapse{{$key+1}}-{{$ke+1}}">{{$ke+1}}- {{$checkup->name}} </a>
                                                      </h4>
                                                  </div>
                                                  <div id="collapse{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse in">
                                                      <div class="panel-body table-responsive">
                                                          <ul style="border: #00c0ef 2px solid;">
                                                            <div class="bs-example">
                                                                <div id="accordion{{$key+1}}-{{$ke+1}}-1" class="panel-group">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                          <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}-1" href="#collapse{{$key+1}}-{{$ke+1}}-1">الإجراءات Procedures </a>
                                                                          </h4>
                                                                        </div>
                                                                        <div id="collapse{{$key+1}}-{{$ke+1}}-1" class="panel-collapse collapse in">
                                                                          <div class="panel-body table-responsive">
                                                                            <ul style="border: #00c0ef 2px solid;">
                                                                              <table  class="table table-bordered table-striped" data-page-length='100'>
                                                                                <thead>
                                                                                  <tr>
                                                                                    <th> # </th>
                                                                                    <th>الإجراءات Procedures</th>
                                                                                    <th>التقييم Evaluate</th>
                                                                                  </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                  @foreach($checkup->procedure as $k_procedure=>$proc)
                                                                                  <tr>
                                                                                    <td>
                                                                                      {{$k_procedure+1}}
                                                                                    </td>
                                                                                    <td>{{$proc->procedure}}</td>
                                                                                   <td class="text-center">
                                                                                    <select class="form-control select2"  data-placeholder="التقييم" style="width: 100%;" name="evaluate{{$proc->id}}">
                                                                                            <option value="0" disabled="" selected="">التقييم</option>
                                                                                            <option value="5" @if(isset($proc->evaluate) && ($proc->evaluate==5)) selected @endif >عال جدا Very high</option>
                                                                                            <option value="4" @if(isset($proc->evaluate) && ($proc->evaluate==4)) selected @endif>عال High</option>
                                                                                            <option value="3" @if(isset($proc->evaluate) && ($proc->evaluate==3)) selected @endif>متوسط Medium</option>
                                                                                            <option value="2" @if(isset($proc->evaluate) && ($proc->evaluate==2)) selected @endif>متدن Low</option>
                                                                                            <option value="1" @if(isset($proc->evaluate) && ($proc->evaluate==1)) selected @endif>متدن جدا Very Low</option>
                                                                                          
                                                                                          </select>
                                                                                   </td>
                                                                                  </tr>
                                                                                  @endforeach
                                                                                </tbody>
                                                                              </table> 
                                                                            </ul>
                                                                          </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="bs-example">
                                                                <div id="accordion{{$key+1}}-{{$ke+1}}-2" class="panel-group">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                          <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion{{$key+1}}-{{$ke+1}}-2" href="#collapse{{$key+1}}-{{$ke+1}}-2">المحكات Scrutiny </a>
                                                                          </h4>
                                                                        </div>
                                                                        <div id="collapse{{$key+1}}-{{$ke+1}}-2" class="panel-collapse collapse in">
                                                                          <div class="panel-body table-responsive">
                                                                            <ul style="border: #00c0ef 2px solid;">
                                                                              <table  class="table table-bordered table-striped" data-page-length='100'>
                                                                                <thead>
                                                                                  <tr>
                                                                                    <th> # </th>
                                                                                    <th>المحكات Scrutiny</th>
                                                                                    <th>التقييم Evaluate</th>
                                                                                  </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                  @foreach($checkup->scrutiny as $k_scrutiny=>$proc)
                                                                                  <tr>
                                                                                    <td>
                                                                                      {{$k_scrutiny+1}}
                                                                                    </td>
                                                                                    <td>{{$proc->procedure}}</td>
                                                                                   <td class="text-center">
                                                                                    <select class="form-control select2"  data-placeholder="التقييم" style="width: 100%;" name="evaluate{{$proc->id}}">
                                                                                            <option value="0" disabled="" selected="">التقييم</option>
                                                                                            <option value="5" @if(isset($proc->evaluate) && ($proc->evaluate==5)) selected @endif >5</option>
                                                                                            <option value="4" @if(isset($proc->evaluate) && ($proc->evaluate==4)) selected @endif>4</option>
                                                                                            <option value="3" @if(isset($proc->evaluate) && ($proc->evaluate==3)) selected @endif>3</option>
                                                                                            <option value="2" @if(isset($proc->evaluate) && ($proc->evaluate==2)) selected @endif>2</option>
                                                                                            <option value="1" @if(isset($proc->evaluate) && ($proc->evaluate==1)) selected @endif>1</option>
                                                                                          
                                                                                          </select>
                                                                                   </td>
                                                                                  </tr>
                                                                                  @endforeach
                                                                                </tbody>
                                                                              </table> 
                                                                            </ul>
                                                                          </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                          </ul>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                    @endforeach 
                                    </ul>
                                </div>
                                <div class="box-body">
                                  <div class="form-group">

                                    <div class="col-sm-10">
                                      <input type="text" name="notes{{$domain->id}}" class="form-control" placeholder="" value="@if(isset($domain->checkup[0]->procedure[0])){{$domain->checkup[0]->procedure[0]->notes}}@endif">
                                    </div>
                                    <label for="inputEmail3" class="col-sm-2 control-label center">ملاحظات Notes</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
              <div class="box-footer">
                <button type="submit" class="btn btn-info ">حفظ</button>
              </div>
            </form>
          </ul>
        </div>
        <br>
        <br>
        <br>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection