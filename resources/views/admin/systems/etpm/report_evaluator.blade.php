@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
<div class="content-wrapper report">
	<section class="content-header">
		<h1 class="center">
		 ETPM نموذج التميز في الأداء التدريبي
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
			<li class="active">ETPM نموذج التميز في الأداء التدريبي</li>
		</ol>
	</section>
	@if(\session('success'))
	<div class="alert alert-success">
			{{\session('success')}}
	</div>
	@endif
	@if(\session('error'))
	<div class="alert alert-danger">
			{{\session('error')}}
	</div>
	@endif
	<style type="text/css">
		.bs-example{
			margin: 20px;
		}
		.panel-title .glyphicon{
				font-size: 14px;
		}
	</style>
	<style type="text/css">
		@media print{
			
			table { page-break-inside:auto }
			tr    { page-break-inside:avoid; page-break-after:auto }
			thead { display:table-header-group }
			tfoot { display:table-footer-group }
		}
	</style>
	<section class="content">
		<div class=" box box-info">
			<div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
				<div class="col-md-4 text-center">
					<h3>International System for Standards Training</h3>
					<h3>Excellence Training Performance Model</h3>
				</div>
				<div class="col-md-4  text-center">
				 <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
				</div>
				<div class="col-md-4 text-center">
				 <h3>الأنظمة المهنية للتدريب</h3>
				 <h3>ETPM نموذج التميز في الأداء التدريبي</h3>
				 <h3>{{$ev->name}}</h3>
				</div>
			</div>
			<hr>
			@foreach($evaluator as $key_ev=>$evaluator)
			@php($sum_ev=0)
			@php($count_ev=0)
			<div class="text-center" style="border: #000 3px solid;">
				<h2>البيانات العامة للمقيم</h2>
				<div class="table-responsive">
					<table class="table mytable table-striped">
						<tbody>
							<tr>
								<th style="width:50%">اسم المقيم : </th>
								<td>{{$evaluator->name}}</td>
							</tr>
							<tr>
								<th>الايميل :</th>
								<td>{{$evaluator->email}}</td>
							</tr>
							<tr>
								<th>رقم الجوال :</th>
								<td>{{$evaluator->phone}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<hr>
			<div class="rtl">
				<h2 class="text-center">المعايير Standards</h2>
				<ul style="border: #00c0ef 3px solid;margin: 10px">
					@foreach($evaluator->domain as $key=>$domain)
					@php($sum_check=0)
					@php($count_check=0)
					<div class="bs-example">
						<div id="accordion{{$key_ev+1}}-{{$key+1}}" class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion{{$key_ev+1}}-{{$key+1}}" href="#collapse{{$key_ev+1}}-{{$key+1}}"><b style="font-size: 30px">{{$key+1}}- مؤشرات معيار  {{$domain->name}}</b> </a>
									</h4>
								</div>
								<div id="collapse{{$key_ev+1}}-{{$key+1}}" class="panel-collapse collapse in">
									<div class="panel-body table-responsive">
										<ul style="border: #00c0ef 2px solid;">
											@foreach($domain->checkup as $ke=>$checkup)
											@php($sum_proc=0)
											@php($count_proc=0)
											<div class="bs-example">
												<div id="accordion{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}" class="panel-group">
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a data-toggle="collapse" data-parent="#accordion{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}" href="#collapse{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}">{{$ke+1}}- {{$checkup->name}} </a>
															</h4>
														</div>
														<div id="collapse{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}" class="panel-collapse collapse in">
															<div class="panel-body table-responsive">
																<ul style="border: #00c0ef 2px solid;overflow-x: scroll;">
																	<div class="bs-example">
																		<div id="accordion{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-2" class="panel-group">
																			<div class="panel panel-default">
																				<div class="panel-heading">
																					<h4 class="panel-title">
																						<a data-toggle="collapse" data-parent="#accordion{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-2" href="#collapse{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-2">الإجراءات Procedures </a>
																					</h4>
																				</div>
																				<div id="collapse{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-2" class="panel-collapse collapse in">
																					<div class="panel-body table-responsive">
																						<ul style="border: #00c0ef 2px solid;overflow-x: scroll;">
																							<table  class="table table-bordered table-striped" data-page-length='100'>
																								<thead>
																									<tr>
																										<th> # </th>
																										<th>الإجراءات Procedures</th>
																										@foreach($domain->group as $key_group=>$group)
																										<th>التقييم {{$key_group+1}} Evaluate </th>
																										@endforeach
																									</tr>
																								</thead>
																								<tbody>
																									@foreach($checkup->procedure as $k_procedure=>$proc)
																									<tr>
																										<td>
																											{{$k_procedure+1}}
																										</td>
																										<td>{{$proc->procedure}}</td>
																										@foreach($proc->group as $group)
																										 <td class="text-center">
																												@php($sum_proc+=$group->evaluate)
																												@php($count_proc++)
																												@php($sum_check+=$group->evaluate)
																												@php($count_check++)
																												@php($sum_ev+=$group->evaluate)
																												@php($count_ev++)
																												<input type="text" class="form-control text-center" disabled="" value="@if($group->evaluate==5) عال جدا Very high @elseif($group->evaluate==4) عال High @elseif($group->evaluate==3) متوسط Medium @elseif($group->evaluate==2) متدن Low @elseif($group->evaluate==1) متدن جدا Very Low @endif">
																											 </td>
																											@endforeach
																									</tr>
																									@endforeach
																									<!-- <tr>
																										<td>#</td>
																										<td>ملاحظات Notes</td>
																										@foreach($checkup->procedure[0]->group as $group)
																										<td> 
																											<textarea class="form-control" disabled="">@if(isset($group)){{$group->notes}}@endif</textarea> 
																										</td>
																										@endforeach
																									</tr> -->
																								</tbody>
																							</table> 
																						</ul>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="bs-example">
																		<div id="accordion{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-3" class="panel-group">
																				<div class="panel panel-default">
																						<div class="panel-heading">
																							<h4 class="panel-title">
																								<a data-toggle="collapse" data-parent="#accordion{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-3" href="#collapse{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-3">المحكات Scrutiny </a>
																							</h4>
																						</div>
																						<div id="collapse{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-3" class="panel-collapse collapse in">
																							<div class="panel-body table-responsive">
																								<ul style="border: #00c0ef 2px solid;overflow-x: scroll;">
																									<table  class="table table-bordered table-striped" data-page-length='100'>
																										<thead>
																											<tr>
																												<th> # </th>
																												<th>المحكات Scrutiny</th>
																												@foreach($domain->group as $key_group=>$group)
																												<th>التقييم {{$key_group+1}} Evaluate </th>
																												@endforeach
																											</tr>
																										</thead>
																										<tbody>
																											@foreach($checkup->scrutiny as $k_scrutiny=>$proc)
																											<tr>
																												<td>
																													{{$k_scrutiny+1}}
																												</td>
																												<td>{{$proc->scrutiny}}</td>
																												@foreach($proc->group as $group)
																												 <td class="text-center">
																														<input type="text" class="form-control text-center" disabled="" value="{{$group->evaluate}}">
																													 </td>
																													 @endforeach
																											</tr>
																											@endforeach
																											<!-- <tr>
																												<td>#</td>
																												<td>ملاحظات Notes</td>
																												@foreach($checkup->scrutiny[0]->group as $group)
																												<td> 
																													<textarea class="form-control" disabled="">@if(isset($group)){{$group->notes}}@endif</textarea> 
																												</td>
																												@endforeach
																											</tr> -->
																										</tbody>
																									</table> 
																								</ul>
																							</div>
																						</div>
																				</div>
																		</div>
																	</div>
																	<div class="bs-example">
																		<div id="accordion{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-1" class="panel-group">
																				<div class="panel panel-default">
																						<div class="panel-heading">
																							<h4 class="panel-title">
																								<a data-toggle="collapse" data-parent="#accordion{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-1" href="#collapse{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-1">ملاحظات Notes </a>
																							</h4>
																						</div>
																						<div id="collapse{{$key_ev+1}}-{{$key+1}}-{{$ke+1}}-1" class="panel-collapse collapse in">
																							<div class="panel-body table-responsive">
																								<ul style="border: #00c0ef 2px solid;overflow-x: scroll;">
																									<table  class="table table-bordered table-striped" data-page-length='100'>
																										<thead>
																											<tr>
																												<th> # </th>
																												@foreach($domain->group as $key_group=>$group)
																												<th>التقييم {{$key_group+1}} Evaluate </th>
																												@endforeach
																											</tr>
																										</thead>
																										<tbody>
																											<tr>
																												<td>
																													#
																												</td>
																												@foreach($checkup->scrutiny[0]->group as $group)
																												<td> 
																													<textarea class="form-control" disabled="">@if(isset($group)){{$group->notes}}@endif</textarea> 
																												</td>
																												@endforeach
																											</tr>
																											
																										</tbody>
																									</table> 
																								</ul>
																							</div>
																						</div>
																				</div>
																		</div>
																	</div>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach 
											<!-- <div class="box-body">
												<div class="form-group">
													<div class="col-sm-10">
														<input type="text" name="notes{{$domain->id}}" class="form-control" disabled="" placeholder="" value="@if(isset($domain)){{$domain->notes}}@endif">
													</div>
													<label for="inputEmail3" class="col-sm-2 control-label center">ملاحظات Notes</label>
												</div>
											</div> -->
										</ul>
									</div>
									<div class="box-body chart-responsive">
                                  						<div  id="line-chart-{{$key_ev+1}}-{{$key+1}}" style="width:100%;height: 400px;"></div>
									</div>
									<div class="panel-body table-responsive">
				                                    <ul style="border: #00c0ef 2px solid;overflow-x: scroll;">
				                                      <table  class="table table-bordered table-striped" data-page-length='100'>
				                                        <thead>
				                                        <tr>
				                                          <th>رقم التقييم Evaluate No</th>
				                                          <th> المتوسط الحسابي Mean </th>
				                                          <th>التقدير Evaluate</th>
				                                          <th> الوصف  Description</th>
				                                        </tr>
				                                        </thead>
				                                        <tbody>
				                                        @foreach($domain->group as $key_group=>$g)
				                                        @php($sum_group=0)
				                                        @php($count_group=0)
				                                          @foreach($g->checkup as $ch)
				                                            @php($sum_group+=$ch->evaluate)
				                                          @php($count_group++)
				                                          @endforeach
				                                        <tr>
				                                          <td>
				                                            {{$key_group+1}}
				                                          </td>
				                                          <td>{{round($sum_group/$count_group,2)}}</td>
				                                          @if(($sum_group/$count_group)>=4.2)
				                                          <td>مميز Distinctive</td>
				                                          <td> {{$description_evaluate_5->description}}</td>

				                                          @elseif((($sum_group/$count_group)< 4.2)&& ($sum_group/$count_group)>=3.4)
				                                          <td>ممتاز Excellent</td>
				                                          <td>{{$description_evaluate_4->description}}</td>
				                                          @elseif((($sum_group/$count_group)< 3.4)&& ($sum_group/$count_group)>=2.6)
				                                          <td>متوسط Medium</td>
				                                          <td> {{$description_evaluate_3->description}}</td>

				                                          @elseif((($sum_group/$count_group)< 2.6)&& ($sum_group/$count_group)>=1.8)
				                                          <td>ضعيف Week</td>
				                                          <td> {{$description_evaluate_2->description}}</td>
				                                          @elseif(($sum_group/$count_group)< 1.8)
				                                          <td>مرفوض Refused</td>
				                                          <td>{{$description_evaluate_1->description}}</td>

				                                          @endif
				                                          
				                                        </tr>
				                                        @endforeach
				                                        </tbody>
				                                      </table> 
				                                    </ul>
				                                </div>
	                                			@if($count_check!=0)
									<div class="box-body">
										<div style="margin: 20px;">
											<h3 class="text-center">المتوسط الحسابي لمعيار "{{$domain->name}}" = {{round($sum_check/$count_check,2)}}</h3>
											@if(($sum_check/$count_check)>=4.2)
											<h3 class="text-center">تقدير معيار {{$domain->name}} = مميز Distinctive</h3>
											<h3 class="text-center">الوصف = {{$description_evaluate_5->description}} </h3>
											@elseif((($sum_check/$count_check)<4.2)&& ($sum_check/$count_check)>=3.4)
											<h3 class="text-center">تقدير معيار {{$domain->name}} = ممتاز Excellent</h3>
											<h3 class="text-center">الوصف ={{$description_evaluate_4->description}}</h3>
											@elseif((($sum_check/$count_check)<3.4)&& ($sum_check/$count_check)>=2.6)
											<h3 class="text-center">تقدير معيار {{$domain->name}} = متوسط Medium</h3>
											<h3 class="text-center">الوصف = {{$description_evaluate_3->description}} </h3>
											@elseif((($sum_check/$count_check)<2.6)&& ($sum_check/$count_check)>=1.8)
											<h3 class="text-center">تقدير معيار {{$domain->name}} = ضعيف Week</h3>
											<h3 class="text-center">الوصف ={{$description_evaluate_2->description}}  </h3>
											@elseif(($sum_check/$count_check)<1.8)
											<h3 class="text-center">تقدير معيار {{$domain->name}} = مرفوض Refused</h3>
											<h3 class="text-center">الوصف = {{$description_evaluate_1->description}}</h3>
											@endif
										</div>
									</div>
									@endif
								</div>
							</div>
						</div>
					</div>
					@endforeach

				</ul>
			</div>
	          		@if($count_ev!=0)
		          	<div class="box-body chart-responsive">
		                <div id="bar-chart-{{$key_ev+1}}" style="height: 300px;"></div>
		            </div>
		          	<div class="box-body">
			            <div style="margin: 20px;border: #000 3px solid;direction: rtl;">
			                <h3 class="text-center">المتوسط الحسابي لجميع المعايير  للمقيم  <b style="color: #5252ff">{{$evaluator->name}}</b> = {{round($sum_ev/$count_ev,2)}}</h3>
			                @if(($sum_ev/$count_ev)>=4.2)
			                <h3 class="text-center">التقدير العام = مميز Distinctive</h3>
			                <h3 class="text-center">الوصف = {{$description_evaluate_5->description}} </h3>

			                @elseif((($sum_ev/$count_ev)<4.2) && ($sum_ev/$count_ev)>=3.4)
			                <h3 class="text-center">التقدير العام = ممتاز Excellent</h3>
			                <h3 class="text-center">الوصف ={{$description_evaluate_4->description}}</h3>
			                @elseif((($sum_ev/$count_ev)<3.4)&& ($sum_ev/$count_ev)>=2.6)
			                <h3 class="text-center">التقدير العام = متوسط Medium</h3>
			                <h3 class="text-center">الوصف = {{$description_evaluate_3->description}} </h3>

			                @elseif((($sum_ev/$count_ev)<2.6)&& ($sum_ev/$count_ev)>=1.8)
			                <h3 class="text-center">التقدير العام = ضعيف Week</h3>
			                <h3 class="text-center">الوصف ={{$description_evaluate_2->description}}  </h3>
			                @elseif(($sum_ev/$count_ev)<1.8)
			                <h3 class="text-center">التقدير العام = مرفوض Refused</h3>
			                <h3 class="text-center">الوصف = {{$description_evaluate_1->description}}</h3>

			                @endif
			            </div>
		          	</div>
		          	@endif
			<br>
			<br>
			@endforeach
			<div class="box-footer no-print">
					<a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
						<i class="fa fa-print"></i>
					</a>
				</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@endsection