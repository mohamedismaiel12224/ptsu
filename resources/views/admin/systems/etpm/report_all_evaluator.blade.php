@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper report">
    <section class="content-header">
      <h1 class="center">
        ETPM نموذج التميز في الأداء التدريبي
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">ETPM نموذج التميز في الأداء التدريبي</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<style type="text/css">
  @media print{
    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
  }
</style>
    <section class="content">
      <div class=" box box-info">
        <div class="row report-style" style="border: #00c0ef 3px solid; margin: 0">
          <div class="col-md-4 text-center">
            <h3>International System for Standards Training</h3>
            <h3>Excellence Training Performance Model</h3>
          </div>
          <div class="col-md-4  text-center">
           <img src="{{asset('assets/images/home/'.$logo)}}" width="200px">
          </div>
          <div class="col-md-4 text-center">
           <h3>الأنظمة المهنية للتدريب</h3>
           <h3>ETPM نموذج التميز في الأداء التدريبي</h3>
           <h3>{{$ev->name}}</h3>
          </div>
        </div>
        <hr>
        <div class="rtl">
          <h2 class="text-center">المعايير Standards</h2>
          <ul style="border: #00c0ef 3px solid;margin: 10px">
                @php($all_sum=0)
                @php($all_count=0)
                @foreach($domains_bar as $key=>$domain)
                @php($all_sum+=$domain->sum)
                @php($all_count+=$domain->count)
                <div class="bs-example">
                    <div id="accordion{{$key+1}}" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion{{$key+1}}" href="#collapse{{$key+1}}"><b style="font-size: 30px">{{$key+1}}- مؤشرات معيار  {{$domain->name}}</b></a>
                                </h4>
                            </div>
                            <div id="collapse{{$key+1}}" class="panel-collapse collapse in">
                                <div class="panel-body table-responsive">
                                    <ul style="border: #00c0ef 2px solid;">
                                      <table  class="table table-bordered table-striped" data-page-length='100'>
                                        <thead>
                                        <tr>
                                          <th> # </th>
                                          <th>المؤشر</th>
                                          <th>عال جدا Very high</th>
                                          <th>عال High</th>
                                          <th>متوسط Medium</th>
                                          <th>متدن Low</th>
                                          <th>متدن جدا Very Low</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($domain->checkup as $ke=>$value)
                                        <tr>
                                          <td>
                                            {{$ke+1}}
                                          </td>
                                          <td>{{$value->name}}</td>
                                          <td style="text-align: center;">{{$value->evaluate_5}}</td>
                                          <td style="text-align: center;">{{$value->evaluate_4}}</td>
                                          <td style="text-align: center;">{{$value->evaluate_3}}</td>
                                          <td style="text-align: center;">{{$value->evaluate_2}}</td>
                                          <td style="text-align: center;">{{$value->evaluate_1}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                      </table>
                                      @if($domain->count>0)
                                      <div class="box-body">
                                        <div style="margin: 20px;">
                                            <h3 class="text-center">المستوى الحسابي لمعيار "{{$domain->name}}" = {{round($domain->sum/$domain->count,2)}}</h3>
                                            @if(($domain->sum/$domain->count)>=4.2)
                                            <h3 class="text-center">تقدير معيار {{$domain->name}} = مميز Distinctive</h3>
                                            <h3 class="text-center">الوصف = {{$description_evaluate_5->description}}</h3>

                                            @elseif((($domain->sum/$domain->count)<4.2)&& ($domain->sum/$domain->count)>=3.4)
                                            <h3 class="text-center">تقدير معيار {{$domain->name}} = ممتاز Excellent</h3>
                                            <h3 class="text-center">الوصف ={{$description_evaluate_4->description}}</h3>
                                            @elseif((($domain->sum/$domain->count)<3.4)&& ($domain->sum/$domain->count)>=2.6)
                                            <h3 class="text-center">تقدير معيار {{$domain->name}} = متوسط Medium</h3>
                                            <h3 class="text-center">الوصف = {{$description_evaluate_3->description}} </h3>

                                            @elseif((($domain->sum/$domain->count)<2.6)&& ($domain->sum/$domain->count)>=1.8)
                                            <h3 class="text-center">تقدير معيار {{$domain->name}} = ضعيف Week</h3>
                                            <h3 class="text-center">الوصف ={{$description_evaluate_2->description}}  </h3>
                                            @elseif(($domain->sum/$domain->count)<1.8)
                                            <h3 class="text-center">تقدير معيار {{$domain->name}} = مرفوض Refused</h3>
                                            <h3 class="text-center">الوصف = {{$description_evaluate_1->description}}</h3>

                                            @endif
                                        </div>
                                      </div>
                                      <div class="box-body chart-responsive">
                                          <div  id="bar-chart-{{$key+1}}" style="height: 300px;"></div>
                                      </div>
                                      @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
          </ul>
        </div>
        <hr>
        <hr>
        @if($all_count>0)
        <div class="box-body">
            <div style="margin: 20px;">
                <h3 class="text-center">المستوى الحسابي لجميع المعايير = {{round($all_sum/$all_count,2)}}</h3>
                @if(($all_sum/$all_count)>=4.2)
                <h3 class="text-center">التقدير العام = مميز Distinctive</h3>
                <h3 class="text-center">الوصف = {{$description_evaluate_5->description}}</h3>

                @elseif((($all_sum/$all_count)<4.2) && ($all_sum/$all_count)>=3.4)
                <h3 class="text-center">التقدير العام = ممتاز Excellent</h3>
                <h3 class="text-center">الوصف ={{$description_evaluate_4->description}}</h3>
                @elseif((($all_sum/$all_count)<3.4)&& ($all_sum/$all_count)>=2.6)
                <h3 class="text-center">التقدير العام = متوسط Medium</h3>
                <h3 class="text-center">الوصف = {{$description_evaluate_3->description}} </h3>

                @elseif((($all_sum/$all_count)<2.6)&& ($all_sum/$all_count)>=1.8)
                <h3 class="text-center">التقدير العام = ضعيف Week</h3>
                <h3 class="text-center">الوصف ={{$description_evaluate_2->description}}  </h3>
                @elseif(($all_sum/$all_count)<1.8)
                <h3 class="text-center">التقدير العام = مرفوض Refused</h3>
                <h3 class="text-center">الوصف = {{$description_evaluate_1->description}}</h3>

                @endif
            </div>
          </div>
          <div class="box-body chart-responsive">
                <div  id="bar-chart" style="height: 300px;"></div>
            </div>
          @endif
        <br>
        <br>
        <div class="box-footer no-print">
            <a class="btn btn-primary"  onclick="javascript:window.print();"> طباعة
                <i class="fa fa-print"></i>
            </a>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection