@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($sessions))
				تعديل جلسة تدريبية
				@else
				إضافة جلسة تدريبية
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				@if(isset($sessions))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
					    @if(\session('error'))
						    <div class="alert alert-danger">
						        {{\session('error')}}
						    </div>
						@endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($sessions)) action="{{url('sessions/'.$sessions->id)}}" @else   action="{{url('sessions')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">العنوان</label>
									<div class="col-sm-10">
										<input type="text" name="address" class="form-control" placeholder="" value="@if(isset($sessions)){{$sessions->address}}@else{{old('address')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">التغطية</label>
									<div class="col-sm-10">
										@if(isset($sessions))
											@php($arr = explode(',',$sessions->branch_arr))
										@endif
										<select class="form-control select2" multiple="multiple" data-placeholder="اختر التغطية" style="width: 100%;" name="branch_arr[]">
											@foreach($branches as $val)
											<option value="{{$val->id}}"  
												@if(isset($arr))
													@foreach($arr as $v)
														@if($v==$val->id)selected @endif 
													@endforeach 
												@endif>{{$val->address}}</option>
											@endforeach
						                </select>
									</div>
				                </div>
				                <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">مصادر المحتوى</label>
									<div class="col-sm-10">
										<select class="form-control select2" multiple="" data-placeholder="اختر مصادر المحتوى" style="width: 100%;" name="source_id[]">
											@foreach($sources as $val)
											<option value="{{$val->id}}"  
												@if(isset($sessions) && $sessions->source_id==$val->id)selected @endif>{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
								</div>
								
				               
				                <!-- <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الزمن</label>
									<div class="col-sm-10">
										<input type="text" name="time" class="form-control" placeholder="" value="@if(isset($sessions)){{$sessions->time}}@else{{old('time')}}@endif">
									</div>
				                </div> -->
							</div>
							@if(isset($sessions))
								<input type="hidden" name="_method" value="patch"> 
							@else
								<input type="hidden" name="pro_id" value="{{$mit->id}}">
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection