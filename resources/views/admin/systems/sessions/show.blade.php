@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        الجلسات التدريبية  Training sessions
      </h1>
      <a href="{{url('sessions/create/'.$mit->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة جلسة تدريبية <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">الجلسات التدريبية</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>العنوان Address</th>
                  <th>التغطية Coverage</th>
                  <th>مصارد المحتوى  Content Sources</th>
                  <th>أدوات التقوي  Evaluation Tools</th>
                  <th>الزمن Time</th>
                  <th>العمليات المتاحة Available Operations</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sessions as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->address}}</td>
                    <td>{{print_branch($value->branch_arr)}}</td>
                    <td>{{print_attributes($value->source_id)}}</td>
                    <td>{{print_tools($value->id)}}</td>
                    <td>{{print_time($value->id)}}</td>
                    <td style="display: flex">
                      <a href="{{url('sessions/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('sessions/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection