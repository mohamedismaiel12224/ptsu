@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 class="center">
				طرق التدريب  Training Methods
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">طرق التدريب</li>
			</ol>
		</section>
		@if(\session('success'))
		<div class="alert alert-success">
				{{\session('success')}}
		</div>
		@endif
		@if(\session('error'))
		<div class="alert alert-danger">
				{{\session('error')}}
		</div>
@endif
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body table-responsive">
							<form class="form-horizontal" method="post"  action="{{url('training_method_update/'.$mit->id)}}" enctype="multipart/form-data">
							{{csrf_field()}}

								<table id="example1" class="table table-bordered table-striped" data-page-length='100'>
									<thead>
									<tr>
										<th> # </th>
										<th>نواتج التدريب و الأرتباطات Training Outputs & Collerations</th>
										<th style="width: 50%">طرق التدريب Training Methods</th>
									</tr>
									</thead>
									<tbody>
									@foreach($all as $key=>$value)
									<tr>
											<td>
												{{$key+1}}
											</td>
											<td>{{$value->name}}</td>
											<td>
												@php($arr = explode(',',$value->training_method_id))
												<select class="form-control select2" multiple="multiple" data-placeholder="اختر النوع" style="width: 100%;" name="method{{$value->series_no}}[]">
												@foreach($method as $val)
												<option value="{{$val->id}}"  @foreach($arr as $v)
														@if($v==$val->id)selected @endif 
													@endforeach >{{$val->name}}</option>
												@endforeach
												
								                </select>
											</td>
									</tr>
									@endforeach
									</tbody>
								</table>
								<div class="box-footer">
									<button type="submit" class="btn btn-info ">حفظ</button>
								</div>
								<!-- /.box-footer -->
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection