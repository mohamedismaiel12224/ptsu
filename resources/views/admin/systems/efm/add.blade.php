@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        البيانات العامة
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('efm/efm')}}">مشاريعى</a></li>
        @if(isset($efm))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($efm)) action="{{url('efm/efm/'.$efm->id)}}" @else   action="{{url('efm/efm')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">عنوان الدراسة Study Title</label>

                  <div class="col-sm-10">
                    <input type="text" name="study_title" class="form-control" placeholder="" value="@if(isset($efm)){{$efm->study_title}}@else{{old('study_title')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الجهة المستفيدة the Beneficiary</label>

                  <div class="col-sm-10">
                    <input type="text" name="beneficiary" class="form-control" placeholder="" value="@if(isset($efm)){{$efm->beneficiary}}@else{{old('beneficiary')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">أرقام تواصل Contact</label>

                  <div class="col-sm-10">
                    <input type="text" name="contact" class="form-control" placeholder="" value="@if(isset($efm)){{$efm->contact}}@else{{old('contact')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">رقم الدراسة Number</label>

                  <div class="col-sm-10">
                    <input type="text" name="number" class="form-control" placeholder="" value="@if(isset($efm)){{$efm->number}}@else{{old('number')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الدولة Country</label>

                  <div class="col-sm-10">
                    <input type="text" name="country" class="form-control" placeholder="" value="@if(isset($efm)){{$efm->country}}@else{{old('country')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المدينة City</label>

                  <div class="col-sm-10">
                    <input type="text" name="city" class="form-control" placeholder="" value="@if(isset($efm)){{$efm->city}}@else{{old('city')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الايميل Email</label>

                  <div class="col-sm-10">
                    <input type="text" name="email" class="form-control" placeholder="" value="@if(isset($efm)){{$efm->email}}@else{{old('email')}}@endif">
                  </div>
                </div>

              @if(isset($efm))
                <input type="hidden" name="_method" value="patch"> 
              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection