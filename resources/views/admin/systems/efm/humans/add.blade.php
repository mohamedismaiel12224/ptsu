@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        تعيين الخصائص البشرية Human Characteristics

      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('efm/efm')}}">مشاريعى</a></li>
        @if(isset($note))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post"  action="{{url('efm/human_characteristics/'.$efm->id.'/'.$human->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">اسم الخصائص البشرية Human Characteristics</label>

                  <div class="col-sm-10">
                    <input type="text" disabled="" class="form-control" placeholder="" value="{{$human->name}}">
                  </div>
                </div>
                @foreach($relations as $key=>$relation)
                @if(($relation->id==4 && ($human->id==1||$human->id==4||$human->id==5)) ||  ($relation->id==5 && ($human->id==1||$human->id==3)) )
               @else
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">{{$relation->relation}}</label>

                  <div class="col-sm-10">
                    <select class="form-control select2"  data-placeholder="اختر {{$relation->relation}}" style="width: 100%;" name="rel[{{$relation->id}}]">
                      <option value="0" @if($relation->option==0) selected @endif>اختر</option>
                      <option value="1" @if($relation->option==1) selected @endif>{{$relation->option1}}</option>
                      <option value="2" @if($relation->option==2) selected @endif>{{$relation->option2}}</option>
                      <option value="3" @if($relation->option==3) selected @endif>{{$relation->option3}}</option>
                      @if($relation->option4)
                      <option value="4" @if($relation->option==4) selected @endif>{{$relation->option4}}</option>
                      @endif
                    </select>
                  </div>
                </div>
                @endif
                @endforeach
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection