@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
         تعيين الخصائص البشرية Human Characteristics
      </h1>
<!--       <a href="{{url('efm/gap/create/'.$efm->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
 -->      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تعيين الخصائص البشرية Human Characteristics</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>#</th>
                  @foreach($relations as $relation)
                  <th>{{$relation->relation}}</th>
                  @endforeach
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($humans as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    @foreach($value->relations as $rel)
                    @if($rel->option == 0 )
                    <td> - </td>
                    @elseif($rel->option == 1)
                    <td>{{$rel->option1}}</td>
                    @elseif($rel->option == 2)
                    <td>{{$rel->option2}}</td>
                    @elseif($rel->option == 3)
                    <td>{{$rel->option3}}</td>
                    @elseif($rel->option == 4)
                    <td>{{$rel->option4}}</td>
                    @endif
                    @endforeach
                    <td style="display: flex">
                      <a href="{{url('efm/human_characteristics/'.$efm->id.'/'.$value->id)}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection