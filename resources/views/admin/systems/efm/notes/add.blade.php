@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ملحوظات عامة General Notes
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('efm/efm')}}">مشاريعى</a></li>
        @if(isset($note))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($note)) action="{{url('efm/note/'.$note->id)}}" @else   action="{{url('efm/note')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المجال Field</label>

                  <div class="col-sm-10">
                    <input type="text" name="field" class="form-control" placeholder="" value="@if(isset($note)){{$note->field}}@else{{old('field')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الملحوظة Note</label>

                  <div class="col-sm-10">
                    <input type="text" name="note" class="form-control" placeholder="" value="@if(isset($note)){{$note->note}}@else{{old('note')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">تقدير الحجم Assess</label>

                  <div class="col-sm-10">
                    <input type="number" name="assess" max="100" min="0" class="form-control" placeholder="" value="@if(isset($note)){{$note->assess}}@else{{old('assess')}}@endif">
                  </div>
                </div>
                
              @if(isset($note))
                <input type="hidden" name="_method" value="patch"> 
                @else
                <input type="hidden" name="pro_id" value="{{$efm->id}}"> 

              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection