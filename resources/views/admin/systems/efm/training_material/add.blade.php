@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        إعداد مواد التدريب Materials Training

      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('efm/efm')}}">مشاريعى</a></li>
        <li class="active">تعديل</li>
        
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post"  action="{{url('efm/training_material/'.$efm->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">التقييم العام للحقيبة Evaluation General</label>

                  <div class="col-sm-10">
                    <input type="number" name="general_evaluation" max="100" min="0" class="form-control" placeholder="" value="{{$efm->general_evaluation}}">

                  </div>
                </div>
                <div class="add_here">
                  @if(count($notes)>0)
                  @foreach($notes as $key=>$note)
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label center">ملحوظات على الحقيبة Notes</label>
                    <div class="col-sm-9">
                      <input type="text" name="notes[]" class="form-control" value="{{$note->notes}}" >
                    </div>

                    <label for="name" class="col-sm-1">
                    @if($key==0)
                      <a href="#" id="add_new_note">
                        <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                      </a>
                      @else
                      <a href="#" id="remove_field">
                          <i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i>
                      </a>
                      @endif
                    </label>
                  </div>
                  @endforeach
                  @else
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label center">ملحوظات على الحقيبة Notes</label>
                    <div class="col-sm-9">
                      <input type="text" name="notes[]" class="form-control" >
                    </div>
                    <label for="name" class="col-sm-1">
                      <a href="#" id="add_new_note">
                        <i class="fa fa-plus fa-3x" aria-hidden="true"></i>
                      </a>
                    </label>
                  </div>
                  @endif
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection