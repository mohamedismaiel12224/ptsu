@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
         إعداد مواد التدريب Materials Training

      </h1>
<!--       <a href="{{url('efm/gap/create/'.$efm->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
 -->      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">إعداد مواد التدريب Materials Training</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row" style="    padding-top: 100px;">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                
                <tbody>
                <tr>
                    <td><a class="btn btn-block btn-social btn-info center" href="{{url('efm/training_material/'.$efm->id.'/1')}}">
                <i class="fa fa-check-circle-o"></i> تم تحكيم المواد التدريبية ArbitrationN
              </a></td>
                    <td><a class="btn btn-block btn-social btn-danger" href="{{url('efm/training_material/'.$efm->id.'/2')}}">
                <i class="fa fa-times-circle-o"></i> لم يتم تحكيم مواد التدريب UN Arbitration
              </a></td>
              <td><a class="btn btn-block btn-social btn-success" href="{{url('mit_arbitration')}}">
                <i class="fa fa-plus-circle"></i>  اجراء تحكيم جديد Arbitration New
              </a></td>
                </tr>
                </tbody>
              </table>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection