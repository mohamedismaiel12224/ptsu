@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        Targets تحديدالمستهدفات
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تحديدالمستهدفات</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
  @endif
  @if (count($errors) > 0)
        <div class="alert alert-danger">
          <ul>
            @foreach ( $errors->all() as $error )
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped"  data-page-length='100'>
                  <thead>
                  <tr>
                    <th> # </th>
                    <th>مستوى التقييم  Assessment level</th>
                    <th>المؤشر أو الهدف Indicator or goal</th>
                    <th>القيم التشخيصية Diagnosis values</th>
                    <th>المستهدف Target</th>
                    <th>آثر التدريب Training impact</th>
                    <th>#</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($framing as $key=>$value)
                  <tr>
                      <td>
                        {{$key+1}}
                      </td>
                      <td>{{$value->name}}</td>
                      <td>{{$value->goal}}</td>
                      <td>{{$value->diagnosis_values}} </td>
                      <td>{{$value->target}} </td>
                      <td>{{$value->diagnosis_values - $value->target}} </td>
                      <td style="display: flex">
                      <a href="{{url('efm/targets/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<script >
$(document).ready(function() {
    $('#example1').DataTable( {
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
    } );
} );
</script>
    </section>
    <!-- /.content -->
  </div>
@endsection