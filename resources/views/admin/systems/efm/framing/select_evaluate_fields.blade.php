@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        Areas of assessment تحديد مجالات التقييم 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">تحديد مجالات التقييم </li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
  @endif
  @if (count($errors) > 0)
        <div class="alert alert-danger">
          <ul>
            @foreach ( $errors->all() as $error )
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <form class="form-horizontal" method="post"  action="{{url('efm/update_select_evaluate_fields/'.$efm->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}

                <table id="example1" class="table table-bordered table-striped"  data-page-length='100'>
                  <thead>
                  <tr>
                    <th> # </th>
                    <th>مستوى التقييم  Assessment level</th>
                    <th>مبررات الاختيار  أو عدمه Justifications</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($attr as $key=>$value)
                  <tr>
                      <td>
                        <input type="checkbox" name="tr[{{$value->id}}]" value="1"  @if($value->checked == 1) checked @endif>
                      </td>
                      <td>{{$value->name}}</td>
                      <td>
                        <input type="text" name="justifications[{{$value->id}}]" value="{{$value->justifications}}">
                      </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <div class="box-footer">
                  <button type="submit" class="btn btn-info ">حفظ</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<script >
$(document).ready(function() {
    $('#example1').DataTable( {
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
    } );
} );
</script>
    </section>
    <!-- /.content -->
  </div>
@endsection