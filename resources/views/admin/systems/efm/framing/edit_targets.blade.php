@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Edit Target
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('efm/efm')}}">مشاريعى</a></li>
        @if(isset($framing))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post"   action="{{url('efm/update_targets/'.$framing->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">مستوى التقييم  Assessment level</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" placeholder="" disabled="" value="@if(isset($framing)){{$framing->name}}@else{{old('name')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المؤشر أو الهدف Indicator or goal</label>

                  <div class="col-sm-10">
                    <input type="text" name="goal" class="form-control" placeholder="" value="@if(isset($framing)){{$framing->goal}}@else{{old('goal')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">القيم التشخيصية Diagnosis values</label>

                  <div class="col-sm-10">
                    <input type="number" min="0" max="100" name="diagnosis_values" class="form-control" placeholder="" value="@if(isset($framing)){{$framing->diagnosis_values}}@else{{old('diagnosis_values')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المستهدف Target</label>

                  <div class="col-sm-10">
                    <input type="number" min="0" max="100" name="target" class="form-control" placeholder="" value="@if(isset($framing)){{$framing->target}}@else{{old('target')}}@endif">
                  </div>
                </div>
              
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection