@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        مشاريعى
      </h1>
      <a href="{{url('efm/efm/create')}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">مشاريعى</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>عنوان الدراسة Study Title</th>
                  <th>الجهة المستفيدة the Beneficiary</th>
                  <th>أرقام تواصل Contact</th>
                  <th>رقم الدراسة Number</th>
                  <th>الدولة Country</th>
                  <th>المدينة City</th>
                  <th>الايميل Email</th>
                  <th>التاريخ Date</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->study_title}}</td>
                    <td>{{$value->beneficiary}}</td>
                    <td>{{$value->contact}}</td>
                    <td>{{$value->number}}</td>
                    <td>{{$value->country}}</td>
                    <td>{{$value->city}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{date('Y-m-d',strtotime($value->created_at))}}</td>
                    <td style="display: flex">
                      <a href="{{url('efm/efm/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('efm/efm/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>
                      <a href="{{url('efm/efm/'.$value->id)}}" class="btn btn-primary" style="margin-right: 10px;">فتح المشروع <i class="fa fa-television"></i></a>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection