@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        نموذج العمل Business model ( A )
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('efm/efm')}}">مشاريعى</a></li>
        @if(isset($analysis))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($analysis)) action="{{url('efm/analysis/'.$analysis->id)}}" @else   action="{{url('efm/analysis')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الخطوة Step</label>

                  <div class="col-sm-10">
                    <input type="text" name="step" class="form-control" placeholder="" value="@if(isset($analysis)){{$analysis->step}}@else{{old('step')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">مكان التنفيذ Place of execution</label>

                  <div class="col-sm-10">
                    <input type="text" name="place" class="form-control" placeholder="" value="@if(isset($analysis)){{$analysis->place}}@else{{old('place')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الزمن Time</label>

                  <div class="col-sm-10">
                    <input type="text" name="time"  class="form-control" placeholder="" value="@if(isset($analysis)){{$analysis->time}}@else{{old('time')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المسئول Responsible</label>
                  <div class="col-sm-10">
                    <input type="text" name="responsible" class="form-control" placeholder="" value="@if(isset($analysis)){{$analysis->responsible}}@else{{old('responsible')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">مالحظات Notes</label>

                  <div class="col-sm-10">
                    <input type="text" name="notes" class="form-control" placeholder="" value="@if(isset($analysis)){{$analysis->notes}}@else{{old('notes')}}@endif">
                  </div>
                </div>
              @if(isset($analysis))
                <input type="hidden" name="_method" value="patch"> 
                @else
                <input type="hidden" name="pro_id" value="{{$efm->id}}"> 

              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection