@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 class="center">
				نموذج العمل Business model ( B )
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">نموذج العمل Business model ( B )</li>
			</ol>
		</section>
		@if(\session('success'))
		<div class="alert alert-success">
				{{\session('success')}}
		</div>
		@endif
		@if(\session('error'))
		<div class="alert alert-danger">
				{{\session('error')}}
		</div>
		@endif
		@if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body table-responsive">
							<form class="form-horizontal" method="post"  action="{{url('efm/analysis_b/'.$efm->id)}}" enctype="multipart/form-data">
							{{csrf_field()}}

								<table id="example1" class="table table-bordered table-striped" data-page-length='100'>
									<thead>
									<tr>
										<th> # </th>
										<th>مستوى التقييم  Assessment level</th>
							                   <th>المؤشر أو الهدف Indicator or goal</th>
							                   <!-- <th>القيم التشخيصية Diagnosis values</th> -->
							                   <th>المستهدف Target</th>
							                   <!-- <th>آثر التدريب Training impact</th> -->
										<th>الأدوات Tools</th>
										<th>مصادر البيانات Data Sources</th>
										<th>فترة التطبيق Application period</th>
										<th>المسئول responsible</th>
									</tr>
									</thead>
									<tbody>
									@foreach($attr as $key=>$value)
									<tr>
											<td>
												{{$key+1}}
											</td>
											<td>{{$value->name}}</td>
						                      		<td>{{$value->goal}}</td>
							                      	<!-- <td>{{$value->diagnosis_values}} </td> -->
							                      	<td>{{$value->target}} </td>
							                      	<!-- <td>{{$value->diagnosis_values - $value->target}} </td> -->
											<td style="width: 450px">
												<select class="form-control select2"  data-placeholder="اختر الأداة" style="width: 100%;" name="tool_id[{{$value->id}}]">
												<option value="0" disabled="" selected="">اختر الأداة</option>
												@foreach($tools as $tool)
												<option value="{{$tool->id}}" @if($value->tool_id == $tool->id) selected="" @endif>{{$tool->tool}}</option>
												@endforeach
												
								                </select>
											</td>
											<td>
												<input type="text" name="source[{{$value->id}}]" value="{{$value->source}}">
											</td>
											<td>
												<input type="text" name="period[{{$value->id}}]" value="{{$value->period}}">
											</td>
											<td>
												<input type="text" name="responsible[{{$value->id}}]" value="{{$value->responsible}}">
												
											</td>
									</tr>
									@endforeach
									</tbody>
								</table>
								<div class="box-footer">
									<button type="submit" class="btn btn-info ">حفظ</button>
								</div>
								<!-- /.box-footer -->
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection