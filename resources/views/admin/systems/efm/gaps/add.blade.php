@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        تحديد الفجوات Gaps Assessment
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('efm/efm')}}">مشاريعى</a></li>
        @if(isset($gap))
        <li class="active">تعديل</li>
        @else
        <li class="active">إضافة</li>
        @endif
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
            </div>
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ( $errors->all() as $error )
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" @if(isset($gap)) action="{{url('efm/gap/'.$gap->id)}}" @else   action="{{url('efm/gap')}}" @endif enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المسئولية Responsibility</label>

                  <div class="col-sm-10">
                    <input type="text" name="responsibility" class="form-control" placeholder="" value="@if(isset($gap)){{$gap->responsibility}}@else{{old('responsibility')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الفجوات Gaps</label>

                  <div class="col-sm-10">
                    <input type="text" name="gaps" class="form-control" placeholder="" value="@if(isset($gap)){{$gap->gaps}}@else{{old('gaps')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">النسبة Percentage</label>

                  <div class="col-sm-10">
                    <input type="number" name="percentage" max="100" min="0" class="form-control" placeholder="" value="@if(isset($gap)){{$gap->percentage}}@else{{old('percentage')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">الخبير Expert</label>

                  <div class="col-sm-10">
                    <input type="text" name="expert" class="form-control" placeholder="" value="@if(isset($gap)){{$gap->expert}}@else{{old('expert')}}@endif">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label center">المبررات Justifications</label>

                  <div class="col-sm-10">
                    <input type="text" name="justifications" class="form-control" placeholder="" value="@if(isset($gap)){{$gap->justifications}}@else{{old('justifications')}}@endif">
                  </div>
                </div>
              @if(isset($gap))
                <input type="hidden" name="_method" value="patch"> 
                @else
                <input type="hidden" name="pro_id" value="{{$efm->id}}"> 

              @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">حفظ</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection