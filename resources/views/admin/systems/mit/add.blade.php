@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				البيانات العامة
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('mit')}}">مشاريعى</a></li>
				@if(isset($mit))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($mit)) action="{{url('mit/'.$mit->id)}}" @else   action="{{url('mit')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">اسم المشروع</label>

									<div class="col-sm-10">
										<input type="text" name="name" class="form-control" placeholder="" value="@if(isset($mit)){{$mit->name}}@else{{old('name')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رقم المشروع</label>

									<div class="col-sm-10">
										<input type="text" name="nno" class="form-control" placeholder="" value="@if(isset($mit)){{$mit->nno}}@else{{old('nno')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الجهة المستفيدة</label>

									<div class="col-sm-10">
										<input type="text" name="benefit_establishment" class="form-control" placeholder="" value="@if(isset($mit)){{$mit->benefit_establishment}}@else{{old('benefit_establishment')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">عدد الساعات</label>

									<div class="col-sm-10">
										<input type="text" name="hours" class="form-control" placeholder="" value="@if(isset($mit)){{$mit->hours}}@else{{old('hours')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">عدد الأيام</label>

									<div class="col-sm-10">
										<input type="text" name="days" class="form-control" placeholder="" value="@if(isset($mit)){{$mit->days}}@else{{old('days')}}@endif">
									</div>
								</div>
							</div>
							@if(isset($mit))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection