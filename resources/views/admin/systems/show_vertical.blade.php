@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="center">
        عمل ترابط رأسي Vertical Threading
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">عمل ترابط رأسي</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <form class="form-horizontal" method="post"  action="{{url('vertical/'.$mit->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}

                <table id="example1" class="table table-bordered table-striped"  data-page-length='100'>
                  <thead>
                  <tr>
                    <th> # </th>
                    <th>نواتج التدريب و الإرتباطات  Training Outputs & Collerations</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($training as $key=>$value)
                  <tr>
                      <td>
                        <input type="number" name="tr{{$value->id}}" min="1" @if($value->series_no != 0) value="{{$value->series_no}}" @endif>
                      </td>
                      <td>{{$value->name}}</td>
                  </tr>
                  @endforeach
                  @foreach($collerations as $key=>$value)
                  <tr>
                      <td>
                        <input type="number" name="col{{$value->id}}" min="1" @if($value->series_no != 0) value="{{$value->series_no}}" @endif>
                      </td>
                      <td>{{$value->detail}}</td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <div class="box-footer">
                  <button type="submit" class="btn btn-info ">حفظ</button>
                  <a href="{{url('reseries/'.$mit->id)}}" class="btn btn-danger">إعادة التسلسل</a>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<script >
$(document).ready(function() {
    $('#example1').DataTable( {
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
    } );
} );
</script>
    </section>
    <!-- /.content -->
  </div>
@endsection