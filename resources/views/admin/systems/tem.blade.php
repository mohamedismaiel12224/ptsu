@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 class="center">
				نموذج TEM
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">نموذج TEM</li>
			</ol>
		</section>
		@if(\session('success'))
		<div class="alert alert-success">
				{{\session('success')}}
		</div>
		@endif
		@if(\session('error'))
		<div class="alert alert-danger">
				{{\session('error')}}
		</div>
@endif
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body table-responsive">
							<form class="form-horizontal" method="post"  action="{{url('tem_update/'.$mit->id)}}" enctype="multipart/form-data">
							{{csrf_field()}}

								<table id="example1" class="table table-bordered table-striped" data-page-length='100'>
									<thead>
									<tr>
										<th> # </th>
										<th>نواتج التدريب  Training Outputs</th>
										<th style="width: 30%">أسلوب التقويم  Evaluation Method</th>
										<th style="width: 20%">وقت التقويم    Evaluation Time</th>
										<th style="width: 20%">الأدوات Tools</th>
										<th style="width: 20%">المسئولية  Responsibility</th>
									</tr>
									</thead>
									<tbody>
									@foreach($all as $key=>$value)
									<tr>
										<td>
											{{$key+1}}
										</td>
										<td>{{$value->name}}</td>
										<td>
											<!-- <input type="text" name="style{{$value->series_no}}" value="{{$value->style}}"> -->
											<textarea class="form-control" name="style{{$value->series_no}}" style="width: 100%;">{{$value->style}}</textarea>
										</td>
										<td>
											<select class="form-control select2"  data-placeholder="اختر وقت التقويم" style="width: 100%;" name="time{{$value->series_no}}">
											<option value="0" disabled="" selected="">اختر وقت التقويم</option>
											@foreach($time as $val)
											<option value="{{$val->id}}"  @if($val->id == $value->time) selected="" @endif>{{$val->name}}</option>
											@endforeach
							                </select>
										</td>
										<td>
											<select class="form-control select2"  data-placeholder="اختر اداة" style="width: 100%;" name="tool{{$value->series_no}}">
											<option value="0" disabled="" selected="">اختر اداة</option>
											@foreach($tools as $val)
											<option value="{{$val->id}}"  @if($val->id == $value->tool) selected="" @endif>{{$val->name}}</option>
											@endforeach
							                </select>
										</td>
										<td>
											@php($arr = explode(',',$value->responsibilty))
											<select class="form-control select2"  multiple="" data-placeholder="اختر المسئولية" style="width: 100%;" name="responsibilty{{$value->series_no}}[]">
											@foreach($responsibilty as $val)
											<option value="{{$val->id}}"  @foreach($arr as $v)@if($val->id == $v) selected="" @endif @endforeach>{{$val->name}}</option>
											@endforeach
							                </select>
										</td>
									</tr>
									@endforeach
									</tbody>
								</table>
								<div class="box-footer">
									<button type="submit" class="btn btn-info ">حفظ</button>
								</div>
								<!-- /.box-footer -->
							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection