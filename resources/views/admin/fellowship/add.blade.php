@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($fellowship))
				تعديل زميل اتحاد
				@else
				اضافة زميل اتحاد
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('fellowship')}}">الزملاء</a></li>
				@if(isset($fellowship))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($fellowship)) action="{{url('fellowship/'.$fellowship->id)}}" @else   action="{{url('fellowship')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الأسم</label>
									<div class="col-sm-10">
										<input type="text" name="name" class="form-control" placeholder="" value="@if(isset($fellowship)){{$fellowship->name}}@else{{old('name')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الأيميل</label>
									<div class="col-sm-10">
										<input type="email" name="email" class="form-control" placeholder="" value="@if(isset($fellowship)){{$fellowship->email}}@else{{old('email')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رقم الهاتف</label>
									<div class="col-sm-10">
										<input type="text" name="phone" class="form-control" placeholder="" value="@if(isset($fellowship)){{$fellowship->phone}}@else{{old('phone')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">النوع</label>
									<div class="col-sm-10">
										<div class="radio">
											<label style="padding-left: 50px;">
												<input type="radio" name="gender" id="optionsRadios1" value="1" @if(isset($fellowship) && ($fellowship->gender==1))checked @endif>ذكر
											</label>    
											<label style="padding-left: 50px;">
												<input type="radio" name="gender" id="optionsRadios1" value="0" @if(isset($fellowship) && ($fellowship->gender==0))checked @endif>أنثى
											</label>
										</div>
										
									</div>
				                </div>
				                <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center"> النظام التابع له</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" name="system">
											<option selected="selected" value="0">لست تابع</option>
											@foreach($system as $val)
											<option value="{{$val->id}}"  @if(isset($fellowship) && ($fellowship->system_id==$val->id))selected @endif>{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center"> كلمة المرور</label>
									<div class="col-sm-10">
										<input type="password" name="password" class="form-control" placeholder="">
									</div>
								</div>
							</div>
							@if(isset($fellowship))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection