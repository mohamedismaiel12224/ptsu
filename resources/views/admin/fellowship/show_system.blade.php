@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        الأنظمة التابع لها {{$user->name}}
      </h1>
      <a href="{{url('create_sysuser/'.$user->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">الأنظمة التابع لها</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>اسم النظام</th>
                  <th>تاريخ البدء</th>
                  <th>تاريخ الأنتهاء</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sys as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{date('Y-m-d',strtotime($value->start_date))}}</td>
                    <td>{{date('Y-m-d',strtotime($value->end_date))}}</td>
                    <td style="display: flex">
                      
                      @if($value->system_id ==1)
                        @if($value->status==0)
                        <a href="{{url('sysuser/'.$value->id.'/1')}}" class="btn btn-primary" style="margin-right: 10px;">منح تصميم الحقائب التدريبية <i class="fa fa-pencil-square-o"></i></a>
                        @elseif($value->status==1)
                          <a href="{{url('sysuser/'.$value->id.'/0')}}" class="btn btn-warning" style="margin-right: 10px;">منع تصميم الحقائب التدريبية <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                        @if($value->status_arbitration==0)
                          <a href="{{url('sysuser_arbitration/'.$value->id.'/1')}}" class="btn btn-primary" style="margin-right: 10px;">منح تحكيم الحقائب التدريبية <i class="fa fa-pencil-square-o"></i></a>
                        @elseif($value->status_arbitration==1)
                          <a href="{{url('sysuser_arbitration/'.$value->id.'/0')}}" class="btn btn-warning" style="margin-right: 10px;">منع تحكيم الحقائب التدريبية <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                      @elseif($value->system_id ==3)
                        @if($value->status==0)
                        <a href="{{url('sysuser/'.$value->id.'/1')}}" class="btn btn-primary" style="margin-right: 10px;">منح مهارات العرض والتقديم  <i class="fa fa-pencil-square-o"></i></a>
                        @elseif($value->status==1)
                          <a href="{{url('sysuser/'.$value->id.'/0')}}" class="btn btn-warning" style="margin-right: 10px;">منع مهارات العرض والتقديم <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                        @if($value->status_arbitration==0)
                          <a href="{{url('sysuser_arbitration/'.$value->id.'/1')}}" class="btn btn-primary" style="margin-right: 10px;">منح التقييم الشامل للمدربين <i class="fa fa-pencil-square-o"></i></a>
                        @elseif($value->status_arbitration==1)
                          <a href="{{url('sysuser_arbitration/'.$value->id.'/0')}}" class="btn btn-warning" style="margin-right: 10px;">منع التقييم الشامل للمدربين <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                      @else
                        @if($value->status==0)
                          <a href="{{url('sysuser/'.$value->id.'/1')}}" class="btn btn-primary" style="margin-right: 10px;">منح <i class="fa fa-pencil-square-o"></i></a>
                        @elseif($value->status==1)
                          <a href="{{url('sysuser/'.$value->id.'/0')}}" class="btn btn-warning" style="margin-right: 10px;">منع  <i class="fa fa-pencil-square-o"></i></a>
                        @endif
                      @endif
                      <a href="{{url('sysuser/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                      <form action="{{url('sysuser/'.$value->id)}}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {{csrf_field()}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                      </form>

                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection