@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($sysuser))
				تعديل نظام
				@else
				اضافة نظام
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				@if(isset($sysuser))
				<li class="active">تعديل نظام</li>
				@else
				<li class="active">إضافة نظام</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($sysuser)) action="{{url('sysuser/'.$sysuser->id)}}" @else   action="{{url('sysuser')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
				                <div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">اسم النظام</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" name="system">
											@foreach($system as $val)
											<option value="{{$val->id}}"  @if(isset($sysuser) && ($sysuser->system_id==$val->id))selected @endif>{{$val->name}}</option>
											@endforeach
						                </select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">تاريخ الأنتهاء</label>
									<div class="col-sm-10">
										<input type="text" class="form-control pull-right" id="datepicker" name="updated_at"  value="@if(isset($sysuser)){{date('m/d/Y', strtotime($sysuser->updated_at))}}@else{{old('updated_at')}}@endif">
									</div>
								</div>
							</div>
							@if(isset($sysuser))
								<input type="hidden" name="_method" value="patch"> 
							@else
								<input type="hidden" name="user_id" value="{{$user_id}}">
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection