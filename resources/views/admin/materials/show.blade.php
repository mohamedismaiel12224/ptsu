@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        مرفقات النظام
      </h1>
      <a href="{{url('fellowships/'.$system->id)}}" class="btn btn-primary" style="margin-right: 10px;">إضافة <i class="fa fa-plus"></i></a>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">مرفقات النظام</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>المرفق</th>
                  <th>الصلاحية</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($materials as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    @php($x=explode('.',$value->url))
                    <td>
                    	<a href="@if(isset($x[1])&&$x[1]=='pdf'){{url('view/'.$value->url)}}@else {{url('assets/images/library/'.$value->url)}} @endif" target="_blank"> {{$value->name}}</a></td>
                    <td>@if($value->role==1) عرض فقط  @elseif($value->role==2) تحميل @elseif($value->role==3) عرض وتعديل @endif</td>
                    <td style="display: flex">
                    <form action="{{url('material/'.$value->id)}}" method="post">
                        <input type="hidden" name="_method" value="delete">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')" style="margin-right: 10px;">حذف <i class='fa fa-trash-o'></i></button>
                    </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection