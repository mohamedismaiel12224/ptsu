@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				اضافة مرفق
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('materials')}}">مرفقات النظام</a></li>
				<li class="active">اضافة</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" action="{{url('material')}}" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<input type="hidden" name="system_id" value="{{$sys_id}}"> 
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label center">اسم الملف</label>

									<div class="col-sm-10">
                                    	<input class="form-control" type="text" name="name">
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label center">ارفاق الملف</label>

									<div class="col-sm-10">
                                    	<input class="form-control" type="file" name="file">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">صلاحية الملف</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" name="role">
											<option value="1"  @if(isset($material) && ($material->role==1))selected @endif>عرض فقط</option>
											<option value="2"  @if(isset($material) && ($material->role==2))selected @endif>تحميل</option>
											<option value="3"  @if(isset($material) && ($material->role==3))selected @endif>عرض وتعديل</option>
											
						                </select>
									</div>
								</div>
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection