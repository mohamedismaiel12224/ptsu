@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($logo))
				تعديل @if($banners==1) بنرات اعلانية@else شركاء النجاح@endif
				@else
				اضافة @if($banners==1) بنرات اعلانية@else شركاء النجاح@endif
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				@if($banners==1) <li><a href="{{url('banners')}}">بنرات اعلانية</a></li>@else <li><a href="{{url('logos')}}">شركاء النجاح</a></li> @endif
				
				@if(isset($logo))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						@if($banners==1) 
							@php($x='banners')
						@else 
							@php($x='logos')
						@endif

						<form class="form-horizontal" method="post" @if(isset($logo)) action="{{url($x.'/'.$logo->id)}}" @else   action="{{url($x)}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label center">اللينك</label>

									<div class="col-sm-10">
                                    	<input class="form-control" type="text" name="link" value="@if(isset($logo)){{$logo->link}}@else{{old('link')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label center">اللوجو</label>

									<div class="col-sm-10">
                                    	<input class="form-control" type="file" name="file">
									</div>
								</div>
							</div>
							@if(isset($logo))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection