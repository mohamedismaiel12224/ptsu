@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                سلم التقدير
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                <li class="active">سلم التقدير</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <!-- right column -->
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                        </div>
                        @if(\session('success'))
                        <div class="alert alert-success">
                            {{\session('success')}}
                        </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ( $errors->all() as $error )
                                    <li>{{ $error }}</li>
                                @endforeach
                                </ul>
                            </div>
                        @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="post"  action="{{url('sys_degree_update/')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label center">متفوق</label>
                                    <div class="col-sm-10">
                                        من<input class="form-control" type="text" disabled="" value="0">إلى
                                        <input class="form-control" type="text" name="high_degree" value="{{$setting->high_degree}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label center">صعوبات </label>
                                    <div class="col-sm-10">
                                        من<input class="form-control" type="text" name="low_degree" value="{{$setting->low_degree}}">إلى
                                        <input class="form-control" type="text" disabled="" value="100">
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">تعديل</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection