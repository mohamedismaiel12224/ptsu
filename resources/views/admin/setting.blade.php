@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				الاعدادات
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li class="active">الأعدادات</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
						@if(\session('success'))
					    <div class="alert alert-success">
					        {{\session('success')}}
					    </div>
					    @endif
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post"  action="{{url('setting_update/')}}" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">العنوان</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="address" value="{{$setting->address}}">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الأيميل</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="email" value="{{$setting->email}}">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رقم التواصل</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="phone" value="{{$setting->phone}}">
									</div>
								</div>
<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">الفيس بوك</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="facebook" value="{{$setting->facebook}}">
									</div>
								</div>
<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">تويتر</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="twitter" value="{{$setting->twitter}}">
									</div>
								</div>
<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">لينكد ان</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="linkedin" value="{{$setting->linkedin}}">
									</div>
								</div>
<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center"> جوجل بلس</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" name="google" value="{{$setting->google}}">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">امكانية النقر على اللوجو</label>
									<div class="col-sm-10">
										<div class="radio">
											<label style="padding-left: 50px;">
												<input type="radio" name="link_partner" id="optionsRadios1" value="1" @if(isset($setting) && ($setting->link_partner==1))checked @endif>تمكين
											</label>    
											<label style="padding-left: 50px;">
												<input type="radio" name="link_partner" id="optionsRadios1" value="0" @if(isset($setting) && ($setting->link_partner==0))checked @endif>تعطيل
											</label>
										</div>
										
									</div>
				                </div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">من نحن</label>

									<div class="col-sm-10">
										<textarea id="editor1" class="form-control" name="we_are_ar" rows="10" cols="80">{{$setting->we_are_ar}}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">We are</label>

									<div class="col-sm-10">
										<textarea id="editor2" class="form-control" name="we_are" rows="10" cols="80">{{$setting->we_are}}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رسالتنا</label>

									<div class="col-sm-10">
										<textarea id="editor3" class="form-control" name="our_mission_ar" rows="10" cols="80">{{$setting->our_mission_ar}}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">Our Mission</label>

									<div class="col-sm-10">
										<textarea id="editor4" class="form-control" name="our_mission" rows="10" cols="80">{{$setting->our_mission}}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">رؤيتنا</label>

									<div class="col-sm-10">
										<textarea id="editor5" class="form-control" name="our_vision_ar" rows="10" cols="80">{{$setting->our_vision_ar}}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">Our Vision</label>

									<div class="col-sm-10">
										<textarea id="editor6" class="form-control" name="our_vision" rows="10" cols="80">{{$setting->our_vision}}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">خدماتنا</label>

									<div class="col-sm-10">
										<textarea id="editor7" class="form-control" name="our_rate_ar" rows="10" cols="80">{{$setting->our_rate_ar}}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">Our service</label>

									<div class="col-sm-10">
										<textarea id="editor8" class="form-control" name="our_rate" rows="10" cols="80">{{$setting->our_rate}}</textarea>
									</div>
								</div>
							</div>
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">تعديل</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection