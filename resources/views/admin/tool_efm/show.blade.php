@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        الأدوات
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">الأدوات</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th> # </th>
                  <th>الأداة</th>
                  <th>نوع الأداة</th>
                  <th> # </th>
                  
                </tr>
                </thead>
                <tbody>
                @foreach($tools as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->tool}}</td>
                    <td>@if($value->type_tool == 1 ) الأستبانة @elseif($value->type_tool == 2 ) المجموعات الضابطة والتجريبية @elseif($value->type_tool == 3 ) لا يحتاج @elseif($value->type_tool == 4 ) تقييم الأقران @elseif($value->type_tool == 5 ) تقارير التتبع @elseif($value->type_tool == 6 ) مراكز الاحصاءات @elseif($value->type_tool == 7 ) المقارنة الاحصائية  @elseif($value->type_tool == 8 ) كشاف المؤشر@endif</td>
                    <td style="display: flex">
                    <a href="{{url('tool_efm/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                    <form action="{{url('tool_efm/'.$value->id)}}" method="post">
                        <input type="hidden" name="_method" value="delete">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')">حذف <i class='fa fa-trash-o'></i></button>
                    </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection