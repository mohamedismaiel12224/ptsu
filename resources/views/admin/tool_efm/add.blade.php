@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($tool))
				تعديل أداة
				@else
				اضافة أداة
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('tool_efm')}}">الأدوات</a></li>
				@if(isset($tool))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($tool)) action="{{url('tool_efm/'.$tool->id)}}" @else   action="{{url('tool_efm')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">اسم الأداة</label>
									<div class="col-sm-10">
										<input type="text" name="tool" class="form-control" placeholder="" value="@if(isset($tool)){{$tool->tool}}@else{{old('tool')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">نوع الأداة</label>

									<div class="col-sm-10">
										<select class="form-control select2"  data-placeholder="اختر " style="width: 100%;" name="type_tool">
							                      <option value="0" selected disabled="">اختر</option>
							                      <option value="1" @if(isset($tool) && $tool->type_tool==1) selected @endif>الأستبانة</option>
							                      <option value="2" @if(isset($tool) && $tool->type_tool==2) selected @endif> المجموعات الضابطة والتجريبية </option>
							                      <option value="3" @if(isset($tool) && $tool->type_tool==3) selected @endif>لا يحتاج</option>
							                      <option value="4" @if(isset($tool) && $tool->type_tool==4) selected @endif>تقييم الأقران </option>
							                      <option value="5" @if(isset($tool) && $tool->type_tool==5) selected @endif>تقارير التتبع</option>
							                      <option value="6" @if(isset($tool) && $tool->type_tool==6) selected @endif> مراكز الاحصاءات</option>
							                      <option value="7" @if(isset($tool) && $tool->type_tool==7) selected @endif>المقارنة الاحصائية</option>
							                      <option value="8" @if(isset($tool) && $tool->type_tool==8) selected @endif>كشاف المؤشر</option>
							                    </select>
									</div>
								</div>
								
							</div>
							@if(isset($tool))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection