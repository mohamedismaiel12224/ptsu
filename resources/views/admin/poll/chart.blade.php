@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				نتيجة استطلاع رأى
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('poll')}}">استطلاع رأى</a></li>
				<li class="active">عرض النتائج</li>
			</ol>
		</section>
		<!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- Donut chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">نتيجة استطلاع رأى</h3>
            </div>
            <div class="box-body">
            	<div>
            		<h3>{{$poll->poll}}</h3>
            		<ol>
            			@foreach($suggest as $value)
            			<li><h4>{{$value->suggest}} ({{$value->percentage}}%)</h4></li>
            			@endforeach
            		</ol>
            	</div>
              <div id="donut-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

	</div>
@endsection