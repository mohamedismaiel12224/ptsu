@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($poll))
				تعديل استطلاع رأى
				@else
				اضافة استطلاع رأى
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				<li><a href="{{url('poll')}}">استطلاع رأى</a></li>
				@if(isset($poll))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
					        @if(\session('error'))
						    <div class="alert alert-danger">
						        {{\session('error')}}
						    </div>
						    @endif
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" method="post" @if(isset($poll)) action="{{url('poll/'.$poll->id)}}" @else   action="{{url('poll')}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">السؤال</label>

									<div class="col-sm-10">
										<input type="text" name="poll" class="form-control" placeholder="" value="@if(isset($poll)){{$poll->poll}}@else{{old('poll')}}@endif">
									</div>
								</div>
								@for($i=0;$i<4; $i++)
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label center">اقتراح {{$i+1}}</label>

									<div class="col-sm-10">
										<input type="text" name="suggest[{{$i}}]" class="form-control" placeholder="" value="@if(isset($suggest[$i])){{$suggest[$i]->suggest}}@endif">
										@if(isset($suggest[$i]))
										<input type="hidden" name="key[{{$i}}]" value="{{$suggest[$i]->id}}">
										@endif
									</div>
								</div>
								@endfor
							</div>
							@if(isset($poll))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection