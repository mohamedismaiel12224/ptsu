@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('show')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        استطلاعات الرأى
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li class="active">استطلاعات الرأى</li>
      </ol>
    </section>
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> # </th>
                  <th>السؤال</th>
                  <th>الخيار 1</th>
                  <th>الخيار 2</th>
                  <th>الخيار 3</th>
                  <th>الخيار 4</th>
                  <th>العمليات المتاحة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($poll as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->poll}}</td>
                    @for($i=0; $i<4; $i++)
                    <td>@if(isset($value->suggest[$i])){{$value->suggest[$i]->suggest}}@else - @endif</td>
                    @endfor
                    <td style="display: flex">
                    <a href="{{url('poll/'.$value->id)}}" class="btn btn-primary" style="margin-right: 10px;">عرض النتيجة <i class="fa fa-television"></i></a>
                    <a href="{{url('poll/'.$value->id.'/edit')}}" class="btn btn-success" style="margin-right: 10px;">تعديل <i class="fa fa-pencil-square-o"></i></a>
                    <form action="{{url('poll/'.$value->id)}}" method="post">
                        <input type="hidden" name="_method" value="delete">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('هل انت متأكد؟')">حذف <i class='fa fa-trash-o'></i></button>
                    </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection