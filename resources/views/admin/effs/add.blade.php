@extends('layouts.admin')
@section('title', 'PTSU | Admin')
@section('add')
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				@if(isset($page))
				تعديل @if($norms==1) معايير الأنظمة المهنية@else اختبارات الكفاءة@endif
				@else
				اضافة @if($norms==1) معايير الأنظمة المهنية@else اختبارات الكفاءة@endif
				@endif
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
				@if($norms==1) <li><a href="{{url('norms')}}">معايير الأنظمة المهنية</a></li>@else <li><a href="{{url('effs')}}">اختبارات الكفاءة</a></li> @endif
				
				@if(isset($page))
				<li class="active">تعديل</li>
				@else
				<li class="active">إضافة</li>
				@endif
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<!-- right column -->
				<div class="col-md-12">
					<!-- Horizontal Form -->
					<div class="box box-info">
						<div class="box-header with-border">
						</div>
					    @if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
						<!-- /.box-header -->
						<!-- form start -->
						@if($norms==1) 
							@php($x='norms')
						@else 
							@php($x='effs')
						@endif

						<form class="form-horizontal" method="post" @if(isset($page)) action="{{url($x.'/'.$page->id)}}" @else   action="{{url($x)}}" @endif enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="box-body">
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label center">الأسم</label>

									<div class="col-sm-10">
                                    	<input class="form-control" type="text" name="name" value="@if(isset($page)){{$page->name}}@else{{old('name')}}@endif">
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label center">الصفحة</label>
									<div class="col-sm-10">
                                    	<textarea id="editor1" class="form-control" name="detail" rows="10" cols="80">@if(isset($page)){{$page->detail}}@else{{old('detail')}}@endif</textarea>
									</div>
								</div>
							</div>
							@if(isset($page))
								<input type="hidden" name="_method" value="patch"> 
							@endif
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-info pull-right">حفظ</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>

				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection