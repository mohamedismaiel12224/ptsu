@extends('layouts.layout')
@section('title', 'PTSU | System detail')
@section('systems')
	
<!-- ========================== Innaer Banner ========================= -->
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{$system->name}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{$system->name}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->
<!-- ========================== /Innaer Banner ========================= -->



<!-- ===================== Service details page ================== -->

	<section class="container">
		<div class="row">
			<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 parctise_details_page_content">
				<img src="{{asset('assets/images/home/'.$system->logo)}}" alt="image" class="img-responsive" width="500px">

				<div class="title">
					<h3>{{$system->name}}</h3>
				</div><!--  /title -->
				<div class="text" style="padding-bottom: 55px">
					<p>{!! $system->detail!!}</p>
				</div> <!-- /text -->
			</div> <!-- /parctise_details_page_content -->

<!-- =========================== Side bar ========================= -->

			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 side_bar">
				<!-- ========== Practise category ========== -->
				@if(isset($materials))
				<div class="practise_category">
					<h4>{{trans('lang.materials')}}</h4>
					<ul>
						@foreach($materials as $key=> $value)
						@php($x=explode('.',$value->url))
						<li><a href="@if(isset($x[1])&&$x[1]=='pdf'){{url('view/'.$value->url)}}@else {{url('assets/images/library/'.$value->url)}} @endif" @if($value->role==2) download @endif target="_blank" class="transition"><i class="fa fa-link" aria-hidden="true"></i> {{$value->name}}</a></li>
						@endforeach
					</ul>
				</div> <!-- /practise_category -->
				@endif
				<!-- =========== Our Brochure ============= -->

				<!-- <div class="our_brochure">
					<h4>Our Brochure</h4>
					<p>Our 2016 financial prospectus brochure for easy to read guide all of the services offered.</p>
					<a href="#" class="transition3s" download>Download.PDF</a>
				</div> 

				<div class="call_us">
					<p>Have any Question ?<br>Call Us :</p>
					<a href="tel:(+880)1723801729">(+880) 1723801729</a>
					<span>For Details</span>
				</div>  -->
			</div> <!--  /side_bar -->
		</div>
	</section>
<!-- ===================== /Service details page ================== -->


@endsection