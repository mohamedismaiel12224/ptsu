@extends('layouts.layout')
@section('title', 'PTSU | Videos')
@section('developers')
<!-- ========================== Innaer Banner ========================= -->
<section id="inner_banner">
	<div class="overlay">
		<div class="container">
			<h3>{{trans('lang.introductory_videos')}}</h3>
			<ul>
				<li><a href="{{url('/')}}">Home</a></li>
				<li>/</li>
				<li>{{trans('lang.introductory_videos')}}</li>
			</ul>
		</div>
	</div>
</section> <!-- /inner_banner -->
<section class="gallery container">
	<div class="title_holder_center title_holder">
		<h3>{{trans('lang.introductory_videos')}}</h3>
		<span></span>
	</div> <!-- /title_holder_center -->
	<div class="gallery_item_wrapper row" id="mixitup_list">
		@foreach($videos as $value)		
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 h350">
			<div class="single_item">
				<div class="img_holder">
					{!! $value->iframe !!}
				</div> <!-- End of .img_holder -->
			</div> <!-- End of .single_item -->
		</div> <!-- End of .mix -->
		@endforeach
	</div> <!-- End of #mixitup_list -->
</section> <!-- End of .gallery -->
@endsection