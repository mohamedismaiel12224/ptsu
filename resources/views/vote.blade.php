@extends('layouts.layout')
@section('title', 'PTSU Votes')
@section('elibrary')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.polls')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{trans('lang.polls')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->
<!-- ========================== /Innaer Banner ========================= -->
<div class="faq container">
		<div class="heading">
			<h3>{{trans('lang.polls')}}</h3>
			<p>{{trans('lang.share_us_your_opinion_to_acheive_the_best_service_for_you')}}</p>
		</div> <!-- /heading -->
		<div class="tab_holder">
			<div class="tab_wrapper" style="width:100%">
				<div class="choose_us_panel">
					<div class="panel-group theme-accordion" id="accordion">
						@foreach($poll as $key=>$value)
					  	<div class="panel col-md-6">
							<div class="panel-heading active-panel">
							  <h4 class="panel-title">
							    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key+1}}">{{$value->poll}}</a>
							  </h4>
							</div>
							<div id="collapse{{$key+1}}" class="panel-collapse collapse">
							  <div class="panel-body panel{{$value->id}}"  data-poll_id="{{$value->id}}">
							  	@foreach($value->suggest as $val)
							  	<div class="single_progress_skills">										
									<h5><input class="radioo" type="radio" name="s[{{$value->id}}]" value="{{$val->id}}"> {{$val->suggest}}</h5>
							  		<span>{{$val->percentage}}%</span>
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$val->percentage}}%;"></div>
									</div>
								</div>
								@endforeach
								<button type="button" class="my_btn">Ok</button>
							  </div>
							</div>
						</div>
						@endforeach
					</div> <!-- end #accordion -->
				</div> <!-- /choose_us_panel -->
			</div> <!-- /tab_wrapper -->
		</div> <!-- /tab_holder -->
	</div> <!-- /faq -->

@endsection