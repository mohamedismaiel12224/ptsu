@extends('layouts.layout')
@section('title', 'PTSU | Systems')
@section('systems')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.professional_systems')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{trans('lang.professional_systems')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->



<!-- ======================== Blog Page content ======================= -->

	<section class="container our-service m-top0">
		<div class="practise_item_wrapper m-top0">
			<div class="service-item clearfix block">
				@foreach($systems as $value)
		
				<div class="single-service border_right">
						<div class="icon">
							<img src="{{asset('assets/images/home/'.$value->logo)}}" alt="blog image" height="100px" >
							<h4>{{$value->name}}</h4>
						</div> <!-- End .icon -->
						<div class="hover_overlay transition4s">
							<h4>{{$value->name}}</h4>
							<p>{{mb_substr($value->detail,0,100,'utf-8')}}</p>
							<a href="{{url('system/'.$value->id)}}">{{trans('lang.see_more')}}</a>
						</div> <!-- End .hover_overlay -->
					</div> <!-- End .single-service -->
				@endforeach
			</div> <!-- End /row -->
			</div> <!-- / blog_post -->
	</section> <!-- /blog_with_sidebar -->
<!-- ======================== /Blog Page content ======================= -->
@endsection