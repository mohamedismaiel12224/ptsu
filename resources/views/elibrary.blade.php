@extends('layouts.layout')
@section('title', 'PTSU E-library')
@section('elibrary')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.elibrary')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{trans('lang.elibrary')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->
<!-- ========================== /Innaer Banner ========================= -->
<!-- ======================= Our Service Style One =========================== -->
		<section class="container our-service m-top0">
			<div class="practise_item_wrapper m-top0">
				<div class="service-item clearfix" style="margin-bottom: 55px">
					@foreach($elibrary as $value)
					<div class="single-service border_right">
						<div class="icon">
							<h4>{{$value->section}}</h4>
						</div> <!-- End .icon -->
						<div class="hover_overlay transition4s">
							<h4>{{$value->section}}</h4>
							<a href="{{url('elibraries/'.$value->id)}}">{{trans('lang.show_materials')}}</a>
						</div> <!-- End .hover_overlay -->
					</div> <!-- End .single-service -->
					
					@endforeach
				</div> <!-- End .service-item -->
			</div> <!-- End .practise_item_wrapper -->
		</section>		

<!-- ========================= /Our Service Style One ======================== -->
@endsection