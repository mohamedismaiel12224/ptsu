@extends('layouts.layout')
@section('title', 'PTSU | Login')
@section('login')
		<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.login')}}</h3>
			</div>
		</div>
	</section> <!-- /inner_banner -->

<!-- ============================ Contact Form =========================== -->


	<div class="contact_us_form">
		<div class="container">
			@if(\session('error'))
			<div class="alert alert-danger">
			    {{\session('error')}}
			</div>
			@endif
			<form action="{{url('login2')}}" class="form-validation" method="post">
				<h4>{{trans('lang.we_welcome_you_in_ptsu')}}</h4>
				<div class="input_wrapper row">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
						<input name="email" type="email" placeholder="{{trans('lang.email')}}">
					</div>
					<div class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
						<input name="password" type="password" placeholder="{{trans('lang.password')}}">
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button class="transition3s" title="Send">{{trans('lang.login')}}</button>
					</div>
				</div> <!-- /input_wrapper -->
			</form>
		</div>
	</div> <!-- /contact_form -->

<!-- ============================ /Contact Form =========================== -->
@endsection