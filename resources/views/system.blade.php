@extends('layouts.layout')
@section('title', 'PTSU | Systems')
@section('systems')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.professional_systems')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{trans('lang.professional_systems')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->



<!-- ======================== Blog Page content ======================= -->

	<section class="blog_with_sidebar blog_grid container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog_post">

				<div class="row" style="">
				@foreach($materials as $key=> $value)
				<!-- single post -->
				<div class="blog_single_post col-lg-2 col-md-2 col-sm-6 col-xs-12">
					<div class="img_holder">
						<a href="{{url('view/'.$value->url)}}" @if($value->role==2) download @endif target="_blank" class="transition"><i class="fa fa-link" aria-hidden="true"></i>  {{$value->name}}</a>
					</div> <!-- /img_holder -->
				</div> <!-- /blog_single_post -->
				@endforeach
			</div> <!-- End /row -->
			</div> <!-- / blog_post -->
		</div> <!-- /row -->
	</section> <!-- /blog_with_sidebar -->
<!-- ======================== /Blog Page content ======================= -->
@endsection