<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from creativegigs.net/html/invo-html/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 29 Oct 2017 17:50:46 GMT -->
<head>
	<meta charset="UTF-8">

	<!-- For IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- For Resposive Device -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>@yield('title')</title>

	<!-- Favicon -->
	<link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets/images/fav-icon/favicon-96x96.png')}}">


	<!-- Custom Css -->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive/responsive.css')}}">
	<link rel="stylesheet" type="text/css" media="print" href="{{asset('assets/dist/css/print2.css')}}">
	@if(check_language())
    <link href="{{asset('assets/css/custom/rtl.css')}}" rel="stylesheet">
    @endif

	<!--[if lt IE 9]>
   		<script src="js/html5shiv.js"></script>
	<![endif]-->
<script type="text/javascript">
	var _token="{{csrf_token()}}";
	var base_url='{{url('/')}}';
</script>
</head>

<body class="home">
	 <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

	<!-- Scroll Top Button -->
		<button class="scroll-top transition3s">
			<i class="fa fa-angle-up" aria-hidden="true"></i>
		</button>

	


		
<!-- ========================== Header ==================== -->

	<header class="main_menu no-print">
		<div class="container nav_wrapper">
			<nav class="navbar navbar-default">
			   <!-- Brand and toggle get grouped for better mobile display -->
			   <div class="navbar-header">
			   	<a class="navbar-brand navbar-logo" href="{{url('/')}}">
		        	<img alt="Brand" src="{{asset('assets/images/logo/logo.png')}}" class="img-responsive" width="185px">
		      	</a>
			     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
			       <span class="sr-only">Toggle navigation</span>
			       <span class="icon-bar"></span>
			       <span class="icon-bar"></span>
			       <span class="icon-bar"></span>
			     </button>
			   </div>
			   <!-- Collect the nav links, forms, and other content for toggling -->
			   <div class="collapse navbar-collapse" id="navbar-collapse-1">
			     <ul class="nav navbar-nav">
			       <li><a href="{{url('/')}}" class="transition-ease">{{trans('lang.home')}}</a></li>
			       <li><a href="{{url('certifications')}}" class="transition-ease">{{trans('lang.certifications')}}</a></li>
			       <li><a href="{{url('systems_view')}}" class="transition-ease">{{trans('lang.system')}}</a>
			       <li><a href="{{url('votes')}}" class="transition-ease">{{trans('lang.polls')}}</a></li>
			       @if(count($efficients)>0)
			       <li class="dropdown_menu"><a href="#" class="transition-ease">{{trans('lang.efficients')}}</a>
			       		<ul class="sub_menu">
			       		@foreach($efficients as $val)
							<li><a href="{{url('eff/'.$val->id)}}" class="transition3s">{{$val->name}}</a></li>
						@endforeach
						</ul>
			       </li>
			       @endif
			       @if(count($norms)>0)
			       <li class="dropdown_menu"><a href="#" class="transition-ease">{{trans('lang.norms')}}</a>
			       		<ul class="sub_menu">
			       		@foreach($norms as $val)
							<li><a href="{{url('norm/'.$val->id)}}" class="transition3s">{{$val->name}}</a></li>
						@endforeach
						</ul>
			       </li>
			       @endif
			       <li><a href="{{url('contact_us')}}" class="transition-ease">{{trans('lang.contact_us')}}</a></li>
			       @if (Auth::guest())
			       <li><a href="{{url('login')}}" class="transition-ease">{{trans('lang.login')}}</a>
			       </li>
			       @else
			       <li class="dropdown_menu"><a href="#" class="transition-ease">{{Auth::user()->name}}</a>
			       		<ul class="sub_menu">
							<li><a href="{{url('profile/')}}" class="transition3s">{{trans('lang.profile')}}</a></li>
							<li><a href="{{url('logout/')}}" class="transition3s">{{trans('lang.logout')}}</a></li>
						</ul>
			       	</li>
                    @endif
                    @if(check_language())
                    <li><a href="{{ url("change_language/en") }}">English</a></li>
                    @else
                    <li><a href="{{ url("change_language/ar") }}">عربى</a></li>
                    @endif
			     </ul>
			   </div><!-- /.navbar-collapse -->
			</nav>
		</div><!--  End of .nav_wrapper -->
	</header>

<!-- ========================== /Header =================== -->

<section class="wrap">
	@yield('home')
	@yield('login')
	@yield('about')
	@yield('developers')
	@yield('systems')
	@yield('elibrary')
	@yield('certificate')
</section>

<!-- =============================== Bottom Banner ====================== -->
	<div class="bottom_banner no-print">
		<div class="container">
			<div class="text flt_left">
			</div> <!-- End .text -->
			<div class="clear_fix"></div>
		</div> <!-- End .container -->
	</div> <!-- End .bottom_banner -->
<!-- =============================== /Bottom Banner ====================== -->
<!-- ==================================== Footer ============================ -->


	<footer class="no-print">
		<div class="overlay">
			<div class="container">
				<div class="row">
					
					<!-- _______ Logo ______ -->

					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 logo_footer">
						<a href="{{url('/')}}"><img src="{{asset('assets/images/home/footer-logo.png')}}" alt="logo" class="img-responsive" width="110px"></a>
						<p>{{trans('lang.professional_training_systems_union')}}</p>
						<div class="address">
							<span><i class="fa fa-map-marker"></i> {{$setting->address}}</span>
							<span><i class="fa fa-phone"></i> {{$setting->phone}}</span>
							<span><i class="fa fa-envelope-o"></i> {{$setting->email}}</span>
						</div> <!-- End .address -->

						<ul class="social_icon">
							<li class="round_border transition3s"><a href="{{$setting->facebook}}"><i class="fa fa-facebook"></i></a></li>
							<li class="round_border transition3s"><a href="{{$setting->twitter}}"><i class="fa fa-twitter"></i></a></li>
							<li class="round_border transition3s"><a href="{{$setting->google}}"><i class="fa fa-google-plus"></i></a></li>
							<li class="round_border transition3s"><a href="{{$setting->linkedin}}"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div> <!-- End .logo_footer -->
					
					<!-- _____ Service ________ -->
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 service_footer">
						<h4>{{trans('lang.professional_systems')}}</h4>

						<ul>
							@foreach($sys as $val)
							<li><a href="{{url('systems_view')}}" class="transition3s">{{$val->name}}</a></li>
							@endforeach
						</ul>
					</div> <!-- End .service_footer -->

					<!-- ____________ News area ___________ -->
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 news_footer">
						<h4>{{trans('lang.e_library')}}</h4>
						@foreach($elib as $val)
						<div class="news_post">
							<a href="{{url('elibraries/'.$val->id)}}"><p>{{$val->section}}</p></a><br>
							<span>{{date('d,M Y',strtotime($val->updated_at))}}</span>
						</div> <!-- End .news_post -->
						@endforeach
					</div> <!-- End .news_footer -->

					<!-- _____________ Contact Form _________________ -->

					<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 contact_form_footer">
						<h4>{{trans('lang.emailing_form')}}</h4>
						@if (count($errors) > 0)
					        <div class="alert alert-danger">
					        	<ul>
					            @foreach ( $errors->all() as $error )
					                <li>{{ $error }}</li>
					            @endforeach
					            </ul>
					        </div>
					    @endif
					    @if(\session('success'))
					    <div class="alert alert-success">
					        {{\session('success')}}
					    </div>
					    @endif
						<form action="{{url('mailing')}}" method="post" class="footer-form-validation">
							{{csrf_field()}}
							<input name="email" type="email" placeholder="{{trans('lang.email')}}" title="ادخل ايميلك">
							<input name="phone" type="text" placeholder="{{trans('lang.phone')}}" title="ادخل رقم جوالك">
							<button type="submit" title="Send Us" class="hover_btn transition3s">{{trans('lang.send')}}</button>
						</form>
					</div> <!-- End .contact_form_footer-->
				</div>
			</div> <!-- End .container -->

			<!-- <p class="bottom_text">Theme Created By <a href="" target="_blank"> Creative Gigs </a> with love</p> -->
		</div> <!-- End .overlay -->
	</footer>



<!-- ==================================== /Footer ============================ -->



	<!-- ==================Js File============== -->

	<!-- j Query -->
	<script type="text/javascript" src="{{asset('assets/js/jquery-2.1.4.js')}}"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>
	<!-- revolution -->
	<script src="{{asset('assets/vendor/revolution/jquery.themepunch.tools.min.js')}}"></script>
	<script src="{{asset('assets/vendor/revolution/jquery.themepunch.revolution.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendor/revolution/revolution.extension.slideanims.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendor/revolution/revolution.extension.layeranimation.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendor/revolution/revolution.extension.navigation.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendor/revolution/revolution.extension.kenburn.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendor/revolution/revolution.extension.actions.min.js')}}"></script>
	<!-- Owl carousel -->
	<script src="{{asset('assets/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
	<!-- Fancybox js -->
	<script type="text/javascript" src="{{asset('assets/vendor/fancy-box/jquery.fancybox.pack.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendor/fancy-box/jquery.fancybox-media.js')}}"></script>
	<!-- js count to -->
	<script type="text/javascript" src="{{asset('assets/vendor/jquery.appear.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendor/jquery.countTo.js')}}"></script>

	<!-- Jquery ui js -->
	<script type="text/javascript" src="{{asset('assets/vendor/jquery-ui/jquery-ui.min.js')}}"></script>
	<!-- Validation -->
	<script type="text/javascript" src="{{asset('assets/js/validate.js')}}"></script>
	
	<!-- Theme js -->
	<script type="text/javascript" src="{{asset('assets/js/theme.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/dev.js')}}"></script>

</body>

<!-- Mirrored from creativegigs.net/html/invo-html/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 29 Oct 2017 17:52:41 GMT -->
</html>