<ul class="sidebar-menu" data-widget="tree">
	<li>
		<a href="{{url('mit')}}">
			<i class="fa fa-dashboard"></i> <span>مشاريعى</span>
		</a>
	</li>
@if($mit_pro ==1)
	<li class="header center">نموذج Tcom</li>
	<li>
		<a href="{{url('training_outputs/'.$mit->id)}}">
			<i class="fa fa-outdent"></i> <span>نواتج التدريب</span>
		</a>
	</li>
	@if($mit->step_no>1)
	<li>
		<a href="{{url('colleration/'.$mit->id)}}">
			<i class="fa fa-tasks"></i> <span>الأرتباط</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>2)
	<li>
		<a href="{{url('vertical/'.$mit->id)}}">
			<i class="fa fa-columns"></i> <span>عمل ترابط رأسي</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>3)
	<li>
		<a href="{{url('program_level/'.$mit->id)}}">
			<i class="fa fa-industry"></i> <span>مستوى البرنامج</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>4)
	<li class="header center">نموذج MIT</li>
	<li>
		<a href="{{url('select_level/'.$mit->id)}}">
			<i class="fa fa-industry"></i> <span>اختيار مستوى البرنامج</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>5)
	<li>
		<a href="{{url('mit_type/'.$mit->id)}}">
			<i class="fa fa-road"></i> <span>النوع</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>6)
	<li>
		<a href="{{url('learning_level/'.$mit->id)}}">
			<i class="fa fa-leanpub"></i> <span>مستوى التعلم</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>7)
	<li>
		<a href="{{url('practice/'.$mit->id)}}">
			<i class="fa fa-soccer-ball-o"></i> <span>الممارسة</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>8)
	<li>
		<a href="{{url('training_method/'.$mit->id)}}">
			<i class="fa fa-bullseye"></i> <span>طرق التدريب</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>9)
	<li>
		<a href="{{url('learning_planner/'.$mit->id)}}">
			<i class="fa fa-line-chart"></i> <span>مخطط التعلم</span>
		</a>
	</li>
	<li>
		<a href="{{url('tem/'.$mit->id)}}">
			<i class="fa fa-industry"></i> <span>نموذج TEM</span>
		</a>
	</li>
	@endif
@else
	<li>
		<a href="{{url('training_outputs/'.$mit->id)}}">
			<i class="fa fa-outdent"></i> <span>نموذج Tcom</span>
		</a>
	</li>
	<li>
		<a href="{{url('learning_planner/'.$mit->id)}}">
			<i class="fa fa-line-chart"></i> <span>مخطط التعلم</span>
		</a>
	</li>
	@if($mit->step_no>9)
	<li class="header center">نموذج TEM</li>
	<li>
		<a href="{{url('tem/'.$mit->id)}}">
			<i class="fa fa-industry"></i> <span>نموذج TEM</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>10)
	<li class="header center">استئناف نموذج MIT</li>
	<li>
		<a href="{{url('collect_content/'.$mit->id)}}">
			<i class="fa fa-align-center"></i> <span>جمع المحتوى</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>11)
	<li>
		<a href="{{url('training_activities/'.$mit->id)}}">
			<i class="fa fa-tasks"></i> <span>الأنشطة التدريبية</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>12)
	<li>
		<a href="{{url('achievement/'.$mit->id)}}">
			<i class="fa fa-map-signs"></i> <span>الفواصل وفترة الأنجاز</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>13)
	<li>
		<a href="{{url('branches/'.$mit->id)}}">
			<i class="fa fa-asterisk"></i> <span>الأفرع الرئيسية</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>14)
	<li>
		<a href="{{url('sessions/'.$mit->id)}}">
			<i class="fa fa-calendar-check-o"></i> <span>الجلسات التدريبية</span>
		</a>
	</li>
	@endif
@endif
</ul>