<ul class="sidebar-menu" data-widget="tree">
  <li>
    <a href="{{url('arbitrator/mit/'.$mit->id)}}">
      <i class="fa fa-dashboard"></i> <span>البيانات العامة</span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-folder-open"></i> <span>عمليات التحكيم</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="active"><a href="{{url('arbitrator/mit/training/'.$mit->id)}}"><i class="fa fa-outdent"></i>تحكيم نواتج التدريب</a></li>
      <li><a href="{{url('arbitrator/mit/content/'.$mit->id)}}"><i class="fa fa-align-center"></i> تحكيم المحتوى التدريبي</a></li>
      <li><a href="{{url('arbitrator/mit/activities/'.$mit->id)}}"><i class="fa fa-tasks"></i> تحكيم الأنشطة التدريبية</a></li>
    </ul> 
  </li>
  <li>
    <a href="{{url('arbitrator/mit/evaluate/'.$mit->id)}}">
      <i class="fa fa-map-o"></i> <span>التقيم العام</span>
    </a>
  </li>
</ul>