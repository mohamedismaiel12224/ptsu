<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>@yield('title')</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.7 -->
		<link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="{{asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
		<!-- Ionicons -->
		<link rel="stylesheet" href="{{asset('assets/bower_components/Ionicons/css/ionicons.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
		
		<link rel="stylesheet" href="{{asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

		<!-- AdminLTE Skins. Choose a skin from the css/skins
				 folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="{{asset('assets/dist/css/skins/_all-skins.min.css')}}">
		<!-- Morris chart -->
		<link rel="stylesheet" href="{{asset('assets/bower_components/morris.js/morris.css')}}">
		<!-- jvectormap -->
		<link rel="stylesheet" href="{{asset('assets/bower_components/jvectormap/jquery-jvectormap.css')}}">
		<!-- Date Picker -->
		<link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
		<!-- Daterange picker -->
		<link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
		<!-- bootstrap wysihtml5 - text editor -->
		<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Google Font -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		
		<!-- Theme style -->
		<link rel="stylesheet" href="{{asset('assets/dist/css/AdminLTE.min.css')}}">
		<link rel="stylesheet" type="text/css" media="print" href="{{asset('assets/dist/css/print_admin.css')}}">

		<script type="text/javascript">
			var _token="{{csrf_token()}}";
			var base_url='{{url('/')}}';
		</script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper" >

			<header class="main-header">
				<!-- Logo -->
				<a href="{{url('admin')}}" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini">PTSU</span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>PTSU</b></span>
				</a>
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>

					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
									<span class="hidden-xs">{{Auth::user()->name}}</span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

										<p>
											 إتحاد الأنظمة المهنية للتدريب 
										</p>
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<!-- <div class="pull-left">
											<a href="#" class="btn btn-default btn-flat">Profile</a>
										</div> -->
										<div class="pull-right">
											<a href="{{url('logout')}}" class="btn btn-default btn-flat">تسجيل الخروج</a>
										</div>
									</li>
								</ul>
							</li>
							<!-- Control Sidebar Toggle Button -->
							<li>
								<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
					<!-- Sidebar user panel -->
					<div class="user-panel">
						<div class="pull-left image">
							<img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
						</div>
						<div class="pull-left info">
							<p>{{Auth::user()->name}}</p>
							<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
						</div>
					</div>
					<!-- search form -->
					<!-- <form action="#" method="get" class="sidebar-form">
						<div class="input-group">
							<input type="text" name="q" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
										<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
										</button>
									</span>
						</div>
					</form> -->
					<!-- /.search form -->
					<!-- sidebar menu: : style can be found in sidebar.less -->
					@if(Auth::user()->role==1)
						@if(isset($system_setting))
							@include('layouts.sidebar_system_setting')
						@else
							@include('layouts.sidebar_admin')
						@endif
					@elseif(Auth::user()->role==3)
						@if(isset($mit_pro))
							@include('layouts.sidebar_mit')
						@elseif(isset($mit_arb_pro))
							@include('layouts.sidebar_mit_arb')
						@elseif(isset($need_pro))
							@include('layouts.sidebar_need')
						@elseif(isset($performance_evaluate_pro))
							@include('layouts.sidebar_performance_evaluate')
						@elseif(isset($etpm_pro))
							@include('layouts.sidebar_etpm')
						@elseif(isset($efm_pro))
							@include('layouts.sidebar_efm')
						@else
							@include('layouts.sidebar_fellowship')
						@endif
					@elseif(Auth::user()->role==4)
						@if(isset($arbitrator_mit))
							@include('layouts.sidebar_arbitrator_mit')
						@elseif(isset($performance_evaluator_sidebar))
							@include('layouts.sidebar_arbitrator_performance')
						@elseif(isset($etpm_evaluator_sidebar))
							@include('layouts.sidebar_arbitrator_etpm')
						@endif
					@endif
				</section>
				<!-- /.sidebar -->
			</aside>
			@yield('home')
			@yield('show')
			@yield('add')
			<footer class="main-footer no-print">
				<strong>Copyright &copy;  <a href="{{url('/')}}">PTSU</a>.</strong>
			</footer>
			<div class="control-sidebar-bg"></div>
		</div>
		<!-- jQuery 3 -->
		<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
		<!-- jQuery UI 1.11.4 -->
		<script src="{{asset('assets/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		<!-- Bootstrap 3.3.7 -->
		<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('assets/dist/js/bootbox.min.js')}}"></script>
		<!-- Morris.js charts -->
		<script src="{{asset('assets/bower_components/raphael/raphael.min.js')}}"></script>
		<script src="{{asset('assets/bower_components/morris.js/morris.min.js')}}"></script>
		<script src="{{asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
		<!-- Sparkline -->
		<script src="{{asset('assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
		<!-- jvectormap -->
		<script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
		<script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
		<!-- jQuery Knob Chart -->
		<script src="{{asset('assets/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
		<!-- daterangepicker -->
		<script src="{{asset('assets/bower_components/moment/min/moment.min.js')}}"></script>
		<script src="{{asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
		<!-- datepicker -->
		<script src="{{asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<script src="{{asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
		<!-- Slimscroll -->
		<script src="{{asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
		<!-- FastClick -->
		<script src="{{asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
		<script src="{{asset('assets/bower_components/fastclick/lib/fastclick.js')}}"></script>
		<!-- AdminLTE App -->
		<script src="{{asset('assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
		<script src="{{asset('assets/dist/js/adminlte.min.js')}}"></script>
		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<script src="{{asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
		<script src="{{asset('assets/dist/js/pages/dashboard.js')}}"></script>
		<script src="{{asset('assets/bower_components/Flot/jquery.flot.js')}}"></script>
		<script src="{{asset('assets/bower_components/Flot/jquery.flot.resize.js')}}"></script>
		<script src="{{asset('assets/bower_components/Flot/jquery.flot.pie.js')}}"></script>
		<script src="{{asset('assets/bower_components/Flot/jquery.flot.categories.js')}}"></script>
		<script src="{{asset('assets/bower_components/morris.js/morris.min.js')}}"></script>
		<script src="{{asset('assets/bower_components/Flot/jquery.flot.js')}}"></script>
		<script src="{{asset('assets/bower_components/Flot/jquery.flot.resize.js')}}"></script>
		<script src="{{asset('assets/bower_components/Flot/jquery.flot.pie.js')}}"></script>
		<script src="{{asset('assets/bower_components/Flot/jquery.flot.categories.js')}}"></script>
		<script src="{{asset('assets/dist/js/demo.js')}}"></script>
		@if(isset($chart))
		<script>
			$(function () {
			    var donutData = [
			      @foreach($suggest as $key=>$value)
			      { label: 'الأقتراح {{$key+1}}', data: {{$value->percentage}}, color: '#{{$value->color}}' },
			      @endforeach
			    ]
			    $.plot('#donut-chart', donutData, {
			      series: {
			        pie: {
			          show       : true,
			          radius     : 1,
			          innerRadius: 0.5,
			          label      : {
			            show     : true,
			            radius   : 2 / 3,
			            formatter: labelFormatter,
			            threshold: 0.1
			          }

			        }
			      },
			      legend: {
			        show: false
			      }
			    })
  			})
		</script>
		@endif
		@if(isset($trainers_bar))
		<script>
	    var bar_data = {
	      data : [
	      @foreach($trainers_bar as $bar)
	      ['{{$bar->name}}', {{$bar->sum/$bar->count}}], 
	      @endforeach
	      ],
	      color: '#3c8dbc'
	    }
	    $.plot('#bar-chart', [bar_data], {
	      grid  : {
	        borderWidth: 1,
	        borderColor: '#f3f3f3',
	        tickColor  : '#f3f3f3'
	      },
	      series: {
	        bars: {
	          show    : true,
	          barWidth: 0.5,
	          align   : 'center'
	        }
	      },
	      xaxis : {
	        mode      : 'categories',
	        tickLength: 0
	      }
	    })
	    /* END BAR CHART */
		</script>
    	@endif
    	@if(isset($performance_evaluate_chart))
    	<script>
		  $(function () {
		    
		    /*
		     * LINE CHART
		     * ----------
		     */
		    //LINE randomly generated data
		    @foreach($performance_evaluate_chart as $key_ev=> $evaluator)
		    @foreach($evaluator->domain as $key=>$domain)
		    	var plot_arr = [];
		    	@foreach($domain->group as $key_group=>$group)
				    var sin= [];
				    @foreach($group->checkup as $key_check=>$value)
				    	sin.push(['{{$key_check+1}}', {{$value->evaluate}}])
				    @endforeach
				    var line_data = {
				      data : sin,
				      color: '#{{$group->color}}',
				    }
				    plot_arr.push(line_data);
			    @endforeach
		    $.plot('#line-chart-{{$key_ev+1}}-{{$key+1}}', plot_arr, {
		      grid  : {
		        hoverable  : true,
		        borderColor: '#f3f3f3',
		        borderWidth: 1,
		        tickColor  : '#f3f3f3'
		      },
		      series: {
		        shadowSize: 0,
		        lines     : {
		          show: true
		        },
		        points    : {
		          show: true
		        }
		      },
		      lines : {
		        fill : false,
		        color: ['#3c8dbc', '#f56954']
		      },
		      yaxis : {
		        show: true
		      },
		      xaxis : {
		        show: false,
		      }
		    })
		    @endforeach
		    @endforeach
		    /* END LINE CHART */

		  })

		  /*
		   * Custom Label formatter
		   * ----------------------
		   */
		  function labelFormatter(label, series) {
		    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
		      + label
		      + '<br>'
		      + Math.round(series.percent) + '%</div>'
		  }
		</script>
    	@endif
    	@if(isset($performance_evaluate2_chart))
            @foreach($performance_evaluate2_chart as $key=> $domain)
            <script>
            	var bar_data = {
				      data : [
				      @foreach($domain->checkup as $bar)
				      ['{{$bar->name }} ( {{round($bar->avg,2)}} )', {{round($bar->avg,2)}}], 
				      @endforeach
				      ],
				      color: '#3c8dbc'
				    }
				    $.plot('#bar-chart-{{$key+1}}', [bar_data], {
				      grid  : {
				        borderWidth: 1,
				        borderColor: '#f3f3f3',
				        tickColor  : '#f3f3f3'
				      },
				      series: {
				        bars: {
				          show    : true,
				          barWidth: 0.5,
				          align   : 'center'
				        }
				      },
				      xaxis : {
				        mode      : 'categories',
				        tickLength: 0
				      }
				    })
            </script>
            @endforeach
            @endif
            @if(isset($etpm_evaluate_chart))
    	<script>
		  $(function () {
		    
		    /*
		     * LINE CHART
		     * ----------
		     */
		    //LINE randomly generated data
		    @foreach($etpm_evaluate_chart as $key_ev=> $ev)
		    @foreach($ev->domain as $key=>$domain)
		    	var plot_arr = [];
		    	@foreach($domain->group as $key_group=>$group)
				    var sin= [];
				    @foreach($group->checkup as $key_check=>$value)
				    	sin.push(['{{$key_check+1}}', {{$value->evaluate}}])
				    @endforeach
				    var line_data = {
				      data : sin,
				      color: '#{{$group->color}}',
				    }
				    plot_arr.push(line_data);
			    @endforeach
		    $.plot('#line-chart-{{$key_ev+1}}-{{$key+1}}', plot_arr, {
		      grid  : {
		        hoverable  : true,
		        borderColor: '#f3f3f3',
		        borderWidth: 1,
		        tickColor  : '#f3f3f3'
		      },
		      series: {
		        shadowSize: 0,
		        lines     : {
		          show: true
		        },
		        points    : {
		          show: true
		        }
		      },
		      lines : {
		        fill : false,
		        color: ['#3c8dbc', '#f56954']
		      },
		      yaxis : {
		        show: true
		      },
		      xaxis : {
		        show: false,
		      }
		    })
		    @endforeach
		              var bar_data = {
		              data : [
		              @foreach($ev->domain  as $bar)
		              @if($bar->count!=0)
		              ['{{$bar->name}} ({{round($bar->sum/$bar->count,2)}})', {{$bar->sum/$bar->count}}], 
		              @endif
		              @endforeach
		              ],
		              color: '#3c8dbc'
		            }
		            $.plot('#bar-chart-{{$key_ev+1}}', [bar_data], {
		              grid  : {
		                borderWidth: 1,
		                borderColor: '#f3f3f3',
		                tickColor  : '#f3f3f3'
		              },
		              series: {
		                bars: {
		                  show    : true,
		                  barWidth: 0.5,
		                  align   : 'center'
		                }
		              },
		              xaxis : {
		                mode      : 'categories',
		                tickLength: 0
		              }
		            })
		    @endforeach
		    /* END LINE CHART */

		  })

		  /*
		   * Custom Label formatter
		   * ----------------------
		   */
		  function labelFormatter(label, series) {
		    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
		      + label
		      + '<br>'
		      + Math.round(series.percent) + '%</div>'
		  }
		</script>
    	@endif
		@if(isset($chart_training))
		<script>
			@foreach($chart_training as $key=> $training)
  			$(function () {
			    var donutData = [
			      @foreach($training['checkup'] as $val)
			      { label: '{{$val["name"]}}', data: {{100*$val["count"]/$training["count"]}}, color: '#{{$val["color"]}}' },
			      @endforeach
			    ]
			    $.plot('#donut-chart-{{$key+1}}', donutData, {
			      series: {
			        pie: {
			          show       : true,
			          radius     : 1,
			          innerRadius: 0.5,
			          label      : {
			            show     : true,
			            radius   : 2 / 3,
			            formatter: labelFormatter,
			            threshold: 0.1
			          }

			        }
			      },
			      legend: {
			        show: false
			      }
			    })
  			})        
            @endforeach
			
		</script>
		@endif
		
		@if(isset($domains_bar))
            @foreach($domains_bar as $key=> $domain)
            <script>
            	var bar_data = {
				      data : [
				      @foreach($domain->checkup as $bar)
				      @if($bar->count!=0)
				      ['{{$bar->name}} ({{round($bar->sum/$bar->count,2)}})', {{$bar->sum/$bar->count}}], 
				      @endif
				      @endforeach
				      ],
				      color: '#3c8dbc'
				    }
				    $.plot('#bar-chart-{{$key+1}}', [bar_data], {
				      grid  : {
				        borderWidth: 1,
				        borderColor: '#f3f3f3',
				        tickColor  : '#f3f3f3'
				      },
				      series: {
				        bars: {
				          show    : true,
				          barWidth: 0.5,
				          align   : 'center'
				        }
				      },
				      xaxis : {
				        mode      : 'categories',
				        tickLength: 0
				      }
				    })
            </script>
            @endforeach
            <script>
            	var bar_data = {
				      data : [
				      @foreach($domains_bar as $bar)
				      @if($bar->count!=0)
				      ['{{$bar->name}} ({{round($bar->sum/$bar->count,2)}})', {{$bar->sum/$bar->count}}], 
				      @endif
				      @endforeach
				      ],
				      color: '#3c8dbc'
				    }
				    $.plot('#bar-chart', [bar_data], {
				      grid  : {
				        borderWidth: 1,
				        borderColor: '#f3f3f3',
				        tickColor  : '#f3f3f3'
				      },
				      series: {
				        bars: {
				          show    : true,
				          barWidth: 0.5,
				          align   : 'center'
				        }
				      },
				      xaxis : {
				        mode      : 'categories',
				        tickLength: 0
				      }
				    })
            </script>
		@endif
		@if(isset($chart_ev_trainer))
		<script >
			
  			$(function () {
			    var donutData = [
			      @foreach($chart_ev_trainer as $val)
			      { label: '{{$val["name"]}}', data: {{$val["count"]}}, color: '#{{$val["color"]}}' },
			      @endforeach
			    ]
			    $.plot('#donut-chart-1', donutData, {
			      series: {
			        pie: {
			          show       : true,
			          radius     : 1,
			          innerRadius: 0.5,
			          label      : {
			            show     : true,
			            radius   : 2 / 3,
			            formatter: labelFormatter,
			            threshold: 0.1
			          }

			        }
			      },
			      legend: {
			        show: false
			      }
			    })
  			})        
		</script>
		@endif
		@if(isset($chart_ev_material))
		<script >
			
  			$(function () {
			    var donutData = [
			      @foreach($chart_ev_material as $val)
			      { label: '{{$val["name"]}}', data: {{$val["count"]}}, color: '#{{$val["color"]}}' },
			      @endforeach
			    ]
			    $.plot('#donut-chart-2', donutData, {
			      series: {
			        pie: {
			          show       : true,
			          radius     : 1,
			          innerRadius: 0.5,
			          label      : {
			            show     : true,
			            radius   : 2 / 3,
			            formatter: labelFormatter,
			            threshold: 0.1
			          }

			        }
			      },
			      legend: {
			        show: false
			      }
			    })
  			})        
		</script>
		@endif
		@if(isset($chart_task))
		<script>
			@foreach($responsibility as $key_resp=> $tasks)
			@foreach($tasks->tasks as $key_task=> $task)
  			$(function () {
			    var donutData = [
			      @foreach($chart_task[$key_resp]['tasks'][$key_task]['task'] as $val)
			      { label: '{{$val["name"]}}', data: @if($chart_task[$key_resp]['tasks'][$key_task]['count']!=0){{100*$val["count"]/$chart_task[$key_resp]['tasks'][$key_task]['count']}} @else 0 @endif, color: '#{{$val["color"]}}' },
			      @endforeach
			    ]
			    $.plot('#donut-chart-{{$key_resp+1}}-{{$key_task+1}}', donutData, {
			      series: {
			        pie: {
			          show       : true,
			          radius     : 1,
			          innerRadius: 0.5,
			          label      : {
			            show     : true,
			            radius   : 2 / 3,
			            formatter: labelFormatter,
			            threshold: 0.1
			          }

			        }
			      },
			      legend: {
			        show: false
			      }
			    })
  			})        
            @endforeach
            @endforeach
			
		</script>
		@endif
		@if(isset($chart_task2))
		<script>
			@foreach($responsibility as $key_resp=> $tasks)
			@foreach($tasks->tasks as $key_task=> $task)
  			$(function () {
			    var donutData = [
			      @foreach($chart_task2[$key_resp]['tasks'][$key_task]['task'] as $val)
			      { label: '{{$val["name"]}}', data: @if($chart_task2[$key_resp]['tasks'][$key_task]['count']!=0){{100*$val["count"]/$chart_task2[$key_resp]['tasks'][$key_task]['count']}} @else 0 @endif, color: '#{{$val["color"]}}' },
			      @endforeach
			    ]
			    $.plot('#donut-chart2-{{$key_resp+1}}-{{$key_task+1}}', donutData, {
			      series: {
			        pie: {
			          show       : true,
			          radius     : 1,
			          innerRadius: 0.5,
			          label      : {
			            show     : true,
			            radius   : 2 / 3,
			            formatter: labelFormatter,
			            threshold: 0.1
			          }

			        }
			      },
			      legend: {
			        show: false
			      }
			    })
  			})        
            @endforeach
            @endforeach
			
		</script>
		@endif
		@if(isset($planner_chart))
		<script>
		    var bar_data = {
		      data : [
			      @foreach($all as $value)
			      ['{{$value->name}}', {{$value->difficult}}],
			      @endforeach
		      ],
		      color: '#3c8dbc'
		    }
		    $.plot('#bar-chart', [bar_data], {
		      grid  : {
		        borderWidth: 1,
		        borderColor: '#f3f3f3',
		        tickColor  : '#f3f3f3'
		      },
		      series: {
		        bars: {
		          show    : true,
		          barWidth: 0.5,
		          align   : 'center'
		        }
		      },
		      xaxis : {
		        mode      : 'categories',
		        tickLength: 0
		      }
		    })
		    /* END BAR CHART */

		</script>
		@endif
		<script>
		    $(document).ready(function(){
		        // Add minus icon for collapse element which is open by default
		        $(".collapse.in").each(function(){
		          $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
		        });
		        
		        // Toggle plus minus icon on show hide of collapse element
		        $(".collapse").on('show.bs.collapse', function(){
		          $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
		        }).on('hide.bs.collapse', function(){
		          $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
		        });
		    });
		</script>
		<script>
			$(function () {
				$('#datepicker').datepicker({
			      autoclose: true
			    })
				$('.select2').select2()
				$('#example1').DataTable()
				$('#example2').DataTable({
					'paging'      : true,
					'lengthChange': false,
					'searching'   : false,
					'ordering'    : true,
					'info'        : true,
					'autoWidth'   : false
				})
			});
			$(function () {
				if ($("#editor1").length) {
					CKEDITOR.replace('editor1');
				}
				if ($("#editor2").length) {
					CKEDITOR.replace('editor2');
				}
				if ($("#editor3").length) {
					CKEDITOR.replace('editor3');
				}
				if ($("#editor4").length) {
					CKEDITOR.replace('editor4');
				}
				if ($("#editor5").length) {
					CKEDITOR.replace('editor5');
				}
				if ($("#editor6").length) {
					CKEDITOR.replace('editor6');
				}
				if ($("#editor7").length) {
					CKEDITOR.replace('editor7');
				}
				if ($("#editor8").length) {
					CKEDITOR.replace('editor8');
				}
				//bootstrap WYSIHTML5 - text editor
				$('.textarea').wysihtml5()
			});


  /*
   * Custom Label formatter
   * ----------------------
   */
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }
		</script>
	</body>
</html>
