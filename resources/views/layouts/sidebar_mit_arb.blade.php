<ul class="sidebar-menu" data-widget="tree">
	<li>
		<a href="{{url('mit_arbitration')}}">
			<i class="fa fa-dashboard"></i> <span>مشاريعى</span>
		</a>
	</li>
@if($mit_arb_pro ==1)
	<li>
		<a href="{{url('arb_category/'.$mit->id)}}">
			<i class="fa fa-outdent"></i> <span>دواعي الاحتياج</span>
		</a>
	</li>
	@if($mit->step_no>1)
	<li>
		<a href="{{url('arb_goal/'.$mit->id)}}">
			<i class="fa fa-asterisk"></i> <span>أهداف الحقيبة التدريبية</span>
		</a>
	</li>
	@endif
	@if($mit->step_no>2)
	<li>
		<a href="{{url('arb_link/'.$mit->id)}}">
			<i class="fa fa-link"></i> <span>الرابط للمحكمين</span>
		</a>
	</li>
	<li>
		<a href="{{url('arbitrators/mit/'.$mit->id)}}">
			<i class="fa fa-users"></i> <span>المحكمين</span>
		</a>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-file"></i> <span>التقارير لكل المحكمين</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('mit_arbitration/report_training/'.$mit->id)}}"><i class="fa fa-circle-o  text-aqua"></i>تحكيم نواتج التدريب</a></li>
			<li class="active"><a href="{{url('mit_arbitration/report_content/'.$mit->id)}}"><i class="fa fa-circle-o  text-aqua"></i>تحكيم محتوى التدريب</a></li>
			<li class="active"><a href="{{url('mit_arbitration/report_activities/'.$mit->id)}}"><i class="fa fa-circle-o  text-aqua"></i>تحكيم الأنشطة التدريبية</a></li>
			<li class="active"><a href="{{url('mit_arbitration/report_evaluate/'.$mit->id)}}"><i class="fa fa-circle-o  text-aqua"></i>التقييم العام</a></li>
		</ul>
	</li>
	@endif
@endif
</ul>