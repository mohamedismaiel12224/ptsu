<ul class="sidebar-menu" data-widget="tree">
	<li>
		<a href="{{url('change_pass')}}">
			<i class="fa fa-gears"></i> <span>تغيير الرقم السري</span>
		</a>
	</li>
	<li>
		<a href="{{url('/')}}">
			<i class="fa fa-dashboard"></i> <span>العودة للرئيسية</span>
		</a>
	</li>
	<li>
		<a href="{{url('sys_fellow')}}">
			<i class="fa fa-dashboard"></i> <span>الأنظمة المهنية</span>
		</a>
	</li>
	<!-- <li class="treeview">
		<a href="#">
			<i class="fa fa-sticky-note"></i> <span>مرفقاتى فى النظام</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('material')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('material/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul> 
	</li>  -->
	@if(Auth::user()->status==1)
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-book"></i> <span>البرامج التدريبية </span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('courses')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('courses/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-graduation-cap"></i> <span>المتدربين</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('trainer')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('trainer/create')}}"><i class="fa fa-user-plus"></i> إضافة</a></li>
		</ul>
	</li>
	@endif
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-image"></i> <span>بنرات اعلانية</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('banners')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('banners/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
</ul>