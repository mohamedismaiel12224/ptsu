<ul class="sidebar-menu" data-widget="tree">
	<!-- <li>
		<a href="{{url('admin')}}">
			<i class="fa fa-dashboard"></i> <span>الرئيسية</span>
		</a>
	</li> -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i> <span>المكتبة الألكترونية</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('section_lib')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('section_lib/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-video-camera"></i> <span>الفيديوهات التعريفية</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('video')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('video/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul> 
	</li> 
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-book"></i> <span>البرامج التدريبية </span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('courses')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('courses/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
	<!-- <li>
		<a href="{{url('give_certificate')}}">
			<i class="fa fa-certificate"></i> <span>منح شهادات</span>
		</a>
	</li> -->
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-users"></i> <span>زملاء الأتحاد</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('fellowship')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('fellowship/create')}}"><i class="fa fa-user-plus"></i> إضافة</a></li>
		</ul>
	</li>
	<li>
		<a href="{{url('requests')}}">
			<i class="fa fa-object-group"></i> <span>صلاحية طلبات الزمالة</span>
		</a>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-graduation-cap"></i> <span>المتدربين</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('trainer')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('trainer/create')}}"><i class="fa fa-user-plus"></i> إضافة</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-bank"></i> <span>الأنظمة المهنية</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('systems')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('systems/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
	<li>
		<a href="{{url('sys_degree')}}">
			<i class="fa fa-object-group"></i> <span>اعدادات الأنظمة</span>
		</a>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-bar-chart"></i> <span>استطلاعات الرأى</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('poll')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('poll/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li> 
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-users"></i> <span>المطورين</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('developers')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('developers/create')}}"><i class="fa fa-user-plus"></i> إضافة</a></li>
		</ul>
	</li> 
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-image"></i> <span>شركاء النجاح</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('logos')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('logos/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-sticky-note"></i> <span>اختبارات الكفاءة</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('effs')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('effs/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li> 
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-newspaper-o"></i> <span>اخر الأخبار</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('news')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('news/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li> 
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-image"></i> <span>بنرات اعلانية</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('banners')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('banners/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-sticky-note"></i> <span>معايير الأنظمة المهنية</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('norms')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('norms/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa  fa-image"></i> <span>السلايدر</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="active"><a href="{{url('slider')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
			<li><a href="{{url('slider/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
		</ul>
	</li>
	<li>
		<a href="{{url('mail')}}">
			<i class="fa fa-envelope"></i> <span>القائمة البريدية</span>
		</a>
	</li>
	<li>
		<a href="{{url('setting')}}">
			<i class="fa fa-gears"></i> <span>الأعدادات</span>
		</a>
	</li>
</ul>