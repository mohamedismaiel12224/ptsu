<ul class="sidebar-menu" data-widget="tree">
  <li>
    <a href="{{url('need')}}">
      <i class="fa fa-dashboard"></i> <span>مشاريعى</span>
    </a>
  </li>
@if($need_pro ==1)
  <li>
    <a href="{{url('responsiblity_need/'.$need->id)}}">
      <i class="fa fa-outdent"></i> <span>تحليل الفجوة </span>
    </a>
  </li>
  @if($need->step_no>1)
  <li>
    <a href="{{url('tool_type/'.$need->id)}}">
      <i class="fa fa-tasks"></i> <span>نوع الأداة</span>
    </a>
  </li>
  @endif
  @if($need->step_no>2)
  <li>
    <a href="{{url('questions_need/'.$need->id)}}">
      <i class="fa fa-building-o"></i> <span>بناء أداة التحديد العام</span>
    </a>
  </li>
  @endif
  @if($need->step_no>3)
  <li>
    <a href="{{url('link_trainers/'.$need->id)}}">
      <i class="fa fa-link"></i> <span>رابط للمتدربين</span>
    </a>
  </li>
  <li>
    <a href="{{url('need/all_trainers/'.$need->id)}}">
      <i class="fa fa-users"></i> <span>المتدربين</span>
    </a>
  </li>
  @if(all_checked($need->id))
  <li>
    <a href="{{url('need/report_trainers/'.$need->id)}}">
      <i class="fa fa-file"></i> <span>تقرير كل المتدربين</span>
    </a>
  </li>
  <li>
    <a href="{{url('need/affect_improve/'.$need->id)}}">
      <i class="fa fa-building"></i> <span> تحديد المؤثرات وفرص التحسين</span>
    </a>
  </li>
  @endif
  @endif
  @if($need->step_no>4)
  <li>
    <a href="{{url('need/select_responsibility/'.$need->id)}}">
      <i class="fa fa-tasks"></i> <span>اختيار مسئولية</span>
    </a>
  </li>
  @endif
@elseif($need_pro==2)
  <li>
    <a href="{{url('responsiblity_need/'.$need->id)}}">
      <i class="fa fa-outdent"></i> <span>العودة لتحليل الفجوة </span>
    </a>
  </li>
  @if($need->step_no>4)
  <li>
    <a href="{{url('need/select_responsibility/'.$need->id)}}">
      <i class="fa fa-tasks"></i> <span>اختيار مسئولية</span>
    </a>
  </li>
  @endif
  @if($need->step_no>5)
  <li>
    <a href="{{url('questions_need_selected/'.$need->id)}}">
      <i class="fa fa-building-o"></i> <span>مهام وعبارات المسئوليات المختارة</span>
    </a>
  </li>
  <li>
    <a href="{{url('link_arbitrators/'.$need->id)}}">
      <i class="fa fa-link"></i> <span>لينك للمحكمين</span>
    </a>
  </li>
  <li>
    <a href="{{url('need/all_arbitrators/'.$need->id)}}">
      <i class="fa fa-users"></i> <span>المحكمين</span>
    </a>
  </li>
  <li>
    <a href="{{url('need/report_arbitrators/'.$need->id)}}">
      <i class="fa fa-file"></i> <span>تقرير المحكمين</span>
    </a>
  </li>
  <li>
    <a href="{{url('link_need_knowledge/'.$need->id)}}">
      <i class="fa fa-link"></i> <span>لينك بناء أداة المجال المعرفي والاتجاهي</span>
    </a>
  </li>
  <li>
    <a href="{{url('need/arbitrators_knowledge/'.$need->id)}}">
      <i class="fa fa-users"></i> <span>متتدربي اداة المجال المعرفي والاتجاهي</span>
    </a>
  </li>
  <li>
    <a href="{{url('need/report_arbitrator_knowledge/'.$need->id)}}">
      <i class="fa fa-file"></i> <span>تقرير اداة المجال المعرفي والاتجاهي</span>
    </a>
  </li>
  <li>
    <a href="{{url('link_need_skill/'.$need->id)}}">
      <i class="fa fa-link"></i> <span>لينك بناء أداة المجال المهاري</span>
    </a>
  </li>
  <li>
    <a href="{{url('need/arbitrators_skill/'.$need->id)}}">
      <i class="fa fa-users"></i> <span>متتدربي اداة المجال المهاري</span>
    </a>
  </li>
  <li>
    <a href="{{url('need/report_arbitrator_skill/'.$need->id)}}">
      <i class="fa fa-file"></i> <span>تقرير اداة المجال المهاري</span>
    </a>
  </li>
  @endif
@endif
</ul>