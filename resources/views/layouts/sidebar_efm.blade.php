<ul class="sidebar-menu" data-widget="tree">
  <li>
    <a href="{{url('efm/efm')}}">
      <i class="fa fa-dashboard"></i> <span>مشاريعى</span>
    </a>
  </li>
@if($efm_pro ==1)
  <li class="header center">المرحلة الاولى : التركيز Focus</li>

  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i> <span>تحديد الفجوات</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="active"><a href="{{url('efm/gap/'.$efm->id)}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
      <li><a href="{{url('efm/gap/create/'.$efm->id)}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i> <span>الملحوظات العامة</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="active"><a href="{{url('efm/note/'.$efm->id)}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
      <li><a href="{{url('efm/note/create/'.$efm->id)}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
    </ul>
  </li>
  <li>
    <a href="{{url('efm/human_characteristics/'.$efm->id)}}">
      <i class="fa fa-dashboard"></i> <span>الخصائص البشرية</span>
    </a>
  </li>
  @if($efm->step_no>1)
  <li class="header center">المرحلة الثانية : التصميم Designing</li>

  <li>
    <a href="{{url('efm/training_material/'.$efm->id)}}">
      <i class="fa fa-tasks"></i> <span> إعداد مواد التدريب</span>
    </a>
  </li>
  @if($efm->training_material == 1 )
  <li>
    <a href="{{url('efm/training_material/'.$efm->id.'/1')}}">
      <i class="fa fa-tasks"></i> <span> ملحوظات على الحقيبة</span>
    </a>
  </li>
  @endif
  @endif
  @if($efm->step_no >2)
  <li class="header center">المرحلة الثالثة : التأطير Framing</li>

  <li>
    <a href="{{url('efm/select_evaluate_fields/'.$efm->id)}}">
      <i class="fa fa-users"></i> <span>تحديد مجالات التقييم</span>
    </a>
  </li>
  <li>
    <a href="{{url('efm/targets/'.$efm->id)}}">
      <i class="fa fa-users"></i> <span>تحديد المستهدفات</span>
    </a>
  </li>
  @endif
  @if($efm->step_no >3)
  <li class="header center">المرحلة الرابعة : التحليل Analysis</li>
  <!-- <li class="header center">وضع خطة التطبيق الميداني Plan Application</li> -->

  <li>
    <a href="{{url('efm/analysis/'.$efm->id)}}">
      <i class="fa fa-users"></i> <span>نموذج العمل Business model ( A )</span>
    </a>
  </li>
  <li>
    <a href="{{url('efm/analysis_b/'.$efm->id)}}">
      <i class="fa fa-users"></i> <span>نموذج العمل Business model ( B )</span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i> <span>المتدربين</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="active"><a href="{{url('etpm/trainers/'.$efm->id)}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
      <li><a href="{{url('etpm/trainers/export/'.$efm->id)}}"><i class="fa fa-circle-o"></i> استيراد بيانات من مشاريعي </a></li>
      <li><a href="{{url('etpm/trainers/export_user/'.$efm->id)}}"><i class="fa fa-circle-o"></i> استيراد بيانات من المتدربين </a></li>
      <li><a href="{{url('etpm/trainers/create/'.$efm->id)}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
    </ul>
  </li>
  <li>
    <a href="{{url('etpm/report_evaluate/'.$efm->id)}}">
      <i class="fa fa-file"></i> <span>تقرير المقيمين</span>
    </a>
  </li>
  @endif
@endif
</ul>