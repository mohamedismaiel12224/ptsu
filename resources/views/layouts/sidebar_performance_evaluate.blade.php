<ul class="sidebar-menu" data-widget="tree">
  <li>
    <a href="{{url('performance_evaluate')}}">
      <i class="fa fa-dashboard"></i> <span>مشاريعى</span>
    </a>
  </li>
@if($performance_evaluate_pro ==1)
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i> <span>المجموعات</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="active"><a href="{{url('groups_trainers_evaluate/'.$ev->id)}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
      <li><a href="{{url('groups_trainers_evaluate/create/'.$ev->id)}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i> <span>المتدربين</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="active"><a href="{{url('performance_evaluate/trainers/'.$ev->id)}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
      <li><a href="{{url('performance_evaluate/trainers/export/'.$ev->id)}}"><i class="fa fa-circle-o"></i> استيراد بيانات من مشاريعي </a></li>
      <li><a href="{{url('performance_evaluate/trainers/export_user/'.$ev->id)}}"><i class="fa fa-circle-o"></i> استيراد بيانات من المتدربين </a></li>
      <li><a href="{{url('performance_evaluate/trainers/create/'.$ev->id)}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
    </ul>
  </li>
  @if($ev->step_no>1)
  <li>
    <a href="{{url('performance_evaluate/link_evaluate/'.$ev->id)}}">
      <i class="fa fa-tasks"></i> <span>رابط التقييم للمقيمين</span>
    </a>
  </li>
  <li>
    <a href="{{url('performance_evaluate/all_trainers/'.$ev->id)}}">
      <i class="fa fa-users"></i> <span>نتائج المتدربين</span>
    </a>
  </li>
  <li>
    <a href="{{url('performance_evaluate/report_evaluate/'.$ev->id)}}">
      <i class="fa fa-file"></i> <span>تقرير المقيمين</span>
    </a>
  </li>
  @endif
@endif
@if(isset($evaluator_sidebar))
<li>
    <a href="{{url('performance_evaluate/all_trainers/'.$ev->id)}}">
      <i class="fa fa-users"></i> <span>المتدربين</span>
    </a>
  </li>
@endif
</ul>