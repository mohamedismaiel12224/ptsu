<ul class="sidebar-menu" data-widget="tree">
	<li>
		<a href="{{url('admin')}}">
			<i class="fa fa-dashboard"></i> <span>الرئيسية</span>
		</a>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i> <span>نموذج MIT</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li>
				<a href="{{url('sys_degree')}}">
					<i class="fa fa-object-group"></i> <span>سلم التقدير</span>
				</a>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>مستوى التقويم</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('ev_egree')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('ev_egree/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>طرق التدريب</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('tr_methods')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('tr_methods/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>وقت التقويم</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('ev_time')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('ev_time/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>الأدوات</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('tools')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('tools/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>المسئولية</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('responsiblity')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('responsiblity/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>الفئات</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('category')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('category/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>معينات الوصول</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('aids')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('aids/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>آلية اللتنفيذ</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('imp_mechanism')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('imp_mechanism/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>مصادر المحتوى</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('con_source')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('con_source/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i> <span>تحكيم الحقائب التدريبية</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>مجالات نواتج التدريب</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('domain_training')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('domain_training/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>مجالات المحتوى التدريبي</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('domain_content')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('domain_content/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>مجالات الأنشطة التدريبية</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('domain_activities')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('domain_activities/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>مجالات التقييم العام</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('domain_evaluate')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('domain_evaluate/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i> <span>نموذج تحليل وتحديد الاحتياجات</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>المؤثرات</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('affects_need')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('affects_need/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>الحلول</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('solution_need')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('solution_need/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i> <span>نموذج تقييم الاداء</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>تقييم مهارات العرض والالقاء</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('domain_presentation_casting')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('domain_presentation_casting/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>تقييم المتدربين والمادة التدريبية</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('ev_trainers')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('ev_trainers/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i> <span>ETPM نموذج التميز في الأداء التدريبي</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>المعايير</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('etpm/standard')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('etpm/standard/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li>
				<a href="{{url('description_evaluate_etpm')}}">
					<i class="fa fa-dashboard"></i> <span>وصف النتائج</span>
				</a>
			</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-laptop"></i> <span>نموذج EFM</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>مستوى التقويم</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('ev_efm')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('ev_efm/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-laptop"></i> <span>الأدوات</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="{{url('tool_efm')}}"><i class="fa fa-circle-o  text-aqua"></i>عرض</a></li>
					<li><a href="{{url('tool_efm/create')}}"><i class="fa fa-circle-o"></i> إضافة</a></li>
				</ul>
			</li>
		</ul>
	</li>
</ul>