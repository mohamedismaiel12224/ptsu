@extends('layouts.layout')
@section('title', 'Certification PTSU')
@section('certificate')
    <section id="inner_banner">
        <div class="overlay">
            <div class="container">
                <h3>{{trans('lang.verify_the_certificate')}}</h3>
                <ul>
                    <li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
                    <li>/</li>
                    <li>{{trans('lang.certifications')}}</li>
                </ul>
            </div>
        </div>
    </section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->
<!-- ======================== Blog Page content ======================= -->

    <section class="blog_with_sidebar blog_grid container">

        <div class="row fixed_height cert-fixed_height">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog_post">
                <section class="content" >
                    <div class="invoice">
                        <img src="{{asset('assets/images/cert.jpg')}}">
                        <div class="ab5ab5">
                            <div style="text-align: center;">
                                <h1 >Certificate</h1>
                                <h4 >HEREBY CERTIFICATE THAT</h4>
                                <h3 >{{$user->name}}</h3>
                            </div>
                            <div class="row" style="text-align: center;">
                                <h4>Has completed a course training in The Profassional Training Systems Union</h4>
                                <h4>which was conducted</h4>
                                <h4>according to the standard and guidelines established by PTSU</h4>
                                <h4>The holder of this certificate has been trained by a registered PTSU</h4>
                                <h4>.and has achieved the standards required to merit this certificate</h4>
                                <h3>The Profassional Training Systems Union</h3>
                                <h4>Has therefore conferred upon the holder this course in</h4>
                                <h3>{{$course->address_en}}</h3>
                                <h5>With <b>{{$course->hours}}</b> training hours, and accordingly the certificate on {{date('d/m/Y',strtotime($course->created_at))}}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 invoice-block no-print">
                                <a class="btn btn-primary"  onclick="certPrint();"> طباعة
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>

                            <h6> {{$user->cert_no}} </h6>
                        </div>
                    </div>
                </section>
            </div> <!-- / blog_post -->
        </div> <!-- /row -->
    </section> <!-- /blog_with_sidebar -->
<!-- ======================== /Blog Page content ======================= -->
@endsection