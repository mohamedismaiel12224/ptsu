@extends('layouts.layout')
@section('title', 'Certification PTSU')
@section('certificate')
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.verify_the_certificate')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">{{trans('lang.home')}}</a></li>
					<li>/</li>
					<li>{{trans('lang.certifications')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->
<!-- ======================== Blog Page content ======================= -->

	<section class="blog_with_sidebar blog_grid container">

		<div class="row fixed_height cert-fixed_height">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 side_bar_style_two">

				<!-- ================== Search form ============== -->
				<h4 class="color">{{trans('lang.To_check_the_certificate_enter_your_name_or_certificate_number')}}</h4>
				<form class="SearchForm" method="post" action="{{url('search_cert')}}">
					{{csrf_field()}}
					<input class="inputSearch" type="text" name="search" placeholder="{{trans('lang.Search_here')}}" required="">
					<button class="btnSearch" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
				</form>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog_post">
				
			</div> <!-- / blog_post -->
		</div> <!-- /row -->
	</section> <!-- /blog_with_sidebar -->
<!-- ======================== /Blog Page content ======================= -->
@endsection