@extends('layouts.layout')
@section('title', 'PTSU')
@section('home')
<!-- ======================= Banner ======================== -->
<section class="banner">
	<div class="rev_slider_wrapper">
		<div id="main_slider" class="rev_slider"  data-version="5.0">
			<ul>
				<!-- SLIDE  -->
				@foreach($slider as $key=>$value)
				<li data-index="rs-28{{$key}}" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-saveperformance="off"  data-title="Intro" data-description="">
					<!-- MAIN IMAGE -->
					<img src="{{asset('assets/images/home/'.$value->img)}}"  alt="image"  class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
					<!-- LAYERS -->

					<!-- LAYER NR. 3 -->
					<div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0" 
						data-x="['center','center','center','center']" data-hoffset="['-14','0','0','0']" 
						data-y="['middle','middle','middle','middle']" data-voffset="['-13','0','-42','-20']" 
						data-fontsize="['50','70','40','30']"
						data-lineheight="['50','70','40','30']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;"
						data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
						data-start="1000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on" 
						style="z-index: 7; white-space: nowrap;">
						<h1>System . Standards . Activities</h1>
					</div>
					<!-- LAYER NR. 3 -->
					
					<!-- LAYER NR. 4 -->
					<div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" 
						data-x="['center','center','center','center']" data-hoffset="['-2','0','0','0']" 
						data-y="['middle','middle','middle','middle']" data-voffset="['81','84','23','45']" 
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" 
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
						data-mask_out="x:inhe-rit;y:inherit;s:inherit;e:inherit;" 
						data-start="1000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on" 
						style="z-index: 8; white-space: nowrap;">
						<p>We measure our success by your success. If you are not satisfied, We dont</p>
					</div>

					<!-- LAYER NR. 5 -->
					<div class="tp-caption  rs-parallaxlevel-0" 
						data-x="['center','center','center','center']" data-hoffset="['-10','0','0','0']" 
						data-y="['bottom','bottom','bottom','bottom']" data-voffset="['278','280','300','220']" 
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;"
						data-style_hover="cursor:default;"
						data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeInOut;" 
						data-transform_out="opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-start="1000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on" 
						data-responsive="off"
						style="z-index: 9; white-space: nowrap;">
						<a href="{{url('register')}}" class="smooth_scroll">{{trans('lang.fellowship_request')}}</a>
					</div>
				</li>
				@endforeach

			</ul>
		</div>
	</div>
</section> 

<!-- =========================== /Banner =========================== -->

<!-- ======================== We are Right Firm ==================== -->

<section class="right_firm container">
	<div class="title_holder_center title_holder">
		<p class="upper_title">{{trans('lang.about_us')}}</p>
		<h3>We are <span class="firm">PTSU</span></h3>
		<span></span>
		<p>{{trans('lang.professional_training_systems_union')}}</p>
	</div> <!-- /title_holder_center -->
	<div class="our_goal row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div>
				<h4 class="mTop">{{trans('lang.who_are')}}</h4>
				<p>@if(isset($setting))@if(check_language()){!!mb_substr($setting->we_are_ar,0,600,'utf-8')!!}@else{!!mb_substr($setting->we_are,0,200,'utf-8')!!}@endif @endif</p>
				<a href="{{url('about')}}" class="transition3s">{{trans('lang.see_more')}}</a>
			</div>
			<div class="invo-mfix">
				<h4 class="mTop">{{trans('lang.our_mission')}}</h4>
				<p>@if(isset($setting))@if(check_language()){!!mb_substr($setting->our_mission_ar,0,600,'utf-8')!!}@else{!!mb_substr($setting->our_mission,0,200,'utf-8')!!}@endif @endif</p>
				<a href="{{url('about')}}" class="transition3s">{{trans('lang.see_more')}}</a>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			
			<div>
				<h4 class="mTop">{{trans('lang.our_vision')}}</h4>
				<p>@if(isset($setting))@if(check_language()){!!mb_substr($setting->our_vision_ar,0,600,'utf-8')!!}@else{!!mb_substr($setting->our_vision,0,200,'utf-8')!!}@endif @endif</p>
				<a href="{{url('about')}}" class="transition3s">{{trans('lang.see_more')}}</a>
			</div>
			<div class="invo-mfix">
				<h4 class="mTop">{{trans('lang.our_service')}}</h4>
			<p>@if(isset($setting))@if(check_language()){!!mb_substr($setting->our_rate_ar,0,600,'utf-8')!!}@else{!!mb_substr($setting->our_rate,0,200,'utf-8')!!}@endif @endif</p>
			<a href="{{url('about')}}" class="transition3s">{{trans('lang.see_more')}}</a>
			</div>
		</div>
	</div>
</section>

<!-- ======================== /We are Right Firm ==================== -->

<section class="gallery container">
	<div class="title_holder_center title_holder">
		<h3>{{trans('lang.introductory_videos')}}</h3>
		<span></span>
	</div> <!-- /title_holder_center -->
	<div class="gallery_item_wrapper row" id="mixitup_list">
		@foreach($videos as $value)		
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="single_item">
				<div class="img_holder">
					{!! $value->iframe !!}
				</div> <!-- End of .img_holder -->
			</div> <!-- End of .single_item -->
		</div> <!-- End of .mix -->
		@endforeach
	</div> <!-- End of #mixitup_list -->
	<h5 class="center"><a class="center" href="{{url('videos')}}">>>{{trans('lang.see_more')}}<<</a></h5>
</section> <!-- End of .gallery -->

<!-- ======================= Company Achivement =================== -->

<!-- <section class="company_achievement">
	<div class="half1"><div class="overlay"></div></div>
	<div class="half2"><div class="overlay"></div></div>
	<div class="container-wrapper">
		<div class="container">
			<div class="left_half">
				<h2><span class="timer" data-from="10" data-to="600" data-speed="4500" data-refresh-interval="50">90</span><span>+</span><br>Project Complated<br>Successfully</h2>
				<p>We measure our success by your success If <br>you are not satisfied, We dont</p>
				<a href="contact-us.html" class="btn_color main_button transition3s">Contact us</a>
			</div>
			<div class="logo_holder"><img src="{{asset('assets/images/logo/logo-round.png')}}" alt="logo" width="120px"></div>
			<div class="right_half">
				<h2><span class="timer" data-from="10" data-to="153" data-speed="4500" data-refresh-interval="50">90</span><span>+</span><br>National Award<br>Winner</h2>
				<p>We measure our success by your success If <br> you are not satisfied, We dont</p>
				<a href="contact-us.html" class="hover_btn main_button transition3s">Contact us</a>
			</div>
		</div>
	</div>
</section> --> <!-- /company_achievement -->
<div class="clear_fix"></div>

<!-- ======================= /Company Achivement =================== -->


<!-- ========================== Our Team ========================-->
	<section class="our-team container">
		<div class="title_holder">
			<h3>{{trans('lang.system_developers')}}</h3>
			<span></span>
		</div>
		<div class="row owl_slider">
			<div id="attorney_slider">
				@foreach($developer as $value)
				<div class="item">
					<div class="team_member">
						<div class="img_holder">
							<img src="{{asset('assets/images/team/'.$value->profilepic)}}" alt="developer" class="img-responsive">
							<div class="overlay">
								<span><a href="{{url('developer/'.$value->id)}}">+</a></span>
							</div>
						</div> <!-- /img_holder -->
						<div class="text">
							<a href="{{url('developer/'.$value->id)}}"><h4>{{$value->name}}</h4></a>
							<span>{{mb_substr($value->cv,0,100,'utf-8')}}</span>
							<ul>
								@if($value->facebook != '#')
								<li class="round_border transition3s"><a href="{{$value->facebook}}" class="transition3s"><i class="fa fa-facebook"></i></a></li>
								@endif
								@if($value->twitter != '#')
								<li class="round_border transition3s"><a href="{{$value->twitter}}" class="transition3s"><i class="fa fa-twitter"></i></a></li>
								@endif
								@if($value->linkedin != '#')
								<li class="round_border transition3s"><a href="{{$value->linkedin}}" class="transition3s"><i class="fa fa-linkedin"></i></a></li>
								@endif
								@if($value->google != '#')
								<li class="round_border transition3s"><a href="{{$value->google}}" class="transition3s"><i class="fa fa-google-plus"></i></a></li>
								@endif
							</ul>
						</div>
					</div> <!-- /team_member -->
				</div>
				@endforeach
			</div> <!-- /#attorney_slider -->
				<h5 class="center"><a class="center" href="{{url('all_developers')}}">>>{{trans('lang.all_developers')}}<<</a></h5>
			<div class="customNavigation">
				 <a class="prev transition3s"><i class="fa fa-angle-left"></i></a>
				 <a class="next transition3s"><i class="fa fa-angle-right"></i></a>
			</div>
		</div> <!-- /owl_slider -->
	</section> <!-- /our-team -->
<!-- ========================== /Our Team ========================-->

<section class="why_choose_us">
	<div class="left_side"></div>
	<div class="right_side">
		<div class="opacity">
			<div class="title_holder">
				<h3>{{trans('lang.e_library')}}</h3>
				<span></span>
			</div>
			<div class="text side_bar_style_two">
				<div class="category">
					<ul>
						@foreach($elibrary as $value)
						<li><a href="{{url('elibraries/'.$value->id)}}" class="transition3s"><i class="fa fa-star" aria-hidden="true"></i> {{$value->section}}</a></li>
						@endforeach
						<li class="center"><a href="{{url('elibraries')}}">>> {{trans('lang.see_more')}}<<</a></li>
					</ul>
				</div>
			</div> <!-- End .text -->
		</div> <!-- End .opacity -->
	</div> <!-- End .right_side -->
	<div class="clear_fix"></div>
</section>
<!-- ============================= Latest news ================== -->
<div class="faq container">
		<div class="heading">
			<h3>{{trans('lang.poll')}}</h3>
		</div> <!-- /heading -->
		<div class="tab_holder">
			<div class="tab_wrapper" style="width:100%">
				<div class="choose_us_panel">
					<div class="panel-group theme-accordion" id="accordion">
						@foreach($poll as $key=>$value)
					  	<div class="panel col-md-6">
							<div class="panel-heading active-panel">
							  <h4 class="panel-title">
							    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key+1}}">{{$value->poll}}</a>
							  </h4>
							</div>
							<div id="collapse{{$key+1}}" class="panel-collapse collapse">
							  <div class="panel-body panel{{$value->id}}"  data-poll_id="{{$value->id}}">
							  	@foreach($value->suggest as $val)
							  	<div class="single_progress_skills">										
									<h5><input class="radioo" type="radio" name="s[{{$value->id}}]" value="{{$val->id}}"> {{$val->suggest}}</h5>
							  		<span>{{$val->percentage}}%</span>
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$val->percentage}}%;"></div>
									</div>
								</div>
								@endforeach
								<button type="button" class="my_btn">Ok</button>
							  </div>
							  
							</div>
						</div>
						@endforeach
					</div> <!-- end #accordion -->
				</div> <!-- /choose_us_panel -->
			</div> <!-- /tab_wrapper -->
		</div> <!-- /tab_holder -->
	</div> <!-- /faq -->

	<section class="clinet_feedback">
		<div class="container">
			<div class="title_holder_center title_holder">
<!-- 				<p class="upper_title">{{trans('lang.our_expert')}}</p>
 -->				<h3>{{trans('lang.latest_news')}}</h3>
				<span></span>
			</div> <!-- /title_holder_center -->
			<div id="client_slider" class="row">
				@foreach($news as $value)
				<div class="item">
					{!! $value->news !!}
				</div> <!-- /item -->
				@endforeach
			</div> <!-- /#client_slider -->
		</div>
	</section> <!-- /clinet_feedback -->

<!-- ============================= /Latest news ================== -->

<!-- ============================ Partners Logo  ======================= -->
	<div class="container partners">
		<h2 class="center">{{trans('lang.partners')}}</h2>
		<div id="partner_logo" class="owl-carousel owl-theme">
			@foreach($partners as $value)
			<div class="item">
				@if($setting->link_partner==1)
				<a href="{{$value->link}}" target="_blank">
				@endif
					<img src="{{asset('assets/images/home/'.$value->logo)}}" alt="logo">
				@if($setting->link_partner==1)
				</a>
				@endif
			</div>
			@endforeach
		</div> <!-- End .partner_logo -->
	</div>


<!-- ============================ Banners Logo  ======================= -->
	<section class="clinet_feedback">
		<div class="container partners">
			<h2 class="center">{{trans('lang.banners')}}</h2>
			<div id="banner_logo" class="owl-carousel owl-theme">
				@foreach($banners as $value)
				<div class="item">
					<a href="{{$value->link}}" target="_blank">
						<img src="{{asset('assets/images/home/'.$value->logo)}}" alt="logo">
					</a>
				</div>
				@endforeach
			</div> <!-- End .partner_logo -->
		</div>
	</section> <!-- /clinet_feedback -->
@endsection