@extends('layouts.layout')
@section('title', 'PTSU | Developers')
@section('developers')
<!-- ========================== Innaer Banner ========================= -->
	<section id="inner_banner">
		<div class="overlay">
			<div class="container">
				<h3>{{trans('lang.system_developers')}}</h3>
				<ul>
					<li><a href="{{url('/')}}">Home</a></li>
					<li>/</li>
					<li>{{trans('lang.developers')}}</li>
				</ul>
			</div>
		</div>
	</section> <!-- /inner_banner -->
<!-- ========================== /Innaer Banner ========================= -->
<!-- ========================== Our Team ========================-->
	<section class="our-team our-developer container single-inner-page">
		<div class="row owl_slider">
			<div class="clearfix">
				@foreach($developers as $value)
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="team_member">
						<div class="img_holder">
							<img src="{{asset('assets/images/team/'.$value->profilepic)}}" alt="images" class="img-responsive">
							<div class="overlay">
								<span><a href="{{url('developer/'.$value->id)}}">+</a></span>
							</div>
						</div> <!-- /img_holder -->
						<div class="text">
							<a href="{{url('developer/'.$value->id)}}"><h4>{{$value->name}}</h4></a>
							<span>{{mb_substr($value->cv,0,100,'utf-8')}}</span>
							<ul>
								@if($value->facebook != '#')
								<li class="round_border transition3s"><a href="{{$value->facebook}}" class="transition3s"><i class="fa fa-facebook"></i></a></li>
								@endif
								@if($value->twitter != '#')
								<li class="round_border transition3s"><a href="{{$value->twitter}}" class="transition3s"><i class="fa fa-twitter"></i></a></li>
								@endif
								@if($value->linkedin != '#')
								<li class="round_border transition3s"><a href="{{$value->linkedin}}" class="transition3s"><i class="fa fa-linkedin"></i></a></li>
								@endif
								@if($value->google != '#')
								<li class="round_border transition3s"><a href="{{$value->google}}" class="transition3s"><i class="fa fa-google-plus"></i></a></li>
								@endif
							</ul>
						</div>
					</div> <!-- /team_member -->
				</div>
				@endforeach
			</div> <!-- /#attorney_slider -->
		</div> <!-- /owl_slider -->
	</section> <!-- /our-team -->

<!-- ========================== /Our Team ========================-->

@endsection