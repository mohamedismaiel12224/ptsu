<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'between' => [
        'numeric' => 'حقل :attribute يجب أن يكون بين  :min و  :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'حقل :attribute لا يطابق تأكيد كلمه المرور',
    'date' => 'حقل :attribute يجب أن يكون تاريخ صحيح.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'حقل :attribute غير  صحيح.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field is required.',
    'image' => 'حقل :attribute يجب أن تكون صوره صحيحه',
    'in' => 'حقل :attribute غير صحيح.',
    'in_array' => 'حقل :attribute غير صحيح',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => 'يجب حقل :attribute يتكون من أرقام.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'حقل :attribute خطأ.',
    'required' => 'حقل :attribute مطلوب',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'يجب حقل :attribute و :other متماثلين.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'حقل :attribute غير متاح.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name' => 'الاسم',
        'personal_number' => 'الرقم الشخصى',
        'birth_date' => 'تاريخ الميلاد',
        'title' => 'العنوان',
        'image' => 'صوره البطاقه',
        'image_medicine' => 'صوره الكشف الطبى',
        'first_name' => 'الاسم الأول',
        'last_name' => 'الاسم الاخير',
        'email' => 'البريد الإلكتروني',
        'password' => 'كلمة المرور',
        'password_confirmation' => 'تأكيد كلمة المرور',
        'phone' => 'رقم الهاتف',
        'gender' => 'النوع',
        'description' => 'الوصف',
        'content' => 'المحتوى',
        'subscribe_date' => 'تاريخ بدأ الإشتراك',
        'subscribe_number' => 'عدد أشهر الإشتراك',
        'status' => 'الحاله',
        'group' => 'الجروب الخاص بالمدرب',
        'groups' => 'الجروب ',
        'level' => 'المستوى',
        'section' => 'الفئة',
        'file' => 'الملف',
        'we_are' => 'من نحن',
        'our_mission' => 'رسالتنا',
        'our_vision' => 'رؤيتنا',
        'our_rate' => 'قيمنا',
        'we_are_ar' => 'من نحن',
        'our_mission_ar' => 'رسالتنا',
        'our_vision_ar' => 'رؤيتنا',
        'our_rate_ar' => 'قيمنا',
        'poll' => 'الأستطلاع',
        'suggest' => 'الاقتراح',
        'nno' => 'رقم المشروع',
        'country' => 'الدولة',
        'hours' => 'الساعات',
        'co_no' => 'رقم البرنامج',
        'link' => 'اللينك',
        'facebook' => 'فيس بوك',
        'twitter' => 'تويتر',
        'linkedin' => 'لينكدان',
        'google' => 'جوجل بلس', 
        'address' => 'العنوان',
        'detail' => 'الوصف',
        'confirm_password' => 'تأكيد الرقم السري',
        'days' => 'الأيام',
        'benefit_establishment' => 'الجهة المستفيدة',
    ],

];
