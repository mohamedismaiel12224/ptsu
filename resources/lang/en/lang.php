<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // Settings Words
    'sitename' => 'PTSU',
    'about_us' =>'About Us',
    'professional_training_systems_union' => 'Professional Training Systems Union',
    'who_are' => 'Who are',
    'our_mission' => 'Our Mission',
    'our_vision' => 'Our Vision',
    'our_service' => 'Our Service',
    'see_more' => 'See more',
    'partners' => 'Partners',
    'system_developers' => 'System Developers',
    'system_developer' => 'System Developer',
    'all_developers' => 'All Developers',
    'developers' => 'Developers',
    'cv' => 'CV',
    'introductory_videos' => 'Introductory videos',
    'home' => 'Home',
    'latest_news' => 'Latest News',
    'our_expert' => 'Our expert',
    'professional_systems' => 'Professional system',
    'continue_reading' => 'Continue Reading',
    'verify_the_certificate' => 'Verify the certificate',
    'To_check_the_certificate_enter_your_name_or_certificate_number' => 'To check the certificate, enter your name or certificate number',
    'certifications' => 'Certifications',
    'e_library' => 'Electronic Library',
    'login' => 'Login',
    'profile' => 'My Profile',
    'fellowship' => 'Fellowship',
    'poll' => 'Poll',
    'polls' => 'Polls',
    'system' => 'System',
    'systems' => 'System',
    'download' => 'Download',
    'logout' => 'Logout',
    'norms' => 'Standards',
    'efficients' => 'Efficiency tests',
    'certification' => 'Certification',
    'no_data' => 'No Data',
    'show_materials' => 'Show Materials',
    'materials' => 'Materials',
    'elibrary' => 'E-library',
    'material' => 'Material',
    'emailing_form' => 'Emailing Form',
    'contact_us' => 'Contact Us',
    'banners' => 'Banners',
    'email' => 'Email',
    'phone' => 'Phone Number',
    'send' => 'Send',
    'password' => 'Password',
    'we_welcome_you_in_ptsu' => 'We Welcome you in PTSU',
    'Search_here' => 'Search here ...',
    'share_us_your_opinion_to_acheive_the_best_service_for_you' => 'Share us your Opinion to acheive the best service for you',
    'fellowship_request' => 'Fellowship Request',
    'name' => 'Name',
    'confirm_password' => 'Confirm Password',
    'male' => 'Male',
    'female' => 'Female',
    'register' => 'Register',
    'system_name' => 'System Name',

];
