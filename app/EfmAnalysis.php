<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EfmAnalysis extends Model
{
    	protected $fillable = [
    		'step',
    		'place',
    		'time',
    		'responsible',
    		'notes',
    		'pro_id',
    	];
}
