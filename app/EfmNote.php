<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EfmNote extends Model
{
    	protected $fillable = [
    		'field',
    		'note',
    		'assess',
    		'pro_id',
    	];
}
