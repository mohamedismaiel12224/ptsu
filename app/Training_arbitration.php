<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training_arbitration extends Model
{
    protected $primaryKey = 'id';
    public $table='training_arbitration';
    public $timestamps = false;
}