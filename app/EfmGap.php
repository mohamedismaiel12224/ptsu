<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EfmGap extends Model
{
    	protected $fillable = [
    		'responsibility',
    		'gaps',
    		'percentage',
    		'expert',
    		'justifications',
    		'pro_id',
    	];
}
