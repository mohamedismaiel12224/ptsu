<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Activities;
use App\Training_outputs;
use App\Branches;
use App\Sessions;
use DB;
class MitProjects extends Model
{
    public static function report($id)
    {
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $sessions = Sessions::where('pro_id',$id)->get();
    	foreach ($sessions as $key => $value) {
            $arr=explode(',', $value->branch_arr);
    		$branches = Branches::where('pro_id',$id)->whereIn('id',$arr)->get();
            $sum =0;
    		foreach ($branches as $ke => $val) {
                $tr_arr = explode(',', $val->series_no);
                $sum += Training_outputs::whereIn('series_no',$tr_arr)->where('pro_id',$id)->sum('achievement');
                $sum += Collerations::whereIn('series_no',$tr_arr)->where('pro_id',$id)->sum('achievement');

    			$training_output = DB::select("select status, type, learning_level, investigation, training_method_id, mental_depth, compulsive, buttress,separate,achievement, series_no, id, detail as name, pro_id, evaluate_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) and series_no in( ".$val->series_no." ) union select 't' as status,type, learning_level, investigation, training_method_id, mental_depth, compulsive, buttress,separate,achievement, series_no, id, name, pro_id, evaluate_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) and series_no in( ".$val->series_no." ) order by series_no");
    			foreach ($training_output as $k => $v) {
    				$activities = Activities::where('pro_id',$id)->where('series_no',$v->series_no)->get();
    				$v->activities = $activities;
    			}
    			$val->training_output = $training_output;
    		}
    		$value->branches = $branches;
            $value->sum = $sum;


    	}
    	return $sessions;
    }
}
