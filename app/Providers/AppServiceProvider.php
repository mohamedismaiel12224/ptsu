<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Setting;
use App\Efficient;
use App\System;
use App\Section_lib;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         View::composer('*', function($view) {
            view::share('setting',Setting::find(1)); 
            view::share('efficients',Efficient::where('status',0)->get()); 
            view::share('norms',Efficient::where('status',1)->get()); 
            view::share('sys',System::limit(6)->orderBy('id','desc')->get()); 
            view::share('elib',Section_lib::limit(6)->orderBy('id','desc')->get()); 
         });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
