<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Checkup;
use App\Procedure;
class ProcedureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id,$type)
    {
        $query['checkup_id'] =$id;
        $query['checkup'] = Checkup::find($id);
        $query['system_setting'] = 1;

        $query['type'] =$type;
        return view('admin/procedures/add',$query);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->input('procedure.*') as $key => $value) {
            if($value){
                $procedure = new Procedure;
                $procedure->procedure = $value;
                $procedure->checkup_id = $request->checkup_id;
                $procedure->type = $request->type;
                $procedure->save();
            }
        }
        return redirect('procedures/'.$procedure->checkup_id.'/'.$procedure->type)->with('success','تم الحفظ بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$type)
    {
        $query['system_setting'] = 1;

        $query['type'] =$type;
        $query['checkup'] = Checkup::find($id);
        $query['procedures'] = Procedure::where('type',$type)->where('checkup_id',$id)->get();
        return view('admin/procedures/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['procedure'] = Procedure::find($id);
        $query['system_setting'] = 1;
        $query['type'] =$query['procedure']->type;
        $query['checkup'] = Checkup::find($query['procedure']->checkup_id);

        return view('admin/procedures/add',$query);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $procedure = Procedure::find($id);
        $procedure->procedure = $request->procedure;
        $procedure->save();
        return redirect('procedures/'.$procedure->checkup_id.'/'.$procedure->type)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $procedure = Procedure::find($id);
        $procedure->delete();
        return redirect()->back()->with('success','تم الحذف بنجاح');
    }
}
