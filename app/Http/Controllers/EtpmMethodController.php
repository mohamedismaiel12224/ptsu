<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EtpmProject;
use App\TrainerEtpm;
use App\System;
use App\EvaluateEtpmTrainer;
use App\Procedure;
use App\ArbitratorProject;
use App\Domain;
use App\Checkup;
use App\EtpmTrainerGroup;
use App\GroupTrainerEtpm;
use App\DescriptionEvaluate;
use Auth;

class EtpmMethodController extends Controller
{
		 public function __construct()
		{
			$this->middleware('auth');
			$this->middleware(function ($request, $next) {
					$this->role = Auth::user()->role;
					if($this->role!=3&&$this->role!=1&&$this->role!=4)
							return redirect('login');
					return $next($request);
			});
		}
		public function link_evaluate($id){
			$query['etpm_pro'] = 1;
			$query['ev'] = EtpmProject::find($id);
			$query['link']=url('etpm/login/'.$id);
			$query['word'] = 'الرابط للمقيمين';
			return view('admin/systems/performance/link',$query);

		}
		public function trainers($id)
		{
				$query['ev'] = EtpmProject::find($id);
				$query['etpm_evaluator_sidebar']  = 1;
				$query['evaluator']  = 1;
				$query['trainers'] = TrainerEtpm::select('trainer_etpms.*','group_trainer_etpms.group')->leftJoin('group_trainer_etpms','group_trainer_etpms.id','trainer_etpms.group_id')->where('trainer_etpms.pro_id',$id)->get();
				return view('admin/systems/etpm/trainers/show',$query);
		}
		public function new_trainer_etpm($trainer_id,$pro_id){
				$check = new EtpmTrainerGroup;
				$check->trainer_id = $trainer_id;
				$check->pro_id = $pro_id;
				$check->evaluator_id = Auth::id();
				$check->save();
				return redirect('etpm/trainer_evaluate/'.$check->id);
		}
		public function trainer_etpm_group($trainer_id,$pro_id){
				$query['etpm_evaluator_sidebar']  = 1;
				$query['ev'] = EtpmProject::find($pro_id);
				$query['groups'] = EtpmTrainerGroup::where('trainer_id',$trainer_id)->where('pro_id',$pro_id)->where('evaluator_id',Auth::id())->get();
				return view('admin/systems/etpm/evaluate_trainers_group',$query);
				
		}
		public function trainer_evaluate($group_id){
				$query['etpm_evaluator_sidebar']  = 1;

				$query['logo'] = System::find(4)->logo;
				$query['group'] = EtpmTrainerGroup::find($group_id);
				$query['trainer'] = TrainerEtpm::find($query['group']->trainer_id);
				$query['ev'] = EtpmProject::find($query['group']->pro_id);
				$query['domain'] = Domain::where('status',6)->get();
				foreach ($query['domain'] as $key => $value) {
					$value->checkup = Checkup::where('domain_id',$value->id)->get();
					foreach ($value->checkup as $ke => $val) {
							$val->procedure = Procedure::where('checkup_id',$val->id)->where('type',1)->get(); 
							foreach ($val->procedure as $key => $v) {
								$v->evaluate = 0;
								$check = EvaluateEtpmTrainer::where('group_id',$group_id)->where('procedure_id',$v->id)->first();
								if($check){
										$v->evaluate = $check->evaluate;
										$v->notes = $check->notes;
								}
							 }

							$val->scrutiny = Procedure::where('checkup_id',$val->id)->where('type',2)->get();
							foreach ($val->scrutiny as $key => $v) {
								$v->evaluate = 0;
								$check = EvaluateEtpmTrainer::where('group_id',$group_id)->where('procedure_id',$v->id)->first();
								if($check){
										$v->evaluate = $check->evaluate;
										$v->notes = $check->notes;
								}
							 } 
					}
				}
				return view('admin/systems/etpm/evaluate',$query);
		}
		public function trainer_evaluate_update($group_id){
			$query['group'] = EtpmTrainerGroup::find($group_id);
			$query['trainer'] = TrainerEtpm::find($query['group']->trainer_id);
			$query['ev'] = EtpmProject::find($query['group']->pro_id);
			$query['domain'] = Domain::where('status',6)->get();
			foreach ($query['domain'] as $key => $value) {
				$notes = 'notes'.$value->id;
				$value->checkup = Checkup::where('domain_id',$value->id)->get();
				foreach ($value->checkup as $ke => $val) {
					$val->procedure = Procedure::where('checkup_id',$val->id)->where('type',1)->get(); 
					foreach ($val->procedure as $key => $v) {
						$name = 'evaluate'.$v->id;
						if(!isset($_POST[$name])||$_POST[$name]==0)
								return redirect()->back()->with('error','يوجد حقل فارغ');

						$check = EvaluateEtpmTrainer::where('group_id',$group_id)->where('procedure_id',$v->id)->first();
						if($check){
								$check->evaluate =  $_POST[$name];
								$check->notes = $_POST[$notes];
								$check->save();
						}else{
								$ev = new EvaluateEtpmTrainer();
								$ev->pro_id = $query['group']->pro_id;
								$ev->trainer_id = $query['group']->trainer_id;
								$ev->group_id = $group_id;
								$ev->evaluator_id = Auth::id();
								$ev->procedure_id = $v->id;
								$ev->evaluate = $_POST[$name];
								$ev->notes = $_POST[$notes];
								$ev->save();
						}
					 }
					 $val->scrutiny = Procedure::where('checkup_id',$val->id)->where('type',2)->get();
					foreach ($val->scrutiny as $key => $v) {
						$name = 'evaluate'.$v->id;
						if(!isset($_POST[$name])||$_POST[$name]==0)
								return redirect()->back()->with('error','يوجد حقل فارغ');

						$check = EvaluateEtpmTrainer::where('group_id',$group_id)->where('procedure_id',$v->id)->first();
						if($check){
							$check->evaluate =  $_POST[$name];
							$check->notes = $_POST[$notes];
							$check->save();
						}else{
							$ev = new EvaluateEtpmTrainer();
							$ev->pro_id = $query['group']->pro_id;
							$ev->trainer_id = $query['group']->trainer_id;
							$ev->group_id = $group_id;
							$ev->evaluator_id = Auth::id();
							$ev->procedure_id = $v->id;
							$ev->evaluate = $_POST[$name];
							$ev->notes = $_POST[$notes];
							$ev->save();
						}
					} 
				}      
			}
			return redirect('evaluator/etpm/'.$query['group']->pro_id)->with('success','تم التقييم بنجاح');
		}      
								
		public function report_evaluator($id,$pro_id){
			$query['description_evaluate_5'] = DescriptionEvaluate::where('evaluate',5)->where('status',5)->first();
			$query['description_evaluate_4'] = DescriptionEvaluate::where('evaluate',4)->where('status',5)->first();
			$query['description_evaluate_3'] = DescriptionEvaluate::where('evaluate',3)->where('status',5)->first();
			$query['description_evaluate_2'] = DescriptionEvaluate::where('evaluate',2)->where('status',5)->first();
			$query['description_evaluate_1'] = DescriptionEvaluate::where('evaluate',1)->where('status',5)->first();
			$query['etpm_pro'] = 1;
			$query['logo'] = System::find(4)->logo;
			$query['trainer'] = TrainerEtpm::find($id);
        		$color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a'];
			$query['ev'] = EtpmProject::find($pro_id);
			$query['etpm_evaluate_chart'] = $query['evaluator'] = ArbitratorProject::join('users','users.id','arbitrator_projects.user_id')->where('arbitrator_projects.status',3)->where('pro_id',$pro_id)->get();
			foreach ($query['evaluator'] as $key => $ev) {
				$ev->domain = Domain::where('status',6)->get();
				foreach ($ev->domain as $key => $domain) {
					$domain->sum = 0;
					$domain->count = 0;
					$domain->checkup = Checkup::where('domain_id',$domain->id)->get();
					
					foreach ($domain->checkup as $ke => $val) {
						$val->procedure = Procedure::where('checkup_id',$val->id)->where('type',1)->get(); 
						foreach ($val->procedure as $key => $v) {
							$v->group = EtpmTrainerGroup::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->get();
				                   foreach ($v->group as $key => $gr) {
				                        $gr->evaluate = 0;
				                        $check = EvaluateEtpmTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->where('group_id',$gr->id)->where('procedure_id',$v->id)->first();
				                        if($check){
				                            $gr->evaluate = $check->evaluate;
				                            $gr->notes = $check->notes;

				                            	$domain->sum +=  $check->evaluate;
									$domain->count ++;
				                        }
				                    }
						 }
						$val->scrutiny = Procedure::where('checkup_id',$val->id)->where('type',2)->get();
						foreach ($val->scrutiny as $key => $v) {
							$v->group = EtpmTrainerGroup::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->get();
				                   foreach ($v->group as $key => $gr) {
				                        $gr->evaluate = 0;
				                        $check = EvaluateEtpmTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->where('group_id',$gr->id)->where('procedure_id',$v->id)->first();
				                        if($check){
				                            $gr->evaluate = $check->evaluate;
				                            $gr->notes = $check->notes;
				                        }
				                   }
						} 
					}
					$domain->group = EtpmTrainerGroup::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->get();

					foreach ($domain->group as $ke_gro => $gro) {
					    	$gro->color = $color[$ke_gro%5];

					    	$gro->checkup = Checkup::where('domain_id',$domain->id)->get();
					    	foreach ($gro->checkup as $key => $v_ch) {
					    		$v_ch->count = 0;
					    		$v_ch->sum = 0;
							$v_ch->evaluate = 0;

					        	$v_ch->procedure = Procedure::where('checkup_id',$v_ch->id)->where('type',1)->get(); 
							foreach ($v_ch->procedure as $key => $v) {
								
								$v->evaluate = 0;
								$check = EvaluateEtpmTrainer::where('group_id',$gro->id)->where('procedure_id',$v->id)->first();
								if($check){
									$v_ch->sum +=$check->evaluate;
					    				$v_ch->count ++;
									$v->evaluate = $check->evaluate;
									$domain->notes = $check->notes;
								}
							 }
							if($v_ch->count !=0)
								$v_ch->evaluate = $v_ch->sum / $v_ch->count;
							$v_ch->scrutiny = Procedure::where('checkup_id',$v_ch->id)->where('type',2)->get();
							foreach ($v_ch->scrutiny as $key => $v) {
								$v->evaluate = 0;
								$check = EvaluateEtpmTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->where('procedure_id',$v->id)->first();
								if($check){
									$v->evaluate = $check->evaluate;
									$domain->notes = $check->notes;
								}
							}
					    }
					}
				}
			}
			// print_r($query['etpm_evaluate_chart'][0]);exit();
			return view('admin/systems/etpm/report_evaluator',$query);
		}
		public function report_all_evaluator($id,$pro_id){
			$query['description_evaluate_5'] = DescriptionEvaluate::where('evaluate',5)->where('status',5)->first();
			$query['description_evaluate_4'] = DescriptionEvaluate::where('evaluate',4)->where('status',5)->first();
			$query['description_evaluate_3'] = DescriptionEvaluate::where('evaluate',3)->where('status',5)->first();
			$query['description_evaluate_2'] = DescriptionEvaluate::where('evaluate',2)->where('status',5)->first();
			$query['description_evaluate_1'] = DescriptionEvaluate::where('evaluate',1)->where('status',5)->first();
				$query['etpm_pro'] = 1;
				$query['logo'] = System::find(4)->logo;
				$query['trainer'] = TrainerEtpm::find($id);
				$query['ev'] = EtpmProject::find($pro_id);

				$query['domains_bar'] = Domain::where('status',6)->get();
				foreach ($query['domains_bar'] as $key => $domain) {
						$domain->checkup = Checkup::where('domain_id',$domain->id)->get();
						$domain->sum = 0;
						$domain->count = 0;
						foreach ($domain->checkup as $ke => $val) {
								$procedure = Procedure::where('checkup_id',$val->id)->where('type',1)->get(); 
								$val->evaluate_5 = 0;
								$val->evaluate_4 = 0;
								$val->evaluate_3 = 0;
								$val->evaluate_2 = 0;
								$val->evaluate_1 = 0;
								foreach ($procedure as $k => $v) {
										$val->evaluate_5+= EvaluateEtpmTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',5)->where('procedure_id',$v->id)->count();
										$val->evaluate_4 += EvaluateEtpmTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',4)->where('procedure_id',$v->id)->count();
										$val->evaluate_3 += EvaluateEtpmTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',3)->where('procedure_id',$v->id)->count();
										$val->evaluate_2 += EvaluateEtpmTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',2)->where('procedure_id',$v->id)->count();
										$val->evaluate_1 += EvaluateEtpmTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',1)->where('procedure_id',$v->id)->count();

								 }
								$val->sum = $val->evaluate_5 * 5 + $val->evaluate_4 * 4 + $val->evaluate_3 * 3 + $val->evaluate_2 * 2 + $val->evaluate_1;
								$val->count = $val->evaluate_5 + $val->evaluate_4 + $val->evaluate_3 + $val->evaluate_2 + $val->evaluate_1;
								$domain->sum += $val->sum;
								$domain->count += $val->count;
						}
				}
				return view('admin/systems/etpm/report_all_evaluator',$query);
		}
		public function report_evaluate($pro_id, $group_id = false){
			$query['description_evaluate_5'] = DescriptionEvaluate::where('evaluate',5)->where('status',5)->first();
			$query['description_evaluate_4'] = DescriptionEvaluate::where('evaluate',4)->where('status',5)->first();
			$query['description_evaluate_3'] = DescriptionEvaluate::where('evaluate',3)->where('status',5)->first();
			$query['description_evaluate_2'] = DescriptionEvaluate::where('evaluate',2)->where('status',5)->first();
			$query['description_evaluate_1'] = DescriptionEvaluate::where('evaluate',1)->where('status',5)->first();
		       $query['etpm_pro'] = 1;
			$query['logo'] = System::find(4)->logo;
			$query['ev'] = EtpmProject::find($pro_id);
		       $color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a'];
		       $query['groups'] = GroupTrainerEtpm::where('user_id',Auth::id())->where('pro_id',$pro_id)->get();
		        if(!$group_id)
		            return view('admin/systems/etpm/specific_group',$query);
		        else{
		            if($group_id=='*'){
		                $trainers = TrainerEtpm::where('pro_id',$pro_id)->get();
		            }else{
		                $trainers = TrainerEtpm::where('pro_id',$pro_id)->where('group_id',$group_id)->get();
		            }
		            $arr = array();
		            foreach ($trainers as $key => $t) {
		                array_push($arr, $t->id);
		            }
		        }

		        $query['domains_bar'] = Domain::where('status',6)->get();

		        foreach ($query['domains_bar'] as $key => $domain) {
					$domain->sum = 0;
					$domain->count = 0;
					$domain->checkup = Checkup::where('domain_id',$domain->id)->get();
					foreach ($domain->checkup as $ke => $val) {
							$procedure = Procedure::where('checkup_id',$val->id)->where('type',1)->get(); 
							$val->evaluate_5 = 0;
							$val->evaluate_4 = 0;
							$val->evaluate_3 = 0;
							$val->evaluate_2 = 0;
							$val->evaluate_1 = 0;
							foreach ($procedure as $k => $v) {
									$val->evaluate_5+= EvaluateEtpmTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',5)->where('procedure_id',$v->id)->count();
									$val->evaluate_4 += EvaluateEtpmTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',4)->where('procedure_id',$v->id)->count();
									$val->evaluate_3 += EvaluateEtpmTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',3)->where('procedure_id',$v->id)->count();
									$val->evaluate_2 += EvaluateEtpmTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',2)->where('procedure_id',$v->id)->count();
									$val->evaluate_1 += EvaluateEtpmTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',1)->where('procedure_id',$v->id)->count();

							 }
							$val->sum = $val->evaluate_5 * 5 + $val->evaluate_4 * 4 + $val->evaluate_3 * 3 + $val->evaluate_2 * 2 + $val->evaluate_1;
							$val->count = $val->evaluate_5 + $val->evaluate_4 + $val->evaluate_3 + $val->evaluate_2 + $val->evaluate_1;
							$domain->sum += $val->sum;
							$domain->count += $val->count;
							
					}
			}
		        return view('admin/systems/etpm/report_all_evaluator',$query);
		    }
		    public function post_evaluate($pro_id)
		    {
		        $group_id = $_POST['group_id'];
		        return redirect('etpm/report_evaluate/'.$pro_id.'/'.$group_id);
		    }
}
