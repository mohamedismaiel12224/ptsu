<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\Developer;
use App\Logos;
use App\News;
use App\Videos;
use App\Section_lib;
use App\Efficient;
use App\Courses;
use App\Elibrary;
use App\Poll;
use App\Trainers_courses;
use App\Fellowship;
use App\Suggest;
use App\Trainer_poll;
use App\Mailing;
use App\System;
use App\Slider;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['setting']=Setting::find(1);
        $query['partners']=Logos::where('status',0)->get();
        $query['banners']=Logos::where('status',1)->get();
        $query['news']=News::all();
        $query['slider']=Slider::all();
        $query['developer']= Developer::limit(5)->get();
        $query['videos']= Videos::limit(2)->orderBy('id','desc')->get();
        $query['elibrary']=Section_lib::limit(5)->get();
        $color=['de66d5','27e9ec','ffe9a4','76fbc5'];
        $query['poll']=Poll::all();
        foreach ($query['poll'] as $key => $value) {
            $value->suggest = Suggest::where('poll_id',$value->id)->get();
            $x= Trainer_poll::where('poll_id',$value->id)->count();
            foreach ($value->suggest as $ke => $val) {
                $y =Trainer_poll::where('poll_id',$value->id)->where('suggest_id',$val->id)->count();
                if($x==0)
                    $val->percentage = 0;
                else
                    $val->percentage = round(($y/$x)*100);
                $val->color=$color[$ke];

            }
        }
        $lang = session()->get('language');
        App::setLocale($lang);
        return view('home',$query);
    }
    public function votes(){
        $query['poll']=Poll::all();
        foreach ($query['poll'] as $key => $value) {
            $value->suggest = Suggest::where('poll_id',$value->id)->get();
            $x= Trainer_poll::where('poll_id',$value->id)->count();
            foreach ($value->suggest as $ke => $val) {
                $y =Trainer_poll::where('poll_id',$value->id)->where('suggest_id',$val->id)->count();
                if($x==0)
                    $val->percentage = 0;
                else
                    $val->percentage = round(($y/$x)*100);
            }
        }
        $lang = session()->get('language');
        App::setLocale($lang);
        return view('vote',$query);
    }
    public function lang($lang){
        App::setLocale($lang);
        if($lang=='ar'){
            session()->put('language','ar');
        }
        else{
            session()->put('language', 'en');
        }
        return Redirect::back();
    }
    public function about()
    {
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['setting']=Setting::find(1);
        return view('about',$query);
    }
    public function contact_us()
    {
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['setting']=Setting::find(1);
        return view('contact_us',$query);
    }
    public function videos()
    {
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['videos']=Videos::orderBy('id','desc')->get();
        return view('videos',$query);
    }
    public function all_developers(){
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['developers'] = Developer::all();
        return view('developers',$query);
    }
    public function developer($id){
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['developer'] = Developer::find($id);
        if(!$query['developer'])
            return view('404');
        return view('developer_profile',$query);
    }
    public function elibraries(){
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['elibrary']=Section_lib::all();
        return view('elibrary',$query);   
    }
    public function elibrary($id){
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['elibrary']=Elibrary::where('section_id',$id)->get();
        return view('materials',$query);  
    }
    public function effs($id){
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['page'] = Efficient::find($id);
        if(!$query['page'])
            return view('404');
        return view('page',$query); 
    }
    public function search(){
        $lang = session()->get('language');
        App::setLocale($lang);
        $x=$_POST['search'];
        $query['cert'] = Trainers_courses::join('users','users.id','trainers_courses.trainer_id')
            ->join('courses','courses.id','trainers_courses.course_id')
            ->select('trainers_courses.id as id','courses.name as name','email','users.name as username')
            ->where('role',2)
            ->where('trainers_courses.status',1)
            ->where(function ($query) use ($x) {
                $query->where('users.name', '=', $x)
                    ->orwhere('cert_no', '=', $x)
                    ->orwhere('nno', '=', $x);
            })
            ->get();
        return view('table',$query);
    }
    public function cert(){
        $lang = session()->get('language');
        App::setLocale($lang);
        $id=$_POST['cert_id'];
        $query['user']=Trainers_courses::join('users','users.id','trainers_courses.trainer_id')->where('trainers_courses.id',$id)->first();
        $query['course']=Courses::find($query['user']->course_id);
        return view('show_certification',$query);
    }
    public function certifications(){
        $lang = session()->get('language');
        App::setLocale($lang);
        return view('certificate');
    }
    public function poll(){
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['poll_id'] = $poll_id=$_POST['poll_id'];
        $suggest_id=$_POST['suggest'];
        $check= Suggest::where('poll_id',$poll_id)->where('id',$suggest_id)->first();
        if(!$check){
            return json_encode(array('error' => 'error'));
        }else{
            $n= new Trainer_poll;
            $n->trainer_id = Auth::id()|0;
            $n->poll_id = $poll_id;
            $n->suggest_id = $suggest_id;
            $n->save();

            $query['suggest'] = Suggest::where('poll_id',$poll_id)->get();
            $x= Trainer_poll::where('poll_id',$poll_id)->count();
            foreach ($query['suggest'] as $ke => $val) {
                $y =Trainer_poll::where('poll_id',$poll_id)->where('suggest_id',$val->id)->count();
                $val->percentage = round(($y/$x)*100);
            }
            return view('block',$query);
        }
    }

    public function systems(){
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['systems'] = System::all();
        return view('systems',$query);
    }

    public function system($id){
        $lang = session()->get('language');
        App::setLocale($lang);
        $query['system'] = System::find($id);
        if(Auth::check()&&Auth::user()->role==3 && Auth::user()->system_id==$id){
            $query['materials'] = Fellowship::join('users','users.system_id','fellowships.system_id')->select('*','fellowships.role as role','fellowships.name as name')->where('fellowships.system_id',$id)->where('users.id',Auth::id())->get();
        }
        return view('system_detail',$query);
    }

    public function mailing(Request $request){
        $this->validate($request, [
            'email' => 'required|email|unique:mailings,email',
            'phone' => 'required|unique:mailings,phone',
        ]);
        $m =new Mailing;
        $m->email=$request->email;
        $m->phone=$request->phone;
        if($m->save())
            session()->flash('success', 'تم الحفظ بنجاح');
    }

    public function viewfile($id)
    {
        return Response::make(file_get_contents(url('assets/images/library/'.$id)), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$id.'"'
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
