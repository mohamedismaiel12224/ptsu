<?php

namespace App\Http\Controllers;
use App\MitProjects;
use App\Collerations;
use App\SystemAttribute;
use App\Training_outputs;
use App\Program_level;
use App\Setting;
use App\System;
use App\Activities;
use Illuminate\Http\Request;
use DB;
use Auth;

class MitMethodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function vertical($id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['collerations'] = Collerations::where('pro_id',$id)->get();
        $query['training'] = Training_outputs::where('pro_id',$id)->get();
        return view('admin/systems/show_vertical',$query);
    }
    public function reseries($id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['collerations'] = Collerations::where('pro_id',$id)->update(['series_no'=>0]);
        $query['training'] = Training_outputs::where('pro_id',$id)->update(['series_no'=>0]);
        $mit = MitProjects::find($id);
        if($mit->step_no==4)
            $mit->step_no=3;
        $mit->save();
        return redirect('vertical/'.$id);
    }
    public function verticalUpdate(Request $request,$id){
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['collerations'] = Collerations::where('pro_id',$id)->get();
        $count_collerations = Collerations::where('pro_id',$id)->count() |0;

        $query['training'] = Training_outputs::where('pro_id',$id)->get();
        $count_training = Training_outputs::where('pro_id',$id)->count() |0;
        $count = $count_training + $count_collerations;

        foreach ($query['training'] as $key => $value) {
            $name= 'tr'.$value->id;
            if(!isset($_POST[$name])||!$_POST[$name])
                return redirect()->back()->with('error','يوجد حقل فارغ');
            if($_POST[$name]<1 || $_POST[$name]> $count)
                return redirect()->back()->with('error','يوجد رقم خطأ');
            if(Training_outputs::where('pro_id',$id)->where('id','!=',$value->id)->where('series_no',$_POST[$name])->first() || Collerations::where('pro_id',$id)->where('series_no',$_POST[$name])->first())
                return redirect()->back()->with('error','يوجد رقم مكرر');
            $tr = Training_outputs::find($value->id);
            $tr->series_no = $_POST[$name];
            $tr->save();
        }
        foreach ($query['collerations'] as $key => $value) {
            $name= 'col'.$value->id;
            if(!isset($_POST[$name])||!$_POST[$name])
                return redirect()->back()->with('error','يوجد حقل فارغ');
            if($_POST[$name]<1 || $_POST[$name]> $count)
                return redirect()->back()->with('error','يوجد رقم خطأ');
            if(training_outputs::where('pro_id',$id)->where('series_no',$_POST[$name])->first() || Collerations::where('pro_id',$id)->where('series_no',$_POST[$name])->first())
                return redirect()->back()->with('error','يوجد رقم مكرر');
            $co = Collerations::find($value->id);
            $co->series_no = $_POST[$name];
            $co->save();
        }
        $mit = MitProjects::find($id);
        if($mit->step_no==3)
            $mit->step_no=4;
        $mit->save();
        return redirect()->route('program_level.show',$id)->with('success','تم التعديل بنجاح');
    }
    public function select_level($id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['program'] = Program_level::where('pro_id',$id)->get();
        return view('admin/systems/select_level',$query);
    }
    public function program_level_selected(Request $request, $id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['mit']->program_level_id=$request->program_level_id;
        if($query['mit']->step_no==5)
            $query['mit']->step_no=6;
        $query['mit']->save();
        return redirect('mit_type/'.$id);
    }

    public function mit_type($id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['all'] = DB::select("select series_no, id, detail as name, pro_id, type from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select series_no, id, name, pro_id, type from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/mit_type',$query);
    }

    public function mit_type_update(Request $request,$id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $all = DB::select("select series_no, id, detail as name, pro_id,'c' as status from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select series_no, id, name, pro_id, 't' as status from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        foreach ($all as $key => $value) {
            $name = 'type'.$value->series_no;
            if(!isset($_POST[$name])||$_POST[$name]==0)
                return redirect()->back()->with('error','يوجد اختيار فارغ');
            if($value->status =='t'){
                $t = Training_outputs::find($value->id);
                $t->type = $_POST[$name];
                $t->save();
            }elseif($value->status=='c'){
                $c = Collerations::find($value->id);
                $c->type = $_POST[$name];
                $c->save();
            }
        }
        $mit = MitProjects::find($id);
        if($mit->step_no==6)
            $mit->step_no=7;
        $mit->save();
        return redirect('learning_level/'.$id);

    }
    public function learning_level($id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['all'] = DB::select("select learning_level, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/learning_level',$query);
    }

    public function learning_level_edit($series_no,$pro_id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $query['series_no'] = $series_no;
        $x=Training_outputs::where('series_no',$series_no)->where('pro_id',$pro_id)->first();
        if($x){
            $query['type']  = $x->type;
        }else{
            $y=Collerations::where('series_no',$series_no)->where('pro_id',$pro_id)->first();
            $query['type']  = $y->type;
        }
        return view('admin/systems/learning_level_edit',$query);
    }

    public function learning_level_update(Request $request,$series_no,$pro_id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();

        $type = $request->status;
        if($type==0){
            if(!$request->value)
                return redirect()->back()->with('error','لا يوجد قيمة');
            $data['learning_level']=$request->value;
        }elseif($type==1){
            if(!$request->select)
                return redirect()->back()->with('error','لم يتم تحديد مستوى');
            $data['learning_level']=$request->select;
        }elseif($type==2){
            $tr=Training_outputs::where('series_no',$series_no)->where('pro_id',$pro_id)->first();
            if($tr){
                $type = $tr->type;
            }else{
                $col=Collerations::where('series_no',$series_no)->where('pro_id',$pro_id)->first();
                $type  = $col->type;
            }
            if($type==2||$type==5){
                if(!$request->M || !$request->m || ! $request->n)
                    return redirect()->back()->with('error','لم يتم وضع كل معامل المعادلة');
                $md = 100*($request->M)/($request->m * $request->n);
                $data['learning_level']= 100 - $md;
            }else{
                if(!$request->a || ! $request->n)
                    return redirect()->back()->with('error','لم يتم وضع كل معامل المعادلة');
                $md = 100*($request->a) / ($request->n);
                $data['learning_level']= 100 - $md;
            }
        }
        Training_outputs::where('series_no',$series_no)->where('pro_id',$pro_id)->update($data);
        Collerations::where('series_no',$series_no)->where('pro_id',$pro_id)->update($data);
        $all = DB::select("select series_no, id, detail as name, pro_id,'c' as status, learning_level from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$pro_id." and series_no in( ".$program->series_no_arr." ) union select series_no, id, name, pro_id, 't' as status, learning_level from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$pro_id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        $update=true;
        foreach ($all as $key => $value) {
            if(!$value->learning_level)
                $update=false;
        }
        if($update){    
            $mit = MitProjects::find($pro_id);
            if($mit->step_no==7)
                $mit->step_no=8;
            $mit->save();
            return redirect('practice/'.$pro_id);

        }
        return redirect('learning_level/'.$pro_id);
    }

    public function practice($id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['all'] = DB::select("select learning_level,type, series_no, id, detail as name, pro_id, investigation from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select learning_level,type, series_no, id, name, pro_id, investigation from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/practice',$query);
    }

    public function investigation(Request $request,$id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $all = DB::select("select series_no, id, detail as name, pro_id,'c' as status from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select series_no, id, name, pro_id, 't' as status from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        foreach ($all as $key => $value) {
            $name = 'investigation'.$value->series_no;
            if(!isset($_POST[$name])||$_POST[$name]==0)
                return redirect()->back()->with('error','يوجد اختيار فارغ');
            if($value->status =='t'){
                $t = Training_outputs::find($value->id);
                $t->investigation = $_POST[$name];
                $t->save();
            }elseif($value->status=='c'){
                $c = Collerations::find($value->id);
                $c->investigation = $_POST[$name];
                $c->save();
            }
        }
        $mit = MitProjects::find($id);
        if($mit->step_no==8)
            $mit->step_no=9;
        $mit->save();
        return redirect('training_method/'.$id);

    }

    public function training_method($id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['method'] = SystemAttribute::where('type_id',2)->get();
        $query['all'] = DB::select("select training_method_id, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select training_method_id, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/training_method',$query);
    }

    public function training_method_update(Request $request,$id){
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $all = DB::select("select series_no, id, detail as name, pro_id,'c' as status from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select series_no, id, name, pro_id, 't' as status from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        foreach ($all as $key => $value) {
            $name = 'method'.$value->series_no;
            if(!isset($_POST[$name])||$_POST[$name]==0)
                return redirect()->back()->with('error','يوجد اختيار فارغ');
            $m = '';
            foreach ($_POST[$name] as $key => $val) {
                if($m=='')
                    $m .= $val;
                else
                    $m .= ','.$val;

            }
            if($value->status =='t'){
                $t = Training_outputs::find($value->id);
                $t->training_method_id = $m;
                $t->save();
            }elseif($value->status=='c'){
                $c = Collerations::find($value->id);
                $c->training_method_id = $m;
                $c->save();
            }
        }
        $mit = MitProjects::find($id);
        if($mit->step_no==9)
            $mit->step_no=10;
        $mit->save();
        return redirect('learning_planner/'.$id);

    }
    public function learning_planner($id){
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['all'] = DB::select("select learning_level, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        foreach ($query['all'] as $key => $value) {
            $value->difficult = MitMethodsController::avg_level($value->learning_level);
        }
        $query['planner_chart'] = 1;
        return view('admin/systems/planner',$query);

    }
    public static function avg_level($learning_level){
        $setting = Setting::find(1);
        if(is_numeric($learning_level)){
            return $learning_level;
        }else{
            if($learning_level == 'd'){
                return ((100+$setting->low_degree)/2);
            }elseif($learning_level == 'm'){
                return ($setting->low_degree + $setting->high_degree)/2; 
            }elseif($learning_level == 's'){
                return $setting->high_degree/2;
            }
        }
    }
    public function tem($id){
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['time'] = SystemAttribute::where('type_id',3)->get();
        $query['tools'] = SystemAttribute::where('type_id',4)->get();
        $query['responsibilty'] = SystemAttribute::where('type_id',5)->get();
        $query['all'] = DB::select("select series_no, id, detail as name, pro_id, type, style,tool,time,responsibilty from collerations where pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select series_no, id, name, pro_id, type, style,tool,time,responsibilty from training_outputs where  pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/tem',$query);
    }

    public function tem_update(Request $request,$id){
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();

        $all = DB::select("select series_no, id, detail as name, pro_id, type, style,tool,time,responsibilty,'c' as status from collerations where pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select series_no, id, name, pro_id, type, style,tool,time,responsibilty,'t' as status from training_outputs where  pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        foreach ($all as $key => $value) {
            $style = 'style'.$value->series_no;
            $time = 'time'.$value->series_no;
            $tool = 'tool'.$value->series_no;
            $responsibilty = 'responsibilty'.$value->series_no;
            if(!isset($_POST[$style])||!isset($_POST[$time])||!isset($_POST[$tool])||!isset($_POST[$responsibilty])||$_POST[$time]==0||$_POST[$tool]==0||count($_POST[$responsibilty])==0)
                return redirect()->back()->with('error','يوجد اختيار فارغ');
            $m = '';
            foreach ($_POST[$responsibilty] as $key => $val) {
                if($m=='')
                    $m .= $val;
                else
                    $m .= ','.$val;

            }
            if($value->status =='t'){
                $t = Training_outputs::find($value->id);
                $t->style = $_POST[$style];
                $t->time = $_POST[$time];
                $t->tool = $_POST[$tool];
                $t->responsibilty = $m;
                $t->save();
            }elseif($value->status=='c'){
                $c = Collerations::find($value->id);
                $c->style = $_POST[$style];
                $c->time = $_POST[$time];
                $c->tool = $_POST[$tool];
                $c->responsibilty = $m;
                $c->save();
            }
        }
        $mit = MitProjects::find($id);
        if($mit->step_no==10)
            $mit->step_no=11;
        $mit->save();
        return redirect('collect_content/'.$id);

    }
    public function collect_content($id){
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['all'] = DB::select("select learning_level,type, series_no, id, detail as name, pro_id, mental_depth, compulsive, buttress from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select learning_level,type, series_no, id, name, pro_id, mental_depth, compulsive, buttress from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/collect_content',$query);
    }
    public function collect_content_update(Request $request,$id){
         $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        if(!isset($_POST['public_depth'])||!$_POST['public_depth'])
                return redirect()->back()->with('error','يوجد حقل فارغ');
        $query['mit']->public_depth=$_POST['public_depth'];
        $query['mit']->save();
        $all = DB::select("select learning_level,type, series_no, id, detail as name, pro_id, mental_depth, compulsive, buttress,'c' as status from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select learning_level,type, series_no, id, name, pro_id, mental_depth, compulsive, buttress,'t' as status from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");

        foreach ($all as $key => $value) {
            $mental_depth = 'mental_depth'.$value->series_no;
            $compulsive = 'compulsive'.$value->series_no;
            $buttress = 'buttress'.$value->series_no;
            if(!isset($_POST[$mental_depth])||!isset($_POST[$compulsive])||!isset($_POST[$buttress])||!$_POST[$mental_depth]||!$_POST[$compulsive]||!$_POST[$buttress])
                return redirect()->back()->with('error','يوجد حقل فارغ');
            
            if($value->status =='t'){
                $t = Training_outputs::find($value->id);
                $t->mental_depth = $_POST[$mental_depth];
                $t->compulsive = $_POST[$compulsive];
                $t->buttress = $_POST[$buttress];
                $t->save();
            }elseif($value->status=='c'){
                $c = Collerations::find($value->id);
                $c->mental_depth = $_POST[$mental_depth];
                $c->compulsive = $_POST[$compulsive];
                $c->buttress = $_POST[$buttress];
                $c->save();
            }
        }
        $mit = MitProjects::find($id);
        if($mit->step_no==11)
            $mit->step_no=12;
        $mit->save();
        return redirect('training_activities/'.$id);

    }
    

    public function achievement($id){
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        config(['app.locale' => 'ar']);
        $query['training'] = DB::select("select separate,achievement,learning_level,type, series_no, id, detail as name, pro_id, mental_depth, compulsive, buttress,'c' as status from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select separate,achievement,learning_level,type, series_no, id, name, pro_id, mental_depth, compulsive, buttress,'t' as status from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        foreach ($query['training'] as $key => $value) {
            $value->sum = Activities::where('pro_id',$id)->where('series_no',$value->series_no)->sum('time');
        }
        return view('admin/systems/achievement',$query);
    }

    public function achievement_update($id){
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $training = DB::select("select separate,achievement,learning_level,type, series_no, id, detail as name, pro_id, mental_depth, compulsive, buttress,'c' as status from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select separate,achievement,learning_level,type, series_no, id, name, pro_id, mental_depth, compulsive, buttress,'t' as status from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        foreach ($training as $key => $value) {
            $separate = 'separate'.$value->series_no;
            $achievement = 'achievement'.$value->series_no;
            if(!isset($_POST[$separate])||!isset($_POST[$separate])||count($_POST[$separate])==0)
                return redirect()->back()->with('error','يوجد اختيار فارغ');
            $sum_time = Activities::where('pro_id',$id)->where('series_no',$value->series_no)->sum('time');
            if($sum_time >= $_POST[$achievement])
                return redirect()->back()->with('error','يوجد فترة انجاز اقل من زمن النشاط');
            $m = '';
            foreach ($_POST[$separate] as $key => $val) {
                if($m=='')
                    $m .= $val;
                else
                    $m .= ','.$val;
            }
            if($value->status =='t'){
                $t = Training_outputs::where('pro_id',$id)->where('series_no',$value->series_no)->first();
                $t->separate = $m;
                $t->achievement = $_POST[$achievement];
                $t->save();
            }elseif($value->status=='c'){
                $c = Collerations::where('pro_id',$id)->where('series_no',$value->series_no)->first();
                $c->separate = $m;
                $c->achievement = $_POST[$achievement];
                $c->save();
            }
        }
        $mit = MitProjects::find($id);
        if($mit->step_no==13)
            $mit->step_no=14;
        $mit->save();
        return redirect('branches/'.$id);
    }

    public function report($id){
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['logo'] = System::find(1)->logo;
        $query['sessions'] = MitProjects::report($id);
        $query['all'] = DB::select("select learning_level, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        foreach ($query['all'] as $key => $value) {
            $value->difficult = MitMethodsController::avg_level($value->learning_level);
        }
        $query['planner_chart'] = 1;
        $query['tem'] = DB::select("select series_no, id, detail as name, pro_id, type, style,tool,time,responsibilty from collerations where pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select series_no, id, name, pro_id, type, style,tool,time,responsibilty from training_outputs where  pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/report_mit',$query);
    }
}


