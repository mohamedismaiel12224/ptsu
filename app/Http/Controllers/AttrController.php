<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\SystemAttribute;
use Auth;

class AttrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['system_setting'] = 1;
        $link = Route::getCurrentRoute()->getPath();
        if($link=='ev_egree'){
            $query['type_id'] = 1;
        }else if($link == 'tr_methods'){
            $query['type_id'] = 2;
        }else if($link == 'ev_time'){
            $query['type_id'] = 3;
        }else if($link == 'tools'){
            $query['type_id'] = 4;
        }else if($link == 'responsiblity'){
            $query['type_id'] = 5;
        }else if($link == 'category'){
            $query['type_id'] = 6;
        }else if($link == 'aids'){
            $query['type_id'] = 7;
        }else if($link == 'imp_mechanism'){
            $query['type_id'] = 8;
        }else if($link == 'con_source'){
            $query['type_id'] = 9;
        }else if($link == 'affects_need'){
            $query['type_id'] = 10;
        }else if($link == 'solution_need'){
            $query['type_id'] = 11;
        }else if($link == 'ev_trainers'){
            $query['type_id'] = 12;
        }else if($link == 'ev_training_material'){
            $query['type_id'] = 13;
        }else if($link == 'ev_efm'){
            $query['type_id'] = 14;
        }

        $query['attr'] = SystemAttribute::where('type_id',$query['type_id'])->get();
        return view('admin/attr/show',$query);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query['system_setting'] = 1;
        $link = Route::getCurrentRoute()->getPath();
        if($link=='ev_egree/create'){
            $query['type_id'] = 1;
        }else if($link == 'tr_methods/create'){
            $query['type_id'] = 2;
        }else if($link == 'ev_time/create'){
            $query['type_id'] = 3;
        }else if($link == 'tools/create'){
            $query['type_id'] = 4;
        }else if($link == 'responsiblity/create'){
            $query['type_id'] = 5;
        }else if($link == 'category/create'){
            $query['type_id'] = 6;
        }else if($link == 'aids/create'){
            $query['type_id'] = 7;
        }else if($link == 'imp_mechanism/create'){
            $query['type_id'] = 8;
        }else if($link == 'con_source/create'){
            $query['type_id'] = 9;
        }else if($link == 'affects_need/create'){
            $query['type_id'] = 10;
        }else if($link == 'solution_need/create'){
            $query['type_id'] = 11;
        }else if($link == 'ev_trainers/create'){
            $query['type_id'] = 12;
        }else if($link == 'ev_training_material/create'){
            $query['type_id'] = 13;
        }else if($link == 'ev_efm/create'){
            $query['type_id'] = 14;
        }
        return view('admin/attr/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $attr = new SystemAttribute;
        $attr->name = $request->name;
        $attr->type_id = $request->type_id;
        $attr->save();
        return redirect()->back()->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['system_setting'] = 1;
        $query['attribute'] = SystemAttribute::find($id);
        $query['type_id'] = $query['attribute']->type_id;
        return view('admin/attr/add',$query);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $attr = SystemAttribute::find($id);
        $attr->name = $request->name;
        $attr->save();
        return redirect()->back()->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attr = SystemAttribute::find($id);
        $attr->delete();
        return redirect()->back()->with('success','تم الحذف بنجاح');
    }
}
