<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branches;
use App\Training_outputs;
use App\Program_level;
use App\Collerations;
use App\Sessions;
use App\MitProjects;
use Auth;
use DB;
class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['training'] = DB::select("select learning_level, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/branches/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pro_id = $request->pro_id;
        $query['mit'] = MitProjects::find($pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'series_no' => 'required',
            'address' => 'required',
        ]);
        $tr_id='';
        foreach ($request->series_no as $key => $value) {
            if($tr_id=='')
                $tr_id .= $value;
            else
                $tr_id .= ','.$value;
            $tr=Training_outputs::where('pro_id',$pro_id)->where('series_no',$value)->first();
            if($tr){
                $tr->in_branch=1;
                $tr->save();
            }

            $col=Collerations::where('pro_id',$pro_id)->where('series_no',$value)->first();
            if($col){
                $col->in_branch=1;
                $col->save();
            }
        }

        $branch = new Branches;
        $branch->pro_id = $pro_id;
        $branch->series_no = $tr_id;
        $branch->address = $request->address;
        $branch->save();

        $all = DB::select("select learning_level, series_no, id, detail as name, pro_id, in_branch from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$pro_id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id, in_branch from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$pro_id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        $update=true;
        foreach ($all as $key => $value) {
            if($value->in_branch==0)
                $update=false;
        }
        if($update){
            $mit = MitProjects::find($pro_id);
            if($mit->step_no==14)
                $mit->step_no=15;
            $mit->save();
        }
        return redirect('branches/'.$pro_id)->with('success','تم الحفظ بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();

        $query['branches'] = Branches::where('pro_id',$id)->get();
        return view('admin/systems/branches/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['branch'] = Branches::find($id);
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($query['branch']->pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['training'] = DB::select("select learning_level, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$query['branch']->pro_id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$query['branch']->pro_id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/branches/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pro_id=Branches::find($id)->pro_id;
        $query['mit'] = MitProjects::find($pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'series_no' => 'required',
            'address' => 'required',
        ]);
        $old_tr = explode(',',Branches::find($id)->series_no);
        Training_outputs::whereIn('series_no',$old_tr)->update(['in_branch'=>0]);
        Collerations::whereIn('series_no',$old_tr)->update(['in_branch'=>0]);

        $tr_id='';
        foreach ($request->series_no as $key => $value) {
            if($tr_id=='')
                $tr_id .= $value;
            else
                $tr_id .= ','.$value;

            $tr=Training_outputs::where('pro_id',$pro_id)->where('series_no',$value)->first();
            if($tr){
                $tr->in_branch=1;
                $tr->save();
            }

            $col=Collerations::where('pro_id',$pro_id)->where('series_no',$value)->first();
            if($col){
                $col->in_branch=1;
                $col->save();
            }
        }

        $branch = Branches::find($id);
        $branch->series_no = $tr_id;
        $branch->address = $request->address;
        $branch->save();

        $all =DB::select("select learning_level, series_no, id, detail as name, pro_id, in_branch from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$branch->pro_id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id, in_branch from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$branch->pro_id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        $update=true;
        foreach ($all as $key => $value) {
            if($value->in_branch==0)
                $update=false;
        }
        $mit = MitProjects::find($branch->pro_id);
        if($update){
            if($mit->step_no==14)
                $mit->step_no=15;
                $mit->save();
        }else{
            $mit->step_no=13;
            $mit->save();
        }

        return redirect('branches/'.$branch->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branches::find($id);
        $branch->delete();
        $session =Sessions::where('pro_id',$branch->pro_id)->get();
        foreach ($session as $key => $value) {
            $check = false;
            $arr =explode(',', $value->branch_arr);
            foreach ($arr as $ke => $val) {
                if($val==$id)
                    $check =true;
            }
            if($check){
                $col = Sessions::find($value->id);
                $col->delete();
            }

        }
        $train = Branches::where('pro_id',$branch->pro_id)->where('series_no',$branch->series_no)->first();
        if(!$train){
            $mit = MitProjects::find($branch->pro_id);
            if($mit->step_no==15)
                $mit->step_no=14;
                $mit->save();
            }
        return redirect()->route('branches.show',$branch->pro_id)->with('success','تم الحذف بنجاح');

    }
}
