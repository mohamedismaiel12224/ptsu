<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domain;
use App\ContentArbitration;
use App\MitArbitration;
use App\Arb_goal;
use App\Checkup;
use Auth;
class ArbitratorContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['arbitrator_mit']=1;
        $query['domains'] = Domain::where('status',2)->get();
        foreach ($query['domains'] as $key => $value) {
            $value->checkup = Checkup::where('domain_id',$value->id)->orderBy('domain_id','asc')->get();
        }
        $query['goals']=Arb_goal::where('pro_id',$id)->get();
        $query['mit'] = MitArbitration::find($id);
        return view('admin/systems/mit_arbitration/content/add',$query);
    }

    public function experience()
    {
        return experience($_POST['tr_id'],Auth::id());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'address' => 'required',
            'training_id' => 'required|numeric',
            'session' => 'required|numeric',
            'time' => 'required',
        ]);

        $arr='';
        $domains = Domain::where('status',2)->get();
        foreach($domains as $domain){
            $name = 'domain'.$domain->id;
            if(!isset($_POST[$name])||!$_POST[$name])
                return redirect()->back()->with('error','يوجد حقل فارغ');
            if($arr=='')
                $arr .= $domain->id.':'.$_POST[$name];
            else
                $arr .= ','.$domain->id.':'.$_POST[$name];
        }
        
        $content = new ContentArbitration;
        $content->address = $request->address;
        $content->training_id = $request->training_id;
        $content->session = $request->session;
        $content->time = $request->time;
        $content->pro_id = $request->pro_id;
        $content->details = $request->details;
        $content->user_id= Auth::id();
        $content->domain_check_arr= $arr;
        $content->save();
        return redirect('arbitrator/mit/content/'.$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['arbitrator_mit']=1;
        $query['domains'] = Domain::where('status',2)->get();
        $query['mit'] = MitArbitration::find($id);
        $query['content'] = ContentArbitration::where('pro_id',$id)->where('user_id',Auth::id())->get();
        return view('admin/systems/mit_arbitration/content/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['arbitrator_mit']=1;
        $query['domains'] = Domain::where('status',2)->get();
        $query['content'] = ContentArbitration::find($id);
        foreach ($query['domains'] as $key => $value) {
            $value->selected =0;
            $value->checkup = Checkup::where('domain_id',$value->id)->orderBy('domain_id','asc')->get();
            $domain_selected = explode(',', $query['content']->domain_check_arr);
            foreach ($domain_selected as $select) {
                $arr = explode(':', $select);
                if(isset($arr[1])&&($arr[0]==$value->id))
                    $value->selected= $arr[1];
            }
        }
        $query['goals']=Arb_goal::where('pro_id',$query['content']->pro_id)->get();
        $query['mit'] = MitArbitration::find($query['content']->pro_id);
        return view('admin/systems/mit_arbitration/content/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'address' => 'required',
            'training_id' => 'required|numeric',
            'session' => 'required|numeric',
            'time' => 'required',
        ]);

        $arr='';
        $domains = Domain::where('status',2)->get();
        foreach($domains as $domain){
            $name = 'domain'.$domain->id;
            if(!isset($_POST[$name])||!$_POST[$name])
                return redirect()->back()->with('error','يوجد حقل فارغ');
            if($arr=='')
                $arr .= $domain->id.':'.$_POST[$name];
            else
                $arr .= ','.$domain->id.':'.$_POST[$name];
        }
        
        $content = ContentArbitration::find($id);
        $content->address = $request->address;
        $content->training_id = $request->training_id;
        $content->session = $request->session;
        $content->time = $request->time;
        $content->details = $request->details;
        $content->domain_check_arr= $arr;
        $content->save();
        return redirect('arbitrator/mit/content/'.$content->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $content = ContentArbitration::find($id);
        if($content)
            $content->delete();
        return redirect('arbitrator/mit/content/'.$content->pro_id)->with('success','تم الحذف بنجاح');

    }
}
