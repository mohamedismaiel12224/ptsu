<?php

namespace App\Http\Controllers;
use App\MitProjects;
use App\Training_outputs;
use App\SystemAttribute;
use App\Collerations;
use Illuminate\Http\Request;
use Auth;

class TrainingOutputsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['evaluate'] = SystemAttribute::where('type_id',1)->get();
        return view('admin/systems/training/add',$query);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'evaluate' => 'required|exists:system_attributes,id',
        ]);
        
        $training = new Training_outputs;
        $training->name = $request->name;
        $training->pro_id = $request->pro_id;
        $training->evaluate_id = $request->evaluate;
        $training->save();

        $mit = MitProjects::find($request->pro_id);
        if($mit->step_no==1)
            $mit->step_no=2;
        $mit->save();
        return redirect()->route('training_outputs.show',$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['training'] = Training_outputs::join('system_attributes','system_attributes.id','training_outputs.evaluate_id')->select('*','system_attributes.name as evaluate','training_outputs.name as name','training_outputs.id as id')->where('pro_id',$id)->get();
        return view('admin/systems/training/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['training'] = Training_outputs::find($id);
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($query['training']->pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $query['evaluate'] = SystemAttribute::where('type_id',1)->get();
        return view('admin/systems/training/add',$query);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'evaluate' => 'required|exists:system_attributes,id',
        ]);
        
        $training = Training_outputs::find($id);
        $training->name = $request->name;
        $training->evaluate_id = $request->evaluate;
        $training->save();
        return redirect()->route('training_outputs.show',$training->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training = Training_outputs::find($id);
        if($training)
            $training->delete();
        $colleration =Collerations::where('pro_id',$training->pro_id)->get();
        foreach ($colleration as $key => $value) {
            $check = false;
            $arr =explode(',', $value->training_outputs_id);
            foreach ($arr as $ke => $val) {
                if($val==$id)
                    $check =true;
            }
            if($check){
                $col = Collerations::find($value->id);
                $col->delete();
            }

        }
        $train = Training_outputs::first();
        if(!$train){
            $mit = MitProjects::find($training->pro_id);
            if($mit->step_no==2)
                $mit->step_no=1;
            $mit->save();
        }
        return redirect()->route('training_outputs.show',$training->pro_id)->with('success','تم الحذف بنجاح');


    }
}
