<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poll;
use App\Suggest;
use App\Trainer_poll;
use Auth;
class PollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['poll']=Poll::all();
        foreach ($query['poll'] as $key => $value) {
            $value->suggest = Suggest::where('poll_id',$value->id)->get();
        }
        return view('admin/poll/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/poll/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'poll' => 'required',
            'suggest' => 'required',
        ]);

        $poll = new poll;
        $poll->poll = $request->poll;
        $poll->save();
        foreach ($request->input("suggest") as $key => $value) {
            $suggest = new Suggest;
            $suggest->suggest = $value;
            $suggest->poll_id = $poll->id;
            if($value)
                $suggest->save();
        }
        return redirect()->route('poll.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $color=['de66d5','27e9ec','ffe9a4','76fbc5'];
        $query['poll']=Poll::find($id);
        $query['suggest'] = Suggest::where('poll_id',$id)->get();
        $x= Trainer_poll::where('poll_id',$id)->count();
        foreach ($query['suggest'] as $ke => $val) {
            $y =Trainer_poll::where('poll_id',$id)->where('suggest_id',$val->id)->count();
            if($x==0)
                $val->percentage = 0;
            else
                $val->percentage = round(($y/$x)*100);
            $val->color=$color[$ke];
        }
        $query['chart']=1;
        return view('admin/poll/chart',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['poll'] = Poll::find($id);
        $query['suggest'] = Suggest::where('poll_id',$id)->get();
        return view('admin/poll/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'poll' => 'required',
            'suggest' => 'required',
        ]);

        $poll = poll::find($id);
        $poll->poll = $request->poll;
        $poll->save();
        $suggest=Suggest::where('poll_id',$id)->delete();
        foreach ($request->input("suggest") as $key => $value) {
            $suggest = new Suggest;
            $suggest->id = $request->input("key.".$key);
            $suggest->suggest = $value;
            $suggest->poll_id = $poll->id;
            if($value)
                $suggest->save();
        }
        return redirect()->route('poll.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $poll=Poll::find($id);
        $poll->delete();
        $suggest=Suggest::where('poll_id',$id)->delete();
        Trainer_poll::where('poll_id',$id)->delete();
        return redirect()->route('poll.index')->with('success','تم الحذف بنجاح');
    }
}
