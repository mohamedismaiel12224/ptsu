<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Setting;
use  App\Courses;
use App\Mailing;
use App\Trainers_courses;
use Auth;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        // return view('admin/home');
        $query['setting']=Setting::find(1);
        return view('admin/setting',$query);
    }

    public function setting(){
        $query['setting']=Setting::find(1);
        return view('admin/setting',$query);
    }

    public function sys_degree(){
        $query['setting']=Setting::find(1);
        $query['system_setting'] = 1;
        return view('admin/sys_degree',$query);
    }
    
    public function mailing(){
        $query['mailing']=Mailing::orderBy('id','desc')->get();
        return view('admin/mailing',$query);
    }
    // public function get_trainer($course_id){
    //     $query['users']=Trainers_courses::join('users','users.id','trainers_courses.trainer_id')->select('*','users.id as id')->where('users.role',2)->where('course_id',$course_id)->get();
    //     $query['course_id']=$course_id;
    //     return view('trainers_courese',$query);

    // }
    
    public function setting_update(Request $request){
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'address' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            'facebook' => 'required',
            'twitter' => 'required',
            'linkedin' => 'required',
            'google' => 'required',
            'link_partner' => 'required',
            'we_are_ar' => 'required',
            'we_are' => 'required',
            'our_mission_ar' => 'required',
            'our_mission' => 'required',
            'our_vision_ar' => 'required',
            'our_vision' => 'required',
            'our_rate_ar' => 'required',
            'our_rate' => 'required',
        ]);
        $setting = Setting::find(1);
        $setting->we_are = $request->we_are;
        $setting->our_mission = $request->our_mission;
        $setting->our_vision = $request->our_vision;
        $setting->our_rate = $request->our_rate;
        $setting->address = $request->address;
        $setting->link_partner = $request->link_partner;
        $setting->email = $request->email;
        $setting->phone = $request->phone;
        $setting->facebook= $request->facebook;
        $setting->twitter= $request->twitter;
        $setting->linkedin= $request->linkedin;
        $setting->google= $request->google;
        $setting->we_are_ar = $request->we_are_ar;
        $setting->our_mission_ar = $request->our_mission_ar;
        $setting->our_vision_ar = $request->our_vision_ar;
        $setting->our_rate_ar = $request->our_rate_ar;
        $setting->save();
        return redirect()->back()->with('success','تم التعديل بنجاح');

    }

    public function sys_degree_update(Request $request){
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'high_degree' => 'required|min:0',
            'low_degree' => 'required|max:100',
        ]);
        $setting = Setting::find(1);
        $setting->high_degree = $request->high_degree;
        $setting->low_degree = $request->low_degree;
        $setting->save();
        return redirect()->back()->with('success','تم التعديل بنجاح');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $query['courses']= Courses::all();
        return view('admin/give_certificate',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
