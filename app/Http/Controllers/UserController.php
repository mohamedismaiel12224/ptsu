<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Users;
use App\Courses;
use App\System;
use App\Trainers_courses;
use App\SystemUser;
use Auth;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='trainer'){
            if(Auth::user()->role==1){
                $id=0;
            }else{
                $id=Auth::id();
            }
            $query['users'] = Users::where('role',2)->get();
            return view('admin/trainer/show',$query);
        }else if($link=='fellowship'){
            $query['users'] = Users::where('role',3)->where('status','!=',2)->get();
            foreach ($query['users'] as $key => $value) {
                $x= System::find($value->system_id);
                if($x)
                    $value->system =$x->name;
                else
                    $value->system ='-';

            }
            return view('admin/fellowship/show',$query);   
        }
    }
    public function requests(){
        $query['users'] = Users::where('role',3)->where('status',2)->get();
        foreach ($query['users'] as $key => $value) {
            $x= System::find($value->system_id);
            if($x)
                $value->system =$x->name;
            else
                $value->system ='-';
        }
        return view('admin/fellowship/show_req',$query);   

    }
    public function give_cert($id,$status,$course_id){
        $t= Trainers_courses::where('trainer_id',$id)->where('course_id',$course_id)->first();
        $t->status= $status;
        if($status==1){
            do{
                $rand= rand('11111','99999');
                $x=Trainers_courses::where('cert_no',$rand)->count();
            }while($x==1);
            $t->cert_no=$rand;
        }else{
            $t->cert_no=null;
        }
        $t->save();
        return redirect()->back()->with('success','تم التعديل بنجاح');

    }
    /**
     * Show the form for creating a new resource.
     *4
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='trainer/create'){
            if(Auth::user()->role==1){
                $id=0;
            }else{
                $id=Auth::id();
            }
            $query['courses']=Courses::where('fellow_id',$id)->get();
            return view('admin/trainer/add',$query);
        }else if($link=='fellowship/create'){
            $query['system']= System::all();
            return view('admin/fellowship/add',$query);   
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $link = Route::getCurrentRoute()->getPath();
        $user = new Users;
        if($link=='trainer'){
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'phone' => 'required|numeric',
                'gender' => 'required|in:1,0',
                'nno' => 'required|numeric|unique:users,nno',
                'country' => 'required',
                'course' => 'required',
            ]);
            $user->nno = $request->nno;
            $user->country = $request->country;
     -       $user->role = 2;
        }else if($link=='fellowship'){
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'phone' => 'required|numeric',
                'gender' => 'required|in:1,0',
                'system' => 'required',
                'password' => 'required',
            ]);
            $user->system_id = $request->system;
            $user->role = 3;
            $user->password = bcrypt($request->password);
        }
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->gender = $request->gender;
        $user->save();
        if($link=='trainer'){
            foreach ($request->course as $key => $value) {
                $t = new Trainers_courses;
                $t->trainer_id=$user->id;
                $t->course_id=$value;
                $t->status=0;
                 $t->cert_no=null;
                $t->save();
            }
            return redirect()->route('trainer.index')->with('success','تم الحفظ بنجاح');
        }else if($link=='fellowship'){
            return redirect()->route('fellowship.index')->with('success','تم الحفظ بنجاح');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cert_id)
    {
        $query['user']=Trainers_courses::join('users','users.id','trainers_courses.trainer_id')->find($cert_id);
        $query['course']=Courses::find($query['user']->course_id);
        return view('admin/trainer/certification',$query);
    }
    public function permision($id,$permision){
        $t = Users::find($id);
        $t->status=$permision;
        $t->save();

        $sys = new SystemUser;
        $sys->user_id = $id;
        $sys->system_id = $t->system_id;
        $sys->updated_at = date('Y-m-d',strtotime("+3 month"));
        $sys->save();
        return redirect()->back()->with('success','تم التعديل بنجاح');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='trainer/{trainer}/edit'){
            $query['trainer'] = Users::where('role',2)->find($id);
            $query['courses']= Courses::all();
            $query['courses_selected']= Trainers_courses::where('trainer_id',$id)->get();
            return view('admin/trainer/add',$query);
        }else if($link=='fellowship/{fellowship}/edit'){
            $query['fellowship'] = Users::where('role',3)->where('status','!=',2)->find($id);
            $query['system']= System::all();
            return view('admin/fellowship/add',$query);   
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $link = Route::getCurrentRoute()->getPath();
        if($link=='trainer/{trainer}'){
            $user = Users::where('role',2)->find($id);
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email,'.$id,
                'phone' => 'required|numeric',
                'gender' => 'required|in:1,0',
                'nno' => 'required|numeric|unique:users,nno,'.$id,
                'country' => 'required',
            ]);
            $user->nno = $request->nno;
            $user->country = $request->country;
        }else if($link=='fellowship/{fellowship}'){
            $user = Users::where('role',3)->where('status','!=',2)->find($id);
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email,'.$id,
                'phone' => 'required|numeric',
                'gender' => 'required|in:1,0',
            ]);
            $user->system_id = $request->system;
            if ($request->password)
                $user->password = bcrypt($request->password);
        }
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->gender = $request->gender;
        $user->save();
        if($link=='trainer/{trainer}'){
            foreach ($request->course as $key => $value) {
                $check=Trainers_courses::where('course_id',$value)->where('trainer_id',$id)->first();
                if(!$check){
                    $t = new Trainers_courses;
                    $t->trainer_id=$user->id;
                    $t->course_id=$value;
                    $t->status=0;
                    $t->cert_no=null;
                    $t->save();
                }
            }
            $arr=Trainers_courses::where('trainer_id',$id)->get();
            foreach ($arr as $key => $value) {
                if (!in_array($value->course_id, $request->course)){
                    Trainers_courses::where('id',$value->id)->delete();
                }
            }
            
            return redirect()->route('trainer.index')->with('success','تم التعديل بنجاح');
        }else if($link=='fellowship/{fellowship}'){
            return redirect()->route('fellowship.index')->with('success','تم التعديل بنجاح');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='trainer/{trainer}'){
            $user=Users::where('role',2)->find($id);
            $user->delete();
            Trainers_courses::where('trainer_id',$id)->delete();
            return redirect()->route('trainer.index')->with('success','تم الحذف بنجاح');
        }else if($link=='fellowship/{fellowship}'){
            $user=Users::where('role',3)->find($id);
            $user->delete();
            return redirect()->back()->with('success','تم الحذف بنجاح');
        }
    }
}
