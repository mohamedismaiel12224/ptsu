<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EfmAnalysis;
use App\EfmFrame;
use App\ToolEfm;
use App\EfmProject;
class EfmAnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function analysis_b($pro_id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($pro_id);
        $query['tools'] = ToolEfm::all();

        $query['attr'] = EfmFrame::select('*','system_attributes.id as id')->join('system_attributes','system_attributes.id','efm_frames.type_id')->where('pro_id',$pro_id)->get();
        return view('admin/systems/efm/analysis/modelB',$query);

    }
    public function analysis_b_update(Request $request,$pro_id)
    {
         $attr = EfmFrame::where('pro_id',$pro_id)->get();
         config(['app.locale' => 'ar']);
        $this->validate($request, [
            'tool_id.*' => 'required',
            'source.*' => 'required',
            'period.*' => 'required',
            'responsible.*' => 'required',
        ]);
        foreach ($attr as $key => $value) {
            $value->tool_id = $request->tool_id[$value->type_id];
            $value->source = $request->source[$value->type_id];
            $value->period = $request->period[$value->type_id];
            $value->responsible = $request->responsible[$value->type_id];
            $value->save();

        }
        return redirect()->back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pro_id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($pro_id);
        return view('admin/systems/efm/analysis/add_modelA',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'step' => 'required',
            'place' => 'required',
            'time' => 'required',
            'responsible' => 'required',
            'notes' => 'required',
        ]);
        $analysis = EfmAnalysis::create($request->all());
        return redirect('efm/analysis/'.$analysis->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($id);
        $query['analysis'] = EfmAnalysis::where('pro_id',$id)->get();
        return view('admin/systems/efm/analysis/modelA',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['efm_pro'] = 1;
        $query['analysis'] = EfmAnalysis::find($id);
        $query['efm'] = EfmProject::find($query['analysis']->pro_id);
        return view('admin/systems/efm/analysis/add_modelA',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'step' => 'required',
            'place' => 'required',
            'time' => 'required',
            'responsible' => 'required',
            'notes' => 'required',
        ]);
        $analysis = EfmAnalysis::find($id);
        $analysis->update($request->all());
        return redirect('efm/analysis/'.$analysis->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $analysis = EfmAnalysis::findOrFail($id);
        $analysis->delete();
        return redirect('efm/analysis/'.$analysis->pro_id)->with('success','تم الحذف بنجاح');
    }
}
