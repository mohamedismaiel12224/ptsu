<?php

namespace App\Http\Controllers;
use App\EvaluateProject;
use App\TrainerEvaluate;
use App\EvaluateTrainer;
use App\EvaluateTrainerGroup;
use App\Domain;
use App\Checkup;
use App\System;
use App\ArbitratorProject;
use App\GroupTrainerEvaluate;

use Auth;
use Illuminate\Http\Request;

class PerformanceEvaluateMethodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1&&$this->role!=4)
                return redirect('login');
            return $next($request);
        });
    }
    public function link_evaluate($id){
        $query['performance_evaluate_pro'] = 1;
        $query['ev'] = EvaluateProject::find($id);
        $query['link']=url('performance_evaluate/login/'.$id);
        $query['word'] = 'الرابط للمقييمين';
        return view('admin/systems/performance/link',$query);

    }
    public function trainers($id)
    {
        $query['performance_evaluator_sidebar']  = 1;
        $query['ev'] = EvaluateProject::find($id);
        $query['evaluator']  = 1;
        $query['trainers'] = TrainerEvaluate::select('trainer_evaluates.*','group_trainer_evaluates.group')->leftJoin('group_trainer_evaluates','group_trainer_evaluates.id','trainer_evaluates.group_id')->where('trainer_evaluates.pro_id',$id)->get();
        return view('admin/systems/performance/trainers/show',$query);
    }
    public function new_trainer_evaluate($trainer_id,$pro_id){
        $check = new EvaluateTrainerGroup;
        $check->trainer_id = $trainer_id;
        $check->pro_id = $pro_id;
        $check->evaluator_id = Auth::id();
        $check->save();
        return redirect('trainer_evaluate/'.$check->id);
    }
    public function trainer_evaluate_group($trainer_id,$pro_id){
        $query['performance_evaluator_sidebar']  = 1;
        $query['ev'] = EvaluateProject::find($pro_id);
        $query['groups'] = EvaluateTrainerGroup::where('trainer_id',$trainer_id)->where('pro_id',$pro_id)->where('evaluator_id',Auth::id())->get();
        return view('admin/systems/performance/evaluate_trainers_group',$query);
        
    }
    public function trainer_evaluate($group_id){
        $query['performance_evaluator_sidebar']  = 1;

        $query['logo'] = System::find(3)->logo;
        $query['group'] = EvaluateTrainerGroup::find($group_id);
        $query['trainer'] = TrainerEvaluate::find($query['group']->trainer_id);
        $query['ev'] = EvaluateProject::find($query['group']->pro_id);
        if($query['ev']->type==1){
            $query['status'] = 5;
        }else if($query['ev']->type==2){
            $query['status'] = 7;            
        }
        $query['domain'] = Domain::where('status',$query['status'])->get();
        foreach ($query['domain'] as $key => $value) {
                $value->checkup = Checkup::where('domain_id',$value->id)->get();
                foreach ($value->checkup as $ke => $val) {
                    $val->evaluate = 0;
                    $check = EvaluateTrainer::where('group_id',$group_id)->where('checkup_id',$val->id)->first();
                    if($check){
                        $val->evaluate = $check->evaluate;
                        $val->notes = $check->notes;
                    }
                }
            }
            return view('admin/systems/performance/evaluate_skills',$query);
    }
    public function trainer_evaluate_update($group_id){
        $query['group'] = EvaluateTrainerGroup::find($group_id);
        $query['trainer'] = TrainerEvaluate::find($query['group']->trainer_id);
        $query['ev'] = EvaluateProject::find($query['group']->pro_id);
    	if($query['ev']->type==1){
            $query['domain'] = Domain::where('status',5)->get();
        }else if($query['ev']->type==2){
    		$query['domain'] = Domain::where('status',7)->get();
            
    	}
        foreach ($query['domain'] as $key => $value) {
            $notes = 'notes'.$value->id;
            $value->checkup = Checkup::where('domain_id',$value->id)->get();
            foreach ($value->checkup as $ke => $val) {
                $name = 'evaluate'.$val->id;
                if(!isset($_POST[$name])||$_POST[$name]==0)
                    return redirect()->back()->with('error','يوجد حقل فارغ');

                $check = EvaluateTrainer::where('group_id',$group_id)->where('checkup_id',$val->id)->first();
                if($check){
                    $check->evaluate = $check->evaluate;
                    $check->notes = $_POST[$notes];
                    $check->save();
                }else{
                    $ev = new EvaluateTrainer();
                    $ev->pro_id = $query['group']->pro_id;
                    $ev->trainer_id = $query['group']->trainer_id;
                    $ev->group_id = $group_id;
                    $ev->evaluator_id = Auth::id();
                    $ev->checkup_id = $val->id;
                    $ev->evaluate = $_POST[$name];
                    $ev->notes = $_POST[$notes];
                    $ev->save();
                }
            }
        }
		return redirect('evaluator/performance_evaluate/'.$query['group']->pro_id)->with('success','تم التقييم بنجاح');
    }

    public function report_evaluator($id,$pro_id){
        $query['performance_evaluate_pro'] = 1;
        $query['logo'] = System::find(3)->logo;
        $query['trainer'] = TrainerEvaluate::find($id);
        $query['ev'] = EvaluateProject::find($pro_id);
        $query['performance_evaluate_chart'] = $query['evaluator'] = ArbitratorProject::join('users','users.id','arbitrator_projects.user_id')->where('arbitrator_projects.status',2)->where('pro_id',$pro_id)->get();
        if($query['ev']->type==1){
            $query['status'] = 5;
        }else if($query['ev']->type==2){
            $query['status'] = 7;
        }
        $color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a'];

        foreach ($query['evaluator'] as $key => $ev) {
            $ev->domain = Domain::where('status',$query['status'])->get();
            foreach ($ev->domain as $key_domain => $domain) {
                $domain->checkup = Checkup::where('domain_id',$domain->id)->get();
                
                foreach ($domain->checkup as $ke => $val) {
                    $val->group = EvaluateTrainerGroup::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->get();
                    foreach ($val->group as $key => $gr) {
                        $gr->evaluate = 0;
                        $check = EvaluateTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->where('group_id',$gr->id)->where('checkup_id',$val->id)->first();
                        if($check){
                            $gr->evaluate = $check->evaluate;
                            $gr->notes = $check->notes;
                        }
                    }
                }
                $domain->group = EvaluateTrainerGroup::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->get();
                foreach ($domain->group as $ke_gro => $gro) {
                    $gro->color = $color[$ke_gro%5];

                    $gro->checkup = Checkup::where('domain_id',$domain->id)->get();
                    foreach ($gro->checkup as $key => $v_ch) {
                        $v_ch->evaluate = 0;
                        $check = EvaluateTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluator_id',$ev->user_id)->where('group_id',$gro->id)->where('checkup_id',$v_ch->id)->first();
                        if($check){
                            $v_ch->evaluate = $check->evaluate;
                            $v_ch->notes = $check->notes;
                        }
                    }
                }
            }
        }
        // return response()->json($query, 200, []);
        return view('admin/systems/performance/report_evaluator_skills',$query);
    }

    public function report_all_evaluator($id,$pro_id){
        $query['performance_evaluate_pro'] = 1;
        $query['logo'] = System::find(3)->logo;
        $query['trainer'] = TrainerEvaluate::find($id);
        $query['ev'] = EvaluateProject::find($pro_id);
        $color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a'];
        if($query['ev']->type==1){
            $query['status'] = 5;
        }else if($query['ev']->type==2){
            $query['status'] = 7;
        }
        $query['performance_evaluate2_chart'] =$query['domains'] = Domain::where('status',$query['status'])->get();
        foreach ($query['domains'] as $key => $domain) {
            $domain->checkup = Checkup::where('domain_id',$domain->id)->get();

            foreach ($domain->checkup as $ke => $val) {
                $count = EvaluateTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('checkup_id',$val->id)->count();
                if($count != 0){
                    $val->evaluate_5 = EvaluateTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',5)->where('checkup_id',$val->id)->count();
                    $val->avg_5 = $val->evaluate_5 * 5 / $count;
                    $val->evaluate_4 = EvaluateTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',4)->where('checkup_id',$val->id)->count();
                    $val->avg_4 = $val->evaluate_4 * 4 / $count ;
                    $val->evaluate_3 = EvaluateTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',3)->where('checkup_id',$val->id)->count();
                    $val->avg_3 = $val->evaluate_3 * 3 / $count;
                    $val->evaluate_2 = EvaluateTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',2)->where('checkup_id',$val->id)->count();
                    $val->avg_2 = $val->evaluate_2 * 2 / $count ;
                    $val->evaluate_1 = EvaluateTrainer::where('pro_id',$pro_id)->where('trainer_id',$id)->where('evaluate',1)->where('checkup_id',$val->id)->count();
                    $val->avg_1 = $val->evaluate_1  / $count ;
                    $val->avg = $val->avg_5 + $val->avg_4 + $val->avg_3 + $val->avg_2 + $val->avg_1 ;
                }
            }                
        }
        return view('admin/systems/performance/report_all_evaluator_skills',$query);
    }
    public function report_evaluate($pro_id, $group_id = false){
        $query['performance_evaluate_pro'] = 1;
        $query['logo'] = System::find(3)->logo;
        $color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a'];
        $query['groups'] = GroupTrainerEvaluate::where('user_id',Auth::id())->where('pro_id',$pro_id)->get();
        $query['ev'] = EvaluateProject::find($pro_id);
        if(!$group_id)
            return view('admin/systems/performance/specific_group',$query);
        else{
            if($group_id=='*'){
                $trainers = TrainerEvaluate::where('pro_id',$pro_id)->get();
            }else{
                $trainers = TrainerEvaluate::where('pro_id',$pro_id)->where('group_id',$group_id)->get();
            }
            $arr = array();
            foreach ($trainers as $key => $t) {
                array_push($arr, $t->id);
            }
        }

        if($query['ev']->type==1){
            $query['status'] = 5;
        }else if($query['ev']->type==2){
            $query['status'] = 7;
        }
        $query['performance_evaluate2_chart'] =$query['domains'] = Domain::where('status',$query['status'])->get();

        foreach ($query['domains'] as $key => $domain) {

            $domain->checkup = Checkup::where('domain_id',$domain->id)->get();
            foreach ($domain->checkup as $ke => $val) {
                $count = EvaluateTrainer::where('pro_id',$pro_id)->where('checkup_id',$val->id)->count();
                if($count != 0){
                    $val->evaluate_5 = EvaluateTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',5)->where('checkup_id',$val->id)->count();
                    $val->avg_5 = $val->evaluate_5 * 5 / $count;
                    $val->evaluate_4 = EvaluateTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',4)->where('checkup_id',$val->id)->count();
                    $val->avg_4 = $val->evaluate_4 * 4 / $count ;
                    $val->evaluate_3 = EvaluateTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',3)->where('checkup_id',$val->id)->count();
                    $val->avg_3 = $val->evaluate_3 * 3 / $count;
                    $val->evaluate_2 = EvaluateTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',2)->where('checkup_id',$val->id)->count();
                    $val->avg_2 = $val->evaluate_2 * 2 / $count ;
                    $val->evaluate_1 = EvaluateTrainer::where('pro_id',$pro_id)->whereIn('trainer_id',$arr)->where('evaluate',1)->where('checkup_id',$val->id)->count();
                    $val->avg_1 = $val->evaluate_1  / $count ;
                    $val->avg = $val->avg_5 + $val->avg_4 + $val->avg_3 + $val->avg_2 + $val->avg_1 ;
                }
            }            
        }
        return view('admin/systems/performance/report_all_evaluator_skills',$query);
    }
    public function post_evaluate($pro_id)
    {
        $group_id = $_POST['group_id'];
        return redirect('performance_evaluate/report_evaluate/'.$pro_id.'/'.$group_id);
    }

}
