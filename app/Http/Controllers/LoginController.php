<?php

namespace App\Http\Controllers;
use Auth;
use App\Users;
use App\System;
use App\MitArbitration;
use App\NeedProjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
use App\ArbitratorProject;
use App\EtpmProject;
use App\EvaluateProject;
class LoginController extends Controller
{
    public function view(){
        $lang = session()->get('language');
        App::setLocale($lang);
        if(Auth::check()){
            $user = Users::where('id', Auth::id())->get()->first();
                if($user->role==3)
                    return redirect('/');
                elseif($user->role==1)
                    return redirect()->route('admin.index');
                else
                    return redirect('/');

        }else{
            $link = Route::getCurrentRoute()->getPath();
            if($link=='register'){
                $query['system']=System::all();
                return view('register',$query);
            }
            else
                return view('login');
        }
    }

    public function login_arbitration($id){
        $query['mit'] = MitArbitration::find($id);
        if(Auth::check()&&Auth::user()->role==4)
            return redirect('arbitrator/mit/'.$id);
        else
            return view('admin/systems/mit_arbitration/login',$query);

    }
    public function login_arbitration_update(Request $request,$id){
        config(['app.locale' => 'ar']);
        $check = Users::where('email',$request->email)->where('phone',$request->phone)->where('role',4)->first();
        if($check){
            Auth::loginUsingId($check->id,true);

        }else{
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'phone' => 'required|numeric',
            ]);
            $user = new Users;
            $user->role = 4;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->save();
            Auth::loginUsingId($user->id,true);
        }


        return redirect('arbitrator/mit/'.$id);
    }
    public function need_analysis_system($id){
        $query['need'] = NeedProjects::find($id);
        $query['word'] = 'متدرب';
        $query['word2'] = 'التدريب';
        if(Auth::check()&&Auth::user()->role==5)
            return redirect('need/trainers/'.$id);
        else
            return view('admin/systems/need/login',$query);

    }
    public function need_arbitrators($id){
        $query['need_arbitrators']=1;
        $query['need'] = NeedProjects::find($id);
        $query['word'] = 'محكم';
        $query['word2'] = 'المحكمين';
        if(Auth::check()&&Auth::user()->role==4)
            return redirect('arbitrator/need_arbitration/'.$id);
        else
            return view('admin/systems/need/login',$query);

    }
    public function need_knowledge($id){
        $query['need'] = NeedProjects::find($id);
         $query['word'] = 'متدرب';
        $query['word2'] = 'التدريب';
        $query['need_knowledge'] = 1;
        if(Auth::check()&&Auth::user()->role==5){
            if(!ArbitratorProject::where('user_id',Auth::id())->where('pro_id',$id)->where('status',1)->first()){
                $arb = new ArbitratorProject;
                $arb->user_id = Auth::id();
                $arb->pro_id = $id;
                $arb->save();
            }
            return redirect('arbitrator/need_knowledge/'.$id);
        }
        else
            return view('admin/systems/need/login',$query);

    }
    public function need_skill($id){
        $query['need'] = NeedProjects::find($id);
         $query['word'] = 'تقييم المجال المهاري';
        $query['word2'] = 'التقييم';
        $query['need_skill'] = 1;
        if(Auth::check()&&Auth::user()->role==5){
            return redirect('need/arbitrators_skill/'.$id);
        }
        else
            return view('admin/systems/need/login',$query);

    }
    public function need_arbitration_update(Request $request,$id){
        config(['app.locale' => 'ar']);
        $link = Route::getCurrentRoute()->getPath();
        $role=5;
        if($link=='need_arbitrators/login/{id}')
            $role=4;
        $check = Users::where('email',$request->email)->where('phone',$request->phone)->where('role',$role)->first();
        if($check){
            Auth::loginUsingId($check->id,true);

        }else{
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'phone' => 'required|numeric',
            ]);
            $user = new Users;
            $user->role = $role;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->save();
            Auth::loginUsingId($user->id,true);
        }

        if($link=='need_knowledge/login/{id}'){
            if(!ArbitratorProject::where('user_id',Auth::id())->where('pro_id',$id)->where('status',1)->first()){
                $arb = new ArbitratorProject;
                $arb->user_id = Auth::id();
                $arb->pro_id = $id;
                $arb->save();
            }
            return redirect('arbitrator/need_knowledge/'.$id);
        }
        elseif($link=='need_skill/login/{id}')
            return redirect('need/arbitrators_skill/'.$id);
        elseif($link == 'need_arbitrators/login/{id}')
            return redirect('arbitrator/need_arbitration/'.$id);
        else
            return redirect('need/trainers/'.$id);
    }

    public function performance_evaluate($id){
        $query['evaluator']=1;
        $query['ev'] = EvaluateProject::find($id);
        $query['word'] = 'مقيم';
        $query['word2'] = 'المقيمين';
        if(Auth::check()&&Auth::user()->role==4)
            return redirect('evaluator/performance_evaluate/'.$id);
        else
            return view('admin/systems/performance/login',$query);

    }
    public function etpm($id){
        $query['evaluator']=1;
        $query['ev'] = EtpmProject::find($id);
        $query['word'] = 'مقيم';
        $query['word2'] = 'المقيمين';
        if(Auth::check()&&Auth::user()->role==4)
            return redirect('evaluator/etpm/'.$id);
        else
            return view('admin/systems/etpm/login',$query);

    }

    public function performance_evaluate_update(Request $request,$id){
        config(['app.locale' => 'ar']);
        $link = Route::getCurrentRoute()->getPath();
       
        $role=4;
        $check = Users::where('email',$request->email)->where('phone',$request->phone)->where('role',$role)->first();
        if($check){
            Auth::loginUsingId($check->id,true);

        }else{
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'phone' => 'required|numeric',
            ]);
            $user = new Users;
            $user->role = $role;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->save();
            Auth::loginUsingId($user->id,true);
        }

        if(!ArbitratorProject::where('user_id',Auth::id())->where('pro_id',$id)->where('status',2)->first()){
            $arb = new ArbitratorProject;
            $arb->user_id = Auth::id();
            $arb->pro_id = $id;
            $arb->status = 2;
            $arb->save();
        }
        return redirect('evaluator/performance_evaluate/'.$id);
    
    }
    public function etpm_update(Request $request,$id){
        config(['app.locale' => 'ar']);
        $link = Route::getCurrentRoute()->getPath();
       
        $role=4;
        $check = Users::where('email',$request->email)->where('phone',$request->phone)->where('role',$role)->first();
        if($check){
            Auth::loginUsingId($check->id,true);

        }else{
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'phone' => 'required|numeric',
            ]);
            $user = new Users;
            $user->role = $role;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->save();
            Auth::loginUsingId($user->id,true);
        }

        if(!ArbitratorProject::where('user_id',Auth::id())->where('pro_id',$id)->where('status',3)->first()){
            $arb = new ArbitratorProject;
            $arb->user_id = Auth::id();
            $arb->pro_id = $id;
            $arb->status = 3;
            $arb->save();
        }
        return redirect('evaluator/etpm/'.$id);
    
    }

    public function login(Request $request){
        $rules = array(
            'email' => 'required|email',
            'password' => 'required',
        );
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $userdata = array(
            'email' => $request->email,
            'password' => $request->password,
        );
        if (Auth::validate($userdata)) {
            if (Auth::attempt($userdata,true)) {
                $user = Users::where('id', Auth::id())->get()->first();
                if($user->role==3 && $user->status!=2)
                	return redirect('/');
                elseif($user->role==1)
                    return redirect()->route('admin.index');
                else
                    Auth::logout();
    		}
        }
        return redirect()->back()->with('error','Error in login');
    }
    public function register(Request $request){
        $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'phone' => 'required|numeric',
                'gender' => 'required|in:1,0',
                'system' => 'required',
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ]);
        $user = new Users;
        $user->system_id = $request->system;
        $user->role = 3;
        $user->status = 2;
        $user->password = bcrypt($request->password);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->gender = $request->gender;
        $user->save();
        return redirect()->back()->with('suc','تم ارسال طلبك وسيتم التواصل معك حال قبوله');
    }
    /*@yousef - 20/2/2017 - end :: login */
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
