<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TrainerEvaluate;
use Illuminate\Support\Facades\Route;
use App\EvaluateProject;
use App\GroupTrainerEvaluate;
use App\User;
use Auth;
class TrainerEvaluateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['performance_evaluate_pro'] = 1;
        $query['ev'] = EvaluateProject::find($id);
        $query['groups'] = GroupTrainerEvaluate::where('user_id',Auth::id())->where('pro_id',$id)->get();

        return view('admin/systems/performance/trainers/add',$query);
    }

    public function export($id,Request $request)
    {
        $query['performance_evaluate_pro'] = 1;
        $query['ev'] = EvaluateProject::find($id);
        $query['groups'] = GroupTrainerEvaluate::where('user_id',Auth::id())->where('pro_id',$id)->get();

        $query['projects'] = EvaluateProject::where('user_id',Auth::id())->where('id','!=',$id)->get();
        $query['trainers'] = array();
        if(isset($request->project)){
            $query['trainers'] = TrainerEvaluate::where('pro_id',$request->project)->get();
            return view('admin/systems/performance/trainers/demo',$query);
            
        }

        return view('admin/systems/performance/trainers/export',$query);
    }
    public function export_user($id,Request $request)
    {
        $query['performance_evaluate_pro'] = 1;
        $query['ev'] = EvaluateProject::find($id);
        $query['groups'] = GroupTrainerEvaluate::where('user_id',Auth::id())->where('pro_id',$id)->get();

        $query['trainers'] = User::where('role',2)->get();
        return view('admin/systems/performance/trainers/export_user',$query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        $ev = new TrainerEvaluate;
        $ev->name = $request->name;
        $ev->email = $request->email;
        $ev->phone = $request->phone;
        $ev->pro_id = $request->pro_id;
        $ev->group_id = $request->group_id;
        $ev->save();
        $ev = EvaluateProject::find($request->pro_id);
        if($ev->step_no==1)
            $ev->step_no =2;
        $ev->save();
        return redirect('performance_evaluate/trainers/'.$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    public function store2(Request $request,$id){
        if(count($request->trainers)>0){
            foreach ($request->trainers as $key => $value) {
                $old = TrainerEvaluate::find($value);
                $check= TrainerEvaluate::where('email',$old->email)->where('phone',$old->phone)->where('pro_id',$id)->first();
                if(!$check){
                    $ev = new TrainerEvaluate;
                    $ev->name = $old->name;
                    $ev->email = $old->email;
                    $ev->phone = $old->phone;
                    $ev->pro_id = $id;
                    $ev->group_id = $request->group_id;
                    $ev->save();
                }
            }
        }
        $ev = EvaluateProject::find($id);
        if($ev->step_no==1)
            $ev->step_no =2;
        $ev->save();
        return redirect('performance_evaluate/trainers/'.$id)->with('success','تم الحفظ بنجاح');
    }

    public function store3(Request $request,$id){
        if(count($request->trainers)>0){
            foreach ($request->trainers as $key => $value) {
                $old = User::find($value);
                $check= TrainerEvaluate::where('email',$old->email)->where('phone',$old->phone)->where('pro_id',$id)->first();
                if(!$check){
                    $ev = new TrainerEvaluate;
                    $ev->name = $old->name;
                    $ev->email = $old->email;
                    $ev->phone = $old->phone;
                    $ev->pro_id = $id;
                    $ev->group_id = $request->group_id;

                    $ev->save();
                }
            }
        }
        $ev = EvaluateProject::find($id);
        if($ev->step_no==1)
            $ev->step_no =2;
        $ev->save();
        return redirect('performance_evaluate/trainers/'.$id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $link = Route::getCurrentRoute()->getPath();
        if($link=='performance_evaluate/all_trainers/{id}')
            $query['all_trainers'] = 1;
        $query['performance_evaluate_pro'] = 1;
        $query['ev'] = EvaluateProject::find($id);
        $query['trainers'] = TrainerEvaluate::select('trainer_evaluates.*','group_trainer_evaluates.group')->leftJoin('group_trainer_evaluates','group_trainer_evaluates.id','trainer_evaluates.group_id')->where('trainer_evaluates.pro_id',$id)->get();
        return view('admin/systems/performance/trainers/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['performance_evaluate_pro'] = 1;
        $query['trainer'] = TrainerEvaluate::find($id);
        $query['ev'] = EvaluateProject::find($query['trainer']->pro_id);
        $query['groups'] = GroupTrainerEvaluate::where('user_id',Auth::id())->where('pro_id',$query['trainer']->pro_id)->get();
        return view('admin/systems/performance/trainers/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        $ev = TrainerEvaluate::find($id);
        $ev->name = $request->name;
        $ev->email = $request->email;
        $ev->phone = $request->phone;
        $ev->group_id = $request->group_id;
        
        $ev->save();
       
        return redirect('performance_evaluate/trainers/'.$ev->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ev = TrainerEvaluate::find($id);
        if($ev)
            $ev->delete();
        return redirect()->back()->with('success','تم الحذف بنجاح');
    }
}
