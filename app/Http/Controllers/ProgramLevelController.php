<?php

namespace App\Http\Controllers;
use App\MitProjects;
use App\Program_level;
use Illuminate\Http\Request;
use DB;
use Auth;

class ProgramLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['all'] = DB::select("select series_no, id, detail as name from collerations where pro_id = ".$id." union select series_no, id, name from training_outputs where pro_id = ".$id." order by series_no");
        return view('admin/systems/program_level/add',$query);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $to_id='';
        if(count($request->series)==0)
            return redirect()->back()->with('error','يجب اختيار على الأقل واحد');

        foreach ($request->series as $key => $value) {
            if($to_id=='')
                $to_id .= $value;
            else
                $to_id .= ','.$value;

        }
        if($to_id=='')
            return redirect()->back()->with('error','يجب اختيار على الأقل واحد');

        $pro = new Program_level;
        $pro->series_no_arr = $to_id;
        $pro->pro_id = $request->pro_id;
        $pro->save();
        $mit = MitProjects::find($request->pro_id);
        if($mit->step_no==4)
            $mit->step_no=5;
        $mit->save();
        return redirect()->route('program_level.show',$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['program'] = Program_level::where('pro_id',$id)->get();
        return view('admin/systems/program_level/show',$query);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['program'] = Program_level::find($id);
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($query['program']->pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $query['all'] = DB::select("select series_no, id, detail as name from collerations where pro_id = ".$query['program']->pro_id." union select series_no, id, name from training_outputs where pro_id = ".$query['program']->pro_id." order by series_no");
        return view('admin/systems/program_level/add',$query); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $to_id='';
        foreach ($request->series as $key => $value) {
            if($to_id=='')
                $to_id .= $value;
            else
                $to_id .= ','.$value;

        }
        if($to_id=='')
            return redirect()->back()->with('error','يجب اختيار على الأقل واحد');

        $pro = Program_level::find($id);
        $pro->series_no_arr = $to_id;
        $pro->save();
        return redirect()->route('program_level.show',$pro->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pro = Program_level::find($id);
        $pro->delete();

        $prog = Program_level::first();
        if(!$prog){
            $mit = MitProjects::find($pro->pro_id);
            if($mit->step_no==5)
                $mit->step_no=4;
            $mit->save();
            }
        return redirect()->route('program_level.show',$pro->pro_id)->with('success','تم الحذف بنجاح');
    }
}
