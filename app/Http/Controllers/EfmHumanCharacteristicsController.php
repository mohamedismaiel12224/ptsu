<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HumanRelation;
use App\EfmHumanRelation;
use App\HumanCharacteristic;
use App\EfmProject;
class EfmHumanCharacteristicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($id);
        $query['humans'] = HumanCharacteristic::all();
        $query['relations'] = HumanRelation::all();
        foreach ($query['humans'] as $key => $human) {
                $human->relations = HumanRelation::all();
                foreach ($human->relations as $ke => $rel) {
                        $check = EfmHumanRelation::where('human_id',$human->id)->where('relation_id',$rel->id)->where('pro_id',$id)->first();
                        $rel->option = 0;
                        if($check){
                            $rel->option = $check->option;
                        }
                }
        }
        return view('admin/systems/efm/humans/show',$query);
    }

    public function show_human($pro_id, $human_id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($pro_id);
        $query['human'] = HumanCharacteristic::find($human_id);
        $query['relations'] = HumanRelation::all();
        foreach ($query['relations'] as $ke => $rel) {
                $check = EfmHumanRelation::where('human_id',$human_id)->where('relation_id',$rel->id)->where('pro_id',$pro_id)->first();
                $rel->option = 0;
                if($check){
                    $rel->option = $check->option;
                }
        }
        
        return view('admin/systems/efm/humans/add',$query);
    }

    public function save_human(Request $request,$pro_id, $human_id)
    {
        $query['efm'] = EfmProject::find($pro_id);
        $query['human'] = HumanCharacteristic::find($human_id);
        $relations = $request->rel;
        foreach ($relations as $key => $rel) {
                $check = EfmHumanRelation::where('human_id',$human_id)->where('relation_id',$key)->where('pro_id',$pro_id)->first();
                if($check){
                    $check->option = $rel;
                    $check->save();
                }else{
                    $check = new EfmHumanRelation();
                    $check->human_id = $human_id;
                    $check->relation_id = $key;
                    $check->pro_id = $pro_id;
                    $check->option = $rel;
                    $check->save();
                }
        }
        $count = EfmHumanRelation::where('pro_id',$pro_id)->count();
        if($count==35){
            $query['efm']->step_no = 2;
            $query['efm']->save();
        }

        
        return redirect('efm/human_characteristics/'.$pro_id)->with('success','تم التعديل بنجاح');    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
