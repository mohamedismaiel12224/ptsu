<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SystemUser;
use App\EvaluateProject;
use Auth;
class PerformanceEvaluateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['sys'] = SystemUser::join('systems','systems.id','system_users.system_id','system_users.status as ')->where('user_id',Auth::id())->where('system_users.updated_at','>=',date('Y-m-d'))->where('system_id',3)->first();
        $query['projects'] = EvaluateProject::where('user_id',Auth::id())->get();
        return view('admin/systems/performance/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query['sys'] = SystemUser::join('systems','systems.id','system_users.system_id','system_users.status as ')->where('user_id',Auth::id())->where('system_users.updated_at','>=',date('Y-m-d'))->where('system_id',3)->first();

        return view('admin/systems/performance/add',$query);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
        ]);
        $ev = new EvaluateProject;
        $ev->name = $request->name;
        $ev->type = $request->type;
        $ev->user_id = Auth::id();
        $ev->save();
        return redirect()->route('performance_evaluate.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['per_ev'] = EvaluateProject::find($id);
        if($query['per_ev']->step_no == 1)
            return redirect('performance_evaluate/trainers/'.$id);
        elseif($query['per_ev']->step_no == 2)
            return redirect('performance_evaluate/link_evaluate/'.$id);
        elseif($query['per_ev']->step_no == 3)
            return redirect()->route('tool_type.show',$id);
        elseif($query['per_ev']->step_no == 4)
            return redirect()->route('questions_per_ev.show',$id);
        elseif($query['per_ev']->step_no == 5)
            return redirect('per_ev/select_responsibility/'.$id);
        elseif($query['per_ev']->step_no == 6)
            return redirect('questions_per_ev_selected/'.$id);    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['ev'] = EvaluateProject::where('user_id',Auth::id())->find($id);
        return view('admin/systems/performance/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',

        ]);
        $ev = EvaluateProject::find($id);
        $ev->name = $request->name;
        $ev->type = $request->type;
        $ev->save();
        return redirect()->route('performance_evaluate.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ev = EvaluateProject::where('user_id',Auth::id())->find($id);
        if($ev)
            $ev->delete();
        return redirect()->route('performance_evaluate.index')->with('success','تم الحذف بنجاح');
    }
}
