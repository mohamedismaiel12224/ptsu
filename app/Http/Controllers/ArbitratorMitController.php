<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Arb_goal;
use App\MitArbitration;
use App\Arb_category;
use App\Arb_need;
use App\Domain;
use App\Checkup;
use App\Training_arbitration;
use App\NotesArbitration;
use App\EvaluateArbitration;
use Auth;
use App\System;
use App\User;
class ArbitratorMitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=4&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function show($id)
    {
        $query['arbitrator_mit']=1;
        $query['mit'] = MitArbitration::find($id);
        $query['goals']=Arb_goal::where('pro_id',$id)->get();
        $query['categories']=Arb_category::where('pro_id',$id)->get();
        foreach ($query['categories'] as $key => $value) {
            $value->needs = Arb_need::where('category_id',$value->id)->where('pro_id',$id)->get();
        }
        $query['logo'] = System::find(1)->logo;
        return view('admin/systems/mit_arbitration/report',$query);
    }
    public function arbitrators($id){
        $query['mit_arb_pro']=1;
        $query['mit'] = MitArbitration::find($id);
        $query['arbitrators'] = User::where('role',4)->get();
        foreach ($query['arbitrators'] as $key => $value) {
            if(!Training_arbitration::where('user_id',$value->id)->where('pro_id',$id)->first())
                unset($query['arbitrators'][$key]);
        }
        return view('admin/systems/mit_arbitration/arbitrators',$query);
    }
    public function training($id)
    {
        $query['arbitrator_mit']=1;
        $query['mit'] = MitArbitration::find($id);
        $query['domains'] = Domain::where('status',1)->get();

        $query['goals']=Arb_goal::where('pro_id',$id)->get();
        foreach ($query['goals'] as $key => $value) {
            $query['goals'][$key]['domains'] = Domain::where('status',1)->get();
            $check=Training_arbitration::where('user_id',Auth::id())->where('pro_id',$id)->where('training_id',$value->id)->first();
            foreach ($query['goals'][$key]['domains'] as $ke=>$domain) {
                $query['goals'][$key]['domains'][$ke]['selected']=0;
                $query['goals'][$key]['domains'][$ke]['checkup'] = Checkup::where('domain_id',$domain->id)->orderBy('domain_id','asc')->get();
                if($check){
                    $domain_selected = explode(',', $check->domain_check_arr);
                    foreach ($domain_selected as $select) {
                        $arr = explode(':', $select);
                        if(isset($arr[1])&&($arr[0]==$domain->id))
                            $query['goals'][$key]['domains'][$ke]['selected']= $arr[1];
                    }
                }
            }
        }
        
        return view('admin/systems/mit_arbitration/arbitrator_training',$query);
    }

    public function training_arbitration($id){
        $query['mit'] = MitArbitration::find($id);
        $goals=Arb_goal::where('pro_id',$id)->get();
        $domains = Domain::where('status',1)->get();
        foreach($goals as $key=>$value){
            $arr='';
            foreach($domains as $domain){
                $name = 'check'.$domain->id.'-'.$value->id;
                if(!isset($_POST[$name])||!$_POST[$name])
                    return redirect()->back()->with('error','يوجد حقل فارغ');
                if($arr=='')
                    $arr .= $domain->id.':'.$_POST[$name];
                else
                    $arr .= ','.$domain->id.':'.$_POST[$name];
            }
            $check=Training_arbitration::where('user_id',Auth::id())->where('pro_id',$id)->where('training_id',$value->id)->first();
            if($check){
                $check->domain_check_arr= $arr;
                $check->save();
            }else{
                $tr = new Training_arbitration;
                $tr->user_id= Auth::id();
                $tr->domain_check_arr= $arr;
                $tr->training_id= $value->id;
                $tr->pro_id= $id;
                $tr->save();
            }
        }
        return redirect('arbitrator/mit/content/'.$id)->with('success','تم التخكيم بنجاح');
    }

    public function evaluate($id){
        $query['arbitrator_mit']=1;
        $query['mit'] = MitArbitration::find($id);
        $query['domains'] = Domain::where('status',4)->get();

        foreach ($query['domains'] as $key => $domain) {
            $query['domains'][$key]['checkup'] = Checkup::where('domain_id',$domain->id)->orderBy('domain_id','asc')->get();
            foreach ($query['domains'][$key]['checkup'] as $ke => $value) {
                $check=EvaluateArbitration::where('user_id',Auth::id())->where('pro_id',$id)->where('domain_id',$domain->id)->where('check_id',$value->id)->first();
                
                $query['domains'][$key]['checkup'][$ke]['selected']=0;
                if($check){
                    $query['domains'][$key]['checkup'][$ke]['selected']= $check->value;
                }
            }
        }
        $query['note']=NotesArbitration::where('user_id',Auth::id())->first();
        return view('admin/systems/mit_arbitration/arbitrator_evaluate',$query);
    }

    public function evaluate_arbitration($id){
        $query['mit'] = MitArbitration::find($id);
        $domains = Domain::where('status',4)->get();
        foreach($domains as $key=>$domain){
            $arr='';
            $checkup =  Checkup::where('domain_id',$domain->id)->orderBy('domain_id','asc')->get();
            foreach($checkup as $value){
                $name = 'check'.$domain->id.'-'.$value->id;
                if(!isset($_POST[$name])||!$_POST[$name]||$_POST[$name]==0)
                    return redirect()->back()->with('error','يوجد حقل فارغ');
                
                $check=EvaluateArbitration::where('user_id',Auth::id())->where('pro_id',$id)->where('domain_id',$domain->id)->where('check_id',$value->id)->first();
                if($check){
                    $check->value= $_POST[$name];
                    $check->save();
                }else{
                    $tr = new EvaluateArbitration;
                    $tr->user_id= Auth::id();
                    $tr->check_id= $value->id;
                    $tr->domain_id= $domain->id;
                    $tr->value= $_POST[$name];
                    $tr->pro_id= $id;
                    $tr->save();
                }
            }
        }
        $check_note=NotesArbitration::where('user_id',Auth::id())->first();
        if($check_note){
            $check_note->notes=$_POST['details'];
            $check_note->save();
        }else{
            
            $note = new NotesArbitration;
            $note->user_id=Auth::id();
            $note->notes=$_POST['details'];
            $note->save();
        }
        return redirect('arbitrator/mit/evaluate/'.$id)->with('success','تم التحكيم بنجاح');
    }

}
