<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ToolEfm;
class ToolEfmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['system_setting'] = 1;

        $query['tools'] = ToolEfm::all();
        return view('admin/tool_efm/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query['system_setting'] = 1;
        return view('admin/tool_efm/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'tool' => 'required',
            'type_tool' => 'required',
        ]);
        
        $attr = new ToolEfm;
        $attr->tool = $request->tool;
        $attr->type_tool = $request->type_tool;
        $attr->save();
        return redirect('tool_efm')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['system_setting'] = 1;
        $query['tool'] = ToolEfm::find($id);

        return view('admin/tool_efm/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'tool' => 'required',
            'type_tool' => 'required',
        ]);
        
        $attr =  ToolEfm::find($id);
        $attr->tool = $request->tool;
        $attr->type_tool = $request->type_tool;
        $attr->save();
        return redirect('tool_efm')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attr =  ToolEfm::find($id);
        $attr->delete();
        return redirect('tool_efm')->with('success','تم الحذف بنجاح');
    }
}
