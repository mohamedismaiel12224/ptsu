<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Courses;
use App\Trainers_courses;
use App\Users;
use Auth;
class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        if(Auth::user()->role==1){
            $id=0;
        }else{
            $id=Auth::id();
        }

        $query['courses']=Courses::where('fellow_id',$id)->get();
        return view('admin/courses/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/courses/add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'co_no' => 'required|unique:courses,co_no',
            'address' => 'required',
            'address_en' => 'required',
            'country' => 'required',
            'hours' => 'required|numeric',
            'created_at' => 'required|date',
        ]);
        
        $course = new Courses;
        $course->name = $request->name;
        $course->co_no = $request->co_no;
        $course->address = $request->address;
        $course->address_en = $request->address_en;
        $course->country = $request->country;
        $course->hours = $request->hours;
        if(Auth::user()->role==1){
            $course->fellow_id = 0;
        }else{
            $course->fellow_id = Auth::id();

        }
        $course->created_at = date("Y-m-d", strtotime($request->created_at));
        $course->save();
        return redirect()->route('courses.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['courses'] = Trainers_courses::join('courses','courses.id','trainers_courses.course_id')->select('*','trainers_courses.id as id')->where('trainer_id',$id)->get();
        $query['trainer'] = Users::find($id);
        return view('admin/trainer/show_courses',$query);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['course'] = Courses::find($id);
        return view('admin/courses/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'co_no' => 'required|unique:courses,co_no,'.$id,
            'address' => 'required',
            'address_en' => 'required',
            'country' => 'required',
            'hours' => 'required|numeric',
            'created_at' => 'required|date',
        ]);
        
        $course = Courses::find($id);
        $course->name = $request->name;
        $course->co_no = $request->co_no;
        $course->address = $request->address;
        $course->address_en = $request->address_en;
        $course->country = $request->country;
        $course->hours = $request->hours;
        $course->created_at = date("Y-m-d", strtotime($request->created_at));
        $course->save();
        return redirect()->route('courses.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Courses::find($id);
        $course->delete();
        return redirect()->route('courses.index')->with('success','تم الحذف بنجاح');
    }
}
