<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Fellowship;
use Auth;
class NotAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3)
                return redirect('login');
            return $next($request);
        });
    }


    public function index()
    {
        return view('admin/change_pass');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/change_pass');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request,[
        'password' => 'required',
        'confirm_password' => 'required|same:password',
        ]);
      $password=bcrypt($request->input('password'));
      $user=Users::find(Auth::id());
      $user->password=$password;
      $user->save();
      return redirect()->back()->with('success','تم تغيير الباسورد بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['f'] = Fellowship::find($id);
        if($query['f']->role == 3)
            return view('admin/materials/edit',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'file' => 'required',
        ]);
        $f = Fellowship::find($id);
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $extension = $files->getClientOriginalExtension();
            $f->url =$picture= 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/home';
            $files->move($destinationPath, $picture);
        }
        $f->save();
        return redirect()->route('sys_fellow.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
