<?php

namespace App\Http\Controllers;
use App\Elibrary;
use Illuminate\Http\Request;
use Auth;
class ElibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['section_id']=$id;
        return view('admin/elibrary/add_lib',$query);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['library'] = Elibrary::find($id);
        return view('admin/elibrary/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'file' => 'required',
        ]);
        $library = new Elibrary;
        $library->section_id = $id;
        $library->material_name = $request->material_name;
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $extension = $files->getClientOriginalExtension();
            $library->url =$picture= 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/library';
            $files->move($destinationPath, $picture);
        }
        $library->save();
        return redirect('section_lib/'.$id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $library = Elibrary::find($id);
         $library->delete();
        return redirect('section_lib/'.$library->section_id)->with('success','تم الحذف بنجاح');
    }
}
