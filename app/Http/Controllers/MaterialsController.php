<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fellowship;
use Auth;
class MaterialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['materials']=Fellowship::where('user_id',Auth::id())->get();
        return view('admin/materials/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/materials/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
         $this->validate($request, [
            'name' => 'required',
            'file' => 'required',
            'role' => 'required',

        ]);
        $library = new Fellowship;
        $library->role = $request->role;
        $library->name = $request->name;
        $library->system_id = $request->system_id;
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $extension = $files->getClientOriginalExtension();
            $library->url =$picture= 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/library';
            $files->move($destinationPath, $picture);
        }
        $library->save();
        return redirect('systems/'.$request->system_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news=Fellowship::find($id);
        $news->delete();
        return redirect('systems/'.$news->system_id)->with('success','تم الحذف بنجاح');
    }
}
