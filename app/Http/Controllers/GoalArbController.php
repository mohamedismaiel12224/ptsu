<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MitArbitration;
use App\Arb_goal;

class GoalArbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($id);
        if(!$query['mit'])
            return redirect()->back();
        return view('admin/systems/mit_arbitration/goal/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'training_output' => 'required',
        ]);
        
        $goal = new Arb_goal;
        $type = $request->status;
        if($type==0){
            if(!$request->value)
                return redirect()->back()->with('error','لا يوجد قيمة');
            $goal->learning_level=$request->value;
        }elseif($type==1){
            if(!$request->select)
                return redirect()->back()->with('error','لم يتم تحديد مستوى');
            $goal->learning_level=$request->select;
        }
        $goal->training_output = $request->training_output;
        $goal->details = $request->details;
        $goal->pro_id = $request->pro_id;
        $goal->save();

        $mit = MitArbitration::find($request->pro_id);
        if($mit->step_no==2)
            $mit->step_no=3;
        $mit->save();
        return redirect()->route('arb_goal.show',$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($id);
        if(!$query['mit'])
            return redirect()->back();

        $query['goals'] = Arb_goal::where('pro_id',$id)->get();
        return view('admin/systems/mit_arbitration/goal/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['goal'] = Arb_goal::find($id);
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($query['goal']->pro_id);
        if(!$query['mit'])
            return redirect()->back();
        return view('admin/systems/mit_arbitration/goal/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'training_output' => 'required',
        ]);
        
        $goal = Arb_goal::find($id);
        $type = $request->status;
        if($type==0){
            if($request->value)
                $goal->learning_level=$request->value;
        }elseif($type==1){
            if($request->select)
                $goal->learning_level=$request->select;
        }
        $goal->training_output = $request->training_output;
        $goal->details = $request->details;
        $goal->save();

        return redirect()->route('arb_goal.show',$goal->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $goal = Arb_goal::find($id);
        if($goal)
            $goal->delete();
        $check = Arb_goal::first();
        if(!$check){
            $mit = MitArbitration::find($goal->pro_id);
            $mit->step_no=2;
            $mit->save();
        }
        return redirect()->route('arb_goal.show',$goal->pro_id)->with('success','تم الحذف بنجاح');
    }
}
