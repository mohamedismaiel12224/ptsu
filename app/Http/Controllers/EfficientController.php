<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Efficient;
use Auth;
class EfficientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='norms'){
            $query['pages']=Efficient::where('status',1)->get();
            $query['norms']=1;
        }else{
            $query['norms']=0;
            $query['pages']=Efficient::where('status',0)->get();
        }
        return view('admin/effs/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='norms/create'){
            $query['norms']=1;
        }else{
            $query['norms']=0;
        }
        return view('admin/effs/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'detail' => 'required',
        ]);
        
        $effs = new Efficient;
        $effs->name = $request->name;
        $effs->detail = $request->detail;
        $link = Route::getCurrentRoute()->getPath();
        if($link=='norms'){
            $effs->status =1;
            $effs->save();
            return redirect()->route('norms.index')->with('success','تم الحفظ بنجاح');
        }else{
             $effs->status =0;
            $effs->save();
            return redirect()->route('effs.index')->with('success','تم الحفظ بنجاح');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='effs/{eff}/edit'){
            $query['norms']=0;
        }else{
            $query['norms']=1;
        }
        $query['page'] = Efficient::find($id);
        return view('admin/effs/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'detail' => 'required',
        ]);
        $effs = Efficient::find($id);
        $effs->name = $request->name;
        $effs->detail = $request->detail;
        $effs->save();
        $link = Route::getCurrentRoute()->getPath();
        if($link=='norms/{norm}'){
            return redirect()->route('norms.index')->with('success','تم التعديل بنجاح');
        }else{
            return redirect()->route('effs.index')->with('success','تم التعديل بنجاح');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $logos=Efficient::find($id);
        $logos->delete();
        $link = Route::getCurrentRoute()->getPath();
        if($link=='norms/{norm}'){
            return redirect()->route('norms.index')->with('success','تم التعديل بنجاح');
        }else{
            return redirect()->route('effs.index')->with('success','تم التعديل بنجاح');
        }
    }
}
