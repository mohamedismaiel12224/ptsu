<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\System;
use App\Fellowship;

use Auth;
class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['systems'] = System::all();
        return view('admin/systems/show',$query);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/systems/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'detail' => 'required',
            'file' => 'required',
        ]);
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $extension = $files->getClientOriginalExtension();
            $picture = 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/home';
            $files->move($destinationPath, $picture);
        }
        $system = new System;
        $system->name = $request->name;
        $system->detail = $request->detail;
        $system->logo = $picture;
        $system->save();
        return redirect()->route('systems.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['system'] = System::find($id);
        $query['materials']=Fellowship::where('system_id',$id)->get();
        return view('admin/materials/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['system'] = System::find($id);
        return view('admin/systems/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'detail' => 'required',
        ]);
        $system = System::find($id);
        $system->name = $request->name;
        $system->detail = $request->detail;
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $extension = $files->getClientOriginalExtension();
            $system->logo =$picture= 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/home';
            $files->move($destinationPath, $picture);
        }
        $system->save();
        return redirect()->route('systems.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $systems=System::find($id);
        $systems->delete();
        return redirect()->route('systems.index')->with('success','تم الحذف بنجاح');
    }
}
