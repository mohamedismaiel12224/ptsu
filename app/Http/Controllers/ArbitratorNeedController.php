<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Auth;
use App\NeedProjects;
use App\System;
use App\Responsibility;
use App\Questions;
use App\ArbitratorProject;
use App\User;
use App\ArbitratorNeed;
use App\Answers;
use App\ArbitratorAnswer;
use App\TaskResponsibility;
class ArbitratorNeedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=4&&$this->role!=5&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function show($id)
    {
        $query['arbitrator_need']=1;
        $query['need'] = NeedProjects::find($id);
        $link = Route::getCurrentRoute()->getPath();
       	$query['responsibility'] = Responsibility::where('pro_id',$id);
        if($link=='arbitrator/need_knowledge/{id}'){
            $query['need_knowledge']=1;
            $arr = explode(',', $query['need']->resp_selected);
            $query['responsibility']->whereIn('id',$arr);
        }
        $query['responsibility']=$query['responsibility']->get();
       	foreach ($query['responsibility'] as $key => $resp) {
	        $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$id)->get();
	        foreach ($resp->tasks as $key => $value) {
	            $value->questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id);
                if($link=='arbitrator/need_knowledge/{id}'){
                    $value->questions->whereIn('type',[1,3])->where('place',5);
                }else{
                    $value->questions->where('place',3);

                }
                $value->questions=$value->questions->get();
	            foreach ($value->questions as $ke => $val) {
	            	$val->answers = Answers::where('q_id',$val->id)->get();
	            	$val->answer = ArbitratorAnswer::where('q_id',$val->id)->where('user_id',Auth::id())->first();
	            }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
	        }
       	}
        if($query['need']->tool_type==1||$query['need']->tool_type==2||$link=='arbitrator/need_knowledge/{id}')
        	return view('admin/systems/need/arbitrator_test',$query);
        else
        	return view('admin/systems/need/arbitrator_note',$query);

    }
    public function arbitrator_need(Request $request,$id){
    	$query['arbitrator_need']=1;
        $query['need'] = NeedProjects::find($id);
        $link = Route::getCurrentRoute()->getPath();
        $query['responsibility'] = Responsibility::where('pro_id',$id);
        if($link=='arbitrator_need_knowledge/{id}'){
            $query['need_knowledge']=1;
            $arr = explode(',', $query['need']->resp_selected);
            $query['responsibility']->whereIn('id',$arr);
        }
        $query['responsibility']=$query['responsibility']->get();
        foreach ($query['responsibility'] as $key => $resp) {
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id);
                if($link=='arbitrator_need_knowledge/{id}'){
                    $value->questions->whereIn('type',[1,3])->where('place',5);
                }else{
                    $value->questions->where('place',3);
                }
                $value->questions=$value->questions->get();
                foreach ($value->questions as $ke => $val) {
                    $name = 'answer'.$val->id;
                    if(!isset($_POST[$name])||!$_POST[$name])
                        return redirect()->back()->with('error','يوجد حقل فارغ');
                    $check = ArbitratorAnswer::where('q_id',$val->id)->where('user_id',Auth::id())->first();
                    if($check){
                        $check->answer= $_POST[$name];
                        if($query['need']->tool_type==3||$query['need']->tool_type==4)
                            $check->degree= $_POST[$name];
                        $check->save();
                    }else{
                        $tr = new ArbitratorAnswer;
                        $tr->user_id= Auth::id();
                        $tr->q_id= $val->id;
                        $tr->pro_id= $id;
                        $tr->answer= $_POST[$name];
                        if($query['need']->tool_type==1||$query['need']->tool_type==2||$link=='arbitrator_need_knowledge/{id}'){

                        }else
                            $tr->degree= $_POST[$name];
                        $tr->save();
                    }
                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
            }
        }
        if($query['need']->tool_type==1||$query['need']->tool_type==2||$link=='arbitrator_need_knowledge/{id}')
            return redirect()->back()->with('success','تم حل الاختبار/المقابلة');
        else
            return redirect()->back()->with('success','تم حل الملاحظة/الاستبانة');
    }
    public function all_trainers($id){
        $query['need_pro']=1;
        $query['word']='المتدربين';
        $query['need'] = NeedProjects::find($id);
        if($query['need']->tool_type==1||$query['need']->tool_type==2)
        $query['url']='check_answer_trainers';

        $query['arbitrators'] = User::where('role',5)->get();
        foreach ($query['arbitrators'] as $key => $value) {
            if(!ArbitratorAnswer::where('user_id',$value->id)->where('pro_id',$id)->first())
                unset($query['arbitrators'][$key]);
        }
        return view('admin/systems/need/arbitrators',$query);
    }
    public function arbitrators_knowledge($id){
        $query['need_pro']=2;
        $query['word']='المتدربين';
        $query['url']='check_answer_arbitrator_knowlege';
        $query['url_report']='report_arbitrator_knowledge';
        $query['need'] = NeedProjects::find($id);
        $query['arbitrators'] = User::select('*','users.id as id')->join('arbitrator_projects','arbitrator_projects.user_id','users.id')->where('arbitrator_projects.status',1)->where('role',5)->where('pro_id',$id)->get();
        return view('admin/systems/need/arbitrators',$query);
    }
    public function arbitrators_skill($id){
        $query['need_pro']=2;
        $query['need_skill']=1;
        $query['word']='المتدربين';
        $query['url']='check_answer_arbitrator_skill';
        $query['url_report']='report_arbitrator_skill';
        $query['need'] = NeedProjects::find($id);
        $query['arbitrators'] = User::select('*','users.id as id')->join('arbitrator_projects','arbitrator_projects.user_id','users.id')->where('arbitrator_projects.status',1)->where('role',5)->where('pro_id',$id)->get();
        return view('admin/systems/need/arbitrators',$query);
    }
    public function all_arbitrators($id){
        $query['need_pro']=2;
        $query['uncheck']=1;
        $query['url_report']='report_arbitrators';
        $query['need'] = NeedProjects::find($id);
        $query['arbitrators'] = User::where('role',4)->get();
        foreach ($query['arbitrators'] as $key => $value) {
            if(!ArbitratorNeed::where('user_id',$value->id)->where('pro_id',$id)->first())
                unset($query['arbitrators'][$key]);
        }
        return view('admin/systems/need/arbitrators',$query);
    }
    public function check_answer_trainers($pro_id,$user_id)
    {
        $query['need_pro']=1;
        $query['url']='update_check_answer_trainers';
        $query['user_id']=$user_id;
        $query['need'] = NeedProjects::find($pro_id);
        if($query['need']->tool_type!=1&&$query['need']->tool_type!=2)
        	return redirect()->back();
        $query['responsibility'] = Responsibility::where('pro_id',$pro_id)->get();
       	foreach ($query['responsibility'] as $key => $resp) {
	        $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$pro_id)->get();
	        foreach ($resp->tasks as $key => $value) {
	            $value->questions = Questions::where('questions.pro_id',$pro_id)->where('task_id',$value->id)->where('place',3)->get();
	            foreach ($value->questions as $ke => $val) {
	            	$val->answers = Answers::where('q_id',$val->id)->get();
	            	$val->answer = ArbitratorAnswer::where('q_id',$val->id)->where('user_id',$user_id)->first();
	            }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
	        }
       	}
        return view('admin/systems/need/check_answer',$query);
    }
    public function check_answer_knowlege($pro_id,$user_id)
    {
        $query['need_pro']=2;
        $query['need_knowledge']=1;
        $query['url']='update_check_answer_knowledge';
        $query['user_id']=$user_id;
        $query['need'] = NeedProjects::find($pro_id);
        // if($query['need']->tool_type!=1&&$query['need']->tool_type!=2)
        //     return redirect()->back();
        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$pro_id)->whereIn('id',$arr)->get();
        foreach ($query['responsibility'] as $key => $resp) {
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$pro_id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$pro_id)->where('task_id',$value->id)->whereIn('type',[1,3])->where('place',5)->get();
                foreach ($value->questions as $ke => $val) {
                    $val->answers = Answers::where('q_id',$val->id)->get();
                    $val->answer = ArbitratorAnswer::where('q_id',$val->id)->where('user_id',$user_id)->first();
                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
            }
        }
        return view('admin/systems/need/check_answer',$query);
    }
    public function check_answer_skill($pro_id,$user_id)
    {
        $query['need_pro']=2;
        $query['need_skill']=1;
        $query['url']='update_check_answer_skill';
        $query['user_id']=$user_id;
        $query['need'] = NeedProjects::find($pro_id);
        // if($query['need']->tool_type!=1&&$query['need']->tool_type!=2)
        //     return redirect()->back();
        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$pro_id)->whereIn('id',$arr)->get();
        foreach ($query['responsibility'] as $key_resp => $resp) {
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$pro_id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$pro_id)->where('task_id',$value->id)->where('type',2)->where('place',5)->get();
                foreach ($value->questions as $ke => $val) {
                    $val->answers = Answers::where('q_id',$val->id)->get();
                    $val->answer = ArbitratorAnswer::where('q_id',$val->id)->where('user_id',$user_id)->first();
                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
            }
        }
        return view('admin/systems/need/check_answer',$query);
    }
    public function update_check_answer_trainers(Request $request,$id,$user_id){
        $query['need_pro']=1;
        $query['need'] = NeedProjects::find($id);
        $query['tasks'] = TaskResponsibility::where('pro_id',$id)->get();
        foreach ($query['tasks'] as $key => $value) {
            $value->questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id)->where('place',3)->get();
            foreach ($value->questions as $ke => $val) {

                $name = 'degree'.$val->id;
                if(!isset($_POST[$name])||$_POST[$name]==0)
                    return redirect()->back()->with('error','يوجد حقل فارغ');
                
                $check = ArbitratorAnswer::where('q_id',$val->id)->where('user_id',$user_id)->first();
                if($check){
                    $check->degree= $_POST[$name];
                    $check->save();
                }else{
                    return redirect()->back()->with('error','يوجد خطأ');
                    
                }
            }
        }
        return redirect('need/all_trainers/'.$id)->with('success','تم التصحيح بنجاح');
    }
    public function update_check_answer_knowledge(Request $request,$id,$user_id){
        $query['need_pro']=1;
        $query['need'] = NeedProjects::find($id);
        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$id)->whereIn('id',$arr)->get();
         foreach ($query['responsibility'] as $key => $resp) {
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id)->whereIn('type',[1,3])->where('place',5)->get();
                foreach ($value->questions as $ke => $val) {
                    $name = 'degree'.$val->id;
                    if(!isset($_POST[$name])||$_POST[$name]==0)
                        return redirect()->back()->with('error','يوجد حقل فارغ');
                    
                    $check = ArbitratorAnswer::where('q_id',$val->id)->where('user_id',$user_id)->first();
                    if($check){
                        $check->degree= $_POST[$name];
                        $check->save();
                    }else{
                        return redirect()->back()->with('error','يوجد خطأ');
                        
                    }
                }
            }
        }
        
        return redirect('need/arbitrators_knowledge/'.$id)->with('success','تم التصحيح بنجاح');
    }
    public function update_check_answer_skill(Request $request,$id,$user_id){
        $query['need_pro']=1;
        $query['need'] = NeedProjects::find($id);
        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$id)->whereIn('id',$arr)->get();
         foreach ($query['responsibility'] as $key => $resp) {
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id)->where('type',2)->where('place',5)->get();
                foreach ($value->questions as $ke => $val) {
                    $name = 'degree'.$val->id;
                    if(!isset($_POST[$name])||$_POST[$name]==0)
                        return redirect()->back()->with('error','يوجد حقل فارغ');
                    
                    $check = ArbitratorAnswer::where('q_id',$val->id)->where('user_id',$user_id)->first();
                    if($check){
                        $check->degree= $_POST[$name];
                        $check->save();
                    }else{
                        $deg = new ArbitratorAnswer;
                        $deg->q_id= $val->id;
                        $deg->pro_id= $id;
                        $deg->user_id= $user_id;
                        $deg->degree= $_POST[$name];
                        $deg->save();
                    }
                }
            }
        }
        
        return redirect('need/arbitrators_skill/'.$id)->with('success','تم تقييم الاداء المهاري بنجاح');
    }
    public function report_trainers($pro_id,$user_id=false){
        $query['need_pro']=1;
        $query['need'] = NeedProjects::find($pro_id);
        if($user_id)
            $query['is_user']=1;
        $color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a'];
        $query['tasks'] = TaskResponsibility::select('responsibilities.name','task_responsibilities.*')->join('responsibilities','responsibilities.id','task_responsibilities.resp_id')->where('task_responsibilities.pro_id',$pro_id)->get();
        $query['responsibility'] = Responsibility::where('pro_id',$pro_id)->get();
        foreach ($query['responsibility'] as $key_resp => $resp) {
            $performance = TaskResponsibility::where('resp_id',$resp->id)->sum('performance');
            $importance = TaskResponsibility::where('resp_id',$resp->id)->sum('importance');
            $difficult = TaskResponsibility::where('resp_id',$resp->id)->sum('difficult');
            $count = TaskResponsibility::where('resp_id',$resp->id)->count();
            if($count==0)
                $resp->public_avg = 0;
            else
                $resp->public_avg = ($performance + $importance + $difficult) / $count;
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$pro_id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$pro_id)->where('task_id',$value->id)->where('place',3)->get();
                $count_user=0;
                $count_5=0;
                $count_4=0;
                $count_3=0;
                $count_2=0;
                $count_1=0;
                foreach ($value->questions as $ke => $val) {
                    $val->answers = Answers::where('q_id',$val->id)->get();
                    
                    $val->count_user = ArbitratorAnswer::where('q_id',$val->id)->count();
                    if($user_id)
                        $val->count_user = 1;
                    $val->count_1 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',1);
                    if($user_id)
                        $val->count_1->where('user_id',$user_id);
                    $val->count_1 = $val->count_1->count();
                    $val->count_2 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',2);
                    if($user_id)
                        $val->count_2->where('user_id',$user_id);
                    $val->count_2 = $val->count_2->count();
                    $val->count_3 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',3);
                    if($user_id)
                        $val->count_3->where('user_id',$user_id);
                    $val->count_3 = $val->count_3->count();
                    $val->count_4 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',4);
                    if($user_id)
                        $val->count_4->where('user_id',$user_id);
                    $val->count_4 = $val->count_4->count();
                    $val->count_5 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',5);
                    if($user_id)
                        $val->count_5->where('user_id',$user_id);
                    $val->count_5 = $val->count_5->count();
                    
                    $count_user+=$val->count_user;
                    $count_5+=$val->count_5;
                    $count_4+=$val->count_4;
                    $count_3+=$val->count_3;
                    $count_2 += $val->count_2;
                    $count_1 += $val->count_1;

                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
                $query['chart_task'][$key_resp]['tasks'][$key]['count']=$count_user;

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['count']= $count_5;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['name']='كبيرة جدا Very Big';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['color']=$color[0];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['count']= $count_4;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['name']='كبيرة Big';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['color']=$color[1];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][2]['count']= $count_3;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][2]['name']='متوسطة Medium';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][2]['color']=$color[2];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][3]['count']= $count_2;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][3]['name']='ضعيفة Week';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][3]['color']=$color[3];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][4]['count']= $count_1;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][4]['name']='منعدمة None';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][4]['color']=$color[4];


            }
        }
        
        // print_r($query['chart_task']);
        return view('admin/systems/need/report_arbitrator',$query);
    }
    public function report_arbitrators($pro_id,$user_id=false){
        $query['need_pro']=2;
        $query['need'] = NeedProjects::find($pro_id);
        if($user_id)
            $query['is_user']=1;
        $color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a'];
        $query['tasks'] = TaskResponsibility::select('responsibilities.name','task_responsibilities.*')->join('responsibilities','responsibilities.id','task_responsibilities.resp_id')->where('task_responsibilities.pro_id',$pro_id)->get();

        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$pro_id)->whereIn('id',$arr)->get();
        
        foreach ($query['responsibility'] as $key_resp => $resp) {
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$pro_id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$pro_id)->where('task_id',$value->id)->where('place',5)->get();
                $count_user=0;
                $count_clear_1=0;
                $count_clear_2=0;
                $count_suitable_1=0;
                $count_suitable_2=0;
                foreach ($value->questions as $ke => $val) {
                    
                    $val->count_user = ArbitratorNeed::where('q_id',$val->id)->count();
                    if($user_id)
                        $val->count_user = 1;
                    $val->count_clear_1 = ArbitratorNeed::where('q_id',$val->id)->where('clear',1);
                    if($user_id)
                        $val->count_clear_1->where('user_id',$user_id);
                    $val->count_clear_1 = $val->count_clear_1->count();
                    $val->count_clear_2 = ArbitratorNeed::where('q_id',$val->id)->where('clear',2);
                    if($user_id)
                        $val->count_clear_2->where('user_id',$user_id);
                    $val->count_clear_2 = $val->count_clear_2->count();
                    $val->count_suitable_1 = ArbitratorNeed::where('q_id',$val->id)->where('suitable',1);
                    if($user_id)
                        $val->count_suitable_1->where('user_id',$user_id);
                    $val->count_suitable_1 = $val->count_suitable_1->count();
                    $val->count_suitable_2 = ArbitratorNeed::where('q_id',$val->id)->where('suitable',2);
                    if($user_id)
                        $val->count_suitable_2->where('user_id',$user_id);
                    $val->count_suitable_2 = $val->count_suitable_2->count();
                    $val->suggest= '';
                    if($user_id){
                        $suggest = ArbitratorNeed::where('q_id',$val->id)->where('user_id',$user_id)->first();
                        if($suggest)
                            $val->suggest=$suggest->suggest;
                    }
                    
                    $count_user+=$val->count_user;
                    $count_clear_1+=$val->count_clear_1;
                    $count_clear_2+=$val->count_clear_2;
                    $count_suitable_1+=$val->count_suitable_1;
                    $count_suitable_2 += $val->count_suitable_2;
                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
                $query['chart_task'][$key_resp]['tasks'][$key]['count']=$count_user;
                $query['chart_task2'][$key_resp]['tasks'][$key]['count']=$count_user;

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['count']= $count_clear_1;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['name']='واضح Clear';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['color']=$color[0];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['count']= $count_clear_2;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['name']='غير واضح Not Clear';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['color']=$color[1];

                $query['chart_task2'][$key_resp]['tasks'][$key]['task'][0]['count']= $count_suitable_1;
                $query['chart_task2'][$key_resp]['tasks'][$key]['task'][0]['name']='مناسب Suitable' ;
                $query['chart_task2'][$key_resp]['tasks'][$key]['task'][0]['color']=$color[2];

                $query['chart_task2'][$key_resp]['tasks'][$key]['task'][1]['count']= $count_suitable_2;
                $query['chart_task2'][$key_resp]['tasks'][$key]['task'][1]['name']='غير منلسب Not Suotable';
                $query['chart_task2'][$key_resp]['tasks'][$key]['task'][1]['color']=$color[3];



            }
        }
        
        // print_r($query['chart_task']);
        return view('admin/systems/need/report_arbitrators',$query);
    }

    public function report_knowledge($pro_id,$user_id=false){
        $query['need_pro']=2;
        $query['need'] = NeedProjects::find($pro_id);
        if($user_id)
            $query['is_user']=1;
        $color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a'];
        $query['tasks'] = TaskResponsibility::select('responsibilities.name','task_responsibilities.*')->join('responsibilities','responsibilities.id','task_responsibilities.resp_id')->where('task_responsibilities.pro_id',$pro_id)->get();
        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$pro_id)->whereIn('id',$arr)->get();
        foreach ($query['responsibility'] as $key_resp => $resp) {
            $performance = TaskResponsibility::where('resp_id',$resp->id)->sum('performance');
            $importance = TaskResponsibility::where('resp_id',$resp->id)->sum('importance');
            $difficult = TaskResponsibility::where('resp_id',$resp->id)->sum('difficult');
            $count = TaskResponsibility::where('resp_id',$resp->id)->count();
            if($count==0)
                $resp->public_avg = 0;
            else
                $resp->public_avg = ($performance + $importance + $difficult) / $count;
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$pro_id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$pro_id)->where('task_id',$value->id)->whereIn('type',[1,3])->where('place',5)->get();
                $count_user=0;
                $count_2=0;
                $count_1=0;
                foreach ($value->questions as $ke => $val) {
                    $val->answers = Answers::where('q_id',$val->id)->get();
                    
                    $val->count_user = ArbitratorProject::where('pro_id',$pro_id)->where('status',1)->count();
                    
                    $val->count_11 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',11);
                    if($user_id)
                        $val->count_11->where('user_id',$user_id);
                    $val->count_11 = $val->count_11->count();
                    $val->count_10 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',10);
                    if($user_id)
                        $val->count_10->where('user_id',$user_id);
                    $val->count_10 = $val->count_10->count();
                    
                    
                    $count_user+=$val->count_user;
                    $count_2 += $val->count_10;
                    $count_1 += $val->count_11;

                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
                $query['chart_task'][$key_resp]['tasks'][$key]['count']=$count_user;

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['count']= $count_1;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['name']='اجتاز Passed';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['color']=$color[0];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['count']= $count_2;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['name']='لم يجتاز Failed';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['color']=$color[1];

            }
        }
        
        // print_r($query['chart_task']);
        if($user_id)
            return view('admin/systems/need/report_arbitrator_knowledge',$query);
        else
            return view('admin/systems/need/report_all_arbitrator_knowledge',$query);

    }

    public function report_skill($pro_id,$user_id=false){
        $query['need_pro']=2;
        $query['need'] = NeedProjects::find($pro_id);
        if($user_id)
            $query['is_user']=1;
        $color=['de66d5','27e9ec','ff6d99','76fbc5','f4207a','de66d5','27e9ec','ff6d99','76fbc5','f4207a'];
        $query['tasks'] = TaskResponsibility::select('responsibilities.name','task_responsibilities.*')->join('responsibilities','responsibilities.id','task_responsibilities.resp_id')->where('task_responsibilities.pro_id',$pro_id)->get();
        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$pro_id)->whereIn('id',$arr)->get();
        foreach ($query['responsibility'] as $key_resp => $resp) {
            $performance = TaskResponsibility::where('resp_id',$resp->id)->sum('performance');
            $importance = TaskResponsibility::where('resp_id',$resp->id)->sum('importance');
            $difficult = TaskResponsibility::where('resp_id',$resp->id)->sum('difficult');
            $count = TaskResponsibility::where('resp_id',$resp->id)->count();
            if($count==0)
                $resp->public_avg = 0;
            else
                $resp->public_avg = ($performance + $importance + $difficult) / $count;
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$pro_id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$pro_id)->where('task_id',$value->id)->where('type',2)->where('place',5)->get();
                $count_user=0;
                $count_10=0;
                $count_9=0;
                $count_8=0;
                $count_7=0;
                $count_6=0;
                $count_5=0;
                $count_4=0;
                $count_3=0;
                $count_2=0;
                $count_1=0;
                foreach ($value->questions as $ke => $val) {
                    $val->answers = Answers::where('q_id',$val->id)->get();
                    
                    $val->count_user = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->count();
                    if($user_id)
                        $val->count_user = 1;
                    $val->count_1 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',1);
                    if($user_id)
                        $val->count_1->where('user_id',$user_id);
                    $val->count_1 = $val->count_1->count();
                    $val->count_2 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',2);
                    if($user_id)
                        $val->count_2->where('user_id',$user_id);
                    $val->count_2 = $val->count_2->count();
                    $val->count_3 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',3);
                    if($user_id)
                        $val->count_3->where('user_id',$user_id);
                    $val->count_3 = $val->count_3->count();
                    $val->count_4 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',4);
                    if($user_id)
                        $val->count_4->where('user_id',$user_id);
                    $val->count_4 = $val->count_4->count();
                    $val->count_5 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',5);
                    if($user_id)
                        $val->count_5->where('user_id',$user_id);
                    $val->count_5 = $val->count_5->count();
                    $val->count_6 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',6);
                    if($user_id)
                        $val->count_6->where('user_id',$user_id);
                    $val->count_6 = $val->count_6->count();
                    $val->count_7 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',7);
                    if($user_id)
                        $val->count_7->where('user_id',$user_id);
                    $val->count_7 = $val->count_7->count();
                    $val->count_8 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',8);
                    if($user_id)
                        $val->count_8->where('user_id',$user_id);
                    $val->count_8 = $val->count_8->count();
                    $val->count_9 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',9);
                    if($user_id)
                        $val->count_9->where('user_id',$user_id);
                    $val->count_9 = $val->count_9->count();
                    $val->count_10 = ArbitratorAnswer::where('q_id',$val->id)->where('answer',null)->where('degree',10);
                    if($user_id)
                        $val->count_10->where('user_id',$user_id);
                    $val->count_10 = $val->count_10->count();
                    
                    $count_user+=$val->count_user;
                    $count_10+=$val->count_10;
                    $count_9+=$val->count_9;
                    $count_8+=$val->count_8;
                    $count_7 += $val->count_7;
                    $count_6 += $val->count_6;
                    $count_5+=$val->count_5;
                    $count_4+=$val->count_4;
                    $count_3+=$val->count_3;
                    $count_2 += $val->count_2;
                    $count_1 += $val->count_1;

                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);

                $query['chart_task'][$key_resp]['tasks'][$key]['count']=$count_user;

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['count']= $count_10;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['name']='10';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][0]['color']=$color[0];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['count']= $count_9;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['name']='9';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][1]['color']=$color[1];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][2]['count']= $count_8;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][2]['name']='8';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][2]['color']=$color[2];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][3]['count']= $count_7;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][3]['name']='7';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][3]['color']=$color[3];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][4]['count']= $count_6;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][4]['name']='6';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][4]['color']=$color[4];


                $query['chart_task'][$key_resp]['tasks'][$key]['task'][5]['count']= $count_5;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][5]['name']='5';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][5]['color']=$color[5];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][6]['count']= $count_4;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][6]['name']='4';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][6]['color']=$color[6];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][7]['count']= $count_3;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][7]['name']='3';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][7]['color']=$color[7];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][8]['count']= $count_2;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][8]['name']='2';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][8]['color']=$color[8];

                $query['chart_task'][$key_resp]['tasks'][$key]['task'][9]['count']= $count_1;
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][9]['name']='1';
                $query['chart_task'][$key_resp]['tasks'][$key]['task'][9]['color']=$color[9];


            }
        }
        if($user_id)
            return view('admin/systems/need/report_arbitrator_skill',$query);
        else
            return view('admin/systems/need/report_all_arbitrator_skill',$query);
    }


    public function need_arbitration(Request $request,$id){
        $query['arbitrator_need']=2;
        $query['need'] = NeedProjects::find($id);
        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$id)->whereIn('id',$arr)->get();
        foreach ($query['responsibility'] as $key => $resp) {
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id)->where('place',5)->get();
                foreach ($value->questions as $ke => $val) {
                    // $val->answers = Answers::where('q_id',$val->id)->get();
                    $val->answer = ArbitratorNeed::where('q_id',$val->id)->where('user_id',Auth::id())->first();
                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
            }
        }
        return view('admin/systems/need/need_arbitration',$query);

    }
    public function update_need_arbitration(Request $request,$id){
        $query['arbitrator_need']=2;
        $query['need'] = NeedProjects::find($id);
        $arr = explode(',', $query['need']->resp_selected);
        $query['responsibility'] = Responsibility::where('pro_id',$id)->whereIn('id',$arr)->get();
        foreach ($query['responsibility'] as $key => $resp) {
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id)->where('place',5)->get();
                foreach ($value->questions as $ke => $val) {
                    $clear = 'clear'.$val->id;
                    $suitable = 'suitable'.$val->id;
                    $suggest = 'suggest'.$val->id;
                    if(!isset($_POST[$clear])||!$_POST[$clear]||!isset($_POST[$suitable])||!$_POST[$suitable])
                        return redirect()->back()->with('error','يوجد حقل فارغ');
                    
                    $check = ArbitratorNeed::where('pro_id',$id)->where('q_id',$val->id)->where('user_id',Auth::id())->first();
                    if($check){
                        $check->clear= $_POST[$clear];
                        $check->suitable= $_POST[$suitable];
                        $check->suggest= $_POST[$suggest];
                        $check->save();
                    }else{
                        $new = new ArbitratorNeed;
                        $new->pro_id= $id;
                        $new->q_id= $val->id;
                        $new->user_id= Auth::id();
                        $new->clear= $_POST[$clear];
                        $new->suitable= $_POST[$suitable];
                        $new->suggest= $_POST[$suggest];
                        $new->save();
                        
                    }
                }
                
            }
        }
        return redirect('arbitrator/need_arbitration/'.$id)->with('success','تم التحكيم بنجاح');

    }
}
