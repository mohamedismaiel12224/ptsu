<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Checkup;
use App\Domain;
class DomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['system_setting'] = 1;
        $link = Route::getCurrentRoute()->getPath();
        if($link=='domain_training'){
            $query['type_id'] = 1;
        }else if($link == 'domain_content'){
            $query['type_id'] = 2;
        }else if($link == 'domain_activities'){
            $query['type_id'] = 3;
        }else if($link == 'domain_evaluate'){
            $query['type_id'] = 4;
        }else if($link == 'domain_presentation_casting'){
            $query['type_id'] = 5;
        }else if($link == 'etpm/standard'){
            $query['type_id'] = 6;
        }else if($link == 'ev_trainers'){
            $query['type_id'] = 7;
        }
        $query['domains'] = Domain::where('status',$query['type_id'])->get();
        return view('admin/domain/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query['system_setting'] = 1;
        $link = Route::getCurrentRoute()->getPath();
        if($link=='domain_training/create'){
            $query['type_id'] = 1;
        }else if($link == 'domain_content/create'){
            $query['type_id'] = 2;
        }else if($link == 'domain_activities/create'){
            $query['type_id'] = 3;
        }else if($link == 'domain_evaluate/create'){
            $query['type_id'] = 4;
        }else if($link == 'domain_presentation_casting/create'){
            $query['type_id'] = 5;
        }else if($link == 'etpm/standard/create'){
            $query['type_id'] = 6;
        }else if($link == 'ev_trainers/create'){
            $query['type_id'] = 7;
        }
        return view('admin/domain/add',$query);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $domain = new Domain;
        $domain->name = $request->name;
        $domain->status = $request->type_id;
        $domain->save();
        foreach ($request->input('checkup.*') as $key => $value) {
            if($value){
                $check = new Checkup;
                $check->name = $value;
                $check->domain_id = $domain->id;
                $check->save();
            }
        }
        return redirect()->back()->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['system_setting'] = 1;
        $query['type_id'] =1;
        $query['domain']= Domain::find($id);
        $query['checkups'] =Checkup::where('domain_id',$id)->get();
        return view('admin/domain/show_checkup',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['system_setting'] = 1;
        $query['domain'] = Domain::find($id);
        $query['type_id'] = $query['domain']->status;
        return view('admin/domain/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $domain = Domain::find($id);
        $domain->name = $request->name;
        $domain->save();
        return redirect()->back()->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $domain = Domain::find($id);
        if($domain)
            $domain->delete();
        Checkup::where('domain_id',$id)->delete();
        return redirect()->back()->with('success','تم الحذف بنجاح');
    }
}
