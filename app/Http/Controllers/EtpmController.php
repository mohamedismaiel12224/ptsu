<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EtpmProject;
use Auth;
class EtpmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['projects'] = EtpmProject::where('user_id',Auth::id())->get();
        return view('admin/systems/etpm/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/systems/etpm/add');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        $et = new EtpmProject;
        $et->name = $request->name;
        $et->user_id = Auth::id();
        $et->save();
        return redirect()->route('etpm.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['per_ev'] = EtpmProject::find($id);
        if($query['per_ev']->step_no == 1)
            return redirect('etpm/trainers/'.$id);
        elseif($query['per_ev']->step_no == 2)
            return redirect('etpm/link_evaluate/'.$id);
        elseif($query['per_ev']->step_no == 3)
            return redirect()->route('tool_type.show',$id);
        elseif($query['per_ev']->step_no == 4)
            return redirect()->route('questions_per_ev.show',$id);
        elseif($query['per_ev']->step_no == 5)
            return redirect('per_ev/select_responsibility/'.$id);
        elseif($query['per_ev']->step_no == 6)
            return redirect('questions_per_ev_selected/'.$id);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['ev'] = EtpmProject::where('user_id',Auth::id())->find($id);
        return view('admin/systems/etpm/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',

        ]);
        $ev = EtpmProject::find($id);
        $ev->name = $request->name;
        $ev->save();
        return redirect()->route('etpm.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ev = EtpmProject::where('user_id',Auth::id())->find($id);
        if($ev)
            $ev->delete();
        return redirect()->route('etpm.index')->with('success','تم الحذف بنجاح');
    }
}
