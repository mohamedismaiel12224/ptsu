<?php

namespace App\Http\Controllers;
use App\Section_lib;
use App\Elibrary;
use Illuminate\Http\Request;
use Auth;
class SectionLibController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['elibrary']=Section_lib::all();
        return view('admin/elibrary/show',$query); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/elibrary/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'section' => 'required',
        ]);
        
        $library = new Section_lib;
        $library->section = $request->section;
        $library->save();
        return redirect()->route('section_lib.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['elibrary']=Elibrary::where('section_id',$id)->get();
        $query['Section_lib']=Section_lib::find($id);
        return view('admin/elibrary/show_lib',$query); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['library'] = Section_lib::find($id);
        return view('admin/elibrary/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'section' => 'required',
        ]);
        $library = Section_lib::find($id);
        $library->section = $request->section;
        $library->save();
        return redirect()->route('section_lib.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $library = Section_lib::find($id);
        $library->delete();
        Elibrary::where('section_id',$id)->delete();
        return redirect()->route('section_lib.index')->with('success','تم الحذف بنجاح');
    }
}
