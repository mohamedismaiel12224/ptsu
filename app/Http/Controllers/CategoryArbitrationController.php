<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Arb_category;
use App\Arb_need;
use App\MitArbitration;
class CategoryArbitrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($id);
        if(!$query['mit'])
            return redirect()->back();
        return view('admin/systems/mit_arbitration/category/add',$query);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'category' => 'required',
            'need' => 'required',
        ]);
        
        $cat = new Arb_category;
        $cat->category = $request->category;
        $cat->pro_id = $request->pro_id;
        $cat->save();
        foreach ($request->input('need.*') AS $key => $value) {
            $need = new Arb_need;
            $need->need = $value;
            $need->category_id = $cat->id;
            $need->pro_id = $request->pro_id;
            $need->save();
        }
        $mit = MitArbitration::find($request->pro_id);
        if($mit->step_no==1)
            $mit->step_no=2;
        $mit->save();

        return redirect()->route('arb_category.show',$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($id);
        if(!$query['mit'])
            return redirect()->back();

        $query['categories'] = Arb_category::where('pro_id',$id)->get();
        return view('admin/systems/mit_arbitration/category/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['category'] = Arb_category::find($id);
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($query['category']->pro_id);
        if(!$query['mit'])
            return redirect()->back();
        return view('admin/systems/mit_arbitration/category/add',$query);

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'category' => 'required',
        ]);
        
        $cat = Arb_category::find($id);
        $cat->category = $request->category;
        $cat->save();
        return redirect()->route('arb_category.show',$cat->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Arb_category::find($id);
        if($cat)
            $cat->delete();
        $check = Arb_category::first();
        if(!$check){
            $mit = MitArbitration::find($cat->pro_id);
            $mit->step_no=1;
            $mit->save();
        }
        return redirect()->route('arb_category.show',$cat->pro_id)->with('success','تم التعديل بنجاح');

    }
}
