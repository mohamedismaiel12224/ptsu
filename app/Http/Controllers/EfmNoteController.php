<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EfmNote;
use App\EfmProject;
class EfmNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pro_id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($pro_id);
        return view('admin/systems/efm/notes/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'field' => 'required',
            'note' => 'required',
            'assess' => 'required',
        ]);
        $note = EfmNote::create($request->all());
        return redirect('efm/note/'.$note->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($id);
        $query['notes'] = EfmNote::where('pro_id',$id)->get();
        return view('admin/systems/efm/notes/show',$query);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['efm_pro'] = 1;
        $query['note'] = EfmNote::find($id);
        $query['efm'] = EfmProject::find($query['note']->pro_id);
        return view('admin/systems/efm/notes/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'field' => 'required',
            'note' => 'required',
            'assess' => 'required',
        ]);
        $note = EfmNote::find($id);
        $note->update($request->all());
        return redirect('efm/note/'.$note->pro_id)->with('success','تم التعديل بنجاح');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = EfmNote::find($id);
        $note->delete();
        return redirect('efm/note/'.$note->pro_id)->with('success','تم الحذف بنجاح');
    }
}
