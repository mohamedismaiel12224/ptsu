<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Checkup;
use App\Domain;
class CheckupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['system_setting'] = 1;
        $query['type_id'] =1;
        $query['domain']= Domain::find($id);
        return view('admin/domain/add_checkup',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $check = new Checkup;
        $check->name = $request->name;
        $check->domain_id = $request->domain_id;
        $check->save();
        return redirect()->back()->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['system_setting'] = 1;
        $query['checkup'] = Checkup::find($id);
        $query['domain'] = Domain::find($query['checkup']->domain_id);
        $query['type_id'] = $query['domain']->id;
        return view('admin/domain/add_checkup',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $check = Checkup::find($id);
        $check->name = $request->name;
        $check->save();
        return redirect()->back()->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $domain = Checkup::find($id);
        if($domain)
            $domain->delete();
        return redirect()->back()->with('success','تم الحذف بنجاح');
    }
}
