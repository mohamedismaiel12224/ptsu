<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EfmProject;
use App\SystemAttribute;
use App\EfmFrame;
class EfmFramingController extends Controller
{
		public function select_evaluate_fields($pro_id)
		{
		$query['efm_pro'] = 1;
				$query['attr'] = SystemAttribute::where('type_id',14)->get();
				$query['efm'] = EfmProject::find($pro_id);
				foreach ($query['attr'] as $key => $value) {
					$check = EfmFrame::where('type_id',$value->id)->where('pro_id',$pro_id)->first();
					if($check){
						$value->justifications = $check->justifications;
						$value->checked = 1;
					}
				}
				return view('admin/systems/efm/framing/select_evaluate_fields',$query);
		}
		public function update_select_evaluate_fields(Request $request,$pro_id)
		{
			$this->validate($request, [
					'justifications.*' => 'required_with:tr.*',

				]);
				$query['efm_pro'] = 1;
				$query['attr'] = SystemAttribute::where('type_id',14)->get();
				$query['efm'] = EfmProject::find($pro_id);
				$arr = array();
				foreach ($request->tr as $key => $value) {
					$frame = EfmFrame::where('pro_id',$pro_id)->where('type_id',$key)->first();
					if(!$frame){
						 $frame = new EfmFrame;
					}
					$frame->pro_id = $pro_id;
					$frame->type_id = $key;
					$frame->justifications = $request->justifications[$key];
					$frame->save();
					array_push($arr, $key);
				}
				EfmFrame::where('pro_id',$pro_id)->whereNotIn('type_id',$arr)->delete();
				return redirect('efm/targets/'.$pro_id)->with('success','تم الحفظ بنجاح');
		}

		public function targets($pro_id)
		{
		$query['efm_pro'] = 1;
				$query['framing'] = EfmFrame::select('efm_frames.*','system_attributes.name')->join('system_attributes','system_attributes.id','efm_frames.type_id')->where('pro_id',$pro_id)->get();
				$query['efm'] = EfmProject::find($pro_id);

				return view('admin/systems/efm/framing/targets',$query);
		}
		public function edit_targets($frame_id)
		{
		$query['efm_pro'] = 1;
				$query['framing'] = EfmFrame::select('efm_frames.*','system_attributes.name')->join('system_attributes','system_attributes.id','efm_frames.type_id')->find($frame_id);
				$query['efm'] = EfmProject::find($query['framing']->pro_id);

				return view('admin/systems/efm/framing/edit_targets',$query);
		}
		public function update_targets(Request $request, $frame_id)
		{
		$query['efm_pro'] = 1;
				$framing = EfmFrame::find($frame_id);
				$query['efm'] = EfmProject::find($framing->pro_id);
				$framing->goal = $request->goal;
				$framing->diagnosis_values = $request->diagnosis_values;
				$framing->target = $request->target;
				$framing->save();

				$check = EfmFrame::where('pro_id',$framing->pro_id)->where('diagnosis_values',null)->first();
				if($check){
					$query['efm']->step_no = 3 ;
					$query['efm']->save();
					return redirect('efm/targets/'.$framing->pro_id);
				}else{
					$query['efm']->step_no = 4 ;
					$query['efm']->save();
					return redirect('efm/analysis/'.$framing->pro_id);

				}
				return view('admin/systems/efm/framing/edit_targets',$query);
		}
}
