<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MitArbitration;
use App\Arb_goal;
use App\Arb_category;
use App\Arb_need;
use App\System;
use App\Domain;
use App\Checkup;
use App\Users;
use App\SystemUser;
use App\Training_arbitration;
use App\ContentArbitration;
use App\ActivitiesArbitration;
use App\EvaluateArbitration;
use App\NotesArbitration;
use Auth;
class MitArbitrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        if(!SystemUser::where('system_id',1)->where('user_id',Auth::id())->where('status_arbitration',1)->where('updated_at','>=',date('Y-m-d'))->first())
            return redirect()->back();
        $query['projects'] = MitArbitration::where('user_id',Auth::id())->get();
        return view('admin/systems/mit_arbitration/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/systems/mit_arbitration/add');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'address' => 'required',
            'benefit_establishment' => 'required',
            'hours' => 'required|numeric',
            'date' => 'required|date',
        ]);
        
        $mit = new MitArbitration;
        $mit->address = $request->address;
        $mit->benefit_establishment = $request->benefit_establishment;
        $mit->hours = $request->hours;
        $mit->date = date('Y-m-d',strtotime($request->date));
        $mit->user_id = Auth::id();
        $mit->save();
        return redirect()->route('mit_arbitration.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit'] = MitArbitration::find($id);
        if($query['mit']->step_no == 1)
            return redirect()->route('arb_category.show',$id);
        elseif($query['mit']->step_no == 2)
            return redirect()->route('arb_goal.show',$id);
        // elseif($query['mit']->step_no == 3)
        //     return redirect('vertical/'.$id);
        // elseif($query['mit']->step_no == 4)
        //     return redirect()->route('program_level.show',$id);
        // elseif($query['mit']->step_no == 5)
        //     return redirect('select_level/'.$id);
        // elseif($query['mit']->step_no == 6)
        //     return redirect('mit_type/'.$id);
    }
    public function report($id){
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($id);
        $query['goals']=Arb_goal::where('pro_id',$id)->get();
        $query['categories']=Arb_category::where('pro_id',$id)->get();
        foreach ($query['categories'] as $key => $value) {
            $value->needs = Arb_need::where('category_id',$value->id)->where('pro_id',$id)->get();
        }
        $query['logo'] = System::find(1)->logo;
        return view('admin/systems/mit_arbitration/report',$query);

    }
    public function report_training($pro_id,$user_id=false){
        $query['mit_arb_pro']=1;
        $query['mit'] = MitArbitration::find($pro_id);
        $query['logo'] = System::find(1)->logo;

        $training = Training_arbitration::where('pro_id',$pro_id);
        if($user_id){
            $training->where('user_id',$user_id);
        }
        $training = $training->get();
        $array =array();
        $color=['de66d5','27e9ec','ffe9a4','76fbc5','f4207a'];

        foreach ($training as $key => $value) {
            $domain_selected = explode(',', $value->domain_check_arr);
            foreach ($domain_selected as $select) {
                $arr = explode(':', $select);
                if(isset($arr[1]))
                    $array[$arr[0]][]= $arr[1];
            }
        }
        $i=0;
        foreach ($array as $key => $value) {
            $query['chart_training'][$i]['domain']=Domain::find($key)->name;
            $count=array_count_values($value);
            $query['chart_training'][$i]['count']=count($value);
            $j=0;

            foreach ($count as $ke => $val) {
                $query['chart_training'][$i]['checkup'][$j]['count']=$val;
                $query['chart_training'][$i]['checkup'][$j]['name']=Checkup::find($ke)->name;
                $query['chart_training'][$i]['checkup'][$j]['color']=$color[$j%5];
                $j++;
            }
            $i++;

        }
        if($user_id){
            $query['domains'] = Domain::where('status',1)->get();
            $query['goals']=Arb_goal::where('pro_id',$pro_id)->get();
            foreach ($query['goals'] as $key => $value) {
                $query['goals'][$key]['domains'] = Domain::where('status',1)->get();
                $check=Training_arbitration::where('user_id',$user_id)->where('pro_id',$pro_id)->where('training_id',$value->id)->first();
                foreach ($query['goals'][$key]['domains'] as $ke=>$domain) {
                    $query['goals'][$key]['domains'][$ke]['selected']=0;
                    $query['goals'][$key]['domains'][$ke]['checkup'] = Checkup::where('domain_id',$domain->id)->orderBy('domain_id','asc')->get();
                    if($check){
                        $domain_selected = explode(',', $check->domain_check_arr);
                        foreach ($domain_selected as $select) {
                            $arr = explode(':', $select);
                            if(isset($arr[1])&&($arr[0]==$domain->id))
                                $query['goals'][$key]['domains'][$ke]['selected']= $arr[1];
                        }
                    }
                }
            }
        }
        
        return view('admin/systems/mit_arbitration/report_training',$query);
    }
    public function report_content($pro_id,$user_id=false){
        $query['mit_arb_pro']=1;
        $query['mit'] = MitArbitration::find($pro_id);
        $query['logo'] = System::find(1)->logo;

        $training = ContentArbitration::where('pro_id',$pro_id);
        if($user_id){
            $training->where('user_id',$user_id);
        }
        $training = $training->get();
        $array =array();
        $color=['de66d5','27e9ec','ffe9a4','76fbc5'];

        foreach ($training as $key => $value) {
            $domain_selected = explode(',', $value->domain_check_arr);
            foreach ($domain_selected as $select) {
                $arr = explode(':', $select);
                if(isset($arr[1]))
                    $array[$arr[0]][]= $arr[1];
            }
        }
        $i=0;
        foreach ($array as $key => $value) {
            $query['chart_training'][$i]['domain']=Domain::find($key)->name;
            $count=array_count_values($value);
            $query['chart_training'][$i]['count']=count($value);
            $j=0;

            foreach ($count as $ke => $val) {
                $query['chart_training'][$i]['checkup'][$j]['count']=$val;
                $query['chart_training'][$i]['checkup'][$j]['name']=Checkup::find($ke)->name;
                $query['chart_training'][$i]['checkup'][$j]['color']=$color[$j];
                $j++;
            }
            $i++;

        }
        if($user_id){
            $query['user_id'] = $user_id;
            $query['domains'] = Domain::where('status',2)->get();
            $query['content'] = ContentArbitration::where('pro_id',$pro_id)->where('user_id',$user_id)->get();
        }
        
        return view('admin/systems/mit_arbitration/report_content',$query);
    }
    public function report_activities($pro_id,$user_id=false){
        $query['mit_arb_pro']=1;
        $query['mit'] = MitArbitration::find($pro_id);
        $query['logo'] = System::find(1)->logo;

        $training = ActivitiesArbitration::where('pro_id',$pro_id);
        if($user_id){
            $training->where('user_id',$user_id);
        }
        $training = $training->get();
        $array =array();
        $color=['de66d5','27e9ec','ffe9a4','76fbc5'];

        foreach ($training as $key => $value) {
            $domain_selected = explode(',', $value->domain_check_arr);
            foreach ($domain_selected as $select) {
                $arr = explode(':', $select);
                if(isset($arr[1]))
                    $array[$arr[0]][]= $arr[1];
            }
        }
        $i=0;
        foreach ($array as $key => $value) {
            $query['chart_training'][$i]['domain']=Domain::find($key)->name;
            $count=array_count_values($value);
            $query['chart_training'][$i]['count']=count($value);
            $j=0;

            foreach ($count as $ke => $val) {
                $query['chart_training'][$i]['checkup'][$j]['count']=$val;
                $query['chart_training'][$i]['checkup'][$j]['name']=Checkup::find($ke)->name;
                $query['chart_training'][$i]['checkup'][$j]['color']=$color[$j];
                $j++;
            }
            $i++;

        }
        if($user_id){
            $query['domains'] = Domain::where('status',3)->get();
            $query['activities'] = ActivitiesArbitration::where('pro_id',$pro_id)->where('user_id',$user_id)->get();

        }
        
        return view('admin/systems/mit_arbitration/report_activities',$query);
    }
    public function report_evaluate($pro_id,$user_id=false){
        $query['mit_arb_pro']=1;
        $query['mit'] = MitArbitration::find($pro_id);
        $query['logo'] = System::find(1)->logo;
        
        if($user_id)
            $query['is_user']=1;
        $color=['de66d5','27e9ec','ffe9a4','76fbc5','f4207a'];
         $query['domains'] = Domain::where('status',4)->get();

        foreach ($query['domains'] as $key_domain => $domain) {
            $array =array();
            $query['domains'][$key_domain]['checkup'] = Checkup::where('domain_id',$domain->id)->orderBy('domain_id','asc')->get();
            foreach ($query['domains'][$key_domain]['checkup'] as $key => $check) {
                $query['domains'][$key_domain]['checkup'][$key]['count_5'] = EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('check_id',$check->id)->where('value',5);
                if($user_id){
                    $query['domains'][$key_domain]['checkup'][$key]['count_5']->where('user_id',$user_id);
                }
                $query['domains'][$key_domain]['checkup'][$key]['count_5'] = $query['domains'][$key_domain]['checkup'][$key]['count_5']->count();
                $query['domains'][$key_domain]['checkup'][$key]['count_4'] = EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('check_id',$check->id)->where('value',4);
                if($user_id){
                    $query['domains'][$key_domain]['checkup'][$key]['count_4']->where('user_id',$user_id);
                }
                $query['domains'][$key_domain]['checkup'][$key]['count_4'] = $query['domains'][$key_domain]['checkup'][$key]['count_4']->count();
                $query['domains'][$key_domain]['checkup'][$key]['count_3'] = EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('check_id',$check->id)->where('value',3);
                if($user_id){
                    $query['domains'][$key_domain]['checkup'][$key]['count_3'] ->where('user_id',$user_id);
                }
                $query['domains'][$key_domain]['checkup'][$key]['count_3']  = $query['domains'][$key_domain]['checkup'][$key]['count_3'] ->count();
                $query['domains'][$key_domain]['checkup'][$key]['count_2'] = EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('check_id',$check->id)->where('value',2);
                if($user_id){
                    $query['domains'][$key_domain]['checkup'][$key]['count_2']->where('user_id',$user_id);
                }
                $query['domains'][$key_domain]['checkup'][$key]['count_2'] = $query['domains'][$key_domain]['checkup'][$key]['count_2']->count();
                $query['domains'][$key_domain]['checkup'][$key]['count_1'] = EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('check_id',$check->id)->where('value',1);
                if($user_id){
                    $query['domains'][$key_domain]['checkup'][$key]['count_1']->where('user_id',$user_id);
                }
                $query['domains'][$key_domain]['checkup'][$key]['count_1'] = $query['domains'][$key_domain]['checkup'][$key]['count_1']->count();
            }

            $query['chart_training'][$key_domain]['count']=Checkup::where('domain_id',$domain->id)->count();
            $query['chart_training'][$key_domain]['checkup'][0]['count']= EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('value',5);
                if($user_id){
                    $query['chart_training'][$key_domain]['checkup'][0]['count']->where('user_id',$user_id);
                }
                $query['chart_training'][$key_domain]['checkup'][0]['count'] = $query['chart_training'][$key_domain]['checkup'][0]['count']->count();
            $query['chart_training'][$key_domain]['checkup'][0]['name']='ممتاز';
            $query['chart_training'][$key_domain]['checkup'][0]['color']=$color[0];


            $query['chart_training'][$key_domain]['checkup'][1]['count']= EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('value',4);
                if($user_id){
                    $query['chart_training'][$key_domain]['checkup'][1]['count']->where('user_id',$user_id);
                }
                $query['chart_training'][$key_domain]['checkup'][1]['count'] = $query['chart_training'][$key_domain]['checkup'][1]['count']->count();
            $query['chart_training'][$key_domain]['checkup'][1]['name']='جيد جدا';
            $query['chart_training'][$key_domain]['checkup'][1]['color']=$color[1];


            $query['chart_training'][$key_domain]['checkup'][2]['count']= EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('value',3);
                if($user_id){
                    $query['chart_training'][$key_domain]['checkup'][2]['count']->where('user_id',$user_id);
                }
                $query['chart_training'][$key_domain]['checkup'][2]['count'] = $query['chart_training'][$key_domain]['checkup'][2]['count']->count();
            $query['chart_training'][$key_domain]['checkup'][2]['name']='جيد';
            $query['chart_training'][$key_domain]['checkup'][2]['color']=$color[2];

            $query['chart_training'][$key_domain]['checkup'][3]['count']= EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('value',2);
                if($user_id){
                    $query['chart_training'][$key_domain]['checkup'][3]['count']->where('user_id',$user_id);
                }
                $query['chart_training'][$key_domain]['checkup'][3]['count'] = $query['chart_training'][$key_domain]['checkup'][3]['count']->count();
            $query['chart_training'][$key_domain]['checkup'][3]['name']='ضعيف';
            $query['chart_training'][$key_domain]['checkup'][3]['color']=$color[3];

            $query['chart_training'][$key_domain]['checkup'][4]['count']= EvaluateArbitration::where('pro_id',$pro_id)->where('domain_id',$domain->id)->where('value',1);
                if($user_id){
                    $query['chart_training'][$key_domain]['checkup'][4]['count']->where('user_id',$user_id);
                }
                $query['chart_training'][$key_domain]['checkup'][4]['count'] = $query['chart_training'][$key_domain]['checkup'][4]['count']->count();
            $query['chart_training'][$key_domain]['checkup'][4]['name']='متروك';
            $query['chart_training'][$key_domain]['checkup'][4]['color']=$color[4];

            $query['domains'][$key_domain]['avg'] = 
                $query['chart_training'][$key_domain]['checkup'][0]['count']*5 +
                $query['chart_training'][$key_domain]['checkup'][1]['count']*4 +
                $query['chart_training'][$key_domain]['checkup'][2]['count']*3 +
                $query['chart_training'][$key_domain]['checkup'][3]['count']*2 +
                $query['chart_training'][$key_domain]['checkup'][4]['count']*1 ;
            
            $query['domains'][$key_domain]['count'] = 
                $query['chart_training'][$key_domain]['checkup'][0]['count'] +
                $query['chart_training'][$key_domain]['checkup'][1]['count'] +
                $query['chart_training'][$key_domain]['checkup'][2]['count'] +
                $query['chart_training'][$key_domain]['checkup'][3]['count'] +
                $query['chart_training'][$key_domain]['checkup'][4]['count'] ;
            

            
            
            
        }
        if($user_id)
            $query['note']=NotesArbitration::where('user_id',$user_id)->first();

        // print_r($query);
        return view('admin/systems/mit_arbitration/report_evaluate',$query);
    }
    public function arb_link($id){
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($id);
        return view('admin/systems/mit_arbitration/link',$query);

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['mit_arbitration'] = MitArbitration::find($id);
        return view('admin/systems/mit_arbitration/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'address' => 'required',
            'benefit_establishment' => 'required',
            'hours' => 'required|numeric',
            'date' => 'required|date',
        ]);
        
        $mit = MitArbitration::find($id);
        $mit->address = $request->address;
        $mit->benefit_establishment = $request->benefit_establishment;
        $mit->hours = $request->hours;
        $mit->date = date('Y-m-d',strtotime($request->date));
        $mit->user_id = Auth::id();
        $mit->save();
        return redirect()->route('mit_arbitration.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mit = MitArbitration::find($id);
        if($mit)
            $mit->delete();
        return redirect()->route('mit_arbitration.index')->with('success','تم الحذف بنجاح');
    }
}
