<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NeedProjects;
use App\Responsibility;
use App\TaskResponsibility;
use Auth;
class TaskResponsibilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id,$pro_id)
    {
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($pro_id);        
        if(!$query['need'])
            return redirect()->back();
        $query['resp'] = Responsibility::find($id);
        return view('admin/systems/need/task/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'task' => 'required',
            'performance' => 'required',
            'importance' => 'required',
            'difficult' => 'required',
        ]);
        
        $task = new TaskResponsibility;
        $task->task = $request->task;
        $task->performance = $request->performance;
        $task->importance = $request->importance;
        $task->difficult = $request->difficult;
        $task->resp_id = $request->resp_id;
        $task->pro_id = $request->pro_id;
        $task->save();

        $need = NeedProjects::find($request->pro_id);
        if($need->step_no==1)
            $need->step_no=2;
        $need->save();
        return redirect('task_need/'.$request->resp_id.'/'.$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$pro_id)
    {
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($pro_id);
        if(!$query['need'])
            return redirect()->back();
        $query['resp'] = Responsibility::find($id);
        $query['tasks'] = TaskResponsibility::where('resp_id',$id)->where('pro_id',$pro_id)->get();
        return view('admin/systems/need/task/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['need_pro'] = 1;
        $query['task'] = TaskResponsibility::find($id);
        $query['need'] = NeedProjects::find($query['task']->pro_id);        
        $query['resp'] = Responsibility::find($query['task']->pro_id);        
        if(!$query['need'])
            return redirect()->back();
        return view('admin/systems/need/task/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'task' => 'required',
            'performance' => 'required',
            'importance' => 'required',
            'difficult' => 'required',
        ]);
        
        $task = TaskResponsibility::find($id);
        $task->task = $request->task;
        $task->performance = $request->performance;
        $task->importance = $request->importance;
        $task->difficult = $request->difficult;
        $task->save();

        return redirect('task_need/'.$task->resp_id.'/'.$task->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = TaskResponsibility::find($id);
        if($task)
            $task->delete();
        return redirect('task_need/'.$task->resp_id.'/'.$task->pro_id)->with('success','تم الحذف بنجاح');
    }
}
