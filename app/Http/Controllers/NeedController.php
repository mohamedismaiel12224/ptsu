<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\NeedProjects;
use App\SystemUser;
use App\Justification;
class NeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        if(!SystemUser::where('system_id',2)->where('user_id',Auth::id())->where('status',1)->where('updated_at','>=',date('Y-m-d'))->first())
            return redirect()->back();
        $query['projects'] = NeedProjects::where('user_id',Auth::id())->get();
        return view('admin/systems/need/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/systems/need/add');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'responsible' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'fax' => 'required',
            'goals' => 'required',
            'logo' => 'required',
        ]);
        if ($request->hasFile('logo')) {
            $files = $request->file('logo');
            $extension = $files->getClientOriginalExtension();
            $picture = 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/home';
            $files->move($destinationPath, $picture);
        }
        $need = new NeedProjects;
        $need->name = $request->name;
        $need->location = $request->location;
        $need->responsible = $request->responsible;
        $need->email = $request->email;
        $need->phone = $request->phone;
        $need->fax = $request->fax;
        $need->goals = $request->goals;
        $need->logo = $picture;
        $need->user_id = Auth::id();
        $need->save();
        return redirect()->route('need.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['need'] = NeedProjects::find($id);
        if($query['need']->step_no == 1)
            return redirect()->route('responsiblity_need.show',$id);
        elseif($query['need']->step_no == 2)
            return redirect('tool_type/'.$id);
        elseif($query['need']->step_no == 3)
            return redirect()->route('tool_type.show',$id);
        elseif($query['need']->step_no == 4)
            return redirect()->route('questions_need.show',$id);
        elseif($query['need']->step_no == 5)
            return redirect('need/select_responsibility/'.$id);
        elseif($query['need']->step_no == 6)
            return redirect('questions_need_selected/'.$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['need'] = NeedProjects::where('user_id',Auth::id())->find($id);
        return view('admin/systems/need/add',$query);
        
    }

    public function studying_need_data($id)
    {
        $query['justification'] = Justification::where('pro_id',$id)->get();
        $query['need'] = NeedProjects::where('user_id',Auth::id())->find($id);
        return view('admin/systems/need/studying_data',$query);
        
    }
    public function update_studying_need_data(Request $request,$id)
    {
        $this->validate($request, [
            'employee_no' => 'numeric',
            'qualification' => 'numeric',
            'job_level' => 'numeric',
            'problem_nature' => 'numeric',
            'week_repeat' => 'numeric',
            'need_analysis' => 'numeric',
            'heuristic_sites' => 'numeric',
            'rel_pres_emp' => 'numeric',
            'rel_sup_emp' => 'numeric',
            'rel_emp_emp' => 'numeric',
            'bonus' => 'numeric',
            'motivation' => 'numeric',
            'develop' => 'numeric',
            'safe' => 'numeric',
            'performance' => 'numeric',
            'arrange' => 'numeric',
            'beefs_no' => 'numeric',
            'productivity' => 'numeric',
            
        ]);
        config(['app.locale' => 'ar']);
        $need = NeedProjects::find($id);
        if($request->employee_no)
            $need->employee_no = $request->employee_no;
        if($request->qualification)
            $need->qualification = $request->qualification;
        if($request->job_level)
            $need->job_level = $request->job_level;
        if($request->problem_nature)
            $need->problem_nature = $request->problem_nature;
        if($request->description)
            $need->description = $request->description;
        if($request->week_repeat)
            $need->week_repeat = $request->week_repeat;
        if($request->previouse_treatment)
            $need->previouse_treatment = $request->previouse_treatment;
        if($request->need_analysis)
            $need->need_analysis = $request->need_analysis;
        if($request->heuristic_sites)
            $need->heuristic_sites = $request->heuristic_sites;
        if($request->rel_pres_emp)
            $need->rel_pres_emp = $request->rel_pres_emp;
        if($request->rel_sup_emp)
            $need->rel_sup_emp = $request->rel_sup_emp;
        if($request->rel_emp_emp)
            $need->rel_emp_emp = $request->rel_emp_emp;
        if($request->bonus)
            $need->bonus = $request->bonus;
        if($request->motivation)
            $need->motivation = $request->motivation;
        if($request->develop)
            $need->develop = $request->develop;
        if($request->safe)
            $need->safe = $request->safe;
        if($request->performance)
            $need->performance = $request->performance;
        if($request->arrange)
            $need->arrange = $request->arrange;
        if($request->beefs_no)
            $need->beefs_no = $request->beefs_no;
        if($request->productivity)
            $need->productivity = $request->productivity;
        $need->save();
        Justification::where('pro_id',$id)->delete();

        if($request->justification[0]){
            foreach ($request->input('justification.*') AS $key => $value) {
                if($value){
                    $just = new Justification;
                    $just->justification = $value;
                    $just->pro_id = $id;
                    $just->adjective = $request->input('adjective.' . $key);
                    $just->save();
                }
            }
        }
        return redirect()->route('need.index')->with('success','تم تعديل بيانات  الدراسة بنجاح');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'responsible' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'fax' => 'required',
            'goals' => 'required',
        ]);
        $need = NeedProjects::find($id);
        if ($request->hasFile('logo')) {
            $files = $request->file('logo');
            $extension = $files->getClientOriginalExtension();
            $picture = 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/home';
            $files->move($destinationPath, $picture);
        
            $need->logo = $picture;
        }
        $need->name = $request->name;
        $need->location = $request->location;
        $need->responsible = $request->responsible;
        $need->email = $request->email;
        $need->phone = $request->phone;
        $need->fax = $request->fax;
        $need->goals = $request->goals;
        $need->save();
        return redirect()->route('need.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $need = NeedProjects::where('user_id',Auth::id())->find($id);
        if($need)
            $need->delete();
        return redirect()->route('need.index')->with('success','تم الحذف بنجاح');

    }
}
