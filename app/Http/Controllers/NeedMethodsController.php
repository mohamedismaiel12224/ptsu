<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NeedProjects;
use App\System;
use App\Responsibility;
use App\Questions;
use App\SystemAttribute;
use App\ArbitratorAnswer;
use App\Answers;
use App\TaskResponsibility;
use Auth;
class NeedMethodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function tool_type($id){
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($id);
        if(!$query['need'])
            return redirect()->back();
        return view('admin/systems/need/tool_type',$query);
    }
    public function update_tool_type(Request $request,$id){
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($id);
        if(!$query['need'])
            return redirect()->back();
        $query['need']->tool_type = $request->tool_type;
        if($query['need']->step_no==2)
            $query['need']->step_no=3;
        $query['need']->save();
        return redirect('questions_need/'.$id);
    }
    public function link_trainers($id){
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($id);
        $query['link']=url('need_analysis_system/login/'.$id);
        $query['word'] = 'الرابط للمتدربين';
        return view('admin/systems/need/link',$query);

    }
    public function report($id)
    {
        $query['need_pro']=1;
        $query['need'] = NeedProjects::find($id);
        $query['responsibility'] = Responsibility::where('pro_id',$id)->get();
        foreach ($query['responsibility'] as $key => $value) {
            $performance = TaskResponsibility::where('resp_id',$value->id)->sum('performance');
            $importance = TaskResponsibility::where('resp_id',$value->id)->sum('importance');
            $difficult = TaskResponsibility::where('resp_id',$value->id)->sum('difficult');
            $count = TaskResponsibility::where('resp_id',$value->id)->count();
            if($count==0)
                $value->avg = 0;
            else
                $value->avg = ($performance + $importance + $difficult) / $count;
            $value->tasks = TaskResponsibility::where('resp_id',$value->id)->get();
            
        }
        return view('admin/systems/need/report',$query);
    }
    public function affect_improve($id){
        $query['need_pro']=1;
        $query['need'] = NeedProjects::find($id);
        $query['affect'] = SystemAttribute::where('type_id',10)->get();
        $query['solution'] = SystemAttribute::where('type_id',11)->get();
        
        $query['responsibility'] = Responsibility::where('pro_id',$id)->get();
        foreach ($query['responsibility'] as $key => $resp) {
            $resp->avg_resp=0;
            $performance = TaskResponsibility::where('resp_id',$resp->id)->sum('performance');
            $importance = TaskResponsibility::where('resp_id',$resp->id)->sum('importance');
            $difficult = TaskResponsibility::where('resp_id',$resp->id)->sum('difficult');
            $count = TaskResponsibility::where('resp_id',$resp->id)->count();
            if($count==0)
                $resp->public_avg = 0;
            else
                $resp->public_avg = ($performance + $importance + $difficult) / $count;
            $resp->tasks = TaskResponsibility::where('resp_id',$resp->id)->where('pro_id',$id)->get();
            foreach ($resp->tasks as $key => $value) {
                $value->questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id)->where('place',3)->get();
                $value->avg_task=0;
                foreach ($value->questions as $ke => $val) {
                    $val->answers = Answers::where('q_id',$val->id)->get();

                    $val->count_user = ArbitratorAnswer::where('q_id',$val->id)->count();
                    
                    $val->count_1 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',1)->count();
                    $val->count_2 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',2)->count();
                    $val->count_3 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',3)->count();
                    $val->count_4 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',4)->count();
                    $val->count_5 = ArbitratorAnswer::where('q_id',$val->id)->where('degree',5)->count();
                    if($val->count_user!=0)
                        $val->avg_q = ($val->count_1+$val->count_2*2+$val->count_3*3+$val->count_4*4+$val->count_5*5)/$val->count_user;
                    $value->avg_task+=$val->avg_q;
                    
                }
                if(count($value->questions)==0)
                    unset($resp->tasks[$key]);
                if(count($value->questions)!=0)
                    $value->avg_task = $value->avg_task/ count($value->questions);
                $resp->avg_resp+=$value->avg_task;
            }
            $resp->avg_resp = $resp->avg_resp/count($resp->tasks);
            $resp->first = $resp->avg_resp*$resp->public_avg/5;
            if($resp->first >=0.6 && $resp->first <= 3.73)
                $resp->first_text ='عــــالية High';
            elseif($resp->first >=3.74 && $resp->first <= 6.87)
                $resp->first_text ='متوسطة Medium';
            elseif($resp->first>=6.88 && $resp->first <= 10)
                $resp->first_text ='متدنية Low';
        }
        // print_r($query);exit();/
        return view('admin/systems/need/affect_improve',$query);
    }

    public function update_affects_improve($id){
        $query['need_pro']=1;
        $query['need'] = NeedProjects::find($id);
        $query['affect'] = SystemAttribute::where('type_id',10)->get();
        $query['solution'] = SystemAttribute::where('type_id',11)->get();
        
        $query['responsibility'] = Responsibility::where('pro_id',$id)->get();
        foreach ($query['responsibility'] as $key => $resp) {
            $name = 'affect'.$resp->id;
            $name2 = 'solution'.$resp->id;
            if(!isset($_POST[$name])||$_POST[$name]==0||!isset($_POST[$name2])||$_POST[$name2]==0)
                return redirect()->back()->with('error','يوجد حقل فارغ');
            $resp = Responsibility::find($resp->id);
            $resp->affect = $_POST[$name];
            $resp->solution = $_POST[$name2];
            $resp->save();
        }
        if($query['need']->step_no==4)
            $query['need']->step_no=5;
        $query['need']->save();
        return redirect('need/select_responsibility/'.$id);
    }
    public function select_responsibility($id){
         $query['need_pro']=2;
        $query['need'] = NeedProjects::find($id);
        $query['responsibility'] = Responsibility::where('pro_id',$id)->get();
        return view('admin/systems/need/select_responsibility',$query);
        
    }
    public function selected_responsibility(Request $request,$id){
        $query['need'] = NeedProjects::find($id);
        if(!$query['need'])
            return redirect()->back();
        $query['need_pro'] = 2;
         $m = '';
        foreach ($request->resp as $key => $val) {
            $tasks = TaskResponsibility::where('resp_id',$val)->where('pro_id',$id)->get();
            foreach ($tasks as $key => $value) {
                $questions = Questions::where('questions.pro_id',$id)->where('task_id',$value->id)->where('place',3)->where('copied',0)->get();
                foreach ($questions as $ke => $val_q) {
                    $q=Questions::find($val_q->id);
                    $q->copied=1;
                    $q->save();

                    $que = new Questions;
                    $que->pro_id=$id;
                    $que->task_id =$q->task_id;
                    $que->question =$q->question;
                    $que->type =$q->type;
                    if($q->type_answer==null)
                        $que->type_answer =1;
                    else
                        $que->type_answer =$q->type_answer;
                    $que->place = 5;
                    $que->save();

                    $answers = Answers::where('q_id',$val_q->id)->get();
                    foreach ($answers as $key => $value) {
                        $a = Answers::find($value->id);
                        $ans = new Answers;
                        $ans->q_id=$que->id;
                        $ans->answer=$a->answer;
                        $ans->save();
                    }
                }
                if($m=='')
                    $m .= $val;
                else
                    $m .= ','.$val;
            }
        }
        
        $query['need']->resp_selected = $m;
        if($query['need']->step_no==5)
            $query['need']->step_no=6;
        $query['need']->save();
        return redirect('questions_need_selected/'.$id);
    }

    public function link_need_knowledge($id){
        $query['need_pro'] = 2;
        $query['need'] = NeedProjects::find($id);
        $query['word'] = 'الرابط للمتدربين';

        $query['link']=url('need_knowledge/login/'.$id);

        return view('admin/systems/need/link',$query);

    }
    public function link_need_skill($id){
        $query['need_pro'] = 2;
        $query['need'] = NeedProjects::find($id);
        $query['word'] = 'الرابط للمحكمين';
        
        $query['link']=url('need_skill/login/'.$id);
        return view('admin/systems/need/link',$query);

    }


    public function link_arbitrators($id){
        $query['need_pro'] = 2;
        $query['need'] = NeedProjects::find($id);
        $query['word'] = 'الرابط للمحكمين';
        
        $query['link']=url('need_arbitrators/login/'.$id);
        return view('admin/systems/need/link',$query);

    }
}
