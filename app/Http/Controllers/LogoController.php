<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Logos;
use Auth;
class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='banners'){
            if(Auth::user()->role==1){
                $query['partneers']=Logos::where('status',1)->get();
            }else{
                $id=Auth::id();
                $query['partneers']=Logos::where('status',1)->where('fellow_id',$id)->get();
            }
            
            $query['banners']=1;
        }else{
            $query['banners']=0;
            $query['partneers']=Logos::where('status',0)->get();
        }
        return view('admin/logo/show',$query);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='banners/create'){
            $query['banners']=1;
            if(Logos::where('status',1)->where('fellow_id',Auth::id())->first())
                return redirect()->back()->with('error','لا يمكن لك اكثر من بانر واحدد');
        }else{
            $query['banners']=0;
        }
        return view('admin/logo/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'file' => 'required',
        ]);
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $extension = $files->getClientOriginalExtension();
            $picture = 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/home';
            $files->move($destinationPath, $picture);
        }
        $logo = new Logos;
        $logo->link = '#';
        if($request->link)
            $logo->link = $request->link;
        $logo->logo = $picture;
        $link = Route::getCurrentRoute()->getPath();
        if($link=='banners'){
            if(Auth::user()->role==1){
                $logo->fellow_id=0;
            }else{
                $logo->fellow_id=Auth::id();
            }
            $logo->status =1;
            $logo->save();
            return redirect()->route('banners.index')->with('success','تم الحفظ بنجاح');
        }else{
             $logo->status =0;
            $logo->save();
            return redirect()->route('logos.index')->with('success','تم الحفظ بنجاح');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = Route::getCurrentRoute()->getPath();
        if($link=='logos/{logo}/edit'){
            $query['banners']=0;
        }else{
            $query['banners']=1;
        }
        $query['logo'] = Logos::find($id);
        return view('admin/logo/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $logo = Logos::find($id);
        $logo->link = '#';
        if($request->link)
            $logo->link = $request->link;
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $extension = $files->getClientOriginalExtension();
            $logo->logo =$picture= 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/home';
            $files->move($destinationPath, $picture);
        }
        $logo->save();
        $link = Route::getCurrentRoute()->getPath();
        if($link=='banners/{banner}'){
            return redirect()->route('banners.index')->with('success','تم التعديل بنجاح');
        }else{
            return redirect()->route('logos.index')->with('success','تم التعديل بنجاح');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $logos=Logos::find($id);
        $logos->delete();
        $link = Route::getCurrentRoute()->getPath();
        if($link=='banners/{banner}'){
            return redirect()->route('banners.index')->with('success','تم التعديل بنجاح');
        }else{
            return redirect()->route('logos.index')->with('success','تم التعديل بنجاح');
        }
    }
}
