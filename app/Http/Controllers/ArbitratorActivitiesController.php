<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domain;
use App\ActivitiesArbitration;
use App\MitArbitration;
use App\Arb_goal;
use App\Checkup;
use Auth;
class ArbitratorActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['arbitrator_mit']=1;
        $query['domains'] = Domain::where('status',3)->get();
        foreach ($query['domains'] as $key => $value) {
            $value->checkup = Checkup::where('domain_id',$value->id)->orderBy('domain_id','asc')->get();
        }
        $query['goals']=Arb_goal::where('pro_id',$id)->get();
        $query['mit'] = MitArbitration::find($id);
        return view('admin/systems/mit_arbitration/activities/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'mark' => 'required',
            'training_id' => 'required|numeric',
            'positive_time' => 'required|numeric',
            'repeat_time' => 'required|numeric',
            'negative_time' => 'required|numeric',
            'designer_time' => 'required|numeric|not_in:0',
            'superposing' => 'required',
        ]);

        $arr='';
        $domains = Domain::where('status',3)->get();
        foreach($domains as $domain){
            $name = 'domain'.$domain->id;
            if(!isset($_POST[$name])||!$_POST[$name])
                return redirect()->back()->with('error','يوجد حقل فارغ');
            if($arr=='')
                $arr .= $domain->id.':'.$_POST[$name];
            else
                $arr .= ','.$domain->id.':'.$_POST[$name];
        }
        
        $active = new ActivitiesArbitration;
        $active->mark = $request->mark;
        $active->training_id = $request->training_id;
        $active->positive_time = $request->positive_time;
        $active->repeat_time = $request->repeat_time;
        $active->negative_time = $request->negative_time;
        $active->designer_time = $request->designer_time;
        $active->superposing = $request->superposing;
        $active->pro_id = $request->pro_id;
        $active->details = $request->details;
        $active->user_id= Auth::id();
        $active->domain_check_arr= $arr;
        $active->save();
        return redirect('arbitrator/mit/activities/'.$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['arbitrator_mit']=1;
        $query['domains'] = Domain::where('status',3)->get();
        $query['mit'] = MitArbitration::find($id);
        $query['activities'] = ActivitiesArbitration::where('pro_id',$id)->where('user_id',Auth::id())->get();
        return view('admin/systems/mit_arbitration/activities/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['arbitrator_mit']=1;
        $query['domains'] = Domain::where('status',3)->get();
        $query['activities'] = ActivitiesArbitration::find($id);
        foreach ($query['domains'] as $key => $value) {
            $value->selected =0;
            $value->checkup = Checkup::where('domain_id',$value->id)->orderBy('domain_id','asc')->get();
            $domain_selected = explode(',', $query['activities']->domain_check_arr);
            foreach ($domain_selected as $select) {
                $arr = explode(':', $select);
                if(isset($arr[1])&&($arr[0]==$value->id))
                    $value->selected= $arr[1];
            }
        }
        $query['goals']=Arb_goal::where('pro_id',$query['activities']->pro_id)->get();
        $query['mit'] = MitArbitration::find($query['activities']->pro_id);
        return view('admin/systems/mit_arbitration/activities/add',$query);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'mark' => 'required',
            'training_id' => 'required|numeric',
            'positive_time' => 'required|numeric',
            'repeat_time' => 'required|numeric',
            'negative_time' => 'required|numeric',
            'designer_time' => 'required|numeric|not_in:0',
            'superposing' => 'required',
        ]);

        $arr='';
        $domains = Domain::where('status',3)->get();
        foreach($domains as $domain){
            $name = 'domain'.$domain->id;
            if(!isset($_POST[$name])||!$_POST[$name])
                return redirect()->back()->with('error','يوجد حقل فارغ');
            if($arr=='')
                $arr .= $domain->id.':'.$_POST[$name];
            else
                $arr .= ','.$domain->id.':'.$_POST[$name];
        }
        
        $active = ActivitiesArbitration::find($id);
        $active->mark = $request->mark;
        $active->training_id = $request->training_id;
        $active->positive_time = $request->positive_time;
        $active->repeat_time = $request->repeat_time;
        $active->designer_time = $request->designer_time;

        $active->negative_time = $request->negative_time;
        $active->superposing = $request->superposing;
        $active->details = $request->details;
        $active->domain_check_arr= $arr;
        $active->save();
        return redirect('arbitrator/mit/activities/'.$active->pro_id)->with('success','تم التعديل بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $active = ActivitiesArbitration::find($id);
        if($active)
            $active->delete();
        return redirect('arbitrator/mit/activities/'.$active->pro_id)->with('success','تم الحذف بنجاح');

    }
}
