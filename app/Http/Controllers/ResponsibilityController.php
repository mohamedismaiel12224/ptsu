<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NeedProjects;
use App\Responsibility;
use App\TaskResponsibility;
use Auth;
class ResponsibilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($id);
        if(!$query['need'])
            return redirect()->back();
        return view('admin/systems/need/resp/add',$query);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        $resp = new Responsibility;
        $resp->name = $request->name;
        $resp->pro_id = $request->pro_id;
        $resp->save();
        return redirect()->route('responsiblity_need.show',$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($id);
        if(!$query['need'])
            return redirect()->back();
        $query['resp'] = Responsibility::where('pro_id',$id)->get();
        foreach ($query['resp'] as $key => $value) {
            $performance = TaskResponsibility::where('resp_id',$value->id)->sum('performance');
            $importance = TaskResponsibility::where('resp_id',$value->id)->sum('importance');
            $difficult = TaskResponsibility::where('resp_id',$value->id)->sum('difficult');
            $count = TaskResponsibility::where('resp_id',$value->id)->count();
            if($count==0)
                $value->avg = 0;
            else
                $value->avg = ($performance + $importance + $difficult) / $count;
        }
        return view('admin/systems/need/resp/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['need_pro'] = 1;
        $query['resp'] = Responsibility::find($id);
        $query['need'] = NeedProjects::find($query['resp']->pro_id);
        if(!$query['need'])
            return redirect()->back();
        return view('admin/systems/need/resp/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
        ]);
        $resp = Responsibility::find($id);
        $resp->name = $request->name;
        $resp->save();
        return redirect()->route('responsiblity_need.show',$resp->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resp = Responsibility::find($id);
        if($resp)
            $resp->delete();
        TaskResponsibility::where('resp_id',$id)->delete();
        return redirect()->route('responsiblity_need.show',$resp->pro_id)->with('success','تم الحذف بنجاح');
    }
}
