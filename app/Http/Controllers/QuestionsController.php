<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\NeedProjects;
use App\TaskResponsibility;
use App\Questions;
use App\Answers;
use Auth;
class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($id);
        if(!$query['need'])
            return redirect()->back();
        $link = Route::getCurrentRoute()->getPath();
        if($link=='questions_need_selected/create/{id}'){
            $query['need_pro'] = 2;
            $arr = explode(',', $query['need']->resp_selected);
            $query['tasks'] = TaskResponsibility::select('*','task_responsibilities.id as id')->join('responsibilities','responsibilities.id','task_responsibilities.resp_id')->where('task_responsibilities.pro_id',$id)->whereIn('task_responsibilities.resp_id',$arr)->get();
            $query['url'] = 'questions_need_selected';

        }else{
            $query['tasks'] = TaskResponsibility::select('*','task_responsibilities.id as id')->join('responsibilities','responsibilities.id','task_responsibilities.resp_id')->where('task_responsibilities.pro_id',$id)->get();
            $query['url'] = 'questions_need';


        }
        return view('admin/systems/need/questions/add',$query);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'question' => 'required',
            'type' => 'required',
            'task_id' => 'required',
        ]);
        $q = new Questions;
        $q->question = $request->question;
        $q->task_id = $request->task_id;
        $q->type = $request->type;
        $q->type_answer = $request->type_answer;
        $q->pro_id = $request->pro_id;
        if($request->url=='questions_need_selected')
            $q->place = 5;
        $q->save();
        if($request->type_answer==2){
            foreach ($request->input('answer.*') as $key => $value) {
                if($value){
                    $ans = new Answers;
                    $ans->answer = $value;
                    $ans->q_id = $q->id;
                    $ans->save();
                }
            }
        }
        if($request->type_answer==3){
            foreach ($request->input('answers.*') as $key => $value) {
                if($value){
                    $ans = new Answers;
                    $ans->answer = $value;
                    $ans->q_id = $q->id;
                    $ans->save();
                }
            }
        }
        $query['need'] = NeedProjects::find($request->pro_id);
        if($query['need']->step_no==3)
            $query['need']->step_no=4;
        $query['need']->save();
        return redirect()->route($request->url.'.show',$request->pro_id)->with('success','تم الحفظ بنجاح');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['need_pro'] = 1;
        $query['need'] = NeedProjects::find($id);
        if(!$query['need'])
            return redirect()->back();
        $link = Route::getCurrentRoute()->getPath();
        if($link=='questions_need_selected/{questions_need_selected}'){
            $query['need_pro'] = 2;
            $arr = explode(',', $query['need']->resp_selected);
            $query['questions'] = Questions::select('*','questions.id as id')->join('task_responsibilities','task_responsibilities.id','questions.task_id')->where('questions.pro_id',$id)->whereIn('task_responsibilities.resp_id',$arr)->where('questions.place',5)->get();
            $query['url_create'] = 'questions_need_selected';

        }else{

            $query['questions'] = Questions::select('*','questions.id as id')->join('task_responsibilities','task_responsibilities.id','questions.task_id')->where('questions.pro_id',$id)->where('questions.place',3)->get();
            $query['url_create'] = 'questions_need';
        } 
        return view('admin/systems/need/questions/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['need_pro'] = 1;
        $query['que'] = Questions::find($id);
        $query['need'] = NeedProjects::find($query['que']->pro_id);
        if(!$query['need'])
            return redirect()->back();
        $link = Route::getCurrentRoute()->getPath();

        if($link=='questions_need_selected/{questions_need_selected}/edit'){
            $query['need_pro'] = 2;
            $arr = explode(',', $query['need']->resp_selected);
            $query['tasks'] = TaskResponsibility::select('*','task_responsibilities.id as id')->join('responsibilities','responsibilities.id','task_responsibilities.resp_id')->where('task_responsibilities.pro_id',$query['que']->pro_id)->whereIn('task_responsibilities.resp_id',$arr)->get();
            $query['url'] = 'questions_need_selected';

        }else{
            $query['tasks'] = TaskResponsibility::select('*','task_responsibilities.id as id')->join('responsibilities','responsibilities.id','task_responsibilities.resp_id')->where('task_responsibilities.pro_id',$query['que']->pro_id)->get();
            $query['url'] = 'questions_need';


        }
        $query['answers'] = Answers::where('q_id',$id)->get();
        return view('admin/systems/need/questions/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'question' => 'required',
            'type' => 'required',
            'task_id' => 'required',
        ]);
        $q = Questions::find($id);
        $q->question = $request->question;
        $q->task_id = $request->task_id;
        $q->type = $request->type;
        $q->type_answer = $request->type_answer;
        $q->save();
        Answers::where('q_id',$id)->delete();
        if($request->type_answer==2){
            foreach ($request->input('answer.*') as $key => $value) {
                if($value){
                    $ans = new Answers;
                    $ans->answer = $value;
                    $ans->q_id = $q->id;
                    $ans->save();
                }
            }
        }
        if($request->type_answer==3){
            foreach ($request->input('answers.*') as $key => $value) {
                if($value){
                    $ans = new Answers;
                    $ans->answer = $value;
                    $ans->q_id = $q->id;
                    $ans->save();
                }
            }
        }
        return redirect()->route($request->url.'.show',$q->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resp = Questions::find($id);
        if($resp)
            $resp->delete();
        return redirect()->back()->with('success','تم الحذف بنجاح');
    }
}
