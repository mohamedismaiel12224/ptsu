<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EfmProject;
use App\EfmNoteArbitration;
class EfmTrainingMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($id);
        return view('admin/systems/efm/training_material/show',$query);
    }
    public function training_material($id, $training_material)
    {
        $query['efm_pro'] = 1;
        $query['notes'] = EfmNoteArbitration::where('pro_id',$id)->get();
        $query['efm'] = EfmProject::find($id);
        $query['efm']->training_material = $training_material;
        if($training_material==2){
            $query['efm']->step_no = 3;
            $query['efm']->save();
            return redirect('efm/efm/'.$id);
        }
        $query['efm']->save();
        return view('admin/systems/efm/training_material/add',$query);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_training_material(Request $request, $id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($id);
        $query['efm']->general_evaluation = $request->general_evaluation;
        $query['efm']->step_no = 3;
        $query['efm']->save();
        EfmNoteArbitration::where('pro_id',$id)->delete();
        foreach ($request->notes as $key => $value) {
            if($value){
                $note = new EfmNoteArbitration();
                $note->notes = $value;
                $note->pro_id = $id;
                $note->save();
            }
        }
            return redirect('efm/efm/'.$id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
