<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EfmProject;
use Auth;
class EfmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['projects'] = EfmProject::where('user_id',Auth::id())->get();
        return view('admin/systems/efm/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/systems/efm/add');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'study_title' => 'required',
            'beneficiary' => 'required',
            'contact' => 'required',
            'number' => 'required',
            'country' => 'required',
            'city' => 'required',
            'email' => 'required',
        ]);
        $et = new EfmProject;
        $et->study_title = $request->study_title;
        $et->beneficiary = $request->beneficiary;
        $et->contact = $request->contact;
        $et->number = $request->number;
        $et->country = $request->country;
        $et->city = $request->city;
        $et->email = $request->email;
        $et->user_id = Auth::id();
        $et->save();
        return redirect()->route('efm.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $efm = EfmProject::find($id);
        if($efm->step_no == 1)
            return redirect('efm/gap/'.$id);
        elseif($efm->step_no == 2)
            return redirect('efm/training_material/'.$id);
        elseif($efm->step_no == 3)
            return redirect('efm/select_evaluate_fields/'.$id);
        elseif($efm->step_no == 4)
            return redirect('efm/analysis/'.$id);
        elseif($efm->step_no == 5)
            return redirect('per_ev/select_responsibility/'.$id);
        elseif($efm->step_no == 6)
            return redirect('per_ev/select_responsibility/'.$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['efm'] = EfmProject::where('user_id',Auth::id())->find($id);
        return view('admin/systems/efm/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'study_title' => 'required',
            'beneficiary' => 'required',
            'contact' => 'required',
            'number' => 'required',
            'country' => 'required',
            'city' => 'required',
            'email' => 'required',
        ]);
        $et = EfmProject::find($id);
        $et->study_title = $request->study_title;
        $et->beneficiary = $request->beneficiary;
        $et->contact = $request->contact;
        $et->number = $request->number;
        $et->country = $request->country;
        $et->city = $request->city;
        $et->email = $request->email;
        $et->update();
        return redirect()->route('efm.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ev = EfmProject::where('user_id',Auth::id())->find($id);
        if($ev)
            $ev->delete();
        return redirect()->route('efm.index')->with('success','تم الحذف بنجاح');
    }
}
