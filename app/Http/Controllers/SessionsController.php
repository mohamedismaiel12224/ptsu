<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branches;
use App\Sessions;
use App\SystemAttribute;
use App\Training_outputs;
use App\Collerations;
use App\MitProjects;
use Auth;

class SessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();

        $query['branches'] = Branches::where('pro_id',$id)->get();
        $query['sources'] = SystemAttribute::where('type_id',9)->get();
        return view('admin/systems/sessions/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'address' => 'required',
            'branch_arr' => 'required',
            'source_id' => 'required',
        ]);
        $pro_id = $request->pro_id;
        $branch_arr='';
        foreach ($request->branch_arr as $key => $value) {
            $tr=Branches::find($value);
            if($tr){
                $tr->in_session=1;
                $tr->save();
            }

            if($branch_arr=='')
                $branch_arr .= $value;
            else
                $branch_arr .= ','.$value;
        }

        $source_id='';
        foreach ($request->source_id as $key => $value) {
            if($source_id=='')
                $source_id .= $value;
            else
                $source_id .= ','.$value;
        }

        $sess = new Sessions;
        $sess->pro_id = $pro_id;
        $sess->branch_arr = $branch_arr;
        $sess->address = $request->address;
        $sess->source_id = $source_id;
        $sess->save();

        $all = Branches::where('pro_id',$pro_id)->get();
        $update=true;
        foreach ($all as $key => $value) {
            if($value->in_session==0)
                $update=false;
        }
        if($update){
            $mit = MitProjects::find($pro_id);
            if($mit->step_no==15)
                $mit->step_no=16;
            $mit->save();
        }
        return redirect('sessions/'.$pro_id)->with('success','تم الحفظ بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();

        $query['sessions'] = Sessions::where('pro_id',$id)->get();
        foreach ($query['sessions'] as $key => $value) {
            $arr=explode(',', $value->branch_arr);
            $branches = Branches::where('pro_id',$id)->whereIn('id',$arr)->get();
            $sum =0;
            foreach ($branches as $ke => $val) {
                $tr_arr = explode(',', $val->series_no);
                $sum += Training_outputs::whereIn('series_no',$tr_arr)->where('pro_id',$id)->sum('achievement');
                $sum += Collerations::whereIn('series_no',$tr_arr)->where('pro_id',$id)->sum('achievement');
                
            }
            $value->sum = $sum;
        }
        return view('admin/systems/sessions/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['sessions'] = Sessions::find($id);
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($query['sessions']->pro_id);
        if(!$query['mit'])
            return redirect()->back();

        $query['branches'] = Branches::where('pro_id',$query['sessions']->pro_id)->get();
        $query['sources'] = SystemAttribute::where('type_id',9)->get();
        $query['tools'] = SystemAttribute::where('type_id',4)->get();
        return view('admin/systems/sessions/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'address' => 'required',
            'branch_arr' => 'required',
            'source_id' => 'required',
        ]);
        $branch_arr='';
        $old_br = explode(',',Sessions::find($id)->branch_arr);

        Branches::whereIn('series_no',$old_br)->update(['in_session'=>0]);

        foreach ($request->branch_arr as $key => $value) {
            $tr=Branches::find($value);
            if($tr){
                $tr->in_session=1;
                $tr->save();
            }
            if($branch_arr=='')
                $branch_arr .= $value;
            else
                $branch_arr .= ','.$value;
        }
        $source_id='';
        foreach ($request->source_id as $key => $value) {
            if($source_id=='')
                $source_id .= $value;
            else
                $source_id .= ','.$value;
        }
        $sess = Sessions::find($id);
        $sess->branch_arr = $branch_arr;
        $sess->address = $request->address;
        $sess->source_id = $source_id;
        $sess->save();

        $all = Branches::where('pro_id',$sess->pro_id)->get();
        $update=true;
        foreach ($all as $key => $value) {
            if($value->in_session==0)
                $update=false;
        }
        if($update){
            $mit = MitProjects::find($pro_id);
            if($mit->step_no==15)
                $mit->step_no=16;
            $mit->save();
        }

        return redirect('sessions/'.$sess->pro_id)->with('success','تم التعديل بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sessions = Sessions::find($id);
        $sessions->delete();

        $check = Sessions::first();
        if(!$check){
            $mit = MitProjects::find($sessions->pro_id);
            if($mit->step_no==16)
                $mit->step_no=15;
            $mit->save();
            }
        return redirect()->route('sessions.show',$sessions->pro_id)->with('success','تم الحذف بنجاح');
    }
}
