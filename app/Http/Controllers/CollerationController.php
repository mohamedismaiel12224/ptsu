<?php

namespace App\Http\Controllers;
use App\MitProjects;
use App\Collerations;
use App\Training_outputs;
use App\SystemAttribute;
use Illuminate\Http\Request;
use Auth;

class CollerationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['training'] = Training_outputs::where('pro_id',$id)->get();
        $query['evaluate'] = SystemAttribute::where('type_id',1)->get();
        return view('admin/systems/colleration/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'training_outputs_id' => 'required',
            'detail' => 'required',
            'status' => 'required|in:1,0',
            'evaluate' => 'required|exists:system_attributes,id',

        ]);
        $to_id='';
        foreach ($request->training_outputs_id as $key => $value) {
            if($to_id=='')
                $to_id .= $value;
            else
                $to_id .= ','.$value;

        }
        $col = new Collerations;
        $col->detail = $request->detail;
        $col->pro_id = $request->pro_id;
        $col->evaluate_id = $request->evaluate;
        $col->status = $request->status;
        $col->training_outputs_id = $to_id;
        $col->save();
        $mit = MitProjects::find($request->pro_id);
        if($mit->step_no==2)
            $mit->step_no=3;
        $mit->save();
        return redirect()->route('colleration.show',$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['collerations'] = Collerations::where('pro_id',$id)->get();
        return view('admin/systems/colleration/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['colleration'] = Collerations::find($id);
        $query['mit_pro'] = 1;
        $query['mit'] = MitProjects::find($query['colleration']->pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $query['training'] = Training_outputs::where('pro_id',$query['mit']->id)->get();
        $query['evaluate'] = SystemAttribute::where('type_id',1)->get();

        return view('admin/systems/colleration/add',$query);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'training_outputs_id' => 'required',
            'detail' => 'required',
            'status' => 'required|in:1,0',
        ]);
        $to_id='';
        foreach ($request->training_outputs_id as $key => $value) {
            if($to_id=='')
                $to_id .= $value;
            else
                $to_id .= ','.$value;

        }
        $col = Collerations::find($id);
        $col->detail = $request->detail;
        $col->status = $request->status;
        $col->training_outputs_id = $to_id;
        $col->evaluate_id = $request->evaluate;
        $col->save();
        return redirect()->route('colleration.show',$col->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $col = Collerations::find($id);
        $col->delete();

        $coll = Collerations::first();
        if(!$coll){
            $mit = MitProjects::find($col->pro_id);
            if($mit->step_no==3)
                $mit->step_no=2;
            $mit->save();
            }
        return redirect()->route('colleration.show',$col->pro_id)->with('success','تم الحذف بنجاح');

    }
}
