<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Developer;
use Auth;
class DeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        $query['developers'] = Developer::all();
        return view('admin/developer/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/developer/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'cv' => 'required',
            'profilepic' => 'required',

        ]);
        if ($request->hasFile('profilepic')) {
            $files = $request->file('profilepic');
            $extension = $files->getClientOriginalExtension();
            $picture = 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/team';
            $files->move($destinationPath, $picture);
        }
        $developer = new Developer;
        $developer->facebook = '#';
        $developer->twitter = '#';
        $developer->linkedin = '#';
        $developer->google ='#';
        $developer->name = $request->name;
        $developer->cv = $request->cv;
        if($request->facebook)
            $developer->facebook = $request->facebook;
        if($request->twitter)
            $developer->twitter = $request->twitter;
        if($request->linkedin)
            $developer->linkedin = $request->linkedin;
        if($request->google)
            $developer->google = $request->google;
        $developer->profilepic = $picture;
        $developer->save();
        return redirect()->route('developers.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['developer'] = Developer::find($id);
        return view('developer_profile',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['developer'] = Developer::find($id);
        return view('admin/developer/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'cv' => 'required',
        ]);
        $developer = Developer::find($id);
        $developer->facebook = '#';
        $developer->twitter = '#';
        $developer->linkedin = '#';
        $developer->google ='#';
        $developer->name = $request->name;
        $developer->cv = $request->cv;
        if($request->facebook)
            $developer->facebook = $request->facebook;
        if($request->twitter)
            $developer->twitter = $request->twitter;
        if($request->linkedin)
            $developer->linkedin = $request->linkedin;
        if($request->google)
            $developer->google = $request->google;
        if ($request->hasFile('profilepic')) {
            $files = $request->file('profilepic');
            $extension = $files->getClientOriginalExtension();
            $developer->profilepic =$picture= 'ptsu' .time(). rand(111, 999) . '.' . $extension;
            $destinationPath = public_path() . '/assets/images/team';
            $files->move($destinationPath, $picture);
        }
        $developer->save();
        return redirect()->route('developers.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $developer=Developer::find($id);
        $developer->delete();
        return redirect()->route('developers.index')->with('success','تم الحذف بنجاح');
    }
}
