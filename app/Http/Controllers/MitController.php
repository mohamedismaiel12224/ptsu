<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MitProjects;
use App\SystemUser;
use Auth;
class MitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        if(!SystemUser::where('system_id',1)->where('user_id',Auth::id())->where('status',1)->where('updated_at','>=',date('Y-m-d'))->first())
            return redirect()->back();
        $query['projects'] = MitProjects::where('user_id',Auth::id())->get();
        return view('admin/systems/mit/show',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/systems/mit/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'nno' => 'required',
            'benefit_establishment' => 'required',
            'hours' => 'required|numeric',
            'days' => 'required|numeric',
        ]);
        
        $mit = new MitProjects;
        $mit->name = $request->name;
        $mit->nno = $request->nno;
        $mit->benefit_establishment = $request->benefit_establishment;
        $mit->hours = $request->hours;
        $mit->days = $request->days;
        $mit->user_id = Auth::id();
        $mit->save();
        return redirect()->route('mit.index')->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit'] = MitProjects::find($id);
        if($query['mit']->step_no == 1)
            return redirect()->route('training_outputs.show',$id);
        elseif($query['mit']->step_no == 2)
            return redirect()->route('colleration.show',$id);
        elseif($query['mit']->step_no == 3)
            return redirect('vertical/'.$id);
        elseif($query['mit']->step_no == 4)
            return redirect()->route('program_level.show',$id);
        elseif($query['mit']->step_no == 5)
            return redirect('select_level/'.$id);
        elseif($query['mit']->step_no == 6)
            return redirect('mit_type/'.$id);
        elseif($query['mit']->step_no == 7)
            return redirect('learning_level/'.$id);
        elseif($query['mit']->step_no == 8)
            return redirect('practice/'.$id);
        elseif($query['mit']->step_no == 9)
            return redirect('training_method/'.$id);
        elseif($query['mit']->step_no == 10)
            return redirect('learning_planner/'.$id);
        elseif($query['mit']->step_no == 11)
            return redirect('collect_content/'.$id);
        elseif($query['mit']->step_no == 12)
            return redirect('training_activities/'.$id);
        elseif($query['mit']->step_no == 13)
            return redirect('achievement/'.$id);
        elseif($query['mit']->step_no == 14)
            return redirect('branches/'.$id);
        elseif($query['mit']->step_no == 15)
            return redirect('sessions/'.$id);
        elseif($query['mit']->step_no == 16)
            return redirect('mit/report/'.$id);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['mit'] = MitProjects::find($id);
        return view('admin/systems/mit/add',$query);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'nno' => 'required',
            'benefit_establishment' => 'required',
            'hours' => 'required|numeric',
            'days' => 'required|numeric',
        ]);
        
        $mit = MitProjects::find($id);
        $mit->name = $request->name;
        $mit->nno = $request->nno;
        $mit->benefit_establishment = $request->benefit_establishment;
        $mit->hours = $request->hours;
        $mit->days = $request->days;
        $mit->save();
        return redirect()->route('mit.index')->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mit = MitProjects::find($id);
        if($mit)
            $mit->delete();
        return redirect()->route('mit.index')->with('success','تم الحذف بنجاح');

    }
}
