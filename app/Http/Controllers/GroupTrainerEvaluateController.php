<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EvaluateProject;
use App\GroupTrainerEvaluate;
use Auth;

class GroupTrainerEvaluateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['performance_evaluate_pro'] = 1;
        $query['ev'] = EvaluateProject::find($id);
        return view('admin/groups/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = new GroupTrainerEvaluate;
        $group->group = $request->group;
        $group->user_id = Auth::id();
        $group->pro_id = $request->pro_id;
        $group->save();
        return redirect('groups_trainers_evaluate/'.$request->pro_id)->with('success','تم الحفظ بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['performance_evaluate_pro'] = 1;
        $query['ev'] = EvaluateProject::find($id);
        $query['groups'] = GroupTrainerEvaluate::where('user_id',Auth::id())->where('pro_id',$id)->get();
        return view('admin/groups/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$pro_id)
    {
        $query['performance_evaluate_pro'] = 1;
        $query['ev'] = EvaluateProject::find($pro_id);
        $query['group'] = GroupTrainerEvaluate::findOrFail($id);
        return view('admin/groups/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = GroupTrainerEvaluate::findOrFail($id);
        $group->group = $request->group;
        $group->save();
        return redirect('groups_trainers_evaluate/'.$request->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = GroupTrainerEvaluate::findOrFail($id);
        $group->delete();
        return redirect()->back()->with('success','تم الحذف بنجاح');

        
    }
}
