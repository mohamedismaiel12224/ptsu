<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SystemUser;
use App\User;
use App\System;
use DB;
use Auth;

class SystemUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $sys = SystemUser::where('user_id',$id)->get();
        $arr = [];
        foreach ($sys as $key => $value) {
            $arr[] = $value->system_id;
        }
        $query['user_id'] = $id;
        $query['system'] =system::whereNotIn('id',$arr)->get();
        return view('admin/fellowship/add_system',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'system' => 'required',
            'updated_at' => 'required',
        ]);
        $sys = new SystemUser;
        $sys->user_id = $request->user_id;
        $sys->system_id = $request->system;
        $sys->updated_at = date('Y-m-d',strtotime($request->updated_at));
        $sys->save();
        return redirect('sysuser/'.$request->user_id)->with('success','تم اضافته فى النظام بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['user'] = User::find($id);
        $query['sys'] = SystemUser::join('systems','systems.id','system_users.system_id')->select('*','system_users.created_at as start_date','system_users.updated_at as end_date','system_users.id as id')->where('user_id',$id)->get();
        return view('admin/fellowship/show_system',$query);
    }

    public function permision($id,$permision){
        DB::table('system_users')->where('id',$id)->update(['status'=>$permision]);
        return redirect()->back()->with('success','تم التعديل بنجاح');
    }

    public function permision_arbitration($id,$permision){
        DB::table('system_users')->where('id',$id)->update(['status_arbitration'=>$permision]);
        return redirect()->back()->with('success','تم التعديل بنجاح');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['sysuser'] = SystemUser::find($id);
        $sys = SystemUser::where('user_id',$id)->get();
        $arr = [];
        foreach ($sys as $key => $value) {
            if($value->system_id!=$query['sysuser']->system_id)
                $arr[] = $value->system_id;
        }
        $query['user_id'] = $id;
        $query['system'] =system::whereNotIn('id',$arr)->get();
        return view('admin/fellowship/add_system',$query);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'system' => 'required',
            'updated_at' => 'required',
        ]);
        $sys = SystemUser::find($id);
        $sys->system_id = $request->system;
        $sys->updated_at = date('Y-m-d',strtotime($request->updated_at));
        $sys->save();
        return redirect('sysuser/'.$sys->user_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sys = SystemUser::find($id);
        $sys->delete($id);
        return redirect('sysuser/'.$sys->user_id)->with('success','تم الحذف بنجاح');

    }
}
