<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TrainerEtpm;
use Illuminate\Support\Facades\Route;
use App\GroupTrainerEtpm;
use App\EtpmProject;
use App\User;
use Auth;
class TrainerEtpmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['etpm_pro'] = 1;
        $query['groups'] = GroupTrainerEtpm::where('user_id',Auth::id())->where('pro_id',$id)->get();
        $query['ev'] = EtpmProject::find($id);
        return view('admin/systems/etpm/trainers/add',$query);
    }
    public function export($id,Request $request)
    {
        $query['etpm_pro'] = 1;
        $query['groups'] = GroupTrainerEtpm::where('user_id',Auth::id())->where('pro_id',$id)->get();
        $query['ev'] = EtpmProject::find($id);
        $query['projects'] = EtpmProject::where('user_id',Auth::id())->where('id','!=',$id)->get();
        $query['trainers'] = array();
        if(isset($request->project)){
            $query['trainers'] = TrainerEtpm::where('pro_id',$request->project)->get();
            return view('admin/systems/etpm/trainers/demo',$query);
            
        }

        return view('admin/systems/etpm/trainers/export',$query);
    }
    public function export_user($id,Request $request)
    {
        $query['groups'] = GroupTrainerEtpm::where('user_id',Auth::id())->where('pro_id',$id)->get();
        $query['etpm_pro'] = 1;
        $query['ev'] = EtpmProject::find($id);
        $query['trainers'] = User::where('role',2)->get();
        return view('admin/systems/etpm/trainers/export_user',$query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        $ev = new TrainerEtpm;
        $ev->name = $request->name;
        $ev->email = $request->email;
        $ev->phone = $request->phone;
        $ev->group_id = $request->group_id;
        $ev->pro_id = $request->pro_id;
        $ev->save();
        $ev = EtpmProject::find($request->pro_id);
        if($ev->step_no==1)
            $ev->step_no =2;
        $ev->save();
        return redirect('etpm/trainers/'.$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    public function store2(Request $request,$id){
        if(count($request->trainers)>0){
            foreach ($request->trainers as $key => $value) {
                $old = TrainerEtpm::find($value);
                $check= TrainerEtpm::where('email',$old->email)->where('phone',$old->phone)->where('pro_id',$id)->first();
                if(!$check){
                    $ev = new TrainerEtpm;
                    $ev->name = $old->name;
                    $ev->email = $old->email;
                    $ev->phone = $old->phone;
                    $ev->group_id = $request->group_id;
                    $ev->pro_id = $id;
                    $ev->save();
                }
            }
        }
        $ev = EtpmProject::find($request->pro_id);
        if($ev->step_no==1)
            $ev->step_no =2;
        $ev->save();
        return redirect('etpm/trainers/'.$id)->with('success','تم الحفظ بنجاح');
    }

    public function store3(Request $request,$id){
        if(count($request->trainers)>0){
            foreach ($request->trainers as $key => $value) {
                $old = User::find($value);
                $check= TrainerEtpm::where('email',$old->email)->where('phone',$old->phone)->where('pro_id',$id)->first();
                if(!$check){
                    $ev = new TrainerEtpm;
                    $ev->name = $old->name;
                    $ev->email = $old->email;
                    $ev->group_id = $request->group_id;
                    $ev->phone = $old->phone;
                    $ev->pro_id = $id;
                    $ev->save();
                }
            }
        }
        $ev = EtpmProject::find($request->pro_id);
        if($ev->step_no==1)
            $ev->step_no =2;
        $ev->save();
        return redirect('etpm/trainers/'.$id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $link = Route::getCurrentRoute()->getPath();
        if($link=='etpm/all_trainers/{id}')
            $query['all_trainers'] = 1;
        $query['etpm_pro'] = 1;
        $query['ev'] = EtpmProject::find($id);
        $query['trainers'] = TrainerEtpm::select('trainer_etpms.*','group_trainer_etpms.group')->leftJoin('group_trainer_etpms','group_trainer_etpms.id','trainer_etpms.group_id')->where('trainer_etpms.pro_id',$id)->get();
        return view('admin/systems/etpm/trainers/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['etpm_pro'] = 1;
        $query['trainer'] = TrainerEtpm::find($id);
        $query['groups'] = GroupTrainerEtpm::where('user_id',Auth::id())->where('pro_id',$query['trainer']->pro_id)->get();
        $query['ev'] = EtpmProject::find($query['trainer']->pro_id);
        return view('admin/systems/etpm/trainers/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        $ev = TrainerEtpm::find($id);
        $ev->name = $request->name;
        $ev->email = $request->email;
        $ev->group_id = $request->group_id;
        $ev->phone = $request->phone;
        $ev->save();
       
        return redirect('etpm/trainers/'.$ev->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ev = TrainerEtpm::find($id);
        if($ev)
            $ev->delete();
        return redirect()->back()->with('success','تم الحذف بنجاح');
    }
}
