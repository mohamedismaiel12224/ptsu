<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Arb_need;
use App\Arb_category;
use App\MitArbitration;
class NeedArbitrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category_id,$pro_id)
    {
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $query['category'] = Arb_category::find($category_id);
        return view('admin/systems/mit_arbitration/need/add',$query);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'need' => 'required',
        ]);
        
        $need = new Arb_need;
        $need->need = $request->need;
        $need->category_id = $request->category_id;
        $need->pro_id = $request->pro_id;
        $need->save();

        $mit = MitArbitration::find($request->pro_id);
        if($mit->step_no==1)
            $mit->step_no=2;
        $mit->save();
        return redirect('arb_need/'.$request->category_id.'/'.$request->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$pro_id)
    {
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $query['category'] = Arb_category::find($id);
        $query['needs'] = Arb_need::where('category_id',$id)->where('pro_id',$pro_id)->get();
        return view('admin/systems/mit_arbitration/need/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['need'] = Arb_need::find($id);
        $query['mit_arb_pro'] = 1;
        $query['mit'] = MitArbitration::find($query['need']->pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $query['category'] = Arb_category::find($query['need']->category_id);
        return view('admin/systems/mit_arbitration/need/add',$query);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'need' => 'required',
        ]);
        
        $need = Arb_need::find($id);
        $need->need = $request->need;
        $need->save();

        return redirect('arb_need/'.$need->category_id.'/'.$need->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $need = Arb_need::find($id);
        if($need)
            $need->delete();
        return redirect('arb_need/'.$need->category_id.'/'.$need->pro_id)->with('success','تم التعديل بنجاح');

    }
}
