<?php

namespace App\Http\Controllers;
use App\MitProjects;
use App\Activities;
use App\SystemAttribute;
use App\Training_outputs;
use App\Program_level;
use Illuminate\Http\Request;
use Auth;
use DB;
class TrainingActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            if($this->role!=3&&$this->role!=1)
                return redirect('login');
            return $next($request);
        });
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['category'] = SystemAttribute::where('type_id',6)->get();
        $query['aids'] = SystemAttribute::where('type_id',7)->get();
        $query['imp'] = SystemAttribute::where('type_id',8)->get();
        $query['training'] =DB::select("select learning_level, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/activities/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pro_id = $request->pro_id;
        $query['mit'] = MitProjects::find($pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'series_no' => 'required',
            'address' => 'required',
            'category_id' => 'required',
            'aids_id' => 'required',
            'imp_id' => 'required',
            'idea' => 'required',
            'status' => 'required',
        ]);
        $category_id='';
        foreach ($request->category_id as $key => $value) {
            if($category_id=='')
                $category_id .= $value;
            else
                $category_id .= ','.$value;
        }
        $aids_id='';
        foreach ($request->aids_id as $key => $value) {
            if($aids_id=='')
                $aids_id .= $value;
            else
                $aids_id .= ','.$value;
        }
        $imp_id='';
        foreach ($request->imp_id as $key => $value) {
            if($imp_id=='')
                $imp_id .= $value;
            else
                $imp_id .= ','.$value;
        }
        if($request->status==0){
            if(!$request->value)
                return redirect()->back()->with('error','يجب وضع قيمة');

            $time = $request->value;
        }else{
            if(!$request->o || !$request->m || !$request->p)
                return redirect()->back()->with('error','يجب وضع القيم صحيحة');
            $o=$request->o;
            $m=$request->m;
            $p=$request->p;

            $time = ($o + (4*$m) + $p)/6;
        }
        $active = new Activities;
        $active->pro_id = $pro_id;
        $active->series_no = $request->series_no;
        $active->address = $request->address;
        $active->idea = $request->idea;
        $active->category_id = $category_id;
        $active->aids_id = $aids_id;
        $active->imp_id = $imp_id;
        $active->time = $time;
        $active->save();
        
        $all = DB::select("select learning_level, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$pro_id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$pro_id." and series_no in( ".$program->series_no_arr." )  order by series_no");

        $update=true;
        foreach ($all as $key => $value) {
            if(!Activities::where('pro_id',$pro_id)->where('series_no',$value->series_no)->first())
                $update=false;
        }
        if($update){    
            $mit = MitProjects::find($pro_id);
            if($mit->step_no==12)
                $mit->step_no=13;
            $mit->save();
        }
        return redirect('training_activities/'.$pro_id)->with('success','تم الحفظ بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($id);
        if(!$query['mit'])
            return redirect()->back();
        $query['activities'] = Activities::where('pro_id',$id)->get();
        return view('admin/systems/activities/show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['activities'] = Activities::find($id);
        $query['mit_pro'] = 2;
        $query['mit'] = MitProjects::find($query['activities']->pro_id);
        if(!$query['mit'])
            return redirect()->back();
        $program = Program_level::find($query['mit']->program_level_id);
        if(!$program)
            return redirect()->back();
        $query['category'] = SystemAttribute::where('type_id',6)->get();
        $query['aids'] = SystemAttribute::where('type_id',7)->get();
        $query['imp'] = SystemAttribute::where('type_id',8)->get();
        $query['training'] =DB::select("select learning_level, series_no, id, detail as name, pro_id from collerations where evaluate_id in (1,2,3,4) and pro_id = ".$query['activities']->pro_id." and series_no in( ".$program->series_no_arr." ) union select learning_level, series_no, id, name, pro_id from training_outputs where evaluate_id in (1,2,3,4) and pro_id = ".$query['activities']->pro_id." and series_no in( ".$program->series_no_arr." )  order by series_no");
        return view('admin/systems/activities/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'series_no' => 'required',
            'address' => 'required',
            'category_id' => 'required',
            'aids_id' => 'required',
            'imp_id' => 'required',
            'idea' => 'required',
            'status' => 'required',
        ]);
        $category_id='';
        foreach ($request->category_id as $key => $value) {
            if($category_id=='')
                $category_id .= $value;
            else
                $category_id .= ','.$value;
        }
        $aids_id='';
        foreach ($request->aids_id as $key => $value) {
            if($aids_id=='')
                $aids_id .= $value;
            else
                $aids_id .= ','.$value;
        }
        $imp_id='';
        foreach ($request->imp_id as $key => $value) {
            if($imp_id=='')
                $imp_id .= $value;
            else
                $imp_id .= ','.$value;
        }
        if($request->status==0){
            if($request->value)
                $time = $request->value;
        }else{
            $o=$request->o;
            $m=$request->m;
            $p=$request->p;
            if($request->o)
                $time = ($o + (4*$m) + $p)/6;
        }
        $active = Activities::find($id);
        $active->series_no = $request->series_no;
        $active->address = $request->address;
        $active->idea = $request->idea;
        $active->category_id = $category_id;
        $active->aids_id = $aids_id;
        $active->imp_id = $imp_id;
        if(isset($time))
            $active->time = $time;
        $active->save();

        return redirect('training_activities/'.$active->pro_id)->with('success','تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $active = Activities::find($id);
        $active->delete();

        $check = Activities::where('pro_id',$active->pro_id)->where('series_no',$active->series_no)->first();
        if(!$check){
            $mit = MitProjects::find($active->pro_id);
            if($mit->step_no==13)
                $mit->step_no=12;
            $mit->save();
            }
        return redirect()->route('training_activities.show',$active->pro_id)->with('success','تم الحذف بنجاح');
    }
}
