<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EfmGap;
use App\EfmProject;
class EfmGapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pro_id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($pro_id);
        return view('admin/systems/efm/gaps/add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'responsibility' => 'required',
            'gaps' => 'required',
            'percentage' => 'required',
            'expert' => 'required',
            'justifications' => 'required',
        ]);
        $gap = EfmGap::create($request->all());
        return redirect('efm/gap/'.$gap->pro_id)->with('success','تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['efm_pro'] = 1;
        $query['efm'] = EfmProject::find($id);
        $query['gaps'] = EfmGap::where('pro_id',$id)->get();
        return view('admin/systems/efm/gaps/show',$query);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['efm_pro'] = 1;
        $query['gap'] = EfmGap::find($id);
        $query['efm'] = EfmProject::find($query['gap']->pro_id);
        return view('admin/systems/efm/gaps/add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'ar']);
        $this->validate($request, [
            'responsibility' => 'required',
            'gaps' => 'required',
            'percentage' => 'required',
            'expert' => 'required',
            'justifications' => 'required',
        ]);
        $gap = EfmGap::find($id);
        $gap->update($request->all());
        return redirect('efm/gap/'.$gap->pro_id)->with('success','تم التعديل بنجاح');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gap = EfmGap::find($id);
        $gap->delete();
        return redirect('efm/gap/'.$gap->pro_id)->with('success','تم الحذف بنجاح');
    }
}
