<?php 
use App\SystemAttribute;
use App\Training_outputs;
use App\Training_arbitration;
use App\Collerations;
use App\Branches;
use App\Sessions;
use App\Checkup;
use App\Arb_goal;
use App\Answers;
use App\Setting;
use App\ArbitratorAnswer;
function check_language(){
    if(Session::has('language')) {
        $s=session()->get('language');
        if($s=='ar') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function print_training_outputs($id){
    $arr = explode(',', $id);
    $out = '';
    foreach ($arr as $key => $value) {
        $row = Training_outputs::find($value);
        $out.= ' * '.$row->name;
    }
    return $out;
}
function print_training_outputs_series($series_no,$pro_id){
    $arr = explode(',', $series_no);
    $out = '';
    foreach ($arr as $key => $value) {
        $row = Training_outputs::where('pro_id',$pro_id)->where('series_no',$value)->first();
        if($row){
            $out.= ' * '.$row->name;
        }else{
            $row = Collerations::where('pro_id',$pro_id)->where('series_no',$value)->first();
            if($row)
                $out.= ' * '.$row->detail;
        }
    }
    return $out;
    
}

function print_branch($id){
    $arr = explode(',', $id);
    $out = '';
    foreach ($arr as $key => $value) {
        $row = Branches::find($value);
        $out.= ' * '.$row->address;
    }
    return $out;
}

function print_attributes($id){
    $arr = explode(',', $id);
    $out = '';
    foreach ($arr as $key => $value) {
        $row = SystemAttribute::find($value);
        $out.= ' - '.$row->name;
    }
    return $out;
}
function print_tools($id){
    $ses = Sessions::find($id);
    $arr = explode(',', $ses->branch_arr);
    $bran = Branches::whereIn('id',$arr)->get();
    $arra = array();
    foreach ($bran as $key => $value) {
        $all = DB::select("select  series_no, id, tool, pro_id from collerations where series_no in (".$value->series_no.") and pro_id = ".$ses->pro_id." union select series_no, id, tool, pro_id from training_outputs where pro_id = ".$ses->pro_id." and series_no in( ".$value->series_no." )  order by series_no");
        foreach ($all as $key => $val) {
            array_push($arra, $val->tool);
        }
    }
    $arra=array_unique($arra);
    $out = '';
    foreach ($arra as $key => $value) {
        $row = SystemAttribute::find($value);
        $out.= ' - '.$row->name;
    }
    return $out;
}
function print_time($id){
    $ses = Sessions::find($id);
    $arr = explode(',', $ses->branch_arr);
    $bran = Branches::whereIn('id',$arr)->get();
    $sum = 0;
    foreach ($bran as $key => $value) {
        $all = DB::select("select  series_no, id, achievement, pro_id from collerations where series_no in (".$value->series_no.") and pro_id = ".$ses->pro_id." union select series_no, id, achievement, pro_id from training_outputs where pro_id = ".$ses->pro_id." and series_no in( ".$value->series_no." )  order by series_no");
        foreach ($all as $key => $val) {
            $sum+=$val->achievement;
        }
    }
    
    return $sum;
}

function degree($num){
    if(!$num && $num!=0)
        return '';
    $setting = Setting::find(1);
    if(is_numeric($num)){
        if($setting->high_degree >= $num)
            return 'متفوق  Superior';
        elseif(($setting->low_degree > $num && $setting->high_degree < $num) )
            return 'متوسط  Medium';
        elseif( $setting->low_degree <= $num)
            return 'صعوبات  Difficulties';
    }else{
        if($num == 'd')
            return 'صعوبات  Difficulties';
        elseif($num == 'm')
            return 'متوسط  Medium';
        elseif($num == 's')
            return 'متفوق  Superior';
    }

}
function depth($learning_level, $type){
    if($type==0)
        return '-';
    $setting = Setting::find(1);
    if(is_numeric($learning_level)){
        if($setting->high_degree >= $learning_level){
            if($type==3||$type==6)
                return 'شمول Inclusiveness';
            else
                return 'عمق وشمول  Depth and Inclusiveness';
        }elseif(($setting->low_degree > $learning_level && $setting->high_degree < $learning_level)){
            return 'عمق Depth';
        }elseif( $setting->low_degree <= $learning_level)
            if($type==3||$type==6)
                return 'عمق وشمول  Depth and Inclusiveness';
            else
                return 'شمول Inclusiveness';
    }else{
        if($learning_level == 'd'){
            if($type==3||$type==6)
                return 'عمق وشمول  Depth and Inclusiveness';
            else
                return 'شمول Inclusiveness';
        }elseif($learning_level == 'm'){
            return 'عمق Depth';
        }elseif($learning_level == 's'){
            if($type==3||$type==6)
                return 'شمول Inclusiveness';
            else
                return 'عمق وشمول  Depth and Inclusiveness';
        }
    }

}
function volume($learning_level, $type,$status){
    
    $setting = Setting::find(1);
    if(is_numeric($learning_level)){
        if($setting->high_degree >= $learning_level){
            if($status==1)
                return false;
            else
                return true;
        }elseif(($setting->low_degree > $learning_level && $setting->high_degree < $learning_level)){
            if($status==1){
                return true;
            }else{
                if($type==3||$type==6){
                    return true;
                }else{
                    return false;
                }
            }
        }elseif( $setting->low_degree <= $learning_level)
            if($status==1)
                return true;
            else
                return false;
    }else{
        if($learning_level == 'd'){
            if($status==1)
                return true;
            else
                return false;
        }elseif($learning_level == 'm'){
            if($status==1){
                return true;
            }else{
                if($type==3||$type==6){
                    return true;
                }else{
                    return false;
                }
            }
        }elseif($learning_level == 's'){
            if($status==1)
                return false;
            else
                return true;
        }
    }
}
function print_goal($id)
{
   $arr = explode(',', $id);
    $out = '';
    foreach ($arr as $key => $value) {
        $row = Arb_goal::find($value);
        if($row)
            $out.= ' * '.$row->training_output;
        else
            return $id;
    }
    return $out;
}

function get_check($id,$arr){
    $arr=explode(',', $arr);
    foreach ($arr as $key => $value) {
        $out = explode(':', $value);
        if(isset($out[1])&&$out[0]==$id){
            return Checkup::find($out[1])->name;
        }
    }
}
function evaluate_time($pos,$neg,$rep,$des)
{
    $per=($pos+$neg+4*($rep))/6;
    $x=(($des-$per)/$des)*100;
    if($x<0){
        return 'زمن ناقص';
    }elseif($x>=0&&$x<=10){
        return 'زمن مثالي';
    }elseif($x>10&&$x<=20){
        return 'زمن طبيعي';
    }elseif($x>20&&$x<=30){
        return 'زمن فائض';
    }elseif($x>30){
        return 'زمن عبثي';
    }
}
function experience($tr_id,$user_id)
{
    $row = Arb_goal::find($tr_id);
    if($row)
        $learning_level=$row->learning_level;
    else
        return '';
    $tr=Training_arbitration::where('training_id',$tr_id)->where('user_id',$user_id)->first();
    if($tr){
        $arr=explode(',', $tr->domain_check_arr);
        $arr2=explode(':', $arr[0]);
        if(isset($arr2[1]))
            $type=$arr2[1];
        else
            return '';
            
    }else
        return '';
    $out='';
    if(volume($learning_level,$type,1)) 
        $out.=' إلزامية Core ';
    if(volume($learning_level,$type,2)) 
        $out.=' داعمة Non-core ';
    return $out;
}
function get_answers($id)
{
    $arr = Answers::where('q_id',$id)->get();
    $out = '';
    foreach ($arr as $key => $value) {
        $out.= ' * '.$value->answer;
    }
    return $out;
}

function all_checked($id){
    $check = ArbitratorAnswer::where('pro_id',$id)->where('degree',0)->first();
    if($check)
        return false;
    else
        return true;
    return false;
}