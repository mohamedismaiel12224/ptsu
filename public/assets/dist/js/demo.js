/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */
// $(document).delegate('.course','change',function(){
//   var course_id=$(this).val();
//   var form_data = new FormData();
//         form_data.append('course_id', course_id);
//         form_data.append('_token', _token);
//         $.ajax({
//             url: base_url + '/get_trainer',
//             cache: false,
//             dataType: 'text',
//             cache: false,
//             contentType: false,
//             processData: false,
//             data: form_data,
//             type: 'post',
//             success: function (res) {
//                 $('.form_trainer').html(res);
//             }
//         });
// });
 $(document).delegate('#add_new_field', 'click', function (e) {
    e.preventDefault();
    $(".add_here").append(`
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label center">الأحتياجات التدربية Training Needs</label>
            <div class="col-sm-10">
                <input type="text" name="need[]" class="form-control" >
            </div>
            <label for="name" class="col-sm-1">
              <a href="#" id="remove_field"><i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i></a></label></div>
    `);
});
 $(document).delegate('#add_new_note', 'click', function (e) {
    e.preventDefault();
    $(".add_here").append(`
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label center">ملحوظات على الحقيبة Notes</label>
            <div class="col-sm-9">
                <input type="text" name="notes[]" class="form-control" >
            </div>
            <label for="name" class="col-sm-1">
              <a href="#" id="remove_field"><i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i></a></label></div>
    `);
});
$(document).delegate('#add_checkup_field', 'click', function (e) {
    e.preventDefault();
    $(".add_checkup_here").append(`
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label center">الفحوصات</label>
            <div class="col-sm-10">
                <input type="text" name="checkup[]" class="form-control" >
            </div>
            <label for="name" class="col-sm-1">
              <a href="#" id="remove_field"><i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i></a>
            </label>
        </div>
    `);
});
$(document).delegate('#add_indictor_field', 'click', function (e) {
    e.preventDefault();
    $(".add_indictor_here").append(`
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label center">المؤشرات</label>
            <div class="col-sm-10">
                <input type="text" name="checkup[]" class="form-control" >
            </div>
            <label for="name" class="col-sm-1">
              <a href="#" id="remove_field"><i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i></a>
            </label>
        </div>
    `);
});
$(document).delegate('#add_procedure_field', 'click', function (e) {
    e.preventDefault();
    var x= "";
    if(type==1){
      x='اجراءات';
    }else if(type==2){
      x = 'محكات';
    }
    $(".add_procedure_here").append(`
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label center">${x}</label>
            <div class="col-sm-10">
                <input type="text" name="procedure[]" class="form-control" >
            </div>
            <label for="name" class="col-sm-1">
              <a href="#" id="remove_field"><i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i></a>
            </label>
        </div>
    `);
});
$(document).delegate('#add_answer_field', 'click', function (e) {
    e.preventDefault();
    $(".add_answer_here").append(`
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label center">الاجابات</label>
            <div class="col-sm-10">
                <input type="text" name="answer[]" class="form-control" >
            </div>
            <label for="name" class="col-sm-1">
              <a href="#" id="remove_field"><i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i></a>
            </label>
        </div>
    `);
});

$(document).delegate('#add_justification_field', 'click', function (e) {
    e.preventDefault();
    $(".add_justification_here").append(`
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label center">الصفة</label>
          <div class="col-sm-4">
            <select class="form-control select2" data-placeholder="اختر الصفة" style="width: 100%;" name="adjective[]">
                <option disabled="" selected="" value="0" >اختر الصفة</option>
                <option  value="1" >مالك</option>
                <option  value="2" >مدير مباشر </option>
                <option  value="3" >مشرف</option>
                <option  value="4" >موظف</option>
                <option  value="5" >خبير خارجي</option>
            </select>
          </div>
          <label for="inputEmail3" class="col-sm-1 control-label center">التبرير</label>

          <div class="col-sm-4">
            <input type="text" name="justification[]" class="form-control" placeholder="" >
          </div>
          <label for="name" class="col-sm-1">
              <a href="#" id="remove_field"><i class="fa fa-remove fa-3x" style="color:red;" aria-hidden="true"></i></a>
            </label>
        </div>
    `);
    $('.select2').select2();
});
$(document).delegate('.positive_time', 'keyup', function (e) {
  var pos=$(this).val()|0;
  var neg=$('.negative_time').val()|0;
  var rep=$('.repeat_time').val()|0;
  var des=$('.designer_time').val()|0;
  perfect_time(pos,neg,rep);
  standard(pos,neg);
  evaluate_time(pos,neg,rep,des);
});
$(document).delegate('.negative_time', 'keyup', function (e) {
  var pos=$('.positive_time').val()|0;
  var neg=$(this).val()|0;
  var rep=$('.repeat_time').val()|0;
  var des=$('.designer_time').val()|0;
  perfect_time(pos,neg,rep);
  standard(pos,neg);
  evaluate_time(pos,neg,rep,des);
  
});
$(document).delegate('.repeat_time', 'keyup', function (e) {
  var pos=$('.positive_time').val()|0;
  var neg=$('.negative_time').val()|0;
  var rep=$(this).val()|0;
  var des=$('.designer_time').val()|0;
  perfect_time(pos,neg,rep);
  standard(pos,neg);
  evaluate_time(pos,neg,rep,des);
    
});
$(document).delegate('.designer_time', 'keyup', function (e) {
  var pos=$('.positive_time').val()|0;
  var neg=$('.negative_time').val()|0;
  var rep=$('.repeat_time').val()|0;
  var des=$(this).val()|0;
  perfect_time(pos,neg,rep);
  standard(pos,neg);
  evaluate_time(pos,neg,rep,des);
});
function perfect_time(pos,neg,rep) {
  var perf= ((parseFloat(pos)+parseFloat(neg)+4*parseFloat(rep))/6).toFixed(2);
  $('.perfect_time').val(perf);
}
function standard(pos,neg) {
  var stand= ((parseFloat(neg)-parseFloat(pos))/6).toFixed(2);
  $('.standard').val(stand);
}
function evaluate_time(pos,neg,rep,des) {
  if(des==0){
    $('.evaluate_time').val('');
  }
  var per= ((parseFloat(pos)+parseFloat(neg)+4*parseFloat(rep))/6);
  var x=((parseFloat(des)-per)/des)*100;

  var msg ='';
    if(x<0){
        msg= 'زمن ناقص';
    }else if(x>=0&&x<=10){
        msg= 'زمن مثالي';
    }else if(x>10&&x<=20){
        msg= 'زمن طبيعي';
    }else if(x>20&&x<=30){
        msg= 'زمن فائض';
    }else if(x>30){
        msg= 'زمن عبثي';
    }
  $('.evaluate_time').val(msg);

}
$(document).delegate('#remove_field', 'click', function (e) {
    e.preventDefault();
    $(this).closest(".form-group").remove();
});
  $(document).delegate('#value_method', 'click', function() {
      $('#value_div').show(1000);
      $('#select_div').hide(1000);
      $('#math_div').hide(1000);
  });
  $(document).delegate('#select_method', 'click', function() {
      $('#select_div').show(1000);
      $('#value_div').hide(1000);
      $('#math_div').hide(1000);
  });
  $(document).delegate('#math_method', 'click', function() {
      $('#math_div').show(1000);
      $('#select_div').hide(1000);
      $('#value_div').hide(1000);
  });
  $(document).delegate('.training_output_content','change',function () {
    var tr_id=$(this).val();
    $.ajax({
          type: "POST",
          url: base_url+'/experience',
          data: { tr_id: tr_id, _token: _token},
          cache: false,
          success: function (html) {
              $('.experience_input').val(html);
          }
      });
  });
  $(document).delegate('.select_project','change',function () {
    var tr_id=$(this).val();
    var pathname = window.location.pathname;
    var arr = pathname.split('ptsu/public');
    var urll = arr[1];
    $.ajax({
          type: "POST",
          url: base_url+urll,
          data: { project: tr_id, _token: _token},
          cache: false,
          success: function (html) {
              $('.select_trainers').html(html);
          }
      });
  });
$(function () {
  'use strict'

  /**
   * Get access to plugins
   */

  $('[data-toggle="control-sidebar"]').controlSidebar()
  $('[data-toggle="push-menu"]').pushMenu()

  var $pushMenu       = $('[data-toggle="push-menu"]').data('lte.pushmenu')
  var $controlSidebar = $('[data-toggle="control-sidebar"]').data('lte.controlsidebar')
  var $layout         = $('body').data('lte.layout')

  /**
   * List of all the available skins
   *
   * @type Array
   */
  var mySkins = [
    'skin-blue',
    'skin-black',
    'skin-red',
    'skin-yellow',
    'skin-purple',
    'skin-green',
    'skin-blue-light',
    'skin-black-light',
    'skin-red-light',
    'skin-yellow-light',
    'skin-purple-light',
    'skin-green-light'
  ]

  /**
   * Get a prestored setting
   *
   * @param String name Name of of the setting
   * @returns String The value of the setting | null
   */
  function get(name) {
    if (typeof (Storage) !== 'undefined') {
      return localStorage.getItem(name)
    } else {
      window.alert('Please use a modern browser to properly view this template!')
    }
  }

  /**
   * Store a new settings in the browser
   *
   * @param String name Name of the setting
   * @param String val Value of the setting
   * @returns void
   */
  function store(name, val) {
    if (typeof (Storage) !== 'undefined') {
      localStorage.setItem(name, val)
    } else {
      window.alert('Please use a modern browser to properly view this template!')
    }
  }

  /**
   * Toggles layout classes
   *
   * @param String cls the layout class to toggle
   * @returns void
   */
  function changeLayout(cls) {
    $('body').toggleClass(cls)
    $layout.fixSidebar()
    if ($('body').hasClass('fixed') && cls == 'fixed') {
      $pushMenu.expandOnHover()
      $layout.activate()
    }
    $controlSidebar.fix()
  }

  /**
   * Replaces the old skin with the new skin
   * @param String cls the new skin class
   * @returns Boolean false to prevent link's default action
   */
  function changeSkin(cls) {
    $.each(mySkins, function (i) {
      $('body').removeClass(mySkins[i])
    })

    $('body').addClass(cls)
    store('skin', cls)
    return false
  }

  /**
   * Retrieve default settings and apply them to the template
   *
   * @returns void
   */
  function setup() {
    var tmp = get('skin')
    if (tmp && $.inArray(tmp, mySkins))
      changeSkin(tmp)

    // Add the change skin listener
    $('[data-skin]').on('click', function (e) {
      if ($(this).hasClass('knob'))
        return
      e.preventDefault()
      changeSkin($(this).data('skin'))
    })

    // Add the layout manager
    $('[data-layout]').on('click', function () {
      changeLayout($(this).data('layout'))
    })

    $('[data-controlsidebar]').on('click', function () {
      changeLayout($(this).data('controlsidebar'))
      var slide = !$controlSidebar.options.slide

      $controlSidebar.options.slide = slide
      if (!slide)
        $('.control-sidebar').removeClass('control-sidebar-open')
    })

    $('[data-sidebarskin="toggle"]').on('click', function () {
      var $sidebar = $('.control-sidebar')
      if ($sidebar.hasClass('control-sidebar-dark')) {
        $sidebar.removeClass('control-sidebar-dark')
        $sidebar.addClass('control-sidebar-light')
      } else {
        $sidebar.removeClass('control-sidebar-light')
        $sidebar.addClass('control-sidebar-dark')
      }
    })

    $('[data-enable="expandOnHover"]').on('click', function () {
      $(this).attr('disabled', true)
      $pushMenu.expandOnHover()
      if (!$('body').hasClass('sidebar-collapse'))
        $('[data-layout="sidebar-collapse"]').click()
    })

    //  Reset options
    if ($('body').hasClass('fixed')) {
      $('[data-layout="fixed"]').attr('checked', 'checked')
    }
    if ($('body').hasClass('layout-boxed')) {
      $('[data-layout="layout-boxed"]').attr('checked', 'checked')
    }
    if ($('body').hasClass('sidebar-collapse')) {
      $('[data-layout="sidebar-collapse"]').attr('checked', 'checked')
    }

  }

  // Create the new tab
  var $tabPane = $('<div />', {
    'id'   : 'control-sidebar-theme-demo-options-tab',
    'class': 'tab-pane active'
  })

  // Create the tab button
  var $tabButton = $('<li />', { 'class': 'active' })
    .html('<a href=\'#control-sidebar-theme-demo-options-tab\' data-toggle=\'tab\'>'
      + '<i class="fa fa-wrench"></i>'
      + '</a>')

  // Add the tab button to the right sidebar tabs
  $('[href="#control-sidebar-home-tab"]')
    .parent()
    .before($tabButton)

  // Create the menu
  var $demoSettings = $('<div />')

  // Layout options
  $demoSettings.append(
    '<h4 class="control-sidebar-heading">'
    + 'Layout Options'
    + '</h4>'
    
  )
  var $skinsList = $('<ul />', { 'class': 'list-unstyled clearfix' })

  // Dark sidebar skins
  var $skinBlue =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Blue</p>')
  $skinsList.append($skinBlue)
  var $skinBlack =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Black</p>')
  $skinsList.append($skinBlack)
  var $skinPurple =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Purple</p>')
  $skinsList.append($skinPurple)
  var $skinGreen =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Green</p>')
  $skinsList.append($skinGreen)
  var $skinRed =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Red</p>')
  $skinsList.append($skinRed)
  var $skinYellow =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Yellow</p>')
  $skinsList.append($skinYellow)

  // Light sidebar skins
  var $skinBlueLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Blue Light</p>')
  $skinsList.append($skinBlueLight)
  var $skinBlackLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-black-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Black Light</p>')
  $skinsList.append($skinBlackLight)
  var $skinPurpleLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Purple Light</p>')
  $skinsList.append($skinPurpleLight)
  var $skinGreenLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Green Light</p>')
  $skinsList.append($skinGreenLight)
  var $skinRedLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Red Light</p>')
  $skinsList.append($skinRedLight)
  var $skinYellowLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Yellow Light</p>')
  $skinsList.append($skinYellowLight)

  $demoSettings.append('<h4 class="control-sidebar-heading">Skins</h4>')
  $demoSettings.append($skinsList)

  $tabPane.append($demoSettings)
  $('#control-sidebar-home-tab').after($tabPane)

  setup()

  $('[data-toggle="tooltip"]').tooltip()
})
