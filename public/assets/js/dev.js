$(document).delegate(".inputSearch", "keypress", function (e) {
    var key = e.which;
    if (key == 13) {
		var x = $(this).val();
		search(x);
    }
});

$(document).delegate('.btnSearch', 'click', function() {
	var x=$('.inputSearch').val();
	search(x);
});

function search(x){
	$.ajax({
        type: "POST",
        url: base_url + '/search',
        data: {_token: _token, search: x},
        cache: false,
        success: function (html) {
            $('.fixed_height .blog_post').html(html);
        }
    });
}

$(document).delegate('.my_btn', 'click', function() {
    var poll_id = $(this).parent().attr('data-poll_id');
    var suggest = $(this).parent().find('input[name="s['+poll_id+']"]:checked').val();
    $.ajax({
        type: "POST",
        url: base_url + '/poll_action',
        data: {_token: _token, poll_id: poll_id,suggest:suggest},
        cache: false,
        success: function (res) {
            // var re =jQuery.parseJSON(res);
            // if(!re.error)
                $('.panel'+poll_id).html(res);
        }
    });
});

function certPrint(){
    var html = $('.invoice').html();
    $('.wrap').html(html);
    window.print();
    location.reload();
}