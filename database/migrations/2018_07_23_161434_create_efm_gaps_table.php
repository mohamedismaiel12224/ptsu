<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEfmGapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('efm_gaps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('responsibility');
            $table->string('gaps');
            $table->integer('percentage');
            $table->string('expert');
            $table->string('justifications');
            $table->string('field');
            $table->string('note');
            $table->integer('assess');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('efm_gaps');
    }
}
