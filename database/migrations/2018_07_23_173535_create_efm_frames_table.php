<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEfmFramesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('efm_frames', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pro_id');
            $table->integer('type_id');
            $table->string('justifications');
            $table->string('goal')->nullable();
            $table->integer('diagnosis_values')->nullable();
            $table->integer('target')->nullable();
            $table->string('data_source')->nullable();
            $table->string('period_application')->nullable();
            $table->string('responsible')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('efm_frames');
    }
}
