<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('alter table settings add high_degree double');
        DB::statement('alter table settings add low_degree double');
         DB::statement("INSERT INTO `sysetem_attributes` (`id`, `name` , `type_id`)  VALUES
        (1, 'الأتجاهات',1),
        (2, 'الحصيلة',1),
        (3, 'الممارسة',1),
        (4, 'المعنوية',1);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
