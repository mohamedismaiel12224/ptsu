<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToolEfmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tool_efms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_tool')->comment('1:Questionnaires , 2:Control and experimental , 3:display only , 4: Peer assess,5:Tracking reports , 6:Statistics centers , 7:Statistically comparison , 8:Indicator');
            $table->string('tool');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tool_efms');
    }
}
