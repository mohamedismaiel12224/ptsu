<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessModelAsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_model_as', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pro_id');
            $table->string('step');
            $table->string('execution_place');
            $table->string('time');
            $table->string('responsible');
            $table->string('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_model_as');
    }
}
