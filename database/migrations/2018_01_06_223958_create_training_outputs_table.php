<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_outputs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pro_id');
            $table->string('name');
            $table->integer('evaluate_id');
            $table->integer('series_no')->default(0);
            $table->integer('type')->nullable();
            $table->string('learning_level')->nullable();
            $table->integer('investigation')->nullable();
            $table->integer('training_method_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_outputs');
    }
}
