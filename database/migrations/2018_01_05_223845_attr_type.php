<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AttrType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        DB::statement("INSERT INTO `attributes_type` (`id`, `name`)  VALUES
        (1, 'Evaluation Degree'),
        (2, 'Training methods'),
        (3, 'Evalution Time'),
        (4, 'Tools'),
        (5, 'Responsibility'),
        (6, 'Categorty'),
        (7, 'Learning aids'),
        (8, 'Implementation mechanism'),
        (9, 'Content Sources');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
