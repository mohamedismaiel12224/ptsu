<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Collerations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collerations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pro_id');
            $table->text('detail');
            $table->integer('status')->comment('0:After  1:before');
            $table->string('training_outputs_id')->comment('tr1, tr2, ...');
            $table->integer('series_no')->default(0);
            $table->integer('type')->nullable();
            $table->string('learning_level')->nullable();
            $table->integer('investigation')->nullable();
            $table->integer('training_method_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collerations');
    }
}
