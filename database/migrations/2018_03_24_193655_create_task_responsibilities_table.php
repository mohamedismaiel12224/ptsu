<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskResponsibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_responsibilities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pro_id');
            $table->integer('resp_id');
            $table->string('task');
            $table->integer('performance');
            $table->integer('importance');
            $table->integer('difficult');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_responsibilities');
    }
}
