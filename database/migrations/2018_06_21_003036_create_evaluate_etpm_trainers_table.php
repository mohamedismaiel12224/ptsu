<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluateEtpmTrainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluate_etpm_trainers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('procedure_id');
            $table->integer('trainer_id')->unsigned()->nullable();
            $table->foreign('trainer_id')->references('id')
            ->on('trainer_evaluates')->onDelete('cascade');
            $table->integer('evaluator_id');
            $table->integer('pro_id')->unsigned()->nullable();
            $table->foreign('pro_id')->references('id')
            ->on('etpm_projects')->onDelete('cascade');
            $table->integer('evaluate');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluate_etpm_trainers');
    }
}
