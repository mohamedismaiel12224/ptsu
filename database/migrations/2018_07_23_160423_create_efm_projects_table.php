<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEfmProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('efm_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('study_title');
            $table->string('beneficiary');
            $table->string('contact');
            $table->string('number');
            $table->string('country');
            $table->string('city');
            $table->string('email');
            $table->integer('user_id');
            $table->integer('step_no')->default(1);
            $table->integer('training_material')->comment('1:Arbitration 2:Un Arbitration')->nullable();
            $table->integer('application_plan')->comment('1:A 2:B')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('efm_projects');
    }
}
