<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeedProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('need_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('email');
            $table->string('location');
            $table->string('responsible');
            $table->string('phone');
            $table->string('fax');
            $table->string('goals');
            $table->string('logo');

            $table->string('about')->nullable();
            $table->integer('employee_no')->nullable();
            $table->integer('qualification')->nullable();
            $table->integer('job_level')->nullable();
            $table->integer('problem_nature')->nullable();
            $table->text('description')->nullable();
            $table->integer('week_repeat')->nullable();
            $table->string('previouse_treatment')->nullable();
            $table->integer('need_analysis')->nullable();
            $table->integer('heuristic_sites')->nullable();
            $table->integer('rel_pres_emp')->nullable();
            $table->integer('rel_sup_emp')->nullable();
            $table->integer('rel_emp_emp')->nullable();
            $table->integer('bonus')->nullable();
            $table->integer('motivation')->nullable();
            $table->integer('develop')->nullable();
            $table->integer('safe')->nullable();
            $table->integer('performance')->nullable();
            $table->integer('arrange')->nullable();
            $table->integer('beefs_no')->nullable();
            $table->integer('productivity')->nullable();
            $table->integer('step_no')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('need_projects');
    }
}
