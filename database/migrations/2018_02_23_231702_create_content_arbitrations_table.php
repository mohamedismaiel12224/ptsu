<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentArbitrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_arbitrations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('pro_id');
            $table->string('address');
            $table->text('domain_check_arr');
            $table->integer('training_id');
            $table->integer('session');
            $table->string('time');
            $table->string('details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_arbitrations');
    }
}
