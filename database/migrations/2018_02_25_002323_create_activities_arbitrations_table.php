<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesArbitrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities_arbitrations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('pro_id');
            $table->string('mark');
            $table->text('domain_check_arr');
            $table->integer('training_id');
            $table->integer('positive_time');
            $table->string('repeat_time');
            $table->string('negative_time');
            $table->string('evaluate');
            $table->string('details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities_arbitrations');
    }
}
