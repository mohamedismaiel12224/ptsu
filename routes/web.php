<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear_cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/contact_us', 'HomeController@contact_us');
Route::get('/all_developers', 'HomeController@all_developers');
Route::get('/developer/{id}', 'HomeController@developer');
Route::get('/elibraries/{id}', 'HomeController@elibrary');
Route::get('/eff/{id}', 'HomeController@effs');
Route::get('/norm/{id}', 'HomeController@effs');
Route::get('/elibraries', 'HomeController@elibraries');
Route::get('/change_language/{lang}','HomeController@lang');
Route::post('/search_cert', 'HomeController@search');
Route::post('/cert', 'HomeController@cert');
Route::get('/give_cert/{id}/{status}/{course_id}', 'UserController@give_cert');
Route::get('/get_trainer/{id}', 'AdminController@get_trainer');
Route::post('/poll_action', 'HomeController@poll');
Route::get('/votes', 'HomeController@votes');
Route::post('/mailing', 'HomeController@mailing');
Route::get('/certifications', 'HomeController@certifications');
Route::get('/systems_view', 'HomeController@systems');
Route::get('/system/{id}', 'HomeController@system');

Route::get('/register', 'LoginController@view');
Route::post('/register2', 'LoginController@register');
Route::get('/login', 'LoginController@view');
Route::post('/login2', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::get('/mail', 'AdminController@mailing');
Route::get('/setting', 'AdminController@setting');
Route::get('/give_certificate', 'AdminController@show');
Route::post('/save_certificate', 'AdminController@save_certificate');
Route::post('/setting_update', 'AdminController@setting_update');
Route::get('/videos', 'HomeController@videos');
Route::resource('developers', 'DeveloperController');
Route::resource('systems', 'SystemController');
Route::resource('fellowships', 'FellowshipController');
Route::resource('elibrary', 'ElibraryController');
Route::resource('section_lib', 'SectionLibController');
Route::resource('video', 'VideoController');
Route::resource('poll', 'PollController');
Route::resource('admin', 'AdminController');
Route::resource('fellowship', 'UserController');
Route::get('fellowship/{id}/{permision}', 'UserController@permision');
Route::get('requests', 'UserController@requests');
Route::resource('courses', 'CoursesController');
Route::resource('trainer', 'UserController');
Route::resource('logos', 'LogoController');
Route::resource('banners', 'LogoController');
Route::resource('effs', 'EfficientController');
Route::resource('norms', 'EfficientController');
Route::resource('news', 'NewsController');
Route::resource('slider', 'SliderController');
Route::resource('sysuser', 'SystemUserController');
Route::resource('profile', 'NotAdminController');
Route::resource('change_pass', 'NotAdminController@create');
Route::resource('change_update', 'NotAdminController@store');
Route::resource('material', 'MaterialsController');
Route::resource('attribute', 'AttrController');
Route::resource('checkup', 'CheckupController');
Route::get('checkup/create/{id}','CheckupController@create');
Route::resource('domain', 'DomainController');
Route::get('domain_training','DomainController@index');
Route::get('domain_content','DomainController@index');
Route::get('domain_activities','DomainController@index');
Route::get('domain_evaluate','DomainController@index');
Route::get('domain_presentation_casting','DomainController@index');
Route::get('ev_trainers','DomainController@index');
Route::get('etpm/standard','DomainController@index');
Route::resource('tool_efm', 'ToolEfmController');

Route::get('domain_training/create','DomainController@create');
Route::get('domain_content/create','DomainController@create');
Route::get('domain_activities/create','DomainController@create');
Route::get('domain_evaluate/create','DomainController@create');
Route::get('domain_presentation_casting/create','DomainController@create');
Route::get('ev_trainers/create','DomainController@create');
Route::get('etpm/standard/create','DomainController@create');

Route::resource('procedures', 'ProcedureController');
Route::get('procedures/{id}/{type}','ProcedureController@show');
Route::get('procedures/create/{id}/{type}','ProcedureController@create');

Route::resource('description_evaluate', 'DescriptionEvaluateController');
Route::get('description_evaluate_etpm','DescriptionEvaluateController@index');

Route::resource('sys_fellow','SysytemFellowController');
Route::resource('mit','MitController');
Route::resource('mit_arbitration','MitArbitrationController');
Route::resource('training_outputs','TrainingOutputsController');
Route::resource('colleration','CollerationController');
Route::get('training_outputs/create/{id}','TrainingOutputsController@create');
Route::get('colleration/create/{id}','CollerationController@create');
Route::resource('program_level','ProgramLevelController');
Route::get('program_level/create/{id}','ProgramLevelController@create');
Route::get('vertical/{id}','MitMethodsController@vertical');
Route::post('vertical/{id}','MitMethodsController@verticalUpdate');
Route::get('reseries/{id}','MitMethodsController@reseries');
Route::get('select_level/{id}','MitMethodsController@select_level');
Route::post('program_level_selected/{id}','MitMethodsController@program_level_selected');
Route::get('mit_type/{id}','MitMethodsController@mit_type');
Route::post('mit_type_update/{id}','MitMethodsController@mit_type_update');
Route::get('learning_level/{id}','MitMethodsController@learning_level');
Route::get('learning_level_edit/{series_no}/{pro_id}','MitMethodsController@learning_level_edit');
Route::post('learning_level_update/{series_no}/{pro_id}','MitMethodsController@learning_level_update');
Route::get('practice/{id}','MitMethodsController@practice');
Route::post('investigation/{id}','MitMethodsController@investigation');
Route::get('training_method/{id}','MitMethodsController@training_method');
Route::post('training_method_update/{id}','MitMethodsController@training_method_update');
Route::get('tem/{id}','MitMethodsController@tem');
Route::post('tem_update/{id}','MitMethodsController@tem_update');
Route::get('collect_content/{id}','MitMethodsController@collect_content');
Route::post('collect_content_update/{id}','MitMethodsController@collect_content_update');
Route::get('learning_planner/{id}','MitMethodsController@learning_planner');
Route::resource('training_activities','TrainingActivitiesController');
Route::get('training_activities/create/{id}','TrainingActivitiesController@create');
Route::get('achievement/{id}','MitMethodsController@achievement');
Route::post('achievement_update/{id}','MitMethodsController@achievement_update');
Route::resource('branches','BranchesController');
Route::get('branches/create/{id}','BranchesController@create');
Route::resource('sessions','SessionsController');
Route::get('sessions/create/{id}','SessionsController@create');
Route::get('mit/report/{id}','MitMethodsController@report');

Route::resource('arb_category','CategoryArbitrationController');
Route::get('arb_category/create/{id}','CategoryArbitrationController@create');
Route::resource('arb_need','NeedArbitrationController');
Route::get('arb_need/{id}/{pro_id}','NeedArbitrationController@show');
Route::get('arb_need/create/{id}/{pro_id}','NeedArbitrationController@create');
Route::resource('arb_goal','GoalArbController');
Route::get('arb_goal/create/{id}','GoalArbController@create');
Route::get('mit_arbitration/report/{id}','MitArbitrationController@report');
Route::get('mit_arbitration/report_arbitrator/{pro_id}/{id?}','MitArbitrationController@report_training');
Route::get('mit_arbitration/report_training/{pro_id}/{id?}','MitArbitrationController@report_training');
Route::get('mit_arbitration/report_content/{pro_id}/{id?}','MitArbitrationController@report_content');
Route::get('mit_arbitration/report_activities/{pro_id}/{id?}','MitArbitrationController@report_activities');
Route::get('mit_arbitration/report_evaluate/{pro_id}/{id?}','MitArbitrationController@report_evaluate');
Route::get('arb_link/{id}','MitArbitrationController@arb_link');
Route::get('mit_arbitration/login/{id}','LoginController@login_arbitration');
Route::post('mit_arbitration/login/{id}','LoginController@login_arbitration_update');

Route::get('need_analysis_system/login/{id}','LoginController@need_analysis_system');
Route::get('need_knowledge/login/{id}','LoginController@need_knowledge');
Route::get('need_skill/login/{id}','LoginController@need_skill');
Route::get('need_arbitrators/login/{id}','LoginController@need_arbitrators');

Route::post('need_trainers/login/{id}','LoginController@need_arbitration_update');
Route::post('need_arbitrators/login/{id}','LoginController@need_arbitration_update');
Route::post('need_knowledge/login/{id}','LoginController@need_arbitration_update');
Route::post('need_skill/login/{id}','LoginController@need_arbitration_update');

Route::get('arbitrator/mit/{id}','ArbitratorMitController@show');
Route::get('arbitrators/mit/{id}','ArbitratorMitController@arbitrators');
Route::get('arbitrator/mit/training/{id}','ArbitratorMitController@training');
Route::post('training_arbitration/{id}','ArbitratorMitController@training_arbitration');
Route::resource('arbitrator/mit/content','ArbitratorContentController');
Route::get('arbitrator/mit/content/create/{id}','ArbitratorContentController@create');
Route::resource('arbitrator/mit/activities','ArbitratorActivitiesController');
Route::get('arbitrator/mit/activities/create/{id}','ArbitratorActivitiesController@create');
Route::get('arbitrator/mit/evaluate/{id}','ArbitratorMitController@evaluate');
Route::post('evaluate_arbitration/{id}','ArbitratorMitController@evaluate_arbitration');
Route::post('experience', 'ArbitratorContentController@experience');
// need System
Route::resource('need','NeedController');
Route::get('studying_need_data/{id}','NeedController@studying_need_data');
Route::patch('update_studying_need_data/{id}','NeedController@update_studying_need_data');
Route::resource('responsiblity_need','ResponsibilityController');
Route::get('responsiblity_need/create/{id}','ResponsibilityController@create');
Route::resource('task_need','TaskResponsibilityController');
Route::get('task_need/{id}/{pro_id}','TaskResponsibilityController@show');
Route::get('task_need/create/{id}/{pro_id}','TaskResponsibilityController@create');
Route::get('tool_type/{id}','NeedMethodsController@tool_type');
Route::post('update_tool_type/{id}','NeedMethodsController@update_tool_type');
Route::resource('questions_need','QuestionsController');
Route::resource('questions_need_selected','QuestionsController');
Route::get('questions_need/create/{id}','QuestionsController@create');
Route::get('questions_need_selected/create/{id}','QuestionsController@create');
Route::get('link_trainers/{id}','NeedMethodsController@link_trainers');
Route::get('need/report/{id}','NeedMethodsController@report');
Route::get('need/trainers/{id}','ArbitratorNeedController@show');
Route::get('arbitrator/need_knowledge/{id}','ArbitratorNeedController@show');
Route::get('need/all_arbitrators/{id}','ArbitratorNeedController@all_arbitrators');

Route::post('arbitrator_need/{id}','ArbitratorNeedController@arbitrator_need');
Route::post('arbitrator_need_knowledge/{id}','ArbitratorNeedController@arbitrator_need');
Route::get('need/all_trainers/{id}','ArbitratorNeedController@all_trainers');
Route::get('need/arbitrators_knowledge/{id}','ArbitratorNeedController@arbitrators_knowledge');
Route::get('need/arbitrators_skill/{id}','ArbitratorNeedController@arbitrators_skill');
Route::get('need/check_answer_trainers/{pro_id}/{id}','ArbitratorNeedController@check_answer_trainers');
Route::get('need/check_answer_arbitrator_knowlege/{pro_id}/{id}','ArbitratorNeedController@check_answer_knowlege');
Route::get('need/check_answer_arbitrator_skill/{pro_id}/{id}','ArbitratorNeedController@check_answer_skill');

Route::get('need/report_trainers/{pro_id}/{id?}','ArbitratorNeedController@report_trainers');
Route::get('need/report_arbitrators/{pro_id}/{id?}','ArbitratorNeedController@report_arbitrators');
Route::get('need/report_arbitrator_knowledge/{pro_id}/{id?}','ArbitratorNeedController@report_knowledge');
Route::get('need/report_arbitrator_skill/{pro_id}/{id?}','ArbitratorNeedController@report_skill');
Route::post('need/update_check_answer_trainers/{pro_id}/{id?}','ArbitratorNeedController@update_check_answer_trainers');
Route::post('need/update_check_answer_knowledge/{pro_id}/{id?}','ArbitratorNeedController@update_check_answer_knowledge');
Route::post('need/update_check_answer_skill/{pro_id}/{id?}','ArbitratorNeedController@update_check_answer_skill');
Route::get('need/affect_improve/{id}','NeedMethodsController@affect_improve');
Route::post('need/update_affects_improve/{id}','NeedMethodsController@update_affects_improve');
Route::get('need/select_responsibility/{id}','NeedMethodsController@select_responsibility');
Route::post('need/selected_responsibility/{id}','NeedMethodsController@selected_responsibility');
Route::get('link_need_knowledge/{id}','NeedMethodsController@link_need_knowledge');
Route::get('link_need_skill/{id}','NeedMethodsController@link_need_skill');
Route::get('link_arbitrators/{id}','NeedMethodsController@link_arbitrators');

Route::get('arbitrator/need_arbitration/{id}','ArbitratorNeedController@need_arbitration');
Route::post('update_need_arbitration/{id}','ArbitratorNeedController@update_need_arbitration');


Route::get('ev_egree','AttrController@index');
Route::get('tr_methods','AttrController@index');
Route::get('ev_time','AttrController@index');
Route::get('tools','AttrController@index');
Route::get('responsiblity','AttrController@index');
Route::get('category','AttrController@index');
Route::get('aids','AttrController@index');
Route::get('imp_mechanism','AttrController@index');
Route::get('con_source','AttrController@index');
Route::get('affects_need','AttrController@index');
Route::get('solution_need','AttrController@index');
Route::get('ev_efm','AttrController@index');

Route::get('ev_egree/create','AttrController@create');
Route::get('tr_methods/create','AttrController@create');
Route::get('ev_time/create','AttrController@create');
Route::get('tools/create','AttrController@create');
Route::get('responsiblity/create','AttrController@create');
Route::get('category/create','AttrController@create');
Route::get('aids/create','AttrController@create');
Route::get('imp_mechanism/create','AttrController@create');
Route::get('con_source/create','AttrController@create');
Route::get('affects_need/create','AttrController@create');
Route::get('solution_need/create','AttrController@create');
Route::get('ev_efm/create','AttrController@create');

Route::get('view/{id}', 'HomeController@viewfile');
Route::get('sysuser/{id}/{permision}', 'SystemUserController@permision');
Route::get('sysuser_arbitration/{id}/{permision}', 'SystemUserController@permision_arbitration');
Route::get('create_sysuser/{id}','SystemUserController@create');

Route::get('sys_degree','AdminController@sys_degree');
Route::post('/sys_degree_update', 'AdminController@sys_degree_update');


Route::resource('performance_evaluate','PerformanceEvaluateController');
Route::resource('performance_evaluate/trainers','TrainerEvaluateController');
Route::resource('groups_trainers_evaluate','GroupTrainerEvaluateController');
Route::get('groups_trainers_evaluate/create/{id}','GroupTrainerEvaluateController@create');
Route::get('groups_trainers_evaluate/edit/{id}/{pro_id}','GroupTrainerEvaluateController@edit');
Route::get('performance_evaluate/all_trainers/{id}','TrainerEvaluateController@show');
Route::get('performance_evaluate/trainers/create/{id}','TrainerEvaluateController@create');
Route::any('performance_evaluate/trainers/export/{id}','TrainerEvaluateController@export');
Route::any('performance_evaluate/trainers/export_user/{id}','TrainerEvaluateController@export_user');
Route::post('performance_evaluate/trainers/export2/{id}','TrainerEvaluateController@store2');
Route::post('performance_evaluate/trainers/export3/{id}','TrainerEvaluateController@store3');
Route::get('performance_evaluate/link_evaluate/{id}','PerformanceEvaluateMethodController@link_evaluate');
Route::get('performance_evaluate/login/{id}','LoginController@performance_evaluate');
Route::post('performance_evaluate/login/{id}','LoginController@performance_evaluate_update');
Route::get('evaluator/performance_evaluate/{id}','PerformanceEvaluateMethodController@trainers');
Route::get('new_trainer_evaluate/{id}/{pro_id}','PerformanceEvaluateMethodController@new_trainer_evaluate');
Route::get('trainer_evaluate_group/{id}/{pro_id}','PerformanceEvaluateMethodController@trainer_evaluate_group');
Route::get('trainer_evaluate/{group_id}','PerformanceEvaluateMethodController@trainer_evaluate');
Route::post('trainer_evaluate/{group_id}','PerformanceEvaluateMethodController@trainer_evaluate_update');
Route::get('report_evaluator/{id}/{pro_id}','PerformanceEvaluateMethodController@report_evaluator');
Route::get('report_all_evaluator/{id}/{pro_id}','PerformanceEvaluateMethodController@report_all_evaluator');
Route::get('performance_evaluate/report_evaluate/{pro_id}','PerformanceEvaluateMethodController@report_evaluate');
Route::post('performance_evaluate/report_evaluate/{pro_id}','PerformanceEvaluateMethodController@post_evaluate');
Route::get('performance_evaluate/report_evaluate/{pro_id}/{group_id?}','PerformanceEvaluateMethodController@report_evaluate');


Route::resource('etpm','EtpmController');
Route::resource('etpm/trainers','TrainerEtpmController');
Route::get('etpm/trainers/create/{id}','TrainerEtpmController@create');
Route::resource('groups_trainers_etpm','GroupTrainerEtpmController');
Route::get('groups_trainers_etpm/create/{id}','GroupTrainerEtpmController@create');
Route::get('groups_trainers_etpm/edit/{id}/{pro_id}','GroupTrainerEtpmController@edit');
Route::any('etpm/trainers/export/{id}','TrainerEtpmController@export');
Route::any('etpm/trainers/export_user/{id}','TrainerEtpmController@export_user');
Route::post('etpm/trainers/export2/{id}','TrainerEtpmController@store2');
Route::post('etpm/trainers/export3/{id}','TrainerEtpmController@store3');
Route::get('etpm/all_trainers/{id}','TrainerEtpmController@show');
Route::get('etpm/link_evaluate/{id}','EtpmMethodController@link_evaluate');
Route::get('etpm/login/{id}','LoginController@etpm');
Route::post('etpm/login/{id}','LoginController@etpm_update');
Route::get('evaluator/etpm/{id}','EtpmMethodController@trainers');
Route::get('new_trainer_etpm/{id}/{pro_id}','EtpmMethodController@new_trainer_etpm');
Route::get('trainer_etpm_group/{id}/{pro_id}','EtpmMethodController@trainer_etpm_group');
Route::get('etpm/trainer_evaluate/{group_id}','EtpmMethodController@trainer_evaluate');
Route::post('etpm/trainer_evaluate/{group_id}','EtpmMethodController@trainer_evaluate_update');

Route::get('etpm/report_evaluator/{id}/{pro_id}','EtpmMethodController@report_evaluator');
Route::get('etpm/report_all_evaluator/{id}/{pro_id}','EtpmMethodController@report_all_evaluator');
Route::get('etpm/report_evaluate/{pro_id}','EtpmMethodController@report_evaluate');
Route::post('etpm/report_evaluate/{pro_id}','EtpmMethodController@post_evaluate');
Route::get('etpm/report_evaluate/{pro_id}/{group_id?}','EtpmMethodController@report_evaluate');

Route::group(['prefix' => 'efm/'], function () {
    	Route::resource('/efm', 'EfmController');
    	Route::resource('/gap', 'EfmGapController');
	Route::get('gap/create/{id}','EfmGapController@create');
	Route::resource('/note', 'EfmNoteController');
	Route::get('note/create/{id}','EfmNoteController@create');
	Route::get('/human_characteristics/{pro_id}', 'EfmHumanCharacteristicsController@show');
	Route::get('/human_characteristics/{pro_id}/{human_id}', 'EfmHumanCharacteristicsController@show_human');
	Route::post('/human_characteristics/{pro_id}/{human_id}', 'EfmHumanCharacteristicsController@save_human');
	Route::get('/training_material/{pro_id}', 'EfmTrainingMaterialController@show');
	Route::get('/training_material/{pro_id}/{training_material}', 'EfmTrainingMaterialController@training_material');
	Route::post('/training_material/{pro_id}', 'EfmTrainingMaterialController@update_training_material');
	Route::get('/select_evaluate_fields/{pro_id}', 'EfmFramingController@select_evaluate_fields');
	Route::post('/update_select_evaluate_fields/{pro_id}', 'EfmFramingController@update_select_evaluate_fields');
	Route::get('/targets/{pro_id}', 'EfmFramingController@targets');
	Route::get('/targets/{frame_id}/edit', 'EfmFramingController@edit_targets');
	Route::post('/update_targets/{pro_id}', 'EfmFramingController@update_targets');
    	Route::resource('/analysis', 'EfmAnalysisController');
	Route::get('analysis/create/{id}','EfmAnalysisController@create');
	Route::get('/analysis_b/{pro_id}', 'EfmAnalysisController@analysis_b');
	Route::post('/analysis_b/{pro_id}', 'EfmAnalysisController@analysis_b_update');

});